const { NODE_ENV, CRYPTOCOMPARE_KEY } = process.env

const API_KEY = {
  CRYPTOCOMPARE: NODE_ENV === 'production'
    ? CRYPTOCOMPARE_KEY // API key with 250,000 monthly calls
    : '8984884ef90255e11ad786a0371891c51b24e689946fcad25efa62112406135a', // free plan
}

module.exports = {
  API_KEY,
}
