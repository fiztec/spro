const {
  NODE_ENV,
  EXCHANGE_FEE_RECIPIENT,
  EXCHANGE_SWAP_CONTRACT_ADDRESS,
  CONTRACT_ADDRESS_OF_EXCHANGE,
  EXCHANGE_MAKER_ADDRESS,
  EXCHANGE_MAKER_PK,
} = process.env

const EXCHANGE_STATUS = {
  PENDING: 'PENDING',
  FILLED: 'FILLED',
  CANCELED: 'CANCELED',
}

const EXCHANGE_FEES = {
  MAKER_FEE: 0, // fraction of 1, ex. 0.001 (0.1%)
  TAKER_FEE: 0, // fraction of 1, ex. 0.001 (0.1%)
  FEE_RECIPIENT: NODE_ENV === 'production' ? EXCHANGE_FEE_RECIPIENT : '0x37398a06d8bd553410d7404680ebed4638bb005b',
}

const MAX_EXCHANGE_SLIPPAGE = 0.0005

const SWAP_CONTRACT_ADDRESS = NODE_ENV === 'production'
  ? EXCHANGE_SWAP_CONTRACT_ADDRESS
  : '0x619e403af18365d09719456923c4a93b41ad3db1'
const EXCHANGE_CONTRACT_ADDRESS = NODE_ENV === 'production'
  ? CONTRACT_ADDRESS_OF_EXCHANGE
  : '0x4aa335be177bba2fe3289ceb94d00fb34e94007b'
const MAKER_ADDRESS = NODE_ENV === 'production'
  ? EXCHANGE_MAKER_ADDRESS
  : '0x96241186ca4aa79f5bd593a02c29a38da785251e'

// STORE SECURELY
const MAKER_PK = NODE_ENV === 'production'
  ? Buffer.from(EXCHANGE_MAKER_PK, 'hex')
  : Buffer.from('D96315659BA73E284B3F7C0B4298945F083A9EF3292D9DDF5D2B92B6174C0D44', 'hex')

const BALANCE_OF_DECIMALS_ABI = [{
  'constant': true,
  'inputs': [{
    'name': '',
    'type': 'address',
  }],
  'name': 'balanceOf',
  'outputs': [{
    'name': '',
    'type': 'uint256',
  }],
  'payable': false,
  'stateMutability': 'view',
  'type': 'function',
},
{
  'constant': true,
  'inputs': [],
  'name': 'decimals',
  'outputs': [{
    'name': '',
    'type': 'uint8',
  }],
  'payable': false,
  'stateMutability': 'view',
  'type': 'function',
} ]

module.exports = {
  EXCHANGE_STATUS,
  EXCHANGE_FEES,
  MAX_EXCHANGE_SLIPPAGE,
  EXCHANGE_CONTRACT_ADDRESS,
  MAKER_ADDRESS,
  SWAP_ADDRESS: SWAP_CONTRACT_ADDRESS,
  MAKER_PK,
  BALANCE_OF_DECIMALS_ABI,
}
