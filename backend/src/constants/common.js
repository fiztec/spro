export const PAGINATION_FIRST_PAGE_RECORDS = 30
export const PAGINATION_NEXT_PAGES_RECORDS = 20
export const AVERAGE_GAS_PRICE = 15e9 // set gas price in case online service to get gas prices failed
export const AVERAGE_GAS_PRICE_TIME_FACTOR = 0.5 // calculate gas price to get transaction in block in next XX min
