const USER_ROLE = {
  ADMIN: 'ADMIN',
  CLIENT: 'CLIENT',
  BOT: 'BOT',
}

module.exports = {
  USER_ROLE,
}
