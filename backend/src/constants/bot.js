const { NODE_ENV, BOT_PK, BOT_WALLET } = process.env

const BOT_ACCOUNT = NODE_ENV === 'production'
  ? {
    publicAddress: BOT_WALLET,
    PK: BOT_PK,
  }
  : {
    publicAddress: '0x871b520c51e14833a7151fcbf2d776dd52e12875',
    PK: '93ec4aae09aa026688f1de7c54572fb0bad3eacac67120e12dff31ec95d7006a',
  }

module.exports = {
  BOT_ACCOUNT,
}
