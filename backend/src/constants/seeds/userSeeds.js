const { NODE_ENV, ADMIN_EMAIL, ADMIN_WALLET } = process.env
const { USER_ROLE } = require('../user')
// const base64url = require('base64url/dist/base64url')
const base64url = require('base64url')
const crypto = require('crypto')
const nonceSize = 64

const productionUsers = [
  {
    username: ADMIN_EMAIL,
    nonce: base64url(crypto.randomBytes(nonceSize)),
    publicAddress: ADMIN_WALLET.toLowerCase(),
    role: USER_ROLE.ADMIN,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
]

const devUsers =
  [
    {
      username: 'admin@gmail.com',
      nonce: base64url(crypto.randomBytes(nonceSize)),
      publicAddress: '0x96241186ca4aa79f5bd593a02c29a38da785251e'.toLowerCase(),
      role: USER_ROLE.ADMIN,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      username: 'sivanchenko@s-pro.io',
      nonce: base64url(crypto.randomBytes(nonceSize)),
      publicAddress: '0xA80Ff08D08809b29c733135b1b93a07799E5d17f'.toLowerCase(),
      role: USER_ROLE.CLIENT,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      username: 'rmalizderskyi@s-pro.io',
      nonce: base64url(crypto.randomBytes(nonceSize)),
      publicAddress: '0x7683DDB7451D684d320Baf7AAbCd0298a8C941da'.toLowerCase(),
      role: USER_ROLE.CLIENT,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      username: 'ovelmik@s-pro.io',
      nonce: base64url(crypto.randomBytes(nonceSize)),
      publicAddress: '0x4EC7e7530e247aa3343Aa7Bc17e16779A5758e5c'.toLowerCase(),
      role: USER_ROLE.CLIENT,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      username: 'sergius.iva@gmail.com',
      nonce: base64url(crypto.randomBytes(nonceSize)),
      publicAddress: '0xFB382443daF05158f10fbfF0b925deC06B3cA1f5'.toLowerCase(),
      role: USER_ROLE.CLIENT,
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      username: 'ivanchenko.s@gmail.com',
      nonce: base64url(crypto.randomBytes(nonceSize)),
      publicAddress: '0xdeA38106DD6E7395aE74D43710dc4d1FE5fDb476'.toLowerCase(),
      role: USER_ROLE.CLIENT,
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  ]

module.exports.seedUsers = NODE_ENV === 'production' ? productionUsers : devUsers
