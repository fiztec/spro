const BigNumber = require('bignumber.js')

const ORDER_TYPE = {
  LIMIT: 'LIMIT',
  MARKET: 'MARKET',
}

const MAKER_REBATE_RATE = 100
const FEE_RATE_BASE = 100000

const TRADE_TOKEN_NAME = 'WETH'
const TRADE_TOKEN_DECIMALS = 18
const EXCHANGE_TOKEN_NAME = 'ZRX'

const TRADE_CONTRACT_ADDRESS = process.env.NODE_ENV !== 'production'
  ? '0x251bbfa0abf2dc356a44f7af5e4d7a224a4ec01f' // WETH contract Ropsten address
  : '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2' // WETH contract Mainnet address

const EXCHANGE_CONTRACT_ADDRESS = process.env.NODE_ENV !== 'production'
  ? '0x2a88cf7785e89c7c8bf26d4429b965619dada7e3' // ZRX contract Ropsten address
  : '0xe41d2489571d322189246dafa5ebde1f4699f498' // ZRX contract Mainnet address

const feeToPercentFixed3 = (feeRate) => {
  return new BigNumber(feeRate).times(100).toFixed(3)
}
const feeFromPercentFixed5 = (feeRate) => {
  return new BigNumber(feeRate).div(100).toFixed(5)
}

module.exports = {
  ORDER_TYPE,
  MAKER_REBATE_RATE,
  FEE_RATE_BASE,
  TRADE_CONTRACT_ADDRESS,
  EXCHANGE_CONTRACT_ADDRESS,
  TRADE_TOKEN_NAME,
  EXCHANGE_TOKEN_NAME,
  TRADE_TOKEN_DECIMALS,
  feeToPercentFixed3,
  feeFromPercentFixed5,
}
