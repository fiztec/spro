const { NODE_ENV } = process.env

module.exports = {
  up: async (queryInterface) => {
    const exchangeTokens = [
      {
        tokenSymbol: 'WETH',
        tokenName: 'Wrapped ETH',
        // '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2' @MAINNET
        tokenAddress: '0x251bbfa0abf2dc356a44f7af5e4d7a224a4ec01f',
        tokenProjectUrl: 'https://www.weth.io/',
        tokenDecimals: 18,
        minOrderSize: '0.0000000005',
        currentPriceToWETH: '1.0',
        givePriceToWETH: '1.0',
        receivePriceToWETH: '1.0',
        maxSlippage: '0.00000',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]

    exchangeTokens.push({
      tokenSymbol: 'ETH',
      tokenName: 'ETH',
      // '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2' @MAINNET
      tokenAddress: '0x251bbfa0abf2dc356a44f7af5e4d7a224a4ec01f',
      tokenProjectUrl: 'https://www.ethereum.org/',
      tokenDecimals: 18,
      minOrderSize: '0.0000000005',
      currentPriceToWETH: '1.0',
      givePriceToWETH: '1.0',
      receivePriceToWETH: '1.0',
      maxSlippage: '0.00000',
      createdAt: new Date(),
      updatedAt: new Date(),
    })
    if (NODE_ENV !== 'production') {
      // await queryInterface.bulkUpdate('exchangeToken', exchangeTokens[0], { tokenSymbol: 'ETH' }, {})
      await queryInterface.bulkInsert('exchangeToken', [exchangeTokens[1]], {})
    }
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('exchangeToken', null, {})
  },

}
