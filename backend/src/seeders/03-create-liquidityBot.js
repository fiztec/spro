const { USER_ROLE } = require('../constants/user')
const { BOT_ACCOUNT } = require('../constants/bot')
const { findAssociations } = require('../helpers/models')
const db = require('../models')
const { market: Market } = db

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const markets = await findAssociations(Market, 'tokens')

    const tokens = Object.keys(markets).map(key => key.split('-')[0])

    let bots = []

    for (const token of tokens) {
      bots.push({
        marketId: markets[`${token}-WETH`],
        publicAddress: BOT_ACCOUNT.publicAddress,
        botname: `${token} constant Product bot`,
        role: USER_ROLE.BOT,
        isRunning: false,
        createdAt: new Date(),
        updatedAt: new Date(),
      })
    }

    return queryInterface.bulkInsert('liquidityBot', bots, {})
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('liquidityBot', null, {})
  },
}
