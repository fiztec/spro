const { NODE_ENV } = process.env

module.exports = {
  up: async (queryInterface) => {
    const exchangeTokens = [
      {
        tokenSymbol: 'ETH',
        tokenName: 'wrapped ETH',
        // '0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2' @MAINNET
        tokenAddress: '0x251bbfa0abf2dc356a44f7af5e4d7a224a4ec01f',
        tokenProjectUrl: 'https://www.weth.io/',
        tokenDecimals: 18,
        minOrderSize: '0.0000000005',
        currentPriceToWETH: '1.0',
        givePriceToWETH: '1.0',
        receivePriceToWETH: '1.0',
        maxSlippage: '0.00000',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]

    exchangeTokens.push({
      tokenSymbol: 'EMPR',
      tokenName: 'empowr coin',
      tokenAddress: '0xdaf16fad57bc1d43a57695d1e724013ebe8e154a', // Ropsten
      // tokenAddress: '0xe7D7b37e72510309Db27C460378f957B1B04Bd5d', // Mainnet
      tokenProjectUrl: 'http://empowr.com/',
      tokenDecimals: 18,
      minOrderSize: '0.0000000001',
      currentPriceToWETH: '0.5',
      givePriceToWETH: '0.5',
      receivePriceToWETH: '0.5',
      maxSlippage: '0.20000',
      createdAt: new Date(),
      updatedAt: new Date(),
    },)

    if (NODE_ENV !== 'production') {
      // await queryInterface.bulkUpdate('exchangeToken', exchangeTokens[0], { tokenSymbol: 'ETH' }, {})
      await queryInterface.bulkUpdate('exchangeToken', exchangeTokens[1], { tokenSymbol: 'EMPR' }, {})
    }
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('exchangeToken', null, {})
  },

}
