const { markets } = require('../constants/seeds/marketSeeds')

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('market', markets, {})
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('market', null, {})
  },

}
