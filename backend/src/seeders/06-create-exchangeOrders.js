'use strict'
const { EXCHANGE_STATUS } = require('../constants/exchange')
const { NODE_ENV } = process.env

module.exports = {
  up: async (queryInterface) => {
    const exchangeOrders = [
      {
        userId: 3,
        exchangeOrderIdEth: '0x82650f00bf69bed4f2e738e89abfd93fa2b2627ae5782981184f50431117e0ef',
        giveTokenId: 1,
        receiveTokenId: 12,
        giveAmount: 10000000000000000,
        receiveAmount: 20000000000000000,
        makerAddress: '0x96241186ca4aa79f5bd593a02c29a38da785251e',
        feeRecipient: '0x37398a06d8bd553410d7404680ebed4638bb005b',
        makerFee: 0,
        takerFee: 0,
        makerSignatureV: 27,
        makerSignatureR: '0x5061fa9050e04f311558bd3e40588ebcc1d15f4ec940df32da378ea02e28e9a4',
        makerSignatureS: '0x147ef3c6e9d7cb4c3a2e30377aa46072367ecf00d0647dda7a13fd01caf56f9e',
        expirationTimestampInSec: 1556625401,
        salt: 269833818312128,
        status: EXCHANGE_STATUS.FILLED,
        createdAt: new Date('2019-04-30 11:54:00'),
        updatedAt: new Date('2019-04-30 11:54:00'),
      },
      {
        userId: 3,
        exchangeOrderIdEth: '0x266ec782bc6fd912f592c0d0496c1ffb46dd18debafe46deda92bb72e86661f5',
        giveTokenId: 1,
        receiveTokenId: 12,
        giveAmount: 30000000000000000,
        receiveAmount: 60000000000000000,
        makerAddress: '0x96241186ca4aa79f5bd593a02c29a38da785251e',
        feeRecipient: '0x37398a06d8bd553410d7404680ebed4638bb005b',
        makerFee: 0,
        takerFee: 0,
        makerSignatureV: 27,
        makerSignatureR: '0x8d63b94e2dc1d822b6e0040fa03be1f409d226463b429599dd981dccd02a353d',
        makerSignatureS: '0x4b3afdb5b48666de36db8e0526ccb11e43e33f267907092b54afb82aa7479315',
        expirationTimestampInSec: 1556625950,
        salt: 103037278986250,
        status: EXCHANGE_STATUS.FILLED,
        createdAt: new Date('2019-04-30 12:03:00'),
        updatedAt: new Date('2019-04-30 12:03:00'),
      },
    ]

    if (NODE_ENV !== 'production') {
      await queryInterface.bulkInsert('exchangeOrder', exchangeOrders, {})
    }
  },

  down: async (queryInterface) => {
    if (NODE_ENV !== 'production') {
      await queryInterface.bulkDelete('exchangeOrder', null, {})
    }
  },

}
