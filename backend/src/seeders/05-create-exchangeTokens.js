const { exchangeTokensSeeds } = require('../constants/seeds/exchangeTokenSeeds')

module.exports = {
  up: async (queryInterface) => {
    await queryInterface.bulkInsert('exchangeToken', exchangeTokensSeeds, {})
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('exchangeToken', null, {})
  },
}
