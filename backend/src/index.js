import app from './lib/server'
import { cronUpdateExchangeRates } from './helpers/cron'
import helmet from 'koa-helmet'
import { logger } from './helpers/logger'

const { APP_PORT: port = 3000 } = process.env

/** ======  SECURITY section ===== **/

/** Attackers can use this header (which is enabled by default) to
 * detect apps running Express and then launch specifically-targeted attacks. **/
// app.use(helmet.hidePoweredBy())

/** middleware adds the Strict-Transport-Security header to the response. **/
app.use(helmet({
  hsts: {
    maxAge: 15552000,
  },
}))

/** no-cache **/
app.use(helmet.noCache())

app.use(async (ctx, next) => {
  /** provides clickjacking protection **/
  ctx.set('X-Frame-Options', 'SAMEORIGIN')
  /**  enables the cross-site scripting (XSS) filter built into most recent web browsers **/
  ctx.set('X-XSS-Protection', 1)
  /** prevents browsers from MIME-sniffing a response away from the declared content-type **/
  ctx.set('X-Content-Type-Options', 'nosniff')
  await next()
})

const server = app.listen(port, '0.0.0.0', function () {
  const address = server.address()
  logger.info(`⊙ Open http://${address.address}:${address.port}/ in your browser ✨`)
})

cronUpdateExchangeRates.start()

server.on('close', function () {
  cronUpdateExchangeRates.stop()
})

module.exports = server
