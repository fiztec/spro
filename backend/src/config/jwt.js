const { NODE_ENV, JWT_SECRET } = process.env

export const SECRET = NODE_ENV === 'production'
  ? JWT_SECRET
  : '@)#**&(&F*S&(DUIOWU(&(*^$(!@)DUOSDU'
