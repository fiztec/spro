const { DEV_ENV, DB_USERNAME, DB_PASSWORD, DB_NAME } = process.env
const isDocker = DEV_ENV === 'docker'
const host = isDocker ? 'postgres' : '127.0.0.1'

module.exports = {
  development: {
    username: 'postgres',
    password: 'postgres',
    database: 'app_db',
    host,
    dialect: 'postgres',
    seederStorage: 'sequelize',
    define: {
      timestamps: true,
      freezeTableName: true,
    },
    operatorsAliases: false,
    benchmark: true,
    pool: {
      max: 100,
      min: 0,
      idle: 200000,
      // @note https://github.com/sequelize/sequelize/issues/8133#issuecomment-359993057
      acquire: 1000000,
    },
  },
  test: {
    username: 'postgres',
    password: 'postgres',
    database: 'app_db_test',
    host,
    dialect: 'postgres',
    seederStorage: 'sequelize',
    define: {
      timestamps: true,
      freezeTableName: true,
    },
    logging: false,
    operatorsAliases: false,
    pool: {
      max: 100,
      min: 0,
      idle: 200000,
      acquire: 1000000,
    },
  },
  staging: {
    username: 'postgres',
    password: 'postgres',
    database: 'app_db',
    host,
    dialect: 'postgres',
    seederStorage: 'sequelize',
    define: {
      timestamps: true,
      freezeTableName: true,
    },
    logging: false,
    operatorsAliases: false,
    pool: {
      max: 100,
      min: 0,
      idle: 200000,
      acquire: 1000000,
    },
  },
  production: {
    username: DB_USERNAME,
    password: DB_PASSWORD,
    database: DB_NAME,
    host,
    dialect: 'postgres',
    seederStorage: 'sequelize',
    define: {
      timestamps: true,
      freezeTableName: true,
    },
    logging: false,
    operatorsAliases: false,
    pool: {
      max: 100,
      min: 0,
      idle: 200000,
      acquire: 1000000,
    },
  },
}
