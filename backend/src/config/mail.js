const { NODE_ENV, MAIL_ACCOUNT, MAIL_PASSWORD } = process.env

export const emailTransportOptions = {
  test: {
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
      user: 'drogon800@gmail.com',
      pass: 'Dr0Go^32',
    },
  },
  development: {
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
      user: 'drogon800@gmail.com',
      pass: 'Dr0Go^32',
    },
  },
  staging: {
    host: 'smtp.gmail.com',
    port: 587,
    secure: false,
    auth: {
      user: 'drogon800@gmail.com',
      pass: 'Dr0Go^32',
    },
  },
  production: {
    host: 'smtp.zoho.com',
    port: 587,
    secure: false,
    auth: {
      user: MAIL_ACCOUNT,
      pass: MAIL_PASSWORD,
    },
  },
}

const emailNotification = {
  development: 'sivanchenko@s-pro.io, rmalizderskyi@s-pro.io',
  staging: 'ovelmik@s-pro.io, sivanchenko@s-pro.io, rmalizderskyi@s-pro.io',
  production: 'logs@meterqubes.io',
}

export const NOTIFICATION_EMAIL = emailNotification[NODE_ENV]
export const FROM_EMAIL = emailTransportOptions[NODE_ENV].auth.user
