import * as winston from 'winston'
import moment from 'moment'
require('winston-daily-rotate-file')

const { timestamp, combine, printf } = winston.format

const defaultFormat = printf(
  (info) => `[${moment(info.timestamp).format('YYYY-MM-DD/h:mm:ss')}] ${info.message}`
)

let path = require('path')
const prjMainDir = path.resolve(path.dirname(require.main.filename), '../logs')
const transportError = new (winston.transports.DailyRotateFile)({
  filename: prjMainDir + '/error-%DATE%.csv',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxSize: '5m',
  maxFiles: '14d',
  level: 'error',

})

const transportCombined = new (winston.transports.DailyRotateFile)({
  filename: prjMainDir + '/combined-%DATE%.csv',
  datePattern: 'YYYY-MM-DD',
  zippedArchive: true,
  maxSize: '5m',
  maxFiles: '14d',
})

class LoggerService {
  constructor () {
    const isLocal = process.env.NODE_ENV !== 'production'

    this.infoLog = winston.createLogger({
      level: 'info',
      format: combine(
        timestamp(),
        defaultFormat,

      ),
      transports: [
        transportError,
        transportCombined,
      ],
      exitOnError: false,
    })

    if (isLocal) {
      this.infoLog.add(
        new winston.transports.Console({
          colorize: true,
          showLevel: true,
        }),
      )
    }
  }

  cleanIp (ip) {
    if (typeof (ip) !== 'string') {
      return '0.0.0.0'
    }

    if (ip.substr(0, 7) === '::ffff:') {
      ip = ip.substr(7)
    }
    return ip
  }

  info (message, ip = '0.0.0.0', userId = 'no user') {
    this.infoLog.info(`; ${this.cleanIp(ip)}; ${userId}; ${message};`)
  }

  error (message, ip = '0.0.0.0', userId = 'no user') {
    this.infoLog.error(`; ${this.cleanIp(ip)}; ${userId}; ${message};`)
  }
}

export const logger = new LoggerService()
