import { isOrderMatchedPartValid, MARKET_ATTRIBUTES, ORDER_ATTRIBUTES } from './index'
import { ORDER_PART, ORDER_SIDE, ORDER_STATUS } from '../../constants/order'
import createError from 'http-errors'
import db from '../../models'
import _ from 'lodash'
import BigNumber from 'bignumber.js'
import { logger } from '../logger'
import {
  emitDataChangedInOrdersByUser,

  emitNewTradeToHistory,
  emitToUser,
} from '../../helpers/socket'
import { EVENT } from '../../constants/socket'
import { sendBlockchainErrorEmail } from '../../mailer'
import { RELAYER } from '../../config/web3'
import { sendTransaction } from '../../helpers/web3transaction'

const {
  order: Order,
  market: Market,
  user: User,
  matchedOrder: MatchedOrder,
  sequelize: { Op, cast, col, literal },
} = db
export const { sequelize } = db

const getMatchCondition = (orderSide, leftSide, rightSide) => {
  if (_.isNil(orderSide) ||
    _.isNil(leftSide) ||
    _.isNil(rightSide)
  ) {
    throw createError(500, 'Cannot get match conditions. Not all arguments provided')
  }

  const condition = orderSide === ORDER_SIDE.SELL ? '<=' : '>='

  return { quotes: sequelize.where(literal(leftSide), condition, literal(rightSide)) }
}
const getSumLimitedOrder = (side, marketId, userId, leftSide, rightSide) => {
  const condition = side === ORDER_SIDE.SELL ? '<=' : '>='

  return `
  ( SELECT SUM ( "baseTokenAmount" ) 
    FROM "order" 
    WHERE
      (
        "order"."isMarketOrder" = FALSE 
        AND ( "order"."expiresAt" > ${Math.floor(Date.now() / 1000)} OR "order"."expiresAt" IS NULL ) 
        AND CAST ( "order"."side" AS VARCHAR ) NOT LIKE '${side}' 
        AND "order"."marketId" = ${marketId} 
        AND ${leftSide} ${condition} ${rightSide} 
        AND "order"."userId" != ${userId}
        AND "order"."status" IN ('${ORDER_STATUS.PENDING}','${ORDER_STATUS.PARTIALLY_FILLED}')
      ) 
  )
  `
}
export const getLimitOrders = async (order, userId) => {
  if (
    _.isEmpty(order) ||
    _.isNil(userId)
  ) {
    throw createError(404, 'You need to provide order and userId')
  }

  const { baseTokenAmount, quoteTokenAmount, side, marketId, isMarketOrder } = order

  if (baseTokenAmount === '0') throw createError(500, 'Taker order base token amount is zero. Please check taker order')

  const orderUnitPrice = new BigNumber(quoteTokenAmount).dividedBy(new BigNumber(baseTokenAmount))

  const leftSide = `${quoteTokenAmount}*"baseTokenAmount"`
  const rightSide = `${baseTokenAmount}*"quoteTokenAmount"`

  let matchCondition = isMarketOrder ? {} : getMatchCondition(side, leftSide, rightSide)

  let orderLimitOrders = side === ORDER_SIDE.SELL
    ? [[literal('"quoteTokenAmount"/"baseTokenAmount"'), 'DESC']]
    : [[literal('"quoteTokenAmount"/"baseTokenAmount"'), 'ASC']]

  return _.invokeMap(await Order.findAll({
    attributes: [
      ...ORDER_ATTRIBUTES,
      [literal('"quoteTokenAmount"/"baseTokenAmount"'), 'limitOrderPrice'],
      [literal(`${orderUnitPrice}`), 'newOrderPrice'],
      [literal(leftSide), 'LSide'],
      [literal(rightSide), 'RSide'],
      [literal(getSumLimitedOrder(side, marketId, userId, leftSide, rightSide)), 'sum'],
    ],
    where: {
      [Op.and]: [
        { isMarketOrder: false },
        {
          expiresAt: {
            [Op.or]:
              [
                { [Op.gt]: Math.floor(Date.now() / 1000) },
              ],
          },
        },
        sequelize.where(cast(col('order.side'), 'varchar'), { [Op.notLike]: order.side }),
        { marketId: order.marketId },
        matchCondition,
        { userId: { [Op.ne]: userId } },
        { status: { [Op.or]: [ORDER_STATUS.PENDING, ORDER_STATUS.PARTIALLY_FILLED] } },
      ],
    },
    include: [{
      model: User,
      attributes: ['id', 'publicAddress'],
    }, {
      model: Market,
      attributes: MARKET_ATTRIBUTES,
    },
    ],
    order: [
      ...orderLimitOrders,
      ['createdAt', 'ASC'],
    ],

  }), 'get', { plain: true })
}
export const getMatchedOrders = (takerOrder, limitOrders, ctx) => {
  if (
    _.isEmpty(takerOrder) ||
    _.isEmpty(limitOrders)
  ) {
    throw createError(404, 'You need to provide baseTokenAmount and limit order list')
  }

  const { baseTokenAmount } = takerOrder

  let isLastTakerOrderBaseTokenAmountTotallyMatched = true
  let isTakerBaseTokenAmountFilled = true
  let baseTokenFilledAmounts = []
  const takerBaseTokenAmountBig = new BigNumber(baseTokenAmount)
  let notMatchTakerOrMakerBaseTokenAmount = 0

  if (takerBaseTokenAmountBig.isEqualTo(limitOrders[0].sum)) {
    return {
      matchedOrderToProcess: limitOrders,
      isLastTakerOrderBaseTokenAmountTotallyMatched,
      isTakerBaseTokenAmountFilled,
      notMatchTakerOrMakerBaseTokenAmount,
      baseTokenFilledAmounts: _.map(limitOrders, 'baseTokenAmount'),
    }
  }

  if (takerBaseTokenAmountBig.isGreaterThan(limitOrders[0].sum)) {
    return {
      matchedOrderToProcess: limitOrders,
      isLastTakerOrderBaseTokenAmountTotallyMatched,
      isTakerBaseTokenAmountFilled: false,
      notMatchTakerOrMakerBaseTokenAmount: takerBaseTokenAmountBig.minus(limitOrders[0].sum),
      baseTokenFilledAmounts: _.map(limitOrders, 'baseTokenAmount'),
    }
  }

  let matchedOrderToProcess = []
  let totalMatch = new BigNumber('0')

  for (const limitOrder of limitOrders) {
    totalMatch = totalMatch.plus(new BigNumber(limitOrder.baseTokenAmount))
    matchedOrderToProcess.push(limitOrder)

    if (totalMatch.isLessThan(takerBaseTokenAmountBig)) {
      baseTokenFilledAmounts.push(limitOrder.baseTokenAmount)
      continue
    }

    if (totalMatch.isGreaterThan(takerBaseTokenAmountBig)) {
      notMatchTakerOrMakerBaseTokenAmount = totalMatch.minus(takerBaseTokenAmountBig)
      const matchedMakerOrderAmountPart = new BigNumber(limitOrder.baseTokenAmount)
        .minus(notMatchTakerOrMakerBaseTokenAmount)
        .toFixed(0)

      const isMatchedMakerOrderAmountPartValid = isOrderMatchedPartValid(
        limitOrder,
        matchedMakerOrderAmountPart,
        ORDER_PART.MAKER)
      const isMatchedTakerOrderAmountPartValid = isOrderMatchedPartValid(
        takerOrder,
        matchedMakerOrderAmountPart,
        ORDER_PART.TAKER,
        limitOrder)

      if (!isMatchedMakerOrderAmountPartValid || !isMatchedTakerOrderAmountPartValid) {
        isTakerBaseTokenAmountFilled = false
        matchedOrderToProcess.pop()
        totalMatch = totalMatch.minus(new BigNumber(limitOrder.baseTokenAmount))
        notMatchTakerOrMakerBaseTokenAmount = takerBaseTokenAmountBig.minus(totalMatch)
        continue
      }

      isLastTakerOrderBaseTokenAmountTotallyMatched = false
      isTakerBaseTokenAmountFilled = true
      baseTokenFilledAmounts.push(matchedMakerOrderAmountPart)
      break
    }
    if (totalMatch.isEqualTo(takerBaseTokenAmountBig)) {
      baseTokenFilledAmounts.push(limitOrder.baseTokenAmount)
      isTakerBaseTokenAmountFilled = true
      break
    }
  }

  logger.info(`getMatchedOrders. Taker order= ${takerOrder.id}. Payload: ${JSON.stringify(
    {
      baseTokenAmount,
      notMatchTakerOrMakerBaseTokenAmount,
      isTakerBaseTokenAmountFilled,
      isLastTakerOrderBaseTokenAmountTotallyMatched,
    }
  )} `,
  ctx.request.ip,
  ctx.state.user.id)

  return {
    matchedOrderToProcess,
    isLastTakerOrderBaseTokenAmountTotallyMatched,
    notMatchTakerOrMakerBaseTokenAmount,
    baseTokenFilledAmounts: baseTokenFilledAmounts.map(i => i.toString()),
    isTakerBaseTokenAmountFilled,
  }
}
const orderFullFilledData = {
  status: ORDER_STATUS.FULL_FILLED,
  baseTokenAmount: 0,
  quoteTokenAmount: 0,
}
/**
 *
 * @param {{quoteTokenAmount: string|number, baseTokenAmount: string|number }} order
 * @param {string|number} notMatchedAmount
 * @return {{baseTokenAmount: string, status: string, quoteTokenAmount: string}}
 */
const getPartiallyUpdateAttributes = (order, notMatchedAmount) => {
  const rate = new BigNumber(order.quoteTokenAmount).dividedBy(order.baseTokenAmount)
  const notMatchedAmountBig = new BigNumber(notMatchedAmount)
  const newQuoteTokenAmount = notMatchedAmountBig.times(rate).toFixed(0)

  return {
    status: ORDER_STATUS.PARTIALLY_FILLED,
    baseTokenAmount: notMatchedAmountBig.toFixed(0),
    quoteTokenAmount: newQuoteTokenAmount,
  }
}
const updateTakerOrder = async (
  takerOrder,
  isTakerBaseTokenAmountFilled,
  notMatchTakerOrMakerBaseTokenAmount,
  transaction
) => {
  if (
    !_.isBoolean(isTakerBaseTokenAmountFilled) ||
    _.isEmpty(takerOrder)
  ) {
    throw createError(500, 'Method updateTakerOrder has wrong arguments')
  }

  const { id, userId } = takerOrder

  if (isTakerBaseTokenAmountFilled) {
    await Order.update(orderFullFilledData, { where: { id }, transaction })
    await emitToUser(EVENT.ORDER_CHANGED, { id, ...orderFullFilledData }, userId)
  } else {
    const orderPartiallyFilledData = getPartiallyUpdateAttributes(takerOrder, notMatchTakerOrMakerBaseTokenAmount)
    await Order.update(orderPartiallyFilledData, { where: { id }, transaction })
    await emitToUser(EVENT.ORDER_CHANGED, { id, ...orderPartiallyFilledData }, userId)
  }
}
const createMatchedOrders = async (
  isLastTakerOrderBaseTokenAmountTotallyMatched,
  makerOrders,
  notMatchTakerOrMakerBaseTokenAmount,
  takerOrderId,
  transactionId,
  transaction
) => {
  if (
    !_.isBoolean(isLastTakerOrderBaseTokenAmountTotallyMatched) ||
    _.isEmpty(makerOrders)
  ) {
    throw createError(500, 'Method createMatchedOrders has wrong arguments')
  }

  let matchedOrders = _.map(makerOrders, order => {
    return {
      parentId: takerOrderId,
      matchedOrderId: order.id,
      transactionId,
      baseTokenAmount: order.baseTokenAmount,
      quoteTokenAmount: order.quoteTokenAmount,
    }
  })

  if (!isLastTakerOrderBaseTokenAmountTotallyMatched) {
    const lastOrder = _.last(makerOrders)
    const { baseTokenAmount, quoteTokenAmount } = getPartiallyUpdateAttributes(
      lastOrder,
      notMatchTakerOrMakerBaseTokenAmount
    )

    matchedOrders = _.initial(matchedOrders)

    matchedOrders.push({
      parentId: takerOrderId,
      matchedOrderId: lastOrder.id,
      transactionId,
      baseTokenAmount: lastOrder.initBaseTokenAmount - baseTokenAmount,
      quoteTokenAmount: lastOrder.initQuoteTokenAmount - quoteTokenAmount,
    })
  }

  return MatchedOrder.bulkCreate(matchedOrders, { transaction })
}
const updateMakerOrders = async (
  isLastTakerOrderBaseTokenAmountTotallyMatched,
  matchedOrders,
  notMatchTakerOrMakerBaseTokenAmount,
  transaction
) => {
  if (
    !_.isBoolean(isLastTakerOrderBaseTokenAmountTotallyMatched) ||
    _.isEmpty(matchedOrders)
  ) {
    throw createError(500, 'Method updateMakerOrders has wrong arguments')
  }

  const ids = _.map(matchedOrders, 'id')

  if (isLastTakerOrderBaseTokenAmountTotallyMatched) {
    await Order.update(orderFullFilledData, { where: { id: { [Op.in]: [...ids] } }, transaction })
    await emitDataChangedInOrdersByUser(matchedOrders, orderFullFilledData)
  } else {
    const orderPartiallyFilledData = getPartiallyUpdateAttributes(
      _.last(matchedOrders),
      notMatchTakerOrMakerBaseTokenAmount
    )
    await Order.update(orderPartiallyFilledData, {
      where: { id: { [Op.in]: [_.last(ids)] } },
      transaction,
    })

    await emitDataChangedInOrdersByUser(_.last(matchedOrders), orderPartiallyFilledData)

    if (ids.length > 1) {
      await Order.update(orderFullFilledData, {
        where: { id: { [Op.in]: _.dropRight(ids) } },
        transaction,
      })
      await emitDataChangedInOrdersByUser(_.initial(matchedOrders), orderFullFilledData)
    }
  }
}
const updateMakerTakerOrders = async (
  newOrder,
  isTakerBaseTokenAmountFilled,
  isLastTakerOrderBaseTokenAmountTotallyMatched,
  matchedOrders,
  notMatchTakerOrMakerBaseTokenAmount,
  transactionId,
) => {
  let transaction

  try {
    transaction = await sequelize.transaction()

    await updateTakerOrder(newOrder, isTakerBaseTokenAmountFilled, notMatchTakerOrMakerBaseTokenAmount, transaction)
    await updateMakerOrders(
      isLastTakerOrderBaseTokenAmountTotallyMatched,
      matchedOrders,
      notMatchTakerOrMakerBaseTokenAmount,
      transaction)
    await createMatchedOrders(
      isLastTakerOrderBaseTokenAmountTotallyMatched,
      matchedOrders,
      notMatchTakerOrMakerBaseTokenAmount,
      newOrder.id,
      transactionId,
      transaction
    )

    transaction.commit()
  } catch (e) {
    transaction && await transaction.rollback()
    logger.error(`Attention! Failed to update matched orders after successful blockchain transaction!!!. ${e.message}`)
  }
}
const getBlockchainTransactionOptions = (newOrder, matchedOrders) => {
  const traderOrderParam = [
    newOrder.user.publicAddress,
    newOrder.baseTokenAmount.toString(),
    newOrder.quoteTokenAmount.toString(),
    newOrder.gasTokenAmount.toString(),
    newOrder.data,
    newOrder.signature,
  ]

  const makerOrderParams = matchedOrders.map(order => {
    return [
      order.user.publicAddress,
      order.initBaseTokenAmount.toString(),
      order.initQuoteTokenAmount.toString(),
      order.gasTokenAmount.toString(),
      order.data,
      order.signature,
    ]
  })

  const orderAddressSet = [newOrder.market.baseTokenAddress, newOrder.market.quoteTokenAddress, RELAYER]

  return {
    traderOrderParam,
    makerOrderParams,
    orderAddressSet,
  }
}
export const setOrdersAsMatching = async (orders) => {
  if (!Array.isArray(orders) || _.isEmpty(orders)) throw Error('Cannot set orders as matching. Orders are empty')

  let transaction

  try {
    transaction = await sequelize.transaction()

    for (const { status, id } of orders) {
      await Order.update(
        { status: ORDER_STATUS.MATCHING, previousStatus: status },
        { where: { id }, transaction })
    }

    await emitDataChangedInOrdersByUser(orders, { status: ORDER_STATUS.MATCHING })

    transaction.commit()
  } catch (error) {
    transaction && await transaction.rollback()
    throw createError(500, `Failed to update order status to matching. ${error}`)
  }
}
const rollBackOrderStatus = async (orders) => {
  if (!Array.isArray(orders) || _.isEmpty(orders)) throw Error('Cannot roll back orders status. Orders are empty')

  let transaction

  try {
    transaction = await sequelize.transaction()

    for (const { status, id } of orders) {
      await Order.update(
        { status, previousStatus: null },
        { where: { id }, transaction })
    }

    transaction.commit()
  } catch (error) {
    transaction && await transaction.rollback()
    throw createError(500, `Failed to roll back orders status. ${error}`)
  }
}
export const processMatchingOrders = async (
  newOrder,
  matchedOrders,
  isLastTakerOrderBaseTokenAmountTotallyMatched,
  notMatchTakerOrMakerBaseTokenAmount,
  baseTokenFilledAmounts,
  isTakerBaseTokenAmountFilled,
) => {
  let transactionId
  logger.info(`>>> MATCHING <<< Taker order ${newOrder.id}, Maker order(-s)= ${_.map(matchedOrders, 'id')}`)

  const transactionOption = {
    ...getBlockchainTransactionOptions(newOrder, matchedOrders),
    baseTokenFilledAmounts,
  }

  try {
    transactionId = await sendTransaction(newOrder.id, transactionOption)

    await updateMakerTakerOrders(
      newOrder,
      isTakerBaseTokenAmountFilled,
      isLastTakerOrderBaseTokenAmountTotallyMatched,
      matchedOrders,
      notMatchTakerOrMakerBaseTokenAmount,
      transactionId
    )

    await emitNewTradeToHistory(transactionId)
  } catch (e) {
    const ordersToRevert = [...matchedOrders, newOrder]
    await rollBackOrderStatus(ordersToRevert)

    for (const { id, status, userId } of ordersToRevert) {
      emitToUser(EVENT.ORDER_CHANGED, { id, status }, userId)
    }
    logger.error(`Taker order id = ${newOrder.id}. Send transaction to blockchain failed: ${e.message}. 
    Transaction options: ${JSON.stringify(transactionOption)}`, null, ordersToRevert.userId)

    const ids = _.map(matchedOrders, 'id')
    await sendBlockchainErrorEmail(newOrder.id, ids, e.message)

    logger.error(e.message)
  }
}
