import _ from 'lodash'
import { ORDER_PART_VALUE, ORDER_SIDE, ORDER_STATUS } from '../../constants/order'
import db from '../../models'
import { web3 } from '../../lib/web3'
import { erc20AllowanceABI, erc20BalanceABI } from '../../constants/abi'
import { TRADE_PROXY_CONTRACT_ADDRESS } from '../../config/web3'
import createError from 'http-errors'
import { logger } from '../logger'
import moment from 'moment'
import BigNumber from 'bignumber.js'
import { ORDER_TYPE } from '../../constants/market'

export const ORDER_ATTRIBUTES = [
  'id',
  'side',
  'marketId',
  'baseTokenAmount',
  'quoteTokenAmount',
  'initBaseTokenAmount',
  'initQuoteTokenAmount',
  'gasTokenAmount',
  'data',
  'signature',
  'signatureConfig',
  'signatureR',
  'signatureS',
  'previousStatus',
  'status',
  'userId',
  'isMarketOrder',
]

export const MARKET_ATTRIBUTES = [
  'id',
  'baseTokenAddress',
  'quoteTokenAddress',
  'asMakerFeeRate',
  'asTakerFeeRate',
  'baseTokenDecimals',
  'quoteTokenDecimals',
  'quoteToken',
  'baseToken',
]

const {
  order: Order,
  market: Market,
  user: User,
  sequelize: { Op, literal },
} = db

/**
 *
 * @param {integer} baseTokenDecimals
 * @param {number|string} baseTokenAmount
 * @return {BigNumber}
 */
export const getAmount = (baseTokenDecimals, baseTokenAmount) => {
  if (_.isNil(baseTokenDecimals) ||
    _.isNil(baseTokenAmount) ||
    +baseTokenDecimals === 0) throw Error('Wrong arguments for method getAmount')

  return new BigNumber(baseTokenAmount).dividedBy(Math.pow(10, baseTokenDecimals))
}

export const getPrice = (quoteTokenDecimals, baseTokenDecimals, quoteTokenAmount, baseTokenAmount) => {
  if (_.isNil(quoteTokenDecimals) ||
    _.isNil(baseTokenDecimals) ||
    _.isNil(quoteTokenDecimals) ||
    _.isNil(baseTokenAmount) ||
    +baseTokenDecimals === 0 ||
    +quoteTokenDecimals === 0 ||
    +baseTokenAmount === 0) {
    throw Error(`Wrong arguments for method getPrice. 
    quoteTokenDecimals= ${quoteTokenDecimals} | baseTokenDecimals= ${baseTokenDecimals}
    quoteTokenAmount= ${quoteTokenAmount} | baseTokenAmount= ${baseTokenAmount}`)
  }

  return new BigNumber(quoteTokenAmount)
    .dividedBy(new BigNumber(getAmount(baseTokenDecimals, baseTokenAmount)))
    .dividedBy(new BigNumber(Math.pow(10, quoteTokenDecimals)))
}

export const getOrderHistoryOptions = (marketId) => {
  let where = { status: { [Op.in]: [ORDER_STATUS.PENDING, ORDER_STATUS.MATCHING, ORDER_STATUS.PARTIALLY_FILLED] } }

  if (!_.isNil(marketId)) {
    where.marketId = marketId
  }

  return {
    attributes: [
      'id',
      'side',
      'baseTokenAmount',
      'quoteTokenAmount',
      'userId',
      'marketId',
    ],
    order: [
      [[literal('"quoteTokenAmount"/"baseTokenAmount"'), 'ASC']],
      ['createdAt', 'ASC'],
    ],
    where,
    include: {
      model: Market,
      attributes: ['baseTokenDecimals', 'quoteTokenDecimals', 'pricePrecision'],

    },
  }
}

export const getOrderHistory = async (orders, currentUserId, marketId) => {
  if (!Array.isArray(orders)) orders = [orders]

  let orderHistory = { marketId }

  orderHistory[ORDER_SIDE.BUY] = {}
  orderHistory[ORDER_SIDE.SELL] = {}

  for (const { side, baseTokenAmount, quoteTokenAmount, market, userId } of orders) {
    const amount = (getAmount(market.baseTokenDecimals, baseTokenAmount))
    const price = +(getPrice(
      market.quoteTokenDecimals,
      market.baseTokenDecimals,
      quoteTokenAmount,
      baseTokenAmount,
    ).toPrecision(market.pricePrecision))

    if (!_.has(orderHistory[side], price)) orderHistory[side][price] = { amount: '0.00', myAmount: '0.00', price }

    orderHistory[side][price].amount = new BigNumber(orderHistory[side][price].amount)
      .plus(amount)

    if (userId === currentUserId) {
      orderHistory[side][price].myAmount = new BigNumber(orderHistory[side][price]
        .myAmount).plus(amount)
    }
  }

  orderHistory[ORDER_SIDE.BUY] = _.sortBy(_.values(orderHistory[ORDER_SIDE.BUY]), ['price']).reverse()
  orderHistory[ORDER_SIDE.SELL] = _.sortBy(_.values(orderHistory[ORDER_SIDE.SELL]), ['price']).reverse()

  return orderHistory
}

export const getOrderHistoryByOrderId = async (id) => {
  const options = getOrderHistoryOptions()

  const order = (await Order.findByPk(id, options)).get({ plain: true })
  const orderHistory = await getOrderHistory(order, order.userId, order.marketId)

  return { ...orderHistory, userId: order.userId }
}

/**
 *
 * @param {string} publicAddress
 * @param {BigNumber} amountToCheck
 * @param {string} contractAddress
 * @param {number} tokenDecimals
 * @param {string} tokenName
 * @return {Promise<void>}
 */
export const checkOrderAllowance = async (publicAddress, amountToCheck, contractAddress, tokenDecimals, tokenName) => {
  if (
    !publicAddress ||
    !amountToCheck ||
    !contractAddress ||
    !tokenDecimals ||
    !tokenName
  ) {
    throw Error('checkOrderAllowance failed. Wrong attributes')
  }

  const contractInstance = web3.eth.Contract(erc20AllowanceABI, contractAddress)

  console.time('checkOrderAllowance')
  const allowance = await contractInstance.methods.allowance(publicAddress, TRADE_PROXY_CONTRACT_ADDRESS)
    .call()
    .catch(e => {
      console.timeEnd('checkOrderAllowance')
      throw Error(`Check Order allowance returns error: ${e.message}`)
    })
  console.timeEnd('checkOrderAllowance')

  const allowanceAmount = new BigNumber(allowance).div(Math.pow(10, tokenDecimals))

  if (allowanceAmount.lte(amountToCheck)) {
    throw createError(403, 'Not enough allowance. Allowance = ' +
      allowanceAmount.toPrecision() + ' order amount ' + amountToCheck)
  }
}
/**
 *
 * @param {string} publicAddress
 * @param {BigNumber} amountToCheck
 * @param {string} contractAddress
 * @param {number} tokenDecimals
 * @param {string} tokenName
 * @return {Promise<void>}
 */
const checkOrderBalance = async (publicAddress, amountToCheck, contractAddress, tokenDecimals, tokenName) => {
  if (
    !publicAddress ||
    !amountToCheck ||
    !contractAddress ||
    !tokenDecimals ||
    !tokenName
  ) {
    throw Error('checkOrderBalance failed. Wrong attributes')
  }

  const contractInstance = web3.eth.Contract(erc20BalanceABI, contractAddress)

  console.time('checkOrderBalance')
  const baseTokenBalance = await contractInstance.methods.balanceOf(publicAddress)
    .call()
    .catch(e => {
      console.timeEnd('checkOrderBalance')
      throw Error(`Check Order Balance returns error: ${e.message}`)
    })
  console.timeEnd('checkOrderBalance')

  const walletAmount = new BigNumber(baseTokenBalance).div(Math.pow(10, tokenDecimals))

  if (walletAmount.lt(amountToCheck)) {
    throw createError(403, 'Your actual wallet balance is around ' +
      walletAmount.toPrecision() + ' ' + tokenName +
      ' which is less than the sum of your pending partially filled orders is ' + (+amountToCheck.toPrecision()))
  }
}

/**
 *
 * @param {
 *    Array<{baseTokenAmount: string|number, quoteTokenAmount: string|number, side: string}> |
 *   {baseTokenAmount: string|number, quoteTokenAmount: string|number, side: string}
 *   } orders
 * @param {integer} baseTokenDecimals
 * @return {BigNumber}
 */
const getSumAmount = (orders, baseTokenDecimals) => {
  if (_.isEmpty(orders) ||
    _.isNil(baseTokenDecimals)) {
    throw Error('getSumAmount wrong arguments')
  }

  if (!_.isArray(orders)) orders = [orders]

  let amount = new BigNumber(0)

  _.map(orders, (order) => {
    amount = getAmount(baseTokenDecimals, order.baseTokenAmount).plus(amount)
  })

  return amount
}

/**
 *
 * @param {
 *    Array<{baseTokenAmount: string|number, quoteTokenAmount: string|number, side: string}> |
 *   {baseTokenAmount: string|number, quoteTokenAmount: string|number, side: string}
 *   } orders
 * @param {integer} baseTokenDecimals
 * @return {BigNumber}
 */
const getAveragePriceForUserOrders = (orders, baseTokenDecimals, quoteTokenDecimals) => {
  if (_.isEmpty(orders) ||
    _.isNil(baseTokenDecimals) ||
    _.isNil(quoteTokenDecimals)
  ) {
    throw Error('getAveragePriceForUserOrders wrong arguments')
  }

  if (!_.isArray(orders)) orders = [orders]

  let priceSum = new BigNumber(0)
  let divideBy = orders.length

  for (const order of orders) {
    if (+order.quoteTokenAmount === 0 || +order.baseTokenAmount === 0) {
      divideBy--
      continue
    }

    priceSum = getPrice(quoteTokenDecimals, baseTokenDecimals, order.quoteTokenAmount, order.baseTokenAmount)
      .plus(priceSum)
  }

  return priceSum.div(divideBy)
}

/**
 *
 * @param {string} publicAddress
 * @param {Array<{baseTokenAmount: string|number, quoteTokenAmount: string|number, side: string}> |
 * {baseTokenAmount: string|number, quoteTokenAmount: string|number, side: string}
 * } orders
 * @param {{
 * baseTokenDecimals: integer
 * quoteTokenDecimals: integer
 * baseTokenAddress: string
 * baseToken: string
 * quoteTokenAddress: string
 * quoteToken: string
 * }} market
 * @param {BigNumber=} givenAmount
 * @param {BigNumber=} givenPrice
 * @return {Promise<void>}
 */
export const checkOrderBalanceAllowanceBySide = async (publicAddress, orders, market, givenAmount, givenPrice) => {
  if (!_.isArray(orders)) orders = [orders]

  const { side } = orders[0]
  let contractAddress
  let amountToCheck
  let tokenName
  let amount
  let price

  try {
    amount = givenAmount || getSumAmount(orders, market.baseTokenDecimals)
    price = givenPrice || getAveragePriceForUserOrders(orders, market.baseTokenDecimals, market.quoteTokenDecimals)

    if (side === ORDER_SIDE.SELL) {
      contractAddress = market.baseTokenAddress
      amountToCheck = new BigNumber(amount)
      tokenName = market.baseToken

      await checkOrderBalance(publicAddress, amountToCheck, contractAddress, market.baseTokenDecimals, tokenName)
      await checkOrderAllowance(publicAddress, amountToCheck, contractAddress, market.baseTokenDecimals, tokenName)
    } else {
      contractAddress = market.quoteTokenAddress
      amountToCheck = amount.times(price)
      tokenName = market.quoteToken

      await checkOrderBalance(publicAddress, amountToCheck, contractAddress, market.quoteTokenDecimals, tokenName)
      await checkOrderAllowance(publicAddress, amountToCheck, contractAddress, market.quoteTokenDecimals, tokenName)
    }
  } catch (e) {
    // logger.error(`Check taker balance/allowance failed. User wallet: ${publicAddress}. ${e.message}`)

    logger.error(`Check order balance/allowance failed. User wallet: ${publicAddress}. ${e.message} 
      ------ check details ------
      Users' orders ids = ${_.map(orders, 'id')}  | side= ${side}
      amount= ${amount} | amountToCheck= ${amountToCheck} | price= ${price} 
      wallet= ${publicAddress} | contractAddress= ${contractAddress} | tokenName= ${tokenName}`)

    throw Error(e.message)
  }
}
export const isQuoteTokenAmountMoreTokenGasPriceAndMakerFee = (quoteTokenAmount, gasTokenAmount, makerFee) => {
  if (!quoteTokenAmount || !gasTokenAmount || !makerFee) {
    throw Error('isQuoteTokenAmountMoreTokenGasPriceAndMakerFee gets wrong arguments')
  }

  const gasTokenAmountBig = new BigNumber(gasTokenAmount)
  const makerFeeBig = new BigNumber(makerFee)

  return new BigNumber(quoteTokenAmount).gt((gasTokenAmountBig.plus(makerFeeBig)))
}
export const isOrderPriceValid = (price, side, isMarketOrder) => {
  if (_.isNil(price)) throw Error('Wrong arguments for method isOrderPriceValid')

  if (isMarketOrder && +price === 0 && side === ORDER_SIDE.BUY) return false
  if (!isMarketOrder && +price === 0) return false

  return true
}
/**
 *
 * @param {string|number} baseTokenAmount
 * @param {string|number} quoteTokenAmount
 * @param {string|number} baseTokenFilledAmount
 * @return {string}
 */
const getQuoteTokenFilledAmount = (baseTokenAmount, quoteTokenAmount, baseTokenFilledAmount) => {
  if (
    _.isNil(baseTokenAmount) ||
    _.isNil(quoteTokenAmount) ||
    _.isNil(!baseTokenFilledAmount)
  ) {
    throw Error('getQuoteTokenFilledAmount failed. Not all arguments provided')
  }

  if (
    baseTokenAmount <= 0 ||
    quoteTokenAmount <= 0 ||
    baseTokenFilledAmount <= 0
  ) {
    throw Error('getQuoteTokenFilledAmount failed. Function arguments should be positive number and not zero')
  }

  const quoteTokenAmountBig = new BigNumber(quoteTokenAmount)
  const baseTokenFilledAmountBig = new BigNumber(baseTokenFilledAmount)
  const baseTokenAmountBig = new BigNumber(baseTokenAmount)

  return quoteTokenAmountBig.times(baseTokenFilledAmountBig).div(baseTokenAmountBig).toFixed(0)
}
/**
 *
 * @param {string|number} asMakerTakerFeeRate
 * @param {string|number} quoteTokenFilledAmount
 * @return {string}
 */
export const getMakerFee = (asMakerTakerFeeRate, quoteTokenFilledAmount) => {
  return new BigNumber(quoteTokenFilledAmount).times(new BigNumber(asMakerTakerFeeRate)).toFixed(0)
}
export const filterOrdersPassedGasAndFeeCheck = (limitOrders) => {
  return limitOrders.filter(limitOrder => {
    const makerFee = getMakerFee(limitOrder.market.asMakerFeeRate, limitOrder.quoteTokenAmount)
    const { quoteTokenAmount, gasTokenAmount } = limitOrder
    return isQuoteTokenAmountMoreTokenGasPriceAndMakerFee(quoteTokenAmount, gasTokenAmount, makerFee)
  })
}
/**
 *
 * @param {{
 * gasTokenAmount: string|number,
 * baseTokenAmount: string:number,
 * quoteTokenAmount: string:number,
 * market: {asMakerFeeRate: string|number}
 * }} order
 * @param {string|number} baseTokenFilledAmount
 * @param {string} orderPartValue
 * @return {boolean}
 */
const isQuoteTokenFilledAmountValid = (order, baseTokenFilledAmount, orderPartValue, lastMatchedOrder) => {
  if (orderPartValue !== ORDER_PART_VALUE.MAKER && orderPartValue !== ORDER_PART_VALUE.TAKER) {
    throw Error('isQuoteTokenFilledAmountValid failed. Wrong order part')
  }

  let { baseTokenAmount, quoteTokenAmount, side, isMarketOrder } = order

  if (side === ORDER_SIDE.SELL && isMarketOrder) {
    baseTokenAmount = lastMatchedOrder.baseTokenAmount
    quoteTokenAmount = lastMatchedOrder.quoteTokenAmount
  }

  const amount = getAmount(order.market.baseTokenDecimals, order.baseTokenAmount)
  const price = getPrice(
    order.market.quoteTokenDecimals,
    order.market.baseTokenDecimals,
    order.quoteTokenAmount,
    order.baseTokenAmount)

  const quoteTokenFilledAmount = getQuoteTokenFilledAmount(
    baseTokenAmount,
    quoteTokenAmount,
    baseTokenFilledAmount)

  const makerFee = getMakerFee(order.market[orderPartValue], quoteTokenFilledAmount)

  logger.info(`
    ----baseTokenFilledAmount= ${baseTokenFilledAmount}
    ---quoteTokenFilledAmount= ${quoteTokenFilledAmount}
    -----------------makerFee= ${makerFee}
    -----order.gasTokenAmount= ${order.gasTokenAmount}
    -------------------amount= ${amount}
    --------------------price= ${price}
    <<<<<<<<<<<<<<<<<<<<<<<
    `)

  return isQuoteTokenAmountMoreTokenGasPriceAndMakerFee(
    quoteTokenFilledAmount,
    order.gasTokenAmount,
    makerFee
  )
}
/**
 *
 * @param {{
 * id: number,
 * gasTokenAmount: string|number,
 * baseTokenAmount: string:number,
 * quoteTokenAmount: string:number,
 * market: {asMakerFeeRate: string|number}
 * }} order
 * @param {string|number} matchedOrderAmountPart
 * @param {string<'TAKER','MAKER'>} orderPart
 * @param {{
 * id: number,
 * gasTokenAmount: string|number,
 * baseTokenAmount: string:number,
 * quoteTokenAmount: string:number,
 * market: {asMakerFeeRate: string|number}
 * }|{}}  lastMatchedOrder // apply only for taker Sell Market order
 * @return {boolean}
 */
export const isOrderMatchedPartValid = (order, matchedOrderAmountPart, orderPart, lastMatchedOrder = {}) => {
  logger.info(`>>>>>>>>>>>>>>>>>>>>>>
  Check is matched part valid. ${orderPart} order id= ${order.id}`)
  /** Validation that matched part of maker/taker Order(quoteTokenFilledAmount) is bigger then makerFee + gasFee **/
  const isValid = isQuoteTokenFilledAmountValid(
    order,
    matchedOrderAmountPart,
    ORDER_PART_VALUE[orderPart],
    lastMatchedOrder
  )

  logger.info(`Check is matched part result. ${orderPart} order id= ${order.id} >>> isValid = ${isValid}`)

  return isValid
}
/**
 *
 * @param {{
 * id: number,
 * gasTokenAmount: string|number,
 * baseTokenAmount: string:number,
 * quoteTokenAmount: string:number,
 * side: string<BUY,SELL>
 * }} takerOrder
 * @param {{
 * id: number,
 * gasTokenAmount: string|number,
 * baseTokenAmount: string:number,
 * quoteTokenAmount: string:number,
 * side: string<BUY,SELL>
 * }[]} makerOrders
 */
export const filterByLeftRightSidesCondition = (takerOrder, makerOrders) => {
  const { side } = takerOrder
  let checkedMakerOrders = []

  const isSell = side === ORDER_SIDE.SELL

  for (const makerOrder of makerOrders) {
    const left = new BigNumber(takerOrder.quoteTokenAmount).times(new BigNumber(makerOrder.baseTokenAmount))
    const right = new BigNumber(takerOrder.baseTokenAmount).times(new BigNumber(makerOrder.quoteTokenAmount))

    const result = isSell ? left.lte(right) : left.gte(right)

    if (!result) {
      logger.info(`Check for INVALID_MATCH return fail. Maker order id ${makerOrder.id}`)
      //  more details info for debugging
      //  const condition = side === ORDER_SIDE.SELL ? '<=' : '>='
      //  | isSell= ${isSell}
      //  | left= ${left.toFixed(0)} | ${condition} | right=${right.toFixed(0)}
      //  | result= ${result}`)
    }

    if (result) {
      checkedMakerOrders.push(makerOrder)
    }
  }

  return checkedMakerOrders
}
/**
 *
 * @param {Object} takerOrder
 * @param {Array<number>} baseTokenFilledAmounts
 * @return {Promise<void>}
 */
export const validateTakerMatchedBalance = async (takerOrder, baseTokenFilledAmounts) => {
  const { user, market, quoteTokenAmount, baseTokenAmount } = takerOrder
  const { publicAddress } = user
  let makerMatchedAmountSum = new BigNumber('0')

  try {
    _.forEach(baseTokenFilledAmounts, (amount) => {
      makerMatchedAmountSum = makerMatchedAmountSum.plus(amount)
    })

    console.log('makerMatchedAmountSum= %s', makerMatchedAmountSum)

    const amount = getAmount(market.baseTokenDecimals, makerMatchedAmountSum)
    const price = getPrice(market.quoteTokenDecimals, market.baseTokenDecimals, quoteTokenAmount, baseTokenAmount)

    await checkOrderBalanceAllowanceBySide(publicAddress, takerOrder, market, amount, price)
  } catch (e) {
    throw createError(400, 'Check taker order failed. ' + e.message)
  }
}
/**
 *
 * @param {integer} quoteTokenAmount
 * @param {string} side
 * @param {number|string} gasTokenAmount
 * @param {number} asTakerFeeRate
 */
export const validateMarketOrder = (quoteTokenAmount, side, gasTokenAmount, asTakerFeeRate) => {
  if (quoteTokenAmount !== 0 && side === ORDER_SIDE.SELL) createError(403, 'Sell market order should not have price')
  if (side === ORDER_SIDE.BUY) {
    const makerFee = getMakerFee(asTakerFeeRate, quoteTokenAmount)

    validateQuoteTokenAmountMoreTokenGasPriceAndMakerFee(quoteTokenAmount, gasTokenAmount, makerFee)
  }
}

export function validateQuoteTokenAmountMoreTokenGasPriceAndMakerFee (quoteTokenAmount, gasTokenAmount, makerFee) {
  if (!quoteTokenAmount || !gasTokenAmount || !makerFee) {
    throw Error('isQuoteTokenAmountMoreTokenGasPriceAndMakerFee gets wrong arguments')
  }

  const isQuoteTokenAmountValid = isQuoteTokenAmountMoreTokenGasPriceAndMakerFee.apply(null, arguments)

  if (!isQuoteTokenAmountValid) {
    logger.error(`>>> ERROR <<<. Order amount is too small for current maker fee and gas token transfer.
     quoteTokenAmount=${quoteTokenAmount} should be > gasTokenAmount=${gasTokenAmount} + makerFee=${makerFee}`)
    throw createError(403, 'Order amount is too small for current maker fee and gas token transfer')
  }
}

const checkExpiresAtValid = async (orderExpiresAt) => {
  try {
    if (_.isNil(orderExpiresAt)) throw createError(400, 'Order expire required')

    const latestBlockTimestamp = await web3.eth.getBlock('latest')

    if (moment(orderExpiresAt).isSameOrBefore(latestBlockTimestamp.timestamp)) {
      throw createError(400, 'Order date cannot be earlier than current block time')
    }
  } catch (e) {
    throw createError(e.status || 500, `Check order Expires failed: ${e.message}`)
  }
}
export const validateLimitOrder = async (expiresAt, quoteTokenAmount, gasTokenAmount, asTakerFeeRate) => {
  if (_.isNull(expiresAt)) throw createError(403, 'Expires date is mandatory for limit orders')
  await checkExpiresAtValid(expiresAt)

  const makerFee = getMakerFee(asTakerFeeRate, quoteTokenAmount)

  validateQuoteTokenAmountMoreTokenGasPriceAndMakerFee(quoteTokenAmount, gasTokenAmount, makerFee)
}
/**
 *
 * @param {{
 *   user: {publicAddress: string}
 *   market: {
 * baseTokenDecimals: number
 * quoteTokenDecimals: number
 * baseTokenAddress: string
 * baseToken: string
 * quoteTokenAddress: string
 * quoteToken: string
 * }
 * }} makerOrders
 * @return {Promise<Array>}
 */
export const filterMakerOrdersNoEnoughBalanceAllowance = async (makerOrders) => {
  let makerValidOrders = []
  let userOrders = {}
  let validatedUser = {}

  _.map(makerOrders, order => {
    if (_.has(userOrders, order.user.id)) {
      userOrders[order.user.id].push(order)
    } else {
      userOrders[order.user.id] = []
      userOrders[order.user.id].push(order)
    }
  })

  for (const makerOrder of makerOrders) {
    const { user, market } = makerOrder
    const { publicAddress } = user

    if (_.has(validatedUser, user.id)) {
      console.log('user"s orders validate. Skip order check= %s')
      continue
    }

    try {
      await checkOrderBalanceAllowanceBySide(publicAddress, userOrders[user.id], market)
      validatedUser[user.id] = 1
      makerValidOrders = _.concat(makerValidOrders, userOrders[user.id])
    } catch (e) {
      const lastOrder = userOrders[user.id].pop()
      logger.info(`Maker order ${lastOrder.id} has been excluded from matching 
      as no balance or allowance. Cancel order`)
      await Order.update({ status: ORDER_STATUS.CANCELED }, { where: { id: lastOrder.id } })
    }
  }

  return makerValidOrders
}

export const isOrderFeeRateValid = (orderMakerFeeRate, marketMakerFeeRate) => {
  if (_.isNil(orderMakerFeeRate) || _.isNil(marketMakerFeeRate)) {
    throw Error('Wrong arguments for method isOrderFeeRateValid')
  }

  return new BigNumber(marketMakerFeeRate).isEqualTo(new BigNumber(orderMakerFeeRate))
}

export const isMarketOrderAllow = (isMarketOrder, marketOrderTypes) => {
  return isMarketOrder ? _.indexOf(marketOrderTypes, ORDER_TYPE.MARKET) !== -1
    : true
}

export const isOrderAmountValid = (amount, marketMinOrderAmount) => {
  if (_.isNil(amount) || _.isNil(marketMinOrderAmount)) {
    throw Error('Wrong arguments for method isOrderAmountValid')
  }

  const amountBig = new BigNumber(amount)

  return amountBig.gte(0) && amountBig.gte(new BigNumber(marketMinOrderAmount))
}

export const isAmountPrecisionValid = (amount, marketAmountDecimals) => {
  const amountBig = new BigNumber(amount)

  const integer = new BigNumber(Math.pow(10, marketAmountDecimals))
    .times(amountBig)
    .integerValue(BigNumber.ROUND_FLOOR)

  const decimal = new BigNumber(new BigNumber(Math.pow(10, marketAmountDecimals))
    .times(amountBig))
    .minus(integer)

  return decimal.eq(0)
}

export const getValidatedOrder = async (orderId, userId) => {
  if (
    _.isNil(orderId) ||
    _.isNil(userId)
  ) {
    throw createError(404, 'You need to provide user id and order id to get validated order')
  }

  const order = await Order.findOne(
    {
      attributes: ORDER_ATTRIBUTES,
      where: {
        id: orderId,
        status: { [Op.in]: [ORDER_STATUS.PENDING, ORDER_STATUS.PARTIALLY_FILLED] },
      },
      include: [{
        model: User,
        attributes: ['id', 'publicAddress'],
      }, {
        model: Market,
        attributes: MARKET_ATTRIBUTES,
      },
      ],
    })

  if (_.isNil(order)) throw createError(404, 'Order by given Id not found')
  if (!_.isEqual(order.user.id, userId)) throw createError(404, 'Order by given Id not found for current user')

  return order
}
