export const paginate = (requestedPageNumber, firstPageSize, pageSize) => {
  let offset = 0

  if (requestedPageNumber && requestedPageNumber > 1) { offset = firstPageSize + (requestedPageNumber - 2) * pageSize }
  const limit = offset ? offset + pageSize : firstPageSize

  return { limit, offset }
}
