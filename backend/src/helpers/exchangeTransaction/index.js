import { web3 } from '../../lib/web3'
import {
  NUMBER_BLOCK_TO_WAIT,
} from '../../config/web3'
import createError from 'http-errors'
import db from '../../models'
import _ from 'lodash'
import moment from 'moment'
import {
  EXCHANGE_STATUS,
} from '../../constants/exchange'

const {
  transaction: Transaction,
  exchangeOrder: ExchangeOrder,
} = db

const updateTransactionError = (id, errMsg) => {
  return Transaction.update({ errMsg }, { where: { id } })
}

const updateTransaction = (id, data) => {
  return Transaction.update(data, { where: { id } })
}

const getTransactionReceipt = async (txHash) => {
  let txReceipt = null

  try {
    txReceipt = await web3.eth.getTransactionReceipt(txHash)
    if (_.isNil(txReceipt)) return null
  } catch (e) {
    console.error(`getTransactionReceipt error: ${e.message}`)
    throw Error(`getTransactionReceipt error: ${e.message}`)
  }

  txReceipt.err = ''
  return txReceipt
}

export const handleTxHash = async (exchangeOrderId, txHash, exchangeTransactionId) => {
  if (
    _.isNil(exchangeOrderId) ||
    _.isNil(txHash)
  ) {
    throw createError(500, 'Invalid arguments for handling exchange transaction hash')
  }

  let txReceipt = null
  let waitingBlocks = 1
  let confirmationBlocks = 1
  let blockTimestamp = null

  const subscribeBlockHeaders = web3.eth.subscribe('newBlockHeaders')

  try {
    let watcherResults = []

    console.time('blocksWatcherResult')
    const blocksWatcherResult = await new Promise(async (resolve) => {
      subscribeBlockHeaders.on('error', err => {
        subscribeBlockHeaders.unsubscribe()
        console.error(`subscribeBlockHeaders return event 'error': ${err}`)
        watcherResults.push(`subscribeBlockHeaders return event 'error': ${err}`)
        resolve({
          err: err.message,
          txReceipt: {},
          blockNumber: null,
          blockDate: null,
          watcherResults,
        })
      })

      subscribeBlockHeaders.on('data', async blockHeader => {
        txReceipt = _.isNil(txReceipt)
          ? await getTransactionReceipt(txHash).catch(e => {
            throw Error('e')
          })
          : txReceipt

        if (!_.isNil(txReceipt) && !txReceipt.status) {
          subscribeBlockHeaders.unsubscribe()
          resolve({
            err: `txReceipt returned false`,
            txReceipt: txReceipt || {},
            blockNumber: null,
            blockDate: null,
            watcherResults,
          })
          watcherResults.push(`We receive error after ${waitingBlocks} block(s)!`)
        }

        // WAITING FOR 12 BLOCKs TO GET TX RECEIPT
        if (waitingBlocks <= NUMBER_BLOCK_TO_WAIT && _.isNil(txReceipt)) {
          waitingBlocks++
          watcherResults.push(`waitingBlocks=  ${waitingBlocks}`)
        }

        const isTxReceiptError = !_.isNil(txReceipt) && !_.isEmpty(txReceipt.err)
        const IsNoReceipt = waitingBlocks >= NUMBER_BLOCK_TO_WAIT && _.isNil(txReceipt)

        if (isTxReceiptError || IsNoReceipt) {
          subscribeBlockHeaders.unsubscribe()
          console.error(`txReceipt not received after ${waitingBlocks} block(s)`)
          watcherResults.push(`We receive error after ${waitingBlocks} block(s)!`)
          resolve({
            err: isTxReceiptError
              ? txReceipt.err
              : 'Can not get tx receipt for 12 blocks already. Unsubscribed from newBlockHeaders!',
            txReceipt: {},
            blockNumber: null,
            blockDate: null,
            watcherResults,
          })
        }

        const isValidReceiptReceived = waitingBlocks <= NUMBER_BLOCK_TO_WAIT &&
          !_.isNil(txReceipt) &&
          _.isEmpty(txReceipt.err)

        if (isValidReceiptReceived) {
          blockTimestamp = _.isNil(blockTimestamp) ? blockHeader.timestamp : blockTimestamp

          if (confirmationBlocks <= NUMBER_BLOCK_TO_WAIT) {
            console.log(`Get block ${confirmationBlocks}/${NUMBER_BLOCK_TO_WAIT} to validate transaction ${txHash}`)
            confirmationBlocks++
            watcherResults.push(`confirmationBlocks=  ${confirmationBlocks}`)
          } else {
            subscribeBlockHeaders.unsubscribe()
            const message = `Got tx receipt in block ${txReceipt.blockNumber} after waitingBlocks=${waitingBlocks}
          and confirmationBlocks= ${confirmationBlocks - 1}block(s)! Tx success: ${txReceipt.status}`
            console.log(message)
            watcherResults.push(message)
            resolve({
              err: '',
              txReceipt,
              blockNumber: txReceipt.blockNumber,
              blockDate: moment.unix(blockTimestamp).format(),
              watcherResults,
            })
          }
        }
      })
    }).catch(async e => {
      await updateTransactionError(exchangeTransactionId, 'Transaction subscription failed: ' + e)
      subscribeBlockHeaders.unsubscribe()
      watcherResults.push(`Promise error: ${e.message}`)
      console.timeEnd('blocksWatcherResult')
      throw Error(`Transaction subscription failed: ${e.message}`)
    })
    console.timeEnd('blocksWatcherResult')

    if (!_.isNil(blocksWatcherResult) && !_.isEmpty(blocksWatcherResult.err)) {
      subscribeBlockHeaders.unsubscribe()
      console.error(`Transaction subscription error. blocksWatcherResult: ${blocksWatcherResult}`)
      await updateTransactionError(exchangeTransactionId, blocksWatcherResult.err)
      throw Error(`Transaction subscription error: ${blocksWatcherResult.err}`)
    }

    blocksWatcherResult &&
    await updateTransaction(exchangeTransactionId, { blockDate: blocksWatcherResult.blockDate }) &&
    await ExchangeOrder.update({ status: EXCHANGE_STATUS.FILLED }, { where: { id: exchangeOrderId } })
  } catch (e) {
    subscribeBlockHeaders.unsubscribe()
    throw Error('handleTxHash failed:' + e.message)
  }
}
