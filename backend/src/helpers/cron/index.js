import _ from 'lodash'
import cron from 'node-cron'
import { API_KEY } from '../../constants/third_party_api'
import { TRADE_TOKEN_DECIMALS, TRADE_TOKEN_NAME } from '../../constants/market'
import request from 'request-promise-native'
import db from '../../models'
import BigNumber from 'bignumber.js'

const { NODE_ENV } = process.env
const {
  exchangeToken: ExchangeToken,
} = db

const cryptocompareUri = `https://min-api.cryptocompare.com/data/pricemultifull?api_key=${API_KEY.CRYPTOCOMPARE}`

const getOptions = (uri) => {
  return {
    method: 'GET',
    uri,
    json: true,
    headers: {
      'User-Agent': 'Request-Promise',
    },
  }
}

const cronUpdateFrequency = NODE_ENV === 'production'
  ? '*/12 * * * * *' // For production - run cron every 12 seconds */12 * * * * *
  : '*/15 * * * *' // Not production -  run cron every 15 minutes */15 * * * *

// TODO update crone schedule on every 15 sec for production as limit for free API as 100 000 transaction per month
export const cronUpdateExchangeRates = cron.schedule(cronUpdateFrequency, async () => {
  console.time('cronUpdateExchangeRates')
  const exchangeTokens = await ExchangeToken.findAll({
    attributes: ['id', 'tokenSymbol'],
  })

  try {
    for (const exchangeToken of exchangeTokens) {
      if (exchangeToken.tokenSymbol === TRADE_TOKEN_NAME) continue

      const rateData =
        await request(getOptions(
          `${cryptocompareUri}&fsyms=${exchangeToken.tokenSymbol}&tsyms=ETH`
        ))

      if (!_.has(rateData, 'RAW')) {
        if (_.has(rateData, 'Message') || _.isEmpty(rateData.RAW)) {
          console.error('Cryptocompare get rate failed for token:' + exchangeToken.tokenSymbol + '. ' +
            'Error: ' + rateData.Message)
          continue
        }

        if (_.isEmpty(rateData.RAW)) {
          console.error('Cryptocompare get rate return empty RAW for token: ' + exchangeToken.tokenSymbol)
          continue
        }
        console.error('Cryptocompare get rate failed for token:' + exchangeToken.tokenSymbol)
        continue
      }

      const rate = _.get(rateData, `RAW.${exchangeToken.tokenSymbol}.ETH`)

      const values = {
        currentPriceToWETH: new BigNumber(rate.PRICE).toFixed(TRADE_TOKEN_DECIMALS),
        givePriceToWETH: new BigNumber(rate.HIGHHOUR).toFixed(TRADE_TOKEN_DECIMALS),
        receivePriceToWETH: new BigNumber(rate.LOWHOUR).toFixed(TRADE_TOKEN_DECIMALS),
      }

      await ExchangeToken.update(values, {
        where: {
          id: exchangeToken.id,
        },
      })
    }
  } catch (e) {
    console.error('cronUpdateExchangeRates failed: ', e.message)
  }

  console.timeEnd('cronUpdateExchangeRates')
}, {
  scheduled: false,
})
