const Handlebars = require('handlebars')

// http://doginthehat.com.au/2012/02/comparison-block-helper-for-handlebars-templates/#comment-44
Handlebars.registerHelper('compare', function (lvalue, operator, rvalue, options) {
  let result = null

  if (arguments.length < 3) {
    throw new Error('Handlerbars Helper "compare" needs 2 parameters')
  }

  if (options === undefined) {
    options = rvalue
    rvalue = operator
    operator = '==='
  }

  const operators = {
    '==': function (l, r) { return l == r }, // eslint-disable-line eqeqeq
    '===': function (l, r) { return l === r },
    '!=': function (l, r) { return l != r }, // eslint-disable-line eqeqeq
    '!==': function (l, r) { return l !== r },
    '<': function (l, r) { return l < r },
    '>': function (l, r) { return l > r },
    '<=': function (l, r) { return l <= r },
    '>=': function (l, r) { return l >= r },
    'typeof': function (l, r) { return typeof l === r }, // eslint-disable-line valid-typeof
  }

  if (!operators[operator]) {
    throw new Error('Handlerbars Helper "compare" doesn\'t know the operator ' + operator)
  }

  result = operators[operator](lvalue, rvalue)

  if (result) {
    return options.fn(this)
  } else {
    return options.inverse(this)
  }
})

module.exports = Handlebars
