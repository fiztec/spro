import moment from 'moment'
import _ from 'lodash'
import { getAmount, getPrice } from '../order'
import BigNumber from 'bignumber.js'
import db from '../../models'
import { ORDER_SIDE } from '../../constants/order'
import createError from 'http-errors'

const {
  order: Order,
  market: Market,
  matchedOrder: MatchedOrder,
  transaction: Transaction,
  exchangeOrder: ExchangeOrder,
  exchangeToken: ExchangeToken,
  user: User,
  sequelize: { Op, literal },
} = db

const pricePrecision = 9

const sortMatchOrders = (tradeTransactions) => {
  if (!Array.isArray(tradeTransactions)) tradeTransactions = [tradeTransactions]

  for (const transaction of tradeTransactions) {
    if (transaction.order.side === ORDER_SIDE.BUY) transaction.matchedOrders = _.reverse(transaction.matchedOrders)
  }

  return tradeTransactions
}
/**
 *
 * @param marketId - integer or null. iIf null, look for transactions for all markets
 * @param period - moment amount of unit to subtract e.g. period=24, unit=hour, means get data for last 24hours
 * @param unit - moment unit can be 'days', 'hours', 'months'. If indicated 'hours' we take exactly period
 * from current moment minus amount of hours. For other units it takes start of day
 * @param userId - get only user transaction
 * @returns {{include: *[], attributes: string[], where: {createdAt: {}}, order: *[]}}
 */
const getTradeTransactionOptions = (marketId, period, unit) => {
  let where = {}

  if (!_.isNil(marketId)) where.marketId = marketId

  let transactionWhere = { orderId: { [Op.ne]: null }, blockDate: { [Op.ne]: null } }

  if (!_.isNil(period) && !_.isNil(unit)) {
    transactionWhere.createdAt = unit === 'hours'
      ? { [Op.gte]: moment().subtract(period, unit).format() }
      : { [Op.gte]: moment().subtract(period, unit).startOf('day').format() }
  }

  return {
    attributes: ['id', 'createdAt'],
    order: [
      ['createdAt', 'DESC'],
      [literal('"matchedOrders"."quoteTokenAmount"/"matchedOrders"."baseTokenAmount"'), 'DESC'],
    ],
    where: transactionWhere,
    include: [
      {
        model: MatchedOrder,
        attributes: ['baseTokenAmount', 'quoteTokenAmount', 'matchedOrderId'],
        include: {
          model: Order,
          as: 'matchedOrder',
          attributes: ['side'],
          where,
          include: {
            model: Market,
            attributes: ['baseTokenDecimals', 'quoteTokenDecimals'],
          },
        },
      },
      {
        model: Order,
        attributes: ['side'],
        where,
      },
    ],
  }
}

const getTradeTransactionOptionUserHistory = (marketId) => {
  let where = {}

  if (!_.isNil(marketId)) where.marketId = marketId

  return {
    attributes: ['id', 'createdAt'],
    where: { orderId: { [Op.ne]: null }, blockDate: { [Op.ne]: null } },
    order: [
      ['createdAt', 'DESC'],
      [literal('"matchedOrders"."quoteTokenAmount"/"matchedOrders"."baseTokenAmount"'), 'DESC'],
    ],
    include: [
      {
        model: MatchedOrder,
        attributes: ['baseTokenAmount', 'quoteTokenAmount', 'matchedOrderId'],
        include: {
          model: Order,
          as: 'matchedOrder',
          attributes: [
            'side',
          ],
          where,
          include: {
            model: Market,
            attributes: ['baseTokenDecimals', 'quoteTokenDecimals'],
          },
        },
      },
      {
        model: Order,
        attributes: [
          'side',
          'initBaseTokenAmount',
          'initQuoteTokenAmount',
          'baseTokenAmount',
          'quoteTokenAmount',
        ],
        include: {
          model: Market,
          attributes: ['baseTokenDecimals', 'quoteTokenDecimals'],
        },
      },
    ],
  }
}

const getLastMarketPrices = async () => {
  const options = getTradeTransactionOptions(null, 24, 'hours')

  const markets = _.invokeMap(await Market.findAll({
    attributes: ['id', 'tokens', 'baseTokenDecimals', 'quoteTokenDecimals'],
    order: [[literal('"orders->transactions"."createdAt"'), 'DESC']],
    include: {
      model: Order,
      attributes: ['id'],
      include: {
        model: Transaction,
        ...options,
      },
    },
  }), 'get', { plain: true })

  return markets
}

const getLastFirstPrices = (transactions) => {
  const lastOrder = _.get(transactions, '[0].matchedOrders[0]')
  const firstOrder = _.get(_.last(transactions), 'matchedOrders[0]')

  const lastPrice = new BigNumber(getTransactionPriceByOrder(
    lastOrder.matchedOrder,
    lastOrder.quoteTokenAmount,
    lastOrder.baseTokenAmount))
  const firstPrice = new BigNumber(getTransactionPriceByOrder(
    firstOrder.matchedOrder,
    firstOrder.quoteTokenAmount,
    firstOrder.baseTokenAmount))

  return { lastPrice, firstPrice }
}

const getHighLowPrices = (transactions) => {
  let matchedOrders = _.flatten((transactions.map(t => t.matchedOrders)))

  const prices = matchedOrders.map(t => {
    return { price: getTransactionPriceByOrder(t.matchedOrder, t.quoteTokenAmount, t.baseTokenAmount) }
  })

  return { highPrice: (_.maxBy(prices, 'price')).price, lowPrice: (_.minBy(prices, 'price')).price }
}

const getChangeRate = (transactions, lastPrice, market) => {
  const firstOrder = _.get(_.last(transactions), 'matchedOrders[0]')

  const firstPrice = new BigNumber(getTransactionPriceByOrder(
    { market },
    firstOrder.quoteTokenAmount,
    firstOrder.baseTokenAmount))

  return ((lastPrice.minus(firstPrice)).dividedBy(firstPrice)).multipliedBy(100).toFixed(2)
}

/**
 *
 * @param transactions - object {
 *   matchedOrders: [{
 *     baseTokenAmount,
 *     ...
 *   },
 *   ....
 *   ]
 * }
 * @param market - object {
 *   baseTokenDecimals,
 *   ...
 * }
 */
const getMarketVolume = (transactions, market) => {
  if (!Array.isArray(transactions)) transactions = [transactions]

  const matchedOrders = _.flatten(_.map(transactions, 'matchedOrders', []))

  const baseTokenAmountSum = _.reduce(matchedOrders, (sum, m) => {
    return sum.plus(m.baseTokenAmount)
  }, new BigNumber('0'))

  return getAmount(market.baseTokenDecimals, baseTokenAmountSum)
}

const getTransactionPriceByOrder = (order, txQuoteTokenAmount, txBaseTokenAmount) => {
  return (getPrice(
    order.market.quoteTokenDecimals,
    order.market.baseTokenDecimals,
    txQuoteTokenAmount,
    txBaseTokenAmount,
  )).toPrecision()
}

/** *
 *
 * @param transactions = [
 * {
 *   matchedOrders:[
 *     {
 *       quoteTokenAmount,
 *       baseTokenAmount,
 *       ...
 *     }
 *   ]
 * }]
 * @param market - {
 *   baseTokenDecimals,
 *   ...
 * }
 * @returns {BigNumber}
 */
const getLastPrice = (transactions, market) => {
  const lastOrder = _.get(transactions, '[0].matchedOrders[0]')

  return new BigNumber(getTransactionPriceByOrder(
    { market },
    lastOrder.quoteTokenAmount,
    lastOrder.baseTokenAmount))
}

const getMakerTransactions = async (options, userId) => {
  let makerOptions = _.cloneDeep(options)
  makerOptions.include[1].where = { userId }

  return _.invokeMap(await Transaction.findAll(makerOptions), 'get', { plain: true })
}

const getTakerTransactions = async (options, userId) => {
  let takerOptions = _.cloneDeep(options)
  takerOptions.include[0].include.where.userId = userId

  return _.invokeMap(await Transaction.findAll(takerOptions), 'get', { plain: true })
}

const getDailyTransactions = (transactions) => {
  let dailyTransactions = {}

  for (const transaction of transactions) {
    const date = moment(transaction.createdAt).valueOf()
    if (!_.has(dailyTransactions, date)) dailyTransactions[date] = []

    dailyTransactions[date].push(transaction)
  }

  return dailyTransactions
}

const getDailyChartData = (dailyTransactions, market) => {
  let data = []
  const days = _.keys(dailyTransactions)

  for (const day of days) {
    const dayTransactions = dailyTransactions[day]
    const { lastPrice, firstPrice } = getLastFirstPrices(dayTransactions)
    const { highPrice, lowPrice } = getHighLowPrices(dayTransactions)
    // Uncomment if this data will needed for chart
    // const changeRate = ((lastPrice.minus(firstPrice)).dividedBy(firstPrice)).multipliedBy(100).toFixed(2)
    // const priceChange = lastPrice.minus(firstPrice).toFixed(pricePrecision)
    const volume = getMarketVolume(dayTransactions, market)

    data.push({
      close: lastPrice.toFixed(pricePrecision),
      open: firstPrice.toFixed(pricePrecision),
      low: lowPrice,
      high: highPrice,
      time: day,
      volume,
    })
  }

  return data
}

/**
 *
 * @param transactions - collection. [ {
 *   id,
 *   createdAt,
 *   order: {
 *     side,
 *     baseTokenAmount,
 *     quoteTokenAmount,
 *     initBaseTokenAmount,
 *     initQuoteTokenAmount
 *     market: {
 *       baseTokenDecimals,
 *       quoteTokenDecimals
 *     }
 *   },
 * }]
 *
 * @returns {Promise<void>}
 */
export const getTradeHistory = async (transactions) => {
  if (!Array.isArray(transactions)) transactions = [transactions]

  let tradeHistory = {}

  const sortedTransactions = sortMatchOrders(transactions)

  for (const { id, createdAt, matchedOrders } of sortedTransactions) {
    const cleanDate = moment(createdAt).format('DD/MM/YYYY')

    for (const { baseTokenAmount, quoteTokenAmount, matchedOrder: order } of matchedOrders) {
      if (!_.has(tradeHistory, cleanDate)) tradeHistory[cleanDate] = []

      const amount = getAmount(order.market.baseTokenDecimals, baseTokenAmount)
      const price = getTransactionPriceByOrder(order, quoteTokenAmount, baseTokenAmount)

      const trade = {
        id,
        amount,
        price,
        side: order.side,
        createdAt: moment(createdAt).format(),
      }

      tradeHistory[cleanDate].push(trade)
    }
  }

  return tradeHistory
}

export const getTradeHistoryAllTransactions = async (marketId) => {
  const options = getTradeTransactionOptions(marketId, 5, 'days')

  return _.invokeMap(await Transaction.findAll(options), 'get', { plain: true })
}

export const getTradeHistoryByTransactionId = async (id) => {
  const options = getTradeTransactionOptions()
  const transaction = (await Transaction.findByPk(id, options)).get({ plain: true })

  if (_.isNil(transaction)) throw Error('Transaction for history with given Id not found')

  return getTradeHistory(transaction)
}

export const getTradeDashboardData = async (marketId) => {
  let data = {
    lastPrice: '0.00',
    firstPrice: '0.00',
    changeRate: '0.00',
    priceChange: '0.00',
    highPrice: '0.00',
    lowPrice: '0.00',
  }

  const options = getTradeTransactionOptions(marketId, 24, 'hours')

  let transactions = _.invokeMap(await Transaction.findAll(options), 'get', { plain: true })
  if (_.isEmpty(transactions)) return data

  transactions = sortMatchOrders(transactions)

  const { lastPrice, firstPrice } = getLastFirstPrices(transactions)

  data = { ...data, firstPrice: firstPrice.toFixed(pricePrecision), lastPrice: lastPrice.toFixed(pricePrecision) }

  data.changeRate = ((lastPrice.minus(firstPrice)).dividedBy(firstPrice)).multipliedBy(100).toFixed(2)
  data.priceChange = lastPrice.minus(firstPrice).toFixed(pricePrecision)

  data = { ...data, ...getHighLowPrices(transactions) }

  return data
}

export const getLandingDashboardData = async () => {
  const data = []
  const markets = await getLastMarketPrices()
  const initMarket = {
    lastPrice: '0.00',
    changeRate: '0.00',
  }

  for (const market of markets) {
    const { tokens, orders } = market
    const token = _.get((tokens.split('-')), '0')

    if (_.isEmpty(orders)) {
      data.push({ token, ...initMarket })
      continue
    }

    let transactions = _.flatten(_.map(orders, 'transactions'))

    if (_.isEmpty(transactions)) {
      data.push({ token, ...initMarket })
      continue
    }

    transactions = sortMatchOrders(transactions)

    const lastPrice = getLastPrice(transactions, market)

    if (transactions.length === 1) {
      data.push({
        token,
        lastPrice: lastPrice.toFixed(pricePrecision),
        changeRate: '1.00',
      })
      continue
    }

    data.push({
      token,
      lastPrice: lastPrice.toFixed(pricePrecision),
      changeRate: getChangeRate(transactions, lastPrice, market),
    })
  }

  return data
}

export const getMarketsDashboardData = async () => {
  const data = []
  const markets = await getLastMarketPrices()
  const initMarket = {
    lastPrice: '0.00',
    amount: '0.00',
    changeRate: '0.00',
  }

  for (const market of markets) {
    const { tokens, orders } = market

    if (_.isEmpty(orders)) {
      data.push({ marketId: market.id, tokens, ...initMarket })
      continue
    }

    let transactions = _.flatten(_.map(orders, 'transactions', []))

    if (_.isEmpty(transactions)) {
      data.push({ marketId: market.id, tokens, ...initMarket })
      continue
    }

    transactions = sortMatchOrders(transactions)
    const lastPrice = getLastPrice(transactions, market)
    const volume = getMarketVolume(transactions, market)

    if (transactions.length === 1) {
      data.push({
        marketId: market.id,
        tokens,
        lastPrice: lastPrice.toFixed(pricePrecision),
        amount: volume,
        changeRate: '1.00',
      })
      continue
    }

    data.push({
      marketId: market.id,
      tokens,
      lastPrice: lastPrice.toFixed(pricePrecision),
      amount: volume,
      changeRate: getChangeRate(transactions, lastPrice, market),
    })
  }

  return data
}

export const getChartData = async (market, fromTimestamp) => {
  if (!_.isEmpty(fromTimestamp) && !await isChartDataAvailableFromFirstWeek(market.id, fromTimestamp)) {
    throw createError(404, 'Chart data not available for given from period')
  }

  const period = _.isEmpty(fromTimestamp) ? null : getPeriod(fromTimestamp)

  const options = getTradeTransactionOptions(market.id, period, 'days')

  let transactions = _.invokeMap(await Transaction.findAll(options), 'get', { plain: true })

  if (_.isEmpty(transactions)) return []

  transactions = sortMatchOrders(transactions)

  const dailyTransactions = getDailyTransactions(transactions)

  return getDailyChartData(dailyTransactions, market)
}

const getPeriod = (timeStamp) => {
  if (_.isEmpty(timeStamp)) throw Error('To getPeriod valid timestamp expected')
  return moment().diff(moment.unix(timeStamp).format(), 'days')
}

const isChartDataAvailableFromFirstWeek = async (marketId, fromTimestamp) => {
  const from = moment.unix(fromTimestamp).format()
  const to = moment.unix(fromTimestamp).add(7, 'days').format()
  const whereMarketId = { marketId }

  const transactions = _.invokeMap(await Transaction.findAll({
    attributes: ['id', 'createdAt'],
    where: {
      createdAt: { [Op.and]: [
        { [Op.gte]: from },
        { [Op.lt]: to },
      ] },

    },
    include: [
      {
        model: Order,
        attributes: ['id'],
        where: whereMarketId,
      },
    ],
  }), 'get', { plain: true })

  return !_.isEmpty(transactions)
}

export const getUserTradeHistory = async (marketId, userId) => {
  const options = getTradeTransactionOptionUserHistory(marketId)

  let makerTransactions = await getMakerTransactions(options, userId)

  let takerTransactions = await getTakerTransactions(options, userId)

  let transactions = [...takerTransactions, ...makerTransactions]
  if (_.isEmpty(transactions)) return { marketId, data: {} }

  transactions = sortMatchOrders(transactions)

  let tradeHistory = { marketId }
  tradeHistory.data = _.isEmpty(transactions) ? {} : await getTradeHistory(transactions)

  return tradeHistory
}

const getPaginateTransactionOptions = (groupBy) => {
  let orderWhere = {}
  let transactionWhere = { blockDate: { [Op.ne]: null } }

  if (groupBy && (groupBy === ORDER_SIDE.BUY || groupBy === ORDER_SIDE.SELL)) {
    orderWhere.side = groupBy
    transactionWhere.exchangeOrderId = null
  }

  if (groupBy && (groupBy === ORDER_SIDE.EXCHANGE)) transactionWhere.orderId = null

  return {
    attributes: [
      'id',
      'createdAt',
      'blockDate',
    ],
    where: transactionWhere,
    subQuery: false,

    order: [
      ['createdAt', 'DESC'],
      [literal('"matchedOrders"."quoteTokenAmount"/"matchedOrders"."baseTokenAmount"'), 'DESC'],
    ],
    include: [
      {
        model: MatchedOrder,
        attributes: ['baseTokenAmount', 'quoteTokenAmount', 'matchedOrderId'],
        include: {
          model: Order,
          as: 'matchedOrder',
          attributes: ['side'],
          where: orderWhere,
          include: {
            model: Market,
            attributes: ['tokens', 'baseTokenDecimals', 'quoteTokenDecimals'],
          },
        },
      },
      {
        model: Order,
        attributes: ['side'],
        include: {
          model: User,
          attributes: ['publicAddress'],
        },
      }, {
        model: ExchangeOrder,
        as: 'exchangeOrder',
        attributes: ['id', 'giveTokenId', 'receiveTokenId', 'giveAmount', 'receiveAmount'],
        include: [
          {
            model: ExchangeToken,
            as: 'giveToken',
            attributes: ['tokenSymbol', 'tokenDecimals'],
          }, {
            model: ExchangeToken,
            as: 'receiveToken',
            attributes: ['tokenSymbol'],
          }, {
            model: User,
            attributes: ['publicAddress'],
          },
        ],
      },

    ],
  }
}

export const getGroupedPaginatedTransactions = async (groupBy) => {
  const options = getPaginateTransactionOptions(groupBy)

  const transactions = _.invokeMap(await Transaction.findAll({ ...options }), 'get', { plain: true })

  return transactions
}

const formatTradeTransactions = (tradeTransaction) => {
  const sortedMatchOrderTransaction = _.get(sortMatchOrders(tradeTransaction), '[0]')

  const { id, createdAt, matchedOrders } = sortedMatchOrderTransaction
  let trades = []

  for (const { baseTokenAmount, quoteTokenAmount, matchedOrder: order } of matchedOrders) {
    const amount = getAmount(order.market.baseTokenDecimals, baseTokenAmount)
    const price = getTransactionPriceByOrder(order, quoteTokenAmount, baseTokenAmount)

    trades.push({
      id,
      amount,
      price,
      side: order.side,
      tokens: order.market.tokens,
      takerWallet: tradeTransaction.order.user.publicAddress,
      createdAt: moment(createdAt).format(),
    })
  }

  return trades
}

const formatExchangeTransactions = (exchangeTransaction) => {
  const { id, exchangeOrder, createdAt } = exchangeTransaction
  const { giveAmount, receiveAmount, giveToken, receiveToken } = exchangeOrder

  const amount = getAmount(giveToken.tokenDecimals, giveAmount)
  const price = new BigNumber(receiveAmount).div(giveAmount).toFixed(pricePrecision)

  return {
    id,
    amount,
    price,
    side: ORDER_SIDE.EXCHANGE,
    tokens: `${giveToken.tokenSymbol}-${receiveToken.tokenSymbol}`,
    takerWallet: exchangeTransaction.exchangeOrder.user.publicAddress,
    createdAt: moment(createdAt).format(),
  }
}

export const getFormattedList = async (transactions) => {
  if (!Array.isArray(transactions)) transactions = [transactions]

  let transactionList = []

  for (const transaction of transactions) {
    const isTradeTx = transaction.order !== null
    const isExchangeTx = transaction.order === null

    if (isTradeTx) transactionList = transactionList.concat(formatTradeTransactions(transaction))
    if (isExchangeTx) transactionList.push(formatExchangeTransactions(transaction))
  }

  return transactionList
}
