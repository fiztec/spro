import { exchangeContract, web3, web3Http } from '../../lib/web3'
import {
  CONTRACT_GAS_LIMIT,
  NUMBER_BLOCK_TO_WAIT,
  RELAYER,
  RELAYER_PRIVATE_KEY,
  TRADE_EXCHANGE_CONTRACT_ADDRESS,
} from '../../config/web3'
import createError from 'http-errors'
import db from '../../models'
import _ from 'lodash'
import Tx from 'ethereumjs-tx'
import moment from 'moment'
import request from 'request-promise-native'
import { AVERAGE_GAS_PRICE, AVERAGE_GAS_PRICE_TIME_FACTOR } from '../../constants/common'
import BigNumber from 'bignumber.js'
import { logger } from '../../helpers/logger'
import { nonce } from '../../helpers/web3transaction/nonce'

const {
  transaction: Transaction,
} = db

/**
 *
 * @param dataEncodedAbi = contractInstance.methods.matchOrders(traderOrderParam, makerOrderParams,
 * baseTokenFilledAmounts, orderAddressSet).encodeABI()
 * @returns {Promise<any>} null - if no error or error message
 */
const getContractError = async (dataEncodedAbi) => {
  if (process.env.NODE_ENV !== 'production') {
    logger.info(`>>>>> TRADE_EXCHANGE_CONTRACT_ADDRESS= ${TRADE_EXCHANGE_CONTRACT_ADDRESS}`)
  }
  const resultHex = await web3.eth.call({
    from: RELAYER,
    to: TRADE_EXCHANGE_CONTRACT_ADDRESS,
    data: dataEncodedAbi,
  })

  if (_.isNil(resultHex)) throw createError(500, 'Get contract error fail')
  const result = web3.utils.hexToAscii(resultHex)

  return result ? result.match(/[a-zA-Z_]+/g)[1] : null
}

const createOrderTransaction = async (data) => {
  return Transaction.create(data)
}

const updateTransactionError = (id, errMsg) => {
  return Transaction.update({ errMsg }, { where: { id } })
}

const updateTransaction = (id, data) => {
  return Transaction.update(data, { where: { id } })
}

const getTransactionReceipt = async (txHash) => {
  let txReceipt = null

  try {
    txReceipt = await web3.eth.getTransactionReceipt(txHash)
    if (_.isNil(txReceipt)) return null
  } catch (e) {
    // logger.error(`getTransactionReceipt error: ${e.message}`)
    throw Error(`getTransactionReceipt error: ${e.message}`)
  }

  txReceipt.err = ''
  return txReceipt
}

const getMatchOrdersData = (dataParams) => {
  const {
    traderOrderParam,
    makerOrderParams,
    baseTokenFilledAmounts,
    orderAddressSet,
  } = dataParams
  try {
    return exchangeContract.methods.matchOrders(
      traderOrderParam,
      makerOrderParams,
      baseTokenFilledAmounts,
      orderAddressSet
    ).encodeABI()
  } catch (e) {
    throw Error(`Cannot get data for contract method: ${e.message}`)
  }
}

/*
const checkGasLimit = async (dataParams) => {
  const {
    traderOrderParam,
    makerOrderParams,
    baseTokenFilledAmounts,
    orderAddressSet,
  } = dataParams

  const estimatedGasAmount = await exchangeContract.methods.matchOrders(
    traderOrderParam,
    makerOrderParams,
    baseTokenFilledAmounts,
    orderAddressSet
  ).estimateGas({ from: RELAYER })
    .catch(e => {
      throw Error(`checkGasLimit failed: ${e.message}`)
    })

  if (estimatedGasAmount > CONTRACT_GAS_LIMIT) {
    throw Error(`Matching transaction failed. Contract gas limit exceeded.
    Estimated transaction gas ${estimatedGasAmount} is more then current gas limit ${CONTRACT_GAS_LIMIT}`)
  }
}
*/

export const sendTransaction = async (orderId, transactionOption) => {
  const {
    traderOrderParam,
    makerOrderParams,
    baseTokenFilledAmounts,
    orderAddressSet,
  } = transactionOption

  if (
    _.isNil(orderId) ||
    _.isNil(traderOrderParam) ||
    _.isNil(makerOrderParams) ||
    _.isNil(baseTokenFilledAmounts) ||
    _.isNil(orderAddressSet)
  ) {
    throw createError(500, 'Invalid arguments for send transaction')
  }

  let txReceipt = null
  let waitingBlocks = 1
  let confirmationBlocks = 1
  let blockTimestamp = null
  const txGasPriceUpdateCounter = 3

  const matchOrdersData = getMatchOrdersData(transactionOption)
  const txHash = await getTxHash(transactionOption, matchOrdersData, orderId, txGasPriceUpdateCounter)
  logger.info(`Taker order id= ${orderId} got txHash= ${txHash}`)

  const orderTransaction = await createOrderTransaction({ orderId, txHash })

  const subscribeBlockHeaders = web3.eth.subscribe('newBlockHeaders')

  try {
    let watcherResults = []

    console.time('blocksWatcherResult')
    const blocksWatcherResult = await new Promise(async (resolve) => {
      subscribeBlockHeaders.on('error', err => {
        subscribeBlockHeaders.unsubscribe()
        logger.error(`Taker orderId ${orderId}. subscribeBlockHeaders return event 'error': ${err}`)
        watcherResults.push(`subscribeBlockHeaders return event 'error': ${err}`)
        resolve({
          err: err.message,
          txReceipt: {},
          blockNumber: null,
          blockDate: null,
          watcherResults,
        })
      })

      subscribeBlockHeaders.on('data', async blockHeader => {
        txReceipt = _.isNil(txReceipt)
          ? await getTransactionReceipt(txHash).catch(e => {
            throw Error(`Taker orderId ${orderId}. ${e.message}`)
          })
          : txReceipt

        if (!_.isNil(txReceipt) && !txReceipt.status) {
          txReceipt.err = await getContractError(matchOrdersData)
          subscribeBlockHeaders.unsubscribe()
          resolve({
            err: `txReceipt return false ${txReceipt.err}`,
            txReceipt: txReceipt || {},
            blockNumber: null,
            blockDate: null,
            watcherResults,
          })
          watcherResults.push(`We receive error after ${waitingBlocks} block(s)!`)
        }

        // WAITING FOR 12 BLOCKs TO GET TX RECEIPT
        if (waitingBlocks <= NUMBER_BLOCK_TO_WAIT && _.isNil(txReceipt)) {
          waitingBlocks++
          watcherResults.push(`waitingBlocks=  ${waitingBlocks}`)
        }

        const isTxReceiptError = !_.isNil(txReceipt) && !_.isEmpty(txReceipt.err)
        const IsNoReceipt = waitingBlocks >= NUMBER_BLOCK_TO_WAIT && _.isNil(txReceipt)

        if (isTxReceiptError || IsNoReceipt) {
          subscribeBlockHeaders.unsubscribe()
          logger.error(`Taker orderId ${orderId} txReceipt not received after ${waitingBlocks} block(s)`)
          watcherResults.push(`We receive error after ${waitingBlocks} block(s)!`)
          resolve({
            err: isTxReceiptError
              ? txReceipt.err
              : `Can not get tx receipt for ${NUMBER_BLOCK_TO_WAIT} blocks already. Unsubscribed from newBlockHeaders!`,
            txReceipt: {},
            blockNumber: null,
            blockDate: null,
            watcherResults,
          })
        }

        const isValidReceiptReceived = waitingBlocks <= NUMBER_BLOCK_TO_WAIT &&
          !_.isNil(txReceipt) &&
          _.isEmpty(txReceipt.err)

        if (isValidReceiptReceived) {
          blockTimestamp = _.isNil(blockTimestamp) ? blockHeader.timestamp : blockTimestamp

          if (confirmationBlocks <= NUMBER_BLOCK_TO_WAIT) {
            logger.info(`Taker orderId ${orderId} get block ${confirmationBlocks}/${NUMBER_BLOCK_TO_WAIT} to validate 
            transaction ...${txHash.slice(-6)}`)
            confirmationBlocks++
            watcherResults.push(`confirmationBlocks=  ${confirmationBlocks}`)
          } else {
            subscribeBlockHeaders.unsubscribe()
            const message = `Taker orderId ${orderId} got tx receipt in block ${txReceipt.blockNumber} after 
            waitingBlocks=${waitingBlocks} and confirmationBlocks= ${confirmationBlocks - 1}block(s)! 
            Tx success: ${txReceipt.status}`
            logger.info(message)
            watcherResults.push(message)
            resolve({
              err: '',
              txReceipt,
              blockNumber: txReceipt.blockNumber,
              blockDate: moment.unix(blockTimestamp).format(),
              watcherResults,
            })
          }
        }
      })
    }).catch(async e => {
      await updateTransactionError(orderTransaction.id, 'Transaction subscription failed: ' + e)
      subscribeBlockHeaders.unsubscribe()
      watcherResults.push(`Promise error: ${e.message}`)
      console.timeEnd('blocksWatcherResult')
      throw Error(`Transaction subscription failed: ${e.message}`)
    })
    console.timeEnd('blocksWatcherResult')

    if (!_.isNil(blocksWatcherResult) && !_.isEmpty(blocksWatcherResult.err)) {
      subscribeBlockHeaders.unsubscribe()
      logger.error(`Taker orderId ${orderId} Transaction subscription error. 
      blocksWatcherResult: ${blocksWatcherResult}`)
      await updateTransactionError(orderTransaction.id, blocksWatcherResult.err)
      throw Error(`Transaction subscription error: ${blocksWatcherResult.err}`)
    }

    blocksWatcherResult && await updateTransaction(
      orderTransaction.id, { blockDate: blocksWatcherResult.blockDate })

    logger.info(`Taker order ${orderId} >>>> Blockchain transaction successfully finished. 
    blockNumber= ${blocksWatcherResult.txReceipt.blockNumber}`)

    return orderTransaction.id
  } catch (e) {
    try {
      subscribeBlockHeaders.unsubscribe()
    } catch (err) {
      logger.error(`Taker orderId ${orderId} subscribeBlockHeaders.unsubscribe failed = ${err.message}`)
    }
    throw Error(`Taker orderId ${orderId}. ${e.message}`)
  }
}

/**
 * Returns average gas price in wei, fetched from API ethgasstation.info
 */
export const getAverageGasPrise = async () => {
  console.time('getAverageGasPrise')
  const uri = 'https://ethgasstation.info/json/ethgasAPI.json'
  const options = {
    method: 'GET',
    uri,
    json: true,
    headers: {
      'User-Agent': 'Request-Promise',
    },
  }

  let averageGasPriceData

  try {
    averageGasPriceData = await request(options)
  } catch (e) {
    logger.error(`Failed to get average gas price from API. Falling back to average gas price = ${AVERAGE_GAS_PRICE}!`)
    console.timeEnd('getAverageGasPrise')
    return AVERAGE_GAS_PRICE // Gwei - get from API standard
  }

  try {
    averageGasPriceData = await request(options)

    const averageGasPriceMapped = transformEthgasstationResponse(averageGasPriceData)

    if (!averageGasPriceMapped) {
      throw Error(`Failed to transform value from API response! ${averageGasPriceData}`)
    }

    const averageGasPrice = getGasPriceByTimeFactor(averageGasPriceMapped)

    logger.info(`Gas price is ${averageGasPrice} Gwei`)
    console.timeEnd('getAverageGasPrise')

    return web3.utils.toWei(averageGasPrice.toString(), 'gwei')
  } catch (e) {
    logger.error(`Failed to get gas price: ${e.message}. 
    Falling back to average gas price = ${new BigNumber(AVERAGE_GAS_PRICE).div(10 ** 9)} Gwei!`)
    console.timeEnd('getAverageGasPrise')
    return AVERAGE_GAS_PRICE // Gwei - get from API standard
  }
}

/* Transforms ethgasstation.info API response:
   { fast: 120,
     fastest: 200,
     safeLow: 60,
     average: 70,
     block_time: 18.625,
     blockNum: 7905284,
     speed: 0.9939371944755491,
     safeLowWait: 12.7,
     avgWait: 3,
     fastWait: 0.6,
     fastestWait: 0.6
    }
   *
   * To the next one (sorted by time desc):
   * [
     { gasPrice: 6, time: 12.7 },
     { gasPrice: 7, time: 3 },
     { gasPrice: 12, time: 0.6 },
     { gasPrice: 20, time: 0.6 }
    ]
   */
const transformEthgasstationResponse = (response) => {
  const gasPriceMap = []
  for (const [key, value] of Object.entries(response)) {
    if (key.endsWith('Wait')) {
      const correspondKey = key.replace('Wait', '').replace('avg', 'average')
      if (!correspondKey) {
        throw new Error(`API payload corrupted!`)
      }
      gasPriceMap.push({
        // ethgasstation.info just multiply gas prise in Gwei by 10, so we have to divide it
        gasPrice: Number(response[correspondKey]) / 10,
        time: Number(value),
      })
    }
  }
  return _.orderBy(gasPriceMap, ['time'], ['desc'])
}

const getGasPriceByTimeFactor = (averageGasMap) => {
  const timeFactor = +AVERAGE_GAS_PRICE_TIME_FACTOR

  // Here we have to return first value which less than wanted timefactor
  const averageGasPrice = averageGasMap
    .filter(average => average.time <= timeFactor)
    .shift()

  if (!averageGasPrice) {
    logger.info(`Failed to calculate average gas price based on timefactor: ${timeFactor}. 
    Average gas map: ${JSON.stringify(averageGasMap)}`)
    throw new Error(`Failed to calculate average gas price based on timefactor: ${timeFactor}`)
  }

  return averageGasPrice.gasPrice
}

/**
 *
 * @param {{
    traderOrderParam: Array<*>,
    makerOrderParams: Array<Array<string>>,
    orderAddressSet: Array<string>,
 * }} transactionOption
 * @param {*} matchOrdersData
 * @param {number} orderId
 * @param {BigNumber} updateGasPrice
 * @param {number} txGasPriceUpdateCounter
 * @return {Promise<string>}
 */
const getTxHash = async (transactionOption, matchOrdersData, orderId, txGasPriceUpdateCounter) => {
  const errorMsgUnderpriced = 'replacement transaction underpriced'
  const errorMsgNonce = 'nonce'

  try {
    const txCount = await nonce.get()

    const txData = {
      nonce: web3.utils.toHex(txCount),
      gasLimit: web3.utils.toHex(CONTRACT_GAS_LIMIT),
      gasPrice: web3.utils.toHex(await getAverageGasPrise()),
      from: RELAYER,
      to: TRADE_EXCHANGE_CONTRACT_ADDRESS,
      data: matchOrdersData,
    }

    const transaction = new Tx(txData)
    await transaction.sign(RELAYER_PRIVATE_KEY)

    const serializedTx = await transaction.serialize().toString('hex')
    if (_.isNil(serializedTx)) throw Error('Cannot get serializedTx')

    const checkTransactionError = await getContractError(matchOrdersData)
    if (!_.isNil(checkTransactionError)) throw Error(`Transaction call get error: ${checkTransactionError}`)

    console.time('getTxHash')

    let txHash

    // increase local nonce before we get txHash from blockchain
    await nonce.increase()
    const tx = await web3Http.eth.sendSignedTransaction('0x' + serializedTx)
      .catch(async (e) => {
        logger.info(`Taker order= ${orderId} failed to get txHash. 
        TxGasPriceUpdateCounter=${txGasPriceUpdateCounter} | nonce= ${nonce} | Error: ${e.message}`)
        console.timeEnd('getTxHash')

        if (txGasPriceUpdateCounter > 0) {
          const isErrorMsgUnderpriced = e.message.indexOf(errorMsgUnderpriced) !== -1
          const isErrorMsgNonce = e.message.indexOf(errorMsgNonce) !== -1

          if (isErrorMsgUnderpriced || isErrorMsgNonce) {
            txHash = await errorHandleIncreaseNonce(
              txData,
              transactionOption,
              matchOrdersData,
              orderId,
              txGasPriceUpdateCounter,
              e.message
            )
          } else {
            txHash = await errorHandleOnlineNonce(
              txData,
              transactionOption,
              matchOrdersData,
              orderId,
              txGasPriceUpdateCounter,
              e.message
            )
          }
        }

        if (!txHash) {
          throw Error(`sendSignedTransaction failed: ${e.message}`)
        }
      })

    console.timeEnd('getTxHash')

    if (!txHash && (_.isNil(tx) || _.isEmpty(tx))) {
      const err = await getContractError(matchOrdersData)
      throw Error(`tx is empty. ${err}`)
    }

    return txHash || _.get(tx, 'transactionHash', '')
  } catch (e) {
    throw Error(`getTxHash failed. ${e.message}`)
  }
}

const errorHandleIncreaseNonce = async (
  txData,
  transactionOption,
  matchOrdersData,
  orderId,
  txGasPriceUpdateCounter,
  errorMessage
) => {
  logger.info(`Taker orderId= ${orderId} got error "${errorMessage}". Increase nonce`)

  await nonce.increase()

  return getTxHash(
    transactionOption,
    matchOrdersData,
    orderId,
    txGasPriceUpdateCounter - 1
  )
}

const errorHandleOnlineNonce = async (
  txData,
  transactionOption,
  matchOrdersData,
  orderId,
  txGasPriceUpdateCounter,
  errorMessage
) => {
  logger.info(`Taker order= ${orderId} got error "${errorMessage}. Try to get nonce online and repeat transaction.`)
  await nonce.getNonceOnline()

  return getTxHash(
    transactionOption,
    matchOrdersData,
    orderId,
    txGasPriceUpdateCounter - 1
  )
}
