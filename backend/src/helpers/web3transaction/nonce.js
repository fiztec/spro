import { web3 } from '../../lib/web3'
import { RELAYER } from '../../config/web3'
import _ from 'lodash'

class Nonce {
  async getNonceOnline () {
    console.time('getTxCount')
    this.value = await web3.eth.getTransactionCount(RELAYER)
      .catch(e => {
        throw Error(`Get txCount error: ${e.message}`)
      }
      )
    if (_.isNil(this.value)) throw Error('Cannot get txCount')
    console.timeEnd('getTxCount')

    return this.value
  }

  async get () {
    if (!this.value) {
      this.value = await this.getNonceOnline()
    }
    return this.value
  }

  async increase () {
    if (!this.value) {
      this.value = await this.getNonceOnline()
      return
    }
    this.value++
  }
}

export const nonce = new Nonce()
