import { FROM_EMAIL, NOTIFICATION_EMAIL } from '../config/mail'
import { logger } from '../helpers/logger'

const { createTransport } = require('nodemailer')
const path = require('path')
const fs = require('fs')
const readFileAsync = Promise.promisify(fs.readFile)
const hbs = require('../helpers/handlebars')

const env = process.env.NODE_ENV || 'development'
const { emailTransportOptions } = require('../config/mail')

/**
 *
 * @param mailOptions = object = {
      from: 'admin@gmail.com',
      to: user@gmail.com,
      subject: 'Welcome on gmail' ,
      html: '<p>Hello new user</p>',
    }
 * @returns {Promise<*>}
 */
const sendMail = async (mailOptions = {}) => {
  const transporter = createTransport(
    emailTransportOptions[env],
  )
  const info = await transporter.sendMail(mailOptions)

  console.log('Message sent: %s', info.messageId)

  return info
}

const compileTemplate = async (template, data, strict = true) => {
  const defaultExtension = 'hbs'
  const extension = path.extname(template)
  const tmplPath = path.resolve(__dirname, 'templates', extension ? template : `${template}.${defaultExtension}`)

  try {
    const source = await readFileAsync(tmplPath, 'utf-8')
    return hbs.compile(source, { strict })(data)
  } catch (err) {
    console.log(err)
  }
}

const sendBlockchainErrorEmail = async (takerOrderId, makerOrderIds, errorMessage) => {
  const template = await compileTemplate('blockchainError', { errorMessage, takerOrderId, makerOrderIds })
  const mailOptions = {
    from: `"MeterQubes" <${FROM_EMAIL}>`,
    to: NOTIFICATION_EMAIL,
    subject: 'Blockchain process failed. Taker order ' + takerOrderId + ' | env: ' + process.env.NODE_ENV,
    html: template,
  }

  try {
    await sendMail(mailOptions)
  } catch (e) {
    logger.error(`Send error email failed ${e.message}`)
  }
}

module.exports = {
  sendMail,
  compileTemplate,
  sendBlockchainErrorEmail,
}
