'use strict'
const ethUtil = require('ethereumjs-util')
const { USER_ROLE } = require('../constants/user')

module.exports = (sequelize, DataTypes) => {
  const LiquidityBot = sequelize.define('liquidityBot', {
    publicAddress: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        isLowercase: true,
        isAddress (value) {
          if (!ethUtil.isValidAddress(value)) {
            throw new Error('Invalid publicAddress!')
          }
        },
      },
      unique: {
        args: true,
        msg: 'publicAddress must be unique.',
        fields: [sequelize.fn('lower', sequelize.col('publicAddress'))],
      },
    },
    botname: {
      type: DataTypes.STRING,
      defaultValue: 'Constant Product Market Making Model',
    },
    role: {
      type: DataTypes.ENUM(Object.values(USER_ROLE)),
      defaultValue: USER_ROLE.BOT,
      validate: {
        equals: USER_ROLE.BOT,
      },
    },
    isRunning: {
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
  })

  LiquidityBot.associate = (models) => {
    const {
      market: Market,
      order: Order,
      botSettings: BotSettings,
    } = models

    LiquidityBot.belongsTo(Market, { as: 'market', foreignKey: 'marketId' })
    LiquidityBot.hasMany(Order)
    LiquidityBot.hasOne(BotSettings)
  }

  return LiquidityBot
}
