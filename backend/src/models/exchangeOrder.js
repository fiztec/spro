'use strict'
const ethUtil = require('ethereumjs-util')
const { EXCHANGE_STATUS } = require('../constants/exchange')

module.exports = (sequelize, DataTypes) => {
  const ExchangeOrder = sequelize.define('exchangeOrder', {
    exchangeOrderIdEth: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        len: 66,
      },
    },
    giveTokenId: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    receiveTokenId: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    giveAmount: {
      allowNull: false,
      type: DataTypes.DECIMAL(34, 0),
    },
    receiveAmount: {
      allowNull: false,
      type: DataTypes.DECIMAL(34, 0),
    },
    makerAddress: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        isLowercase: true,
        isAddress (value) {
          if (!ethUtil.isValidAddress(value)) {
            throw new Error('Invalid maker address!')
          }
        },
      },
    },
    feeRecipient: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        isLowercase: true,
        isAddress (value) {
          if (!ethUtil.isValidAddress(value)) {
            throw new Error('Invalid fee recipient address!')
          }
        },
      },
    },
    makerFee: {
      allowNull: false,
      type: DataTypes.FLOAT,
      defaultValue: 0,
      validate: {
        max: 1,
      },
    },
    takerFee: {
      allowNull: false,
      type: DataTypes.FLOAT,
      defaultValue: 0,
      validate: {
        max: 1,
      },
    },
    makerSignatureV: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    makerSignatureR: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    makerSignatureS: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    expirationTimestampInSec: {
      allowNull: false,
      type: DataTypes.INTEGER,
      validate: {
        isFuture (value) {
          if (value <= (Math.floor(Date.now() / 1000))) {
            throw new Error('Invalid expiration time value!')
          }
        },
      },
    },
    salt: {
      allowNull: false,
      type: DataTypes.DECIMAL(24, 0),
    },
    status: {
      allowNull: false,
      type: DataTypes.ENUM(Object.values(EXCHANGE_STATUS)),
      defaultValue: EXCHANGE_STATUS.PENDING,
    },
  })

  ExchangeOrder.associate = (models) => {
    ExchangeOrder.belongsTo(models.user)
    ExchangeOrder.belongsTo(models.exchangeToken, { as: 'giveToken', foreignKey: 'giveTokenId' })
    ExchangeOrder.belongsTo(models.exchangeToken, { as: 'receiveToken', foreignKey: 'receiveTokenId' })

    ExchangeOrder.hasOne(models.transaction)
  }

  return ExchangeOrder
}
