/* Exchange model contains list of tokens for instance exchange.
 * All prices are specified in ETH(WETH)
 * and to be updated regularly by precialized watcher (TODO).
 * For MVP-dev purpose all prices are fixed (db seed) using ddex.io quotes
 */

'use strict'
const ethUtil = require('ethereumjs-util')

module.exports = (sequelize, DataTypes) => {
  const ExchangeToken = sequelize.define('exchangeToken', {
    tokenSymbol: {
      type: DataTypes.STRING,
      validate: {
        min: 3,
        isUppercase: true,
      },
    },
    tokenName: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        min: 3,
      },
    },
    tokenAddress: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        isLowercase: true,
        isAddress (value) {
          if (!ethUtil.isValidAddress(value)) {
            throw new Error('Invalid baseTokenAddress!')
          }
        },
      },
    },
    tokenProjectUrl: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: { isUrl: true },
    },
    tokenDecimals: {
      allowNull: false,
      type: DataTypes.INTEGER,
      validate: {
        len: {
          args: [0, 18],
          msg: 'Base token decimals should be in range 0 to 18',
        },
      },
    },
    minOrderSize: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    currentPriceToWETH: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    givePriceToWETH: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    receivePriceToWETH: {
      allowNull: false,
      type: DataTypes.STRING,
    },

    maxSlippage: {
      allowNull: false,
      type: DataTypes.STRING,
    },
  })

  ExchangeToken.associate = (models) => {
    ExchangeToken.hasMany(models.exchangeOrder)
  }

  return ExchangeToken
}
