'use strict'

module.exports = (sequelize, DataTypes) => {
  const BotSettings = sequelize.define('botSettings', {
    centerPrice: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    minPrice: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    maxPrice: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    priceGap: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    expandInventory: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
  })

  BotSettings.associate = (models) => {
    BotSettings.belongsTo(models.liquidityBot)
  }
  return BotSettings
}
