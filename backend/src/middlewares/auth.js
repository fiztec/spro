import { SECRET } from '../config/jwt'
import { verify } from 'jsonwebtoken'
import _ from 'lodash'

export const getTokenData = (ctx) => {
  try {
    const token = ctx.request.headers['authorization']
    return _.get(verify(token.replace('Bearer ', ''), SECRET), 'payload', {})
  } catch (err) {
    const errorMessage = 'You must be authorized'
    ctx.status = 401
    ctx.body = {
      error: errorMessage,
    }
    throw Error(errorMessage)
  }
}

export const authMiddleware = async (ctx, next) => {
  try {
    ctx.state.user = getTokenData(ctx)
    return next()
  } catch (err) {
    console.error('authMiddleware fail. Error: ' + err.message)
  }
}
