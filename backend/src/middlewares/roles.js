import db from '../models'
import _ from 'lodash'
import { USER_ROLE } from '../constants/user'
import { getTokenData } from './auth'
const { user: User } = db
const ATTRIBUTES = ['role']

export const isAdminMiddleware = async (ctx, next) => {
  try {
    const { id } = getTokenData(ctx)

    const user = await User.findByPk(id, { attributes: ATTRIBUTES })

    ctx.assert(!_.isNil(user), 404, 'User not found')
    ctx.assert(user.role === USER_ROLE.ADMIN, 403, 'Only user with role Admin has access')

    return next()
  } catch (err) {
    ctx.status = err.status || ctx.status
    ctx.body = {
      error: err.message || 'isAdminMiddleware fail. Error: ' + err.message,
    }
  }
}
