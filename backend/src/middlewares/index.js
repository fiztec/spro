import { authMiddleware } from './auth'
import { isAdminMiddleware } from './roles'

module.exports = {
  authMiddleware,
  isAdminMiddleware,
}
