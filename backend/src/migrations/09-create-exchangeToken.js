'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('exchangeToken', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      tokenSymbol: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      tokenName: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      tokenAddress: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      tokenProjectUrl: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      tokenDecimals: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      minOrderSize: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      currentPriceToWETH: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      givePriceToWETH: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      receivePriceToWETH: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      maxSlippage: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('exchangeToken')
  },
}
