'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('liquidityBot', 'isRunning', {
      allowNull: true,
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.changeColumn('liquidityBot', 'isRunning', {
      allowNull: false,
      type: Sequelize.BOOLEAN,
      defaultValue: false,
    })
  },
}
