'use strict'
const { USER_ROLE } = require('../constants/user')

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('user', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      nonce: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      publicAddress: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true,
      },
      username: {
        type: Sequelize.STRING,
      },
      role: {
        type: Sequelize.ENUM(Object.values(USER_ROLE)),
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('user')
  },
}
