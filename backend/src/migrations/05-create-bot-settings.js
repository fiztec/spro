'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('botSettings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      liquidityBotId: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'liquidityBot',
          key: 'id',
        },
      },
      centerPrice: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      minPrice: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      maxPrice: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      priceGap: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      expandInventory: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('botSettings')
  },
}
