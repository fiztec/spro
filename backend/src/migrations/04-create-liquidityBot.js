'use strict'
const { USER_ROLE } = require('../constants/user')

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('liquidityBot', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      marketId: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'market',
          key: 'id',
        },
      },
      publicAddress: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      botname: {
        type: Sequelize.STRING,
        defaultValue: 'Constant Product Market Making Model',
      },
      role: {
        type: Sequelize.ENUM(Object.values(USER_ROLE)),
        defaultValue: USER_ROLE.BOT,
      },
      isRunning: {
        allowNull: false,
        type: Sequelize.BOOLEAN,
        defaultValue: false,
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('liquidityBot')
  },
}
