'use strict'
const { EXCHANGE_STATUS } = require('../constants/exchange')

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('exchangeOrder', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      userId: {
        type: Sequelize.INTEGER,
        references: {
          model: 'user',
          key: 'id',
        },
      },
      exchangeOrderIdEth: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      giveTokenId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'exchangeToken',
          key: 'id',
        },
      },
      receiveTokenId: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'exchangeToken',
          key: 'id',
        },
      },
      giveAmount: {
        allowNull: false,
        type: Sequelize.DECIMAL(34, 0),
      },
      receiveAmount: {
        allowNull: false,
        type: Sequelize.DECIMAL(34, 0),
      },
      makerAddress: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      feeRecipient: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      makerFee: {
        allowNull: false,
        type: Sequelize.FLOAT,
        defaultValue: 0,
      },
      takerFee: {
        allowNull: false,
        type: Sequelize.FLOAT,
        defaultValue: 0,
      },
      makerSignatureV: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      makerSignatureR: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      makerSignatureS: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      expirationTimestampInSec: {
        allowNull: false,
        type: Sequelize.INTEGER,
      },
      salt: {
        allowNull: false,
        type: Sequelize.DECIMAL(24, 0),
      },
      status: {
        allowNull: false,
        type: Sequelize.ENUM(Object.values(EXCHANGE_STATUS)),
        defaultValue: EXCHANGE_STATUS.PENDING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('exchangeOrder')
  },
}
