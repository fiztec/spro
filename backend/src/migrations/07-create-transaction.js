'use strict'

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('transaction', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      orderId: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model: 'order',
          key: 'id',
        },
      },
      exchangeOrderId: {
        allowNull: true,
        type: Sequelize.INTEGER,
      },
      txHash: {
        allowNull: false,
        type: Sequelize.STRING,
        unique: true,
      },
      blockDate: {
        type: Sequelize.DATE,
      },
      errMsg: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('transaction')
  },
}
