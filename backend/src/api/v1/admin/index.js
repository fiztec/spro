import Router from 'koa-router'
import users from './users/router'
import fees from './fees/router'
import token from './token/router'
import transactions from './transactions/router'
import { isAdminMiddleware } from '../../../middlewares'

const router = new Router()

router
  .use('/fees', isAdminMiddleware, fees)
  .use('/users', isAdminMiddleware, users)
  .use('/token', isAdminMiddleware, token)
  .use('/transactions', isAdminMiddleware, transactions)

export default router.routes()
