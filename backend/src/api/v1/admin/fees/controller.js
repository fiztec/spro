import _ from 'lodash'
import db from '../../../../models'
import { feeToPercentFixed3, feeFromPercentFixed5 } from '../../../../constants/market'
import createError from 'http-errors'
const { market: Market } = db

const FEE_ATTRIBUTES = [
  'asMakerFeeRate',
  'asTakerFeeRate',
]

const validateFees = (makerFeeRate, takerFeeRate) => {
  if (!_.isNaN(makerFeeRate) && makerFeeRate <= 0) throw createError(400, 'Fees should be positive and not zero')
  if (!_.isNaN(takerFeeRate) && takerFeeRate <= 0) throw createError(400, 'Fees should be positive and not zero')

  if (makerFeeRate && (makerFeeRate > 10 || makerFeeRate < 0.001)) {
    throw createError(400, 'Maker fee can be from 0.001 to 10')
  }
  if (takerFeeRate && (takerFeeRate > 10 || takerFeeRate < 0.001)) {
    throw createError(400, 'Taker fee can be from 0.001 to 10')
  }
}

export const getFees = async (ctx, next) => {
  await next()

  const market = await Market.findOne({ order: [['id', 'ASC']], attributes: FEE_ATTRIBUTES })
  ctx.assert(!_.isNil(market), 404, 'Market to take fees not found')

  ctx.status = 200
  ctx.body = {
    asMakerFeeRate: feeToPercentFixed3(market.asMakerFeeRate),
    asTakerFeeRate: feeToPercentFixed3(market.asTakerFeeRate),
  }
}

export const setTradeFee = async (ctx, next) => {
  await next()

  let { asMakerFeeRate, asTakerFeeRate } = ctx.request.body

  ctx.assert(!_.isEmpty(ctx.request.body), 400, 'Bed request parameters')
  validateFees(parseFloat(asMakerFeeRate), parseFloat(asTakerFeeRate))

  let newFees = {}
  if (asMakerFeeRate) newFees.asMakerFeeRate = feeFromPercentFixed5(asMakerFeeRate)
  if (asTakerFeeRate) newFees.asTakerFeeRate = feeFromPercentFixed5(asTakerFeeRate)

  await Market.update(newFees, { where: {} })

  ctx.status = 204
}
