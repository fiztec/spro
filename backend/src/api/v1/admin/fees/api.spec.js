const server = require('../../../../index')
const agent = require('supertest').agent(server)
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoxLCJwdWJsaWNBZGRyZXNzIjoiMHg5NjI0MTE4' +
  'NmNhNGFhNzlmNWJkNTkzYTAyYzI5YTM4ZGE3ODUyNTFlIn0sImlhdCI6MTU1NTQyMzE2Mn0.lVzu2XbZ98Cj73wOW7phe5py-LT_uq1uFlaQpjsYqho'
const clientToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoyLCJwdWJsaWNBZGRyZXNzIjoiMHhhODBmZj' +
  'A4ZDA4ODA5YjI5YzczMzEzNWIxYjkzYTA3Nzk5ZTVkMTdmIn0sImlhdCI6MTU1NDI4NzA5NH0.u-tN-7YKjA3MzkTrQlDx5Bkd41lVRE9C8fs3' +
  '_9e6KuM'
const db = require('../../../../models')
const { market: Market } = db
const { feeToPercentFixed3 } = require('../../../../constants/market')

const requestBody = {
  asMakerFeeRate: '0.390',
  asTakerFeeRate: '0.630',
}

const FEE_ATTRIBUTES = [
  'asMakerFeeRate',
  'asTakerFeeRate',
]

const setInitFees = async () => {
  const initFees = {
    asMakerFeeRate: '0.00100',
    asTakerFeeRate: '0.00300',
  }

  await Market.update(initFees, { where: {} })
}

describe('Admin Fees (API)', function () {
  afterAll(async () => {
    await server.close()
  })

  bothFeesChecks()
  makerFeeChecks()
  takerFeeChecks()
  authChecks()
})

function makerFeeChecks () {
  it('set maker fee only', async () => {
    expect.assertions(2)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send({ asMakerFeeRate: requestBody.asMakerFeeRate })
      .catch(err => console.error(err))

    expect(response.status).toEqual(204)
    const market = await Market.findOne({ order: [['id', 'ASC']], attributes: FEE_ATTRIBUTES })
    expect(feeToPercentFixed3(market.asMakerFeeRate)).toEqual(requestBody.asMakerFeeRate)

    await setInitFees()
  })

  it('Negative. Try to set negative market fee', async () => {
    expect.assertions(2)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send({ ...requestBody,
        asMakerFeeRate: '-0.390',
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message).toEqual('Fees should be positive and not zero')
  })

  it('Negative. Try to set market fee equal 0', async () => {
    expect.assertions(2)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send({ ...requestBody,
        asMakerFeeRate: '0',
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message).toEqual('Fees should be positive and not zero')
  })

  it('Negative. Try to set market fee less then min', async () => {
    expect.assertions(2)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send({ ...requestBody,
        asMakerFeeRate: '0.0001',
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message).toEqual('Maker fee can be from 0.001 to 10')
  })

  it('Negative. Try to set market fee more then max', async () => {
    expect.assertions(2)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send({ ...requestBody,
        asMakerFeeRate: '10.001',
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message).toEqual('Maker fee can be from 0.001 to 10')
  })
}

function takerFeeChecks () {
  it('set taker fee only', async () => {
    expect.assertions(2)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send({ asTakerFeeRate: requestBody.asTakerFeeRate })
      .catch(err => console.error(err))

    expect(response.status).toEqual(204)
    const market = await Market.findOne({ order: [['id', 'ASC']], attributes: FEE_ATTRIBUTES })
    expect(feeToPercentFixed3(market.asTakerFeeRate)).toEqual(requestBody.asTakerFeeRate)

    await setInitFees()
  })

  it('Negative. Try to set negative taker fee', async () => {
    expect.assertions(2)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send({ ...requestBody,
        asTakerFeeRate: '-0.390',
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message).toEqual('Fees should be positive and not zero')
  })

  it('Negative. Try to set taker fee equal 0', async () => {
    expect.assertions(2)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send({ ...requestBody,
        asTakerFeeRate: '0',
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message).toEqual('Fees should be positive and not zero')
  })

  it('Negative. Try to set taker fee less then min', async () => {
    expect.assertions(2)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send({ ...requestBody,
        asTakerFeeRate: '0.0001',
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message).toEqual('Taker fee can be from 0.001 to 10')
  })

  it('Negative. Try to set taker fee more then max', async () => {
    expect.assertions(2)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send({ ...requestBody,
        asTakerFeeRate: '10.001',
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message).toEqual('Taker fee can be from 0.001 to 10')
  })
}

function authChecks () {
  it('Check Auth. Get fees', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/admin/fees')
      .send(requestBody)
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })

  it('Check non admin Auth. Get fees', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/admin/fees')
      .set('Authorization', 'Bearer ' + clientToken)
      .send(requestBody)
      .catch(err => console.error(err))

    expect(response.status).toEqual(403)
  })

  it('Check Auth. Set both fees', async () => {
    expect.assertions(1)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .send(requestBody)
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })

  it('Check non admin Auth. Set both fees', async () => {
    expect.assertions(1)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + clientToken)
      .send(requestBody)
      .catch(err => console.error(err))

    expect(response.status).toEqual(403)
  })
}

function bothFeesChecks () {
  it('get fees', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/admin/fees')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body).toMatchSnapshot()
  })

  it('set both fees', async () => {
    expect.assertions(3)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send(requestBody)
      .catch(err => console.error(err))

    expect(response.status).toEqual(204)
    const market = await Market.findOne({ order: [['id', 'ASC']], attributes: FEE_ATTRIBUTES })
    expect(feeToPercentFixed3(market.asMakerFeeRate)).toEqual(requestBody.asMakerFeeRate)
    expect(feeToPercentFixed3(market.asTakerFeeRate)).toEqual(requestBody.asTakerFeeRate)

    await setInitFees()
  })

  it('Negative. Try to set empty fees', async () => {
    expect.assertions(2)

    const response = await agent.patch('/api/v1/admin/fees/trade')
      .set('Authorization', 'Bearer ' + token)
      .send({})
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message).toEqual('Bed request parameters')
  })
}
