import Router from 'koa-router'
import * as feeController from './controller'
import { authMiddleware } from '../../../../middlewares/auth'
const router = new Router()

router
  .get('/', authMiddleware, feeController.getFees)
  .patch('/trade', authMiddleware, feeController.setTradeFee)

export default router.routes()
