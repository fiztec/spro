const server = require('../../../../index')
const agent = require('supertest').agent(server)
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoxLCJwdWJsaWNBZGRyZXNzIjoiMHg5NjI0MTE4' +
  'NmNhNGFhNzlmNWJkNTkzYTAyYzI5YTM4ZGE3ODUyNTFlIn0sImlhdCI6MTU1NTQyMzE2Mn0.lVzu2XbZ98Cj73wOW7phe5py-LT_uq1uFlaQpjsYqho'
const clientToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoyLCJwdWJsaWNBZGRyZXNzIjoiMHhhODBmZj' +
  'A4ZDA4ODA5YjI5YzczMzEzNWIxYjkzYTA3Nzk5ZTVkMTdmIn0sImlhdCI6MTU1NDI4NzA5NH0.u-tN-7YKjA3MzkTrQlDx5Bkd41lVRE9C8fs3' +
  '_9e6KuM'

describe('Admin Users (API)', function () {
  afterAll(async () => {
    await server.close()
  })

  it('get users wallet addresses with right page in query', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/admin/users/wallets?page=1')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
  })

  it('get users wallet addresses without page in query', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/admin/users/wallets')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
  })

  it('get users wallet addresses with wrong page in query', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/admin/users/wallets?page=5')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(404)
    expect(response.body.message).toEqual('Users not found with given page')
  })

  it('check Auth. get users wallet addresses with right page in query', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/admin/users/wallets?page=1')
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })

  it('Check non admin Auth. get users wallet addresses with right page in query', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/admin/users/wallets?page=1')
      .set('Authorization', 'Bearer ' + clientToken)
      .catch(err => console.error(err))

    expect(response.status).toEqual(403)
  })
})
