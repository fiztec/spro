import _ from 'lodash'
import db from '../../../../models'
import { paginate } from '../../../../helpers/common'
import { PAGINATION_FIRST_PAGE_RECORDS, PAGINATION_NEXT_PAGES_RECORDS } from '../../../../constants/common'
const { user: User, sequelize: { literal } } = db
const ATTRIBUTES = ['publicAddress']

const getUserTotal = () => {
  return '(SELECT COUNT(*) FROM "user")'
}

export const getUserWallets = async (ctx, next) => {
  await next()

  const { page } = ctx.query

  let users = _.invokeMap(await User.findAll({
    attributes: [
      ...ATTRIBUTES,
      [literal(getUserTotal()), 'total'],
    ],
    ...paginate(page, PAGINATION_FIRST_PAGE_RECORDS, PAGINATION_NEXT_PAGES_RECORDS),
    order: [['id', 'ASC']],
  }), 'get', { plain: true })
  ctx.assert(!_.isEmpty(users), 404, 'Users not found with given page')

  const totalPages = (Math.floor(users[0].total / PAGINATION_NEXT_PAGES_RECORDS)) + 1

  ctx.body = {
    totalAddresses: +users[0].total,
    page: +page || 1,
    totalPages,
    addresses: _.map(users, 'publicAddress'),
  }
}
