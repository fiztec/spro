import db from '../../../../models'
const { market: Market } = db
const { TRADE_CONTRACT_ADDRESS, TRADE_TOKEN_NAME, TRADE_TOKEN_DECIMALS } = require('../../../../constants/market')

export const addToken = async (ctx, next) => {
  await next()

  const { asMakerFeeRate, asTakerFeeRate } = await Market.findOne({
    order: [['id', 'ASC']],
    attributes: ['id', 'asMakerFeeRate', 'asTakerFeeRate'],
  })

  const defaultValue = {
    quoteToken: TRADE_TOKEN_NAME,
    quoteTokenDecimals: TRADE_TOKEN_DECIMALS,
    quoteTokenAddress: TRADE_CONTRACT_ADDRESS,
    asMakerFeeRate,
    asTakerFeeRate,
  }

  const { baseToken } = ctx.request.body

  const newMarket = {
    tokens: `${baseToken}-${TRADE_TOKEN_NAME}`,
    ...defaultValue,
    ...ctx.request.body,
  }

  ctx.body = await Market.create(newMarket)
}
