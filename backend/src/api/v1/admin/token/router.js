import Router from 'koa-router'
import * as tokenController from './controller'
import { authMiddleware } from '../../../../middlewares/auth'
const router = new Router()

router
  .post('/', authMiddleware, tokenController.addToken)

export default router.routes()
