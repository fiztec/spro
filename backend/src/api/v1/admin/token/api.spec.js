const server = require('../../../../index')
const agent = require('supertest').agent(server)
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoxLCJwdWJsaWNBZGRyZXNzIjoiMHg5NjI0MTE4' +
  'NmNhNGFhNzlmNWJkNTkzYTAyYzI5YTM4ZGE3ODUyNTFlIn0sImlhdCI6MTU1NTQyMzE2Mn0.lVzu2XbZ98Cj73wOW7phe5py-LT_uq1uFlaQpjsYqho'
const clientToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoyLCJwdWJsaWNBZGRyZXNzIjoiMHhhODBmZj' +
  'A4ZDA4ODA5YjI5YzczMzEzNWIxYjkzYTA3Nzk5ZTVkMTdmIn0sImlhdCI6MTU1NDI4NzA5NH0.u-tN-7YKjA3MzkTrQlDx5Bkd41lVRE9C8fs3' +
  '_9e6KuM'
const db = require('../../../../models')
const { market: Market } = db

const requestBody = {
  baseToken: 'DFD',
  baseTokenDecimals: 18,
  baseTokenAddress: '0x64bbf67a8251f7482330c33e65b08b835125e018',
  baseTokenProjectUrl: 'https://etherscan.io/',
  baseTokenName: 'Defender token',
  minOrderSize: '1.000000000000000000',
  pricePrecision: 5,
  priceDecimals: 8,
  amountDecimals: 2,
  asMakerFeeRate: '0.00100',
  asTakerFeeRate: '0.00300',
  supportedOrderTypes: [ 'LIMIT' ],
  marketOrderMaxSlippage: '0.20000',
}

describe('Admin Token (API)', function () {
  afterAll(async () => {
    await server.close()
  })

  it('add new token to market', async () => {
    expect.assertions(3)

    const response = await agent.post('/api/v1/admin/token')
      .set('Authorization', 'Bearer ' + token)
      .send(requestBody)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)

    const market = await Market.findOne({
      where: { baseToken: requestBody.baseToken },
    })
    expect(market).not.toEqual(null)

    expect(response.body.id).toEqual(market.id)

    await Market.destroy({ where: { id: market.id } })
  })

  it('Check Auth. Add new token to market', async () => {
    expect.assertions(1)

    const response = await agent.post('/api/v1/admin/token')
      .send(requestBody)
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })

  it('Check non admin Auth. Add new token to market', async () => {
    expect.assertions(1)

    const response = await agent.post('/api/v1/admin/token')
      .set('Authorization', 'Bearer ' + clientToken)
      .send(requestBody)
      .catch(err => console.error(err))

    expect(response.status).toEqual(403)
  })
})
