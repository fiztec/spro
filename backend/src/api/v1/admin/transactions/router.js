import Router from 'koa-router'
import * as transactionController from './controller'
import { authMiddleware } from '../../../../middlewares'
const router = new Router()

router
  .get('/list', authMiddleware, transactionController.getTransactions)

export default router.routes()
