import _ from 'lodash'
import { getFormattedList, getGroupedPaginatedTransactions } from '../../../../helpers/trade'

export const getTransactions = async (ctx, next) => {
  await next()

  const { groupBy = 'ALL' } = ctx.query

  const transactions = await getGroupedPaginatedTransactions(groupBy)

  const transactionList = _.isEmpty(transactions) ? {} : await getFormattedList(transactions)

  ctx.body = {
    transactions: transactionList,
  }
}
