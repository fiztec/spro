const server = require('../../../../index')
const agent = require('supertest').agent(server)
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoxLCJwdWJsaWNBZGRyZXNzIjoiMHg5NjI0MTE4' +
  'NmNhNGFhNzlmNWJkNTkzYTAyYzI5YTM4ZGE3ODUyNTFlIn0sImlhdCI6MTU1NTQyMzE2Mn0.lVzu2XbZ98Cj73wOW7phe5py-LT_uq1uFlaQpjsYqho'
const clientToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoyLCJwdWJsaWNBZGRyZXNzIjoiMHhhODBmZj' +
  'A4ZDA4ODA5YjI5YzczMzEzNWIxYjkzYTA3Nzk5ZTVkMTdmIn0sImlhdCI6MTU1NDI4NzA5NH0.u-tN-7YKjA3MzkTrQlDx5Bkd41lVRE9C8fs3' +
  '_9e6KuM'

const expectedResultAll = [4, 4, 2, 6, 7, 3, 5, 1, 8]
const expectedResultBuy = [3]
const expectedResultSell = [4, 4, 2, 6, 5, 1]
const expectedResultExchange = [7, 8]
const _ = require('lodash')

describe('Admin Transactions (API)', function () {
  afterAll(async () => {
    await server.close()
  })

  it('get ALL transactions right page in query', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/admin/transactions/list?groupBy=ALL&page=1')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    const ids = _.map(response.body.transactions, 'id')

    expect(response.status).toEqual(200)
    expect(ids).toEqual(expect.arrayContaining(expectedResultAll))
  })

  it('get ALL transactions without page in query', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/admin/transactions/list?groupBy=ALL')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    const ids = _.map(response.body.transactions, 'id')

    expect(response.status).toEqual(200)
    expect(ids).toEqual(expect.arrayContaining(expectedResultAll))
  })

  it('get BUY transactions', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/admin/transactions/list?groupBy=BUY')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    const ids = _.map(response.body.transactions, 'id')

    expect(response.status).toEqual(200)
    expect(ids).toEqual(expect.arrayContaining(expectedResultBuy))
  })

  it('get SELL transactions', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/admin/transactions/list?groupBy=SELL')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    const ids = _.map(response.body.transactions, 'id')

    expect(response.status).toEqual(200)
    expect(ids).toEqual(expect.arrayContaining(expectedResultSell))
  })

  it('get EXCHANGE transactions', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/admin/transactions/list?groupBy=EXCHANGE')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    const ids = _.map(response.body.transactions, 'id')

    expect(response.status).toEqual(200)
    expect(ids).toEqual(expect.arrayContaining(expectedResultExchange))
  })

  it('Check Auth. get ALL transactions', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/admin/transactions/list?groupBy=ALL')
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })

  it('Check non admin Auth. get ALL transactions', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/admin/transactions/list?groupBy=ALL')
      .set('Authorization', 'Bearer ' + clientToken)
      .catch(err => console.error(err))

    expect(response.status).toEqual(403)
  })
})
