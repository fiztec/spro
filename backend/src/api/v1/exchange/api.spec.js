const server = require('../../../index')
const agent = require('supertest').agent(server)
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoxLCJwdWJsaWNBZGRyZXNzIjoiMHg5NjI0MTE4' +
  'NmNhNGFhNzlmNWJkNTkzYTAyYzI5YTM4ZGE3ODUyNTFlIn0sImlhdCI6MTU1NTQyMzE2Mn0.lVzu2XbZ98Cj73wOW7phe5py-LT_uq1uFlaQpjsYqho'

const db = require('../../../models')
const {
  exchangeOrder: ExchangeOrder,
  transaction: Transaction,
} = db
const { _ } = require('lodash')

describe('Exchange (API)', function () {
  afterAll(async () => {
    await server.close()
  })

  getTokenByNameTestSuit()
  getTokenByIdTestSuit()
  getOrderByIdTestSuit()
  getExchangeOrdersByTokenIdTestSuit()
  getExchangeOrdersByUserIdTestSuit()
  addExchangeOrderTestSuit()
  addExchangeTransactionTestSuit()
})

function getTokenByNameTestSuit () {
  it('get token by name', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/exchange/token?token=BNB')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body).toMatchSnapshot()
  })

  it('get not exist token by name', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/exchange/token?token=BNC')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body).toMatchSnapshot()
  })

  it('Check Auth. get trade by name', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/exchange/token?token=BNB')
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })
}

function getTokenByIdTestSuit () {
  // TODO (remove for production) commented to test mirOrderSizeChange
  // it('get token by id', async () => {
  //   expect.assertions(2)
  //
  //   const response = await agent.get('/api/v1/exchange/token/1')
  //     .set('Authorization', 'Bearer ' + token)
  //     .catch(err => console.error(err))
  //
  //   expect(response.status).toEqual(200)
  //   expect(response.body).toMatchSnapshot()
  // })

  it('get token by id that does not exist', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/exchange/token/111')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(404)
    expect(response.body.message).toEqual('Token not found')
  })

  it('Check Auth. get trade by id', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/exchange/token/1')
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })
}

function getOrderByIdTestSuit () {
  it('get order by id', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/exchange/order/1')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body).toMatchSnapshot()
  })

  it('get order by id that does not exist', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/exchange/order/111')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(404)
    expect(response.body.message).toEqual('Exchange order not found')
  })

  it('Check Auth. get order by id', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/exchange/order/1')
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })
}

function getExchangeOrdersByTokenIdTestSuit () {
  it('get swaps for given token id', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/exchange/order-token/1')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body).toMatchSnapshot()
  })

  it('get swaps for given token id that does not exist', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/exchange/order-token/111')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(404)
    expect(response.body.message).toEqual('Token not found')
  })

  it('Check Auth. get swaps for given token id', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/exchange/order-token/1')
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })
}

function getExchangeOrdersByUserIdTestSuit () {
  it('get swaps for given user id', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/exchange/user?userId=3')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body).toMatchSnapshot()
  })

  it('get swaps for given user id with no exchange orders', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/exchange/user?userId=1')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body).toMatchSnapshot()
  })

  it('get swaps for given user id that does not exist', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/exchange/user?userId=311')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(404)
    expect(response.body.message).toEqual('User not found')
  })

  it('Check Auth. get swaps for given user id', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/exchange/user?userId=3')
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })
}

function addExchangeOrderTestSuit () {
  let body = {
    giveTokenId: 1,
    giveAmount: '0.04',
    receiveTokenId: 12,
    receiveAmount: '0.08',
  }

  const bodySameToken = {
    giveTokenId: 1,
    giveAmount: '0.04',
    receiveTokenId: 1,
    receiveAmount: '0.08',
  }

  it('add exchange order', async () => {
    expect.assertions(6)

    const response = await agent.post('/api/v1/exchange')
      .set('Authorization', 'Bearer ' + token)
      .send(body)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(_.has(response.body, 'exchangeOrderIdDB')).toEqual(true)
    expect(response.body.exchangeOrderIdDB).toEqual(3)
    expect(_.has(response.body, 'exchangeOrderIdEth')).toEqual(true)
    expect(_.has(response.body, 'exchangeOrderData')).toEqual(true)
    expect(_.has(response.body, 'exchangeOrderSignature')).toEqual(true)

    await ExchangeOrder.destroy({ where: { id: response.body.exchangeOrderIdDB } })
  })

  it('Negative. Add exchange order. Check given token not found', async () => {
    expect.assertions(2)
    const giveTokenId = 111

    const response = await agent.post('/api/v1/exchange')
      .set('Authorization', 'Bearer ' + token)
      .send({
        ...body,
        giveTokenId,
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(404)
    expect(response.body.message).toEqual(`To be given token not found (tokenId: ${giveTokenId})`)
  })

  it('Negative. Add exchange order. Check receive token not found', async () => {
    expect.assertions(2)
    const receiveTokenId = 111

    const response = await agent.post('/api/v1/exchange')
      .set('Authorization', 'Bearer ' + token)
      .send({
        ...body,
        receiveTokenId,
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(404)
    expect(response.body.message).toEqual(`To be received token not found (tokenId: ${receiveTokenId})`)
  })

  // TODO (remove for production) commented to test mirOrderSizeChange
  // it('Negative. Add exchange order. Check given token amount is too small', async () => {
  //   expect.assertions(2)
  //   const giveAmount = '0.000001'
  //
  //   const response = await agent.post('/api/v1/exchange')
  //     .set('Authorization', 'Bearer ' + token)
  //     .send({
  //       ...body,
  //       giveAmount,
  //     })
  //     .catch(err => console.error(err))
  //
  //   expect(response.status).toEqual(400)
  //   expect(response.body.message.search('To be given token amount is too small!')).not.toEqual(-1)
  // })

  // TODO (remove for production) commented to test mirOrderSizeChange
  // it('Negative. Add exchange order. Check receive token amount is too small', async () => {
  //   expect.assertions(2)
  //   const receiveAmount = '0.000001'
  //
  //   const response = await agent.post('/api/v1/exchange')
  //     .set('Authorization', 'Bearer ' + token)
  //     .send({
  //       ...body,
  //       receiveAmount,
  //     })
  //     .catch(err => console.error(err))
  //
  //   expect(response.status).toEqual(400)
  //   expect(response.body.message.search('To be received token amount is too small!')).not.toEqual(-1)
  // })

  it('Negative. Add exchange order. Check given token amount is too large', async () => {
    expect.assertions(2)
    const giveAmount = '100000'

    const response = await agent.post('/api/v1/exchange')
      .set('Authorization', 'Bearer ' + token)
      .send({
        ...body,
        giveAmount,
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message.search('To be given token amount is too large!')).not.toEqual(-1)
  })

  it('Negative. Add exchange order. Check receive token amount is too large', async () => {
    expect.assertions(2)
    const receiveAmount = '100000000000000000000'

    const response = await agent.post('/api/v1/exchange')
      .set('Authorization', 'Bearer ' + token)
      .send({
        ...body,
        receiveAmount,
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message.search('To be received token amount is too large!')).not.toEqual(-1)
  })

  it('Negative. Add exchange order. Check receive token amount is too large', async () => {
    expect.assertions(2)
    const giveAmount = '0.04'
    const receiveAmount = '0.02'

    const response = await agent.post('/api/v1/exchange')
      .set('Authorization', 'Bearer ' + token)
      .send({
        ...body,
        giveAmount,
        receiveAmount,
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message).toEqual('Price changed substantially! Please submit new exchange order.')
  })

  it('Negative. Add exchange order. Try to exchange token on itself', async () => {
    expect.assertions(2)

    const response = await agent.post('/api/v1/exchange')
      .set('Authorization', 'Bearer ' + token)
      .send({
        ...bodySameToken,
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(403)
    expect(response.body.message).toEqual('You cannot exchange token on itself')
  })

  it('Check Auth. Add exchange order', async () => {
    expect.assertions(1)

    const response = await agent.post('/api/v1/exchange')
      .send(body)
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })
}

function addExchangeTransactionTestSuit () {
  const body = {
    exchangeOrderId: 1,
    txHash: '0xb2084ae0dad38594a40aea5f3825ebf81d72834205be7cdb6e3eebf0b9706d2c',
    errMsg: '',
  }

  it('add exchange transaction', async () => {
    expect.assertions(3)

    const response = await agent.post('/api/v1/exchange/tx')
      .set('Authorization', 'Bearer ' + token)
      .send(body)
      .catch(err => console.error(err))

    expect(response.status).toEqual(204)

    const transaction = await Transaction.findOne({ order: [['id', 'DESC']] })

    expect(transaction.exchangeOrderId).toBe(body.exchangeOrderId)
    expect(transaction.txHash).toBe(body.txHash)

    await Transaction.destroy({ where: { id: transaction.id } })
  })

  it('Negative test. Add exchange transaction with existed hash', async () => {
    expect.assertions(2)

    const response = await agent.post('/api/v1/exchange/tx')
      .set('Authorization', 'Bearer ' + token)
      .send({
        ...body,
        txHash: '0x16ffd26e93257fe965fde7460ee216db120ea00fec536101c33f88fe353249b2',
      })
      .catch(err => console.error(err))

    expect(response.status).toEqual(500)
    expect(response.body.message).toEqual('Failed to handle exchange transaction hash. ' +
      'Error: transaction hash must be unique.')
  })

  it('Check Auth. get trade by name', async () => {
    expect.assertions(1)

    const response = await agent.post('/api/v1/exchange/tx')
      .send(body)
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })
}
