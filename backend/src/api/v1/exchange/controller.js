
import BigNumber from 'bignumber.js'
import db from '../../../models'
import _ from 'lodash'
import crypto from 'crypto'
import { web3 } from '../../../lib/web3'
import createError from 'http-errors'

import {
  getMaxTokenAmount,
  getExchangeOrderHash,
  signExchangeOrderHash,
} from './sdk'

import { handleTxHash } from '../../../helpers/exchangeTransaction'

import {
  EXCHANGE_STATUS,
  EXCHANGE_FEES,
  MAX_EXCHANGE_SLIPPAGE,
  EXCHANGE_CONTRACT_ADDRESS,
  MAKER_ADDRESS,
  SWAP_ADDRESS,
  MAKER_PK, // SECURITY
} from '../../../constants/exchange'

const {
  exchangeToken: ExchangeToken,
  exchangeOrder: ExchangeOrder,
  transaction: Transaction,
  user: User,
  sequelize: { Op },
} = db

const TOKEN_ATTRIBUTES = [
  'id',
  'tokenSymbol',
  'tokenName',
  'tokenAddress',
  'tokenDecimals',
  'minOrderSize',
  'currentPriceToWETH',
  'givePriceToWETH',
  'receivePriceToWETH',
]

const ORDER_ATTRIBUTES = [
  'id',
  'exchangeOrderIdEth',
  'giveTokenId',
  'receiveTokenId',
  'giveAmount',
  'receiveAmount',
  'status',
]

const USER_ATTRIBUTES = [
  'id',
  'publicAddress',
]

export const findExchangeTokenByName = async (ctx, next) => {
  await next()
  const { token } = ctx.query
  const options = token ? {
    where: {
      tokenSymbol: { [Op.iLike]: `%${token}%` },
    },
  } : {}
  options.attributes = TOKEN_ATTRIBUTES
  ctx.body = await ExchangeToken.findAll(options)
}

export const getExchangeTokenById = async (ctx, next) => {
  await next()
  const { params: { tokenId } } = ctx
  const tokenObject = await ExchangeToken.findByPk(tokenId, { attributes: TOKEN_ATTRIBUTES })
  ctx.assert(!_.isNil(tokenObject), 404, 'Token not found')
  ctx.body = tokenObject
}

export const createExchangeOrder = async (ctx, next) => {
  await next()
  const { giveTokenId, giveAmount, receiveTokenId, receiveAmount } = ctx.request.body
  const { publicAddress, id: userId } = ctx.state.user

  ctx.assert(giveTokenId !== receiveTokenId, 403, `You cannot exchange token on itself`)

  try {
    const giveTokenObject = await ExchangeToken.findByPk(giveTokenId, {
      attributes: TOKEN_ATTRIBUTES,
    })
    ctx.assert(!_.isNil(giveTokenObject), 404, `To be given token not found (tokenId: ${giveTokenId})`)

    const receiveTokenObject = await ExchangeToken.findByPk(receiveTokenId, {
      attributes: TOKEN_ATTRIBUTES,
    })
    ctx.assert(!_.isNil(receiveTokenObject), 404, `To be received token not found (tokenId: ${receiveTokenId})`)

    ctx.assert(new BigNumber(giveAmount).gte(new BigNumber(giveTokenObject.minOrderSize)),
      400, `To be given token amount is too small! You should give at least ${giveTokenObject.minOrderSize}`)
    ctx.assert(new BigNumber(receiveAmount).gte(new BigNumber(receiveTokenObject.minOrderSize)),
      400, `To be received token amount is too small! You can receive at least ${receiveTokenObject.minOrderSize}`)

    const giveAmountWei = new BigNumber(giveAmount)
      .times(new BigNumber(Math.pow(10, giveTokenObject.tokenDecimals)))
    const receiveAmountWei = new BigNumber(receiveAmount)
      .times(new BigNumber(Math.pow(10, receiveTokenObject.tokenDecimals)))

    const giveTokenMaxAmount = await getMaxTokenAmount(giveTokenObject.tokenAddress, publicAddress)
    const receiveTokenMaxAmount = await getMaxTokenAmount(receiveTokenObject.tokenAddress, MAKER_ADDRESS)

    ctx.assert(new BigNumber(giveAmountWei).lte(new BigNumber(giveTokenMaxAmount)),
      400, `To be given token amount is too large! You should give at most: ${
        new BigNumber(giveTokenMaxAmount).div(new BigNumber(Math.pow(10, giveTokenObject.tokenDecimals)))
      }`)
    ctx.assert(!new BigNumber(receiveAmountWei).isZero(),
      400, `Sorry, to be received token amount is out of stock!`)
    ctx.assert(new BigNumber(receiveAmountWei).lte(new BigNumber(receiveTokenMaxAmount)),
      400, `To be received token amount is too large! You can receive at most: ${
        new BigNumber(receiveTokenMaxAmount).div(new BigNumber(Math.pow(10, receiveTokenObject.tokenDecimals)))
      }`)

    const latestExchangePrice = new BigNumber(giveTokenObject.currentPriceToWETH)
      .div(new BigNumber(receiveTokenObject.currentPriceToWETH))
    const orderPrice = new BigNumber(receiveAmount).div(new BigNumber(giveAmount))
    const slippage = (latestExchangePrice.minus(orderPrice)).abs().div(orderPrice)
    ctx.assert(slippage.lte(new BigNumber(MAX_EXCHANGE_SLIPPAGE)),
      400, 'Price changed substantially! Please submit new exchange order.')

    console.log('>>> EXCHANGE_CONTRACT_ADDRESS= %s', EXCHANGE_CONTRACT_ADDRESS)

    const exchangeOrder = {
      exchange: EXCHANGE_CONTRACT_ADDRESS,
      maker: MAKER_ADDRESS,
      taker: SWAP_ADDRESS,
      makerToken: receiveTokenObject.tokenAddress,
      takerToken: giveTokenObject.tokenAddress,
      feeRecipient: EXCHANGE_FEES.FEE_RECIPIENT,
      makerTokenAmount: receiveAmountWei,
      takerTokenAmount: giveAmountWei,
      makerFee: EXCHANGE_FEES.MAKER_FEE,
      takerFee: EXCHANGE_FEES.TAKER_FEE,
      expirationTimestampInSec: Math.floor(Date.now() / 1000) + 60 * 3, // set to be 3 minutes ahead (±12 blocks):
      salt: parseInt(crypto.randomBytes(6).toString('hex'), 16),
    }

    const exchangeOrderIdEth = '0x' + crypto.randomBytes(32).toString('hex')
    const exchangeOrderHash = getExchangeOrderHash(exchangeOrder)
    const exchangeOrderSignature = signExchangeOrderHash(exchangeOrderHash, MAKER_PK)

    const newExchangeOrderDB = {
      exchangeOrderIdEth,
      giveTokenId: giveTokenObject.id,
      receiveTokenId: receiveTokenObject.id,
      giveAmount: giveAmountWei,
      receiveAmount: receiveAmountWei,
      makerAddress: MAKER_ADDRESS,
      feeRecipient: EXCHANGE_FEES.FEE_RECIPIENT,
      makerFee: EXCHANGE_FEES.MAKER_FEE,
      takerFee: EXCHANGE_FEES.TAKER_FEE,
      makerSignatureV: exchangeOrderSignature.v,
      makerSignatureR: exchangeOrderSignature.r,
      makerSignatureS: exchangeOrderSignature.s,
      expirationTimestampInSec: exchangeOrder.expirationTimestampInSec,
      salt: exchangeOrder.salt,
      status: EXCHANGE_STATUS.PENDING,
      userId,
      createdAt: new Date(),
      updatedAt: new Date(),
    }

    const exchangeOrderIdDB = _.pick(await ExchangeOrder.create(newExchangeOrderDB), ['id'])

    const exchangeOrderFE = {
      exchangeOrderIdDB: exchangeOrderIdDB.id,
      exchangeOrderIdEth,
      exchangeOrderData: exchangeOrder,
      exchangeOrderSignature,
    }

    ctx.body = exchangeOrderFE
  } catch (e) {
    ctx.throw(e.status || 500, `Can't create exchange order`, e.message)
  }
}

export const handleExchangeTxHash = async (ctx, next) => {
  await next()
  const { exchangeOrderId, txHash, errMsg } = ctx.request.body

  if (errMsg) {
    console.error(`Exchange transaction fails: ${errMsg}`)
    throw createError(404, 'Exchange error to be investigated!')
  } else {
    ctx.assert(web3.utils.isHex(txHash) && txHash.length === 66, 500, 'txHash has wrong format!')
    const exchangeOrderObject = await ExchangeOrder.findByPk(exchangeOrderId, { attributes: ORDER_ATTRIBUTES })
    ctx.assert(!_.isNil(exchangeOrderObject), 404, 'Exchange order not found!')
    try {
      const exchangeTransaction = await Transaction.create({ exchangeOrderId, txHash })

      handleTxHash(exchangeOrderId, txHash, exchangeTransaction.id)
      ctx.status = 204
    } catch (error) {
      throw createError(500, `Failed to handle exchange transaction hash. Error: ${error.message}`)
    }
  }
}

export const getExchangeOrderById = async (ctx, next) => {
  await next()
  const { params: { exchangeOrderId } } = ctx
  const exchangeOrderObject = await ExchangeOrder.findByPk(exchangeOrderId, { attributes: ORDER_ATTRIBUTES })
  ctx.assert(!_.isNil(exchangeOrderObject), 404, 'Exchange order not found')
  ctx.body = exchangeOrderObject
}

export const findExchangeOrderByTokenId = async (ctx, next) => {
  await next()
  const { params: { tokenId } } = ctx
  const tokenObject = await ExchangeToken.findByPk(tokenId, { attributes: TOKEN_ATTRIBUTES })
  ctx.assert(!_.isNil(tokenObject), 404, 'Token not found')
  const exchangeOrders = await ExchangeOrder.findAll({
    where: {
      [Op.or]: [{ giveTokenId: tokenId }, { receiveTokenId: tokenId }],
    },
    attributes: ORDER_ATTRIBUTES,
  })
  ctx.body = exchangeOrders
}

export const findExchangeOrderByUserId = async (ctx, next) => {
  await next()
  const { userId } = ctx.query
  const userObject = await User.findByPk(userId, { attributes: USER_ATTRIBUTES })
  ctx.assert(!_.isNil(userObject), 404, 'User not found')
  const exchangeOrders = await ExchangeOrder.findAll({ where: { userId }, attributes: ORDER_ATTRIBUTES })
  ctx.body = exchangeOrders
}
