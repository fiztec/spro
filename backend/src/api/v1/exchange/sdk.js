
import { ecsign } from 'ethereumjs-util'
import { web3 } from '../../../lib/web3'
import { BALANCE_OF_DECIMALS_ABI } from '../../../constants/exchange'

const getMaxTokenAmount = async (token, user) => {
  const TokenContract = new web3.eth.Contract(BALANCE_OF_DECIMALS_ABI, token)
  const tokenBalance = await web3.eth.call({
    to: token,
    data: TokenContract.methods.balanceOf(user).encodeABI(),
  })

  if (tokenBalance === '0x') return 0

  const tokenBalanceWei = await web3.eth.abi.decodeParameters(['uint256'], tokenBalance)

  return tokenBalanceWei['0']
}

const getExchangeOrderHash = (order) => {
  return web3.utils.soliditySha3(
    order.exchange,
    order.maker,
    order.taker,
    order.makerToken,
    order.takerToken,
    order.feeRecipient,
    order.makerTokenAmount,
    order.takerTokenAmount,
    order.makerFee,
    order.takerFee,
    order.expirationTimestampInSec,
    order.salt
  )
}

const signExchangeOrderHash = (exchangeOrderHash, pk) => {
  const gethSigPrefix = '\x19Ethereum Signed Message:\n32'
  const hashToSign = web3.utils.soliditySha3(gethSigPrefix, exchangeOrderHash)
  let { v, r, s } = ecsign(Buffer.from(hashToSign.slice(2), 'hex'), pk)
  r = '0x' + r.toString('hex')
  s = '0x' + s.toString('hex')
  return { v, r, s }
}

module.exports = {
  getMaxTokenAmount,
  getExchangeOrderHash,
  signExchangeOrderHash,
}
