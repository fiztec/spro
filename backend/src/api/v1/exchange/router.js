
import Router from 'koa-router'
import * as exchangeController from './controller'
import { authMiddleware, isAdminMiddleware } from '../../../middlewares/'
const router = new Router()

router
  .get('/token/', authMiddleware, exchangeController.findExchangeTokenByName)
  .get('/token/:tokenId', authMiddleware, exchangeController.getExchangeTokenById)
  .post('/', authMiddleware, exchangeController.createExchangeOrder)
  .post('/tx/', authMiddleware, exchangeController.handleExchangeTxHash)
  .get('/order/:exchangeOrderId', authMiddleware, exchangeController.getExchangeOrderById)
  .get('/order-token/:tokenId', authMiddleware, exchangeController.findExchangeOrderByTokenId)
  .get('/user/', authMiddleware, isAdminMiddleware, exchangeController.findExchangeOrderByUserId)

export default router.routes()
