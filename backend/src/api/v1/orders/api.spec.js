const server = require('../../../index')
const agent = require('supertest').agent(server)
const tokenUser1 = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoxLCJwdWJsaWNBZGRyZXNzIj' +
  'oiMHg5NjI0MTE4NmNhNGFhNzlmNWJkNTkzYTAyYzI5YTM4ZGE3ODUyNTFlIn0sImlhdCI6MTU1NTQyMzE2Mn0.lVzu2XbZ98Cj' +
  '73wOW7phe5py-LT_uq1uFlaQpjsYqho'
const tokenUser2 = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoyLCJwdWJsaWNBZGRyZXNzIjoiMHhhODBmZjA4' +
  'ZDA4ODA5YjI5YzczMzEzNWIxYjkzYTA3Nzk5ZTVkMTdmIn0sImlhdCI6MTU1NDI4NzA5NH0.u-tN-7YKjA3MzkTrQlDx5Bkd41lVRE9C8fs3_9e6KuM'
// const tokenUser3 = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjozLCJwdWJsaWNBZGRyZXNzIjoiMHg3' +
//   'NjgzZGRiNzQ1MWQ2ODRkMzIwYmFmN2FhYmNkMDI5OGE4Yzk0MWRhIn0sImlhdCI6MTU2NDk5OTY4N' +
//   'n0.ymYvbFHGxX82OvLt2hEKGe4bmpNlzGT0ZA1ysNQ3chE'
const tokenUser4 = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjo1LCJwdWJsaWNBZGRyZXNzIjoiMHhmYjM4MjQ0M' +
  '2RhZjA1MTU4ZjEwZmJmZjBiOTI1ZGVjMDZiM2NhMWY1In0sImlhdCI6MTU2NzUwNTU4NH0.DKJqIgp8cFjkxtutDABR76-MJFKimKhh03CHEnmrQHY'

const db = require('../../../models')
const { order: Order } = db
const { ORDER_STATUS } = require('../../../constants/order')
const { map, invokeMap } = require('lodash')
const { Op } = require('sequelize')

const body = {
  baseTokenAmount: 10000000000000000,
  data: '0x020000005f5091f30064012c00000000c5f9673e2e7200000000000000000000',
  gasTokenAmount: 1900000000000000,
  quoteTokenAmount: 10000000000000000,
  signature: {
    config: '0x1b00000000000000000000000000000000000000000000000000000000000000',
    r: '0x15006c70f590626c0e826c0b164f88c7d5db025bd26c448a6df4e4fb009016a3',
    s: '0x4e39b407dc5adbcf872cbb7908387496337a8e6016cbcf19fa12700e436cd9f9',
  },
  tokens: 'EMPR-WETH',
  trader: '0xa80ff08d08809b29c733135b1b93a07799e5d17f',
}

const buyOrderBody = {
  baseTokenAmount: 1212122000000000000000000,
  data: '0x020000005f466f920064012c000000003f20d26a8cbd00000000000000000000',
  gasTokenAmount: 1900000000000000,
  quoteTokenAmount: 1212122000000000000000000,
  signature: {
    config: '0x1c00000000000000000000000000000000000000000000000000000000000000',
    r: '0x5768cd70383016dc5f75f03e4ecaf3de61f0c5a51738d4cbffed7584e2a0dd5e',
    s: '0x1b8f1b03670c1929df96df81b1451f26326d1a0ae78bdc08ceacc7c7015d595e',
  },
  tokens: 'EMPR-WETH',
  trader: '0x96241186ca4aa79f5bd593a02c29a38da785251e',
}

// const sellOrderBody = {
//   baseTokenAmount: 10000000000000000000000,
//   data: '0x020100005f324acb0064012c000000000f6acf977c4400000000000000000000',
//   gasTokenAmount: 1900000000000000,
//   quoteTokenAmount: 110000000000000000000000,
//   signature: {
//     config: '0x1b00000000000000000000000000000000000000000000000000000000000000',
//     r: '0xc74bb4bfeccca5e44434bfe1a67ba8cebc003f7e1ef44a7e60452959fb5472e9',
//     s: '0x09053529c8853bf2f2135f9c1889cdac6da923e2b3f8145b1e7c0c1d224ef1d7',
//   },
//   tokens: 'EMPR-WETH',
//   trader: '0x96241186ca4aa79f5bd593a02c29a38da785251e',
// }

const buyOrderBodyPriceZero = {
  baseTokenAmount: 1000000000000000,
  data: '0x020000005ef25f1b0064012c0000000018960c43406600000000000000000000',
  gasTokenAmount: 1900000000000,
  quoteTokenAmount: 0,
  signature: {
    config: '0x1c00000000000000000000000000000000000000000000000000000000000000',
    r: '0x45b3ca145f6421d42cc40b03a7c42a6ffe274abff20f295b499638ac91875fed',
    s: '0x50aa4acd5db2fcc327d160f7236d1e0c0ed20189dde6b9b1e02a228ba16a7cd8',
  },
  tokens: 'EMPR-WETH',
  trader: '0x96241186ca4aa79f5bd593a02c29a38da785251e',
}

const buyOrderBodyAmountZero = {
  baseTokenAmount: 0,
  data: '0x020000005ef265950064012c00000000c0a8d07813ef00000000000000000000',
  gasTokenAmount: 0,
  quoteTokenAmount: 0,
  signature: {
    config: '0x1c00000000000000000000000000000000000000000000000000000000000000',
    r: '0x318991e1304281b529c71f027b0663cd1d60ef3e6487cb90c44c0ebbc45881e2',
    s: '0x0dc4996fd94f48093a0e22316b9180cedf71abd66b92f32c3c433ec89c28092d',
  },
  tokens: 'EMPR-WETH',
  trader: '0x96241186ca4aa79f5bd593a02c29a38da785251e',
}

const buyOrderWithAmountLessGasFee = {
  tokens: 'EMPR-WETH',
  trader: '0x96241186ca4aa79f5bd593a02c29a38da785251e',
  baseTokenAmount: 1000000000000000,
  quoteTokenAmount: 277458586953,
  gasTokenAmount: 1900000000000000,
  data: '0x020001005eede4b90064012c00000000b9fa1e3db5be00000000000000000000',
  signature: {
    config:
      '0x1c00000000000000000000000000000000000000000000000000000000000000',
    r:
      '0xebce94f224a34ebc024d7aa75b9343730fe291ebc779c23363637e2a7b81f04f',
    s:
      '0x1c77c540739f2cd14637da4954a9ee190ad237899ba992504adc0f9b205f153d' },
}

const rollbackOrderCancel = async () => {
  const canceledOrders = invokeMap(await Order.findAll({
    where: { status: ORDER_STATUS.CANCELED },
    attributes: ['id'],
    order: [['id', 'ASC']],
  }), 'get', { plain: true })

  const canceledOrderIds = map(canceledOrders, 'id')

  await Order.update({ status: ORDER_STATUS.PENDING }, { where: { id: { [Op.in]: canceledOrderIds } } })

  return canceledOrderIds
}

describe('Orders (API)', function () {
  afterAll(async () => {
    await server.close()
  })

  it('find match for sell order to process in blockchain', async () => {
    expect.assertions(2)
    const response = await agent.put('/api/v1/orders/match/1')
      .set('Authorization', 'Bearer ' + tokenUser2)
      .catch(err => console.error('find order match failed: ', err))
    expect(response.status).toEqual(204)

    const orders = invokeMap(await Order.findAll({
      where: { status: ORDER_STATUS.MATCHING },
      attributes: ['id'],
      order: [['id', 'ASC']],
    }), 'get', { plain: true })

    const ids = map(orders, 'id')
    expect(ids).toEqual(expect.arrayContaining([1, 39]))

    // return impacted orders in previous state
    await rollbackOrderCancel()
  }, 10000)

  it('create order', async () => {
    expect.assertions(1)
    const response = await agent.post('/api/v1/orders')
      .set('Authorization', 'Bearer ' + tokenUser2)
      .send(body)
      .catch(err => console.error('create order failed:', err))

    expect(response.status).toEqual(200)

    await Order.destroy({ where: { id: response.body.id } })
  })

  it('Negative. Create buy order with more WETH amount then user have on the wallet', async () => {
    expect.assertions(2)
    const response = await agent.post('/api/v1/orders')
      .set('Authorization', 'Bearer ' + tokenUser1)
      .send(buyOrderBody)
      .catch(err => console.error(err))

    expect(response.status).toEqual(500)
    expect(response.body.message.search('Your actual wallet balance is around')).not.toEqual(-1)
  })

  // TODO sinon to mock data from api required
  // it('Negative. Create sell order with too high amount', async () => {
  //   expect.assertions(2)
  //   const response = await agent.post('/api/v1/orders')
  //     .set('Authorization', 'Bearer ' + tokenUser2)
  //     .send(sellOrderBody)
  //     .catch(err => console.error(err))
  //
  //   expect(response.status).toEqual(403)
  //   expect(response.body).toHaveProperty('message')
  // })

  it('Negative. Create buy limit order with price = 0', async () => {
    expect.assertions(2)
    const response = await agent.post('/api/v1/orders')
      .set('Authorization', 'Bearer ' + tokenUser1)
      .send(buyOrderBodyPriceZero)
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message).toEqual('Price can not be 0')
  })

  it('Negative. Create buy limit order with price = 0', async () => {
    expect.assertions(2)
    const errMsg = 'Order amount cannot be 0 and it should be more than'

    const response = await agent.post('/api/v1/orders')
      .set('Authorization', 'Bearer ' + tokenUser1)
      .send(buyOrderBodyAmountZero)
      .catch(err => console.error(err))

    expect(response.status).toEqual(400)
    expect(response.body.message.search(errMsg)).not.toEqual(-1)
  })

  it('Negative. Create order with amount less then makerGas + gasTokenAmount ', async () => {
    expect.assertions(2)
    const response = await agent.post('/api/v1/orders')
      .set('Authorization', 'Bearer ' + tokenUser1)
      .send(buyOrderWithAmountLessGasFee)
      .catch(err => console.error(err))

    expect(response.status).toEqual(403)
    expect(response.body.message).toEqual('Order amount is too small for current maker fee and gas token transfer')
  })

  it('find two maker orders but exclude one as quoteTokenFilledAmount < makerFee + gasTokenAmount', async () => {
    expect.assertions(2)
    const response = await agent.put('/api/v1/orders/match/49')
      .set('Authorization', 'Bearer ' + tokenUser1)
      .catch(err => console.error('find order match failed: ', err))
    expect(response.status).toEqual(204)

    const orders = invokeMap(await Order.findAll({
      where: { status: ORDER_STATUS.MATCHING },
      attributes: ['id'],
      order: [['id', 'ASC']],
    }), 'get', { plain: true })

    const ids = map(orders, 'id')

    expect(ids).toEqual(expect.arrayContaining([49, 51]))
  })

  it('get user orders', async () => {
    expect.assertions(1)
    const response = await agent.get('/api/v1/orders/user/1')
      .set('Authorization', 'Bearer ' + tokenUser1)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
  })

  it('Negative. Try to  match order with no enough token balance on one user order', async () => {
    expect.assertions(2)

    const errMsg = 'Find matched orders failed: Check taker order failed. Your actual wallet balance is around'
    const response = await agent.put('/api/v1/orders/match/59')
      .set('Authorization', 'Bearer ' + tokenUser4)
      .catch(err => console.error('find order match failed: ', err))

    expect(response.status).toEqual(400)
    expect(response.body.message.search(errMsg)).not.toEqual(-1)

    await Order.update({ status: ORDER_STATUS.PENDING }, { where: { id: 60 } })
  })

  it('Negative. Try to  match order without allowance', async () => {
    expect.assertions(2)
    const errMsg = 'Find matched orders failed: Check taker order failed. Not enough allowance.'
    const response = await agent.put('/api/v1/orders/match/60')
      .set('Authorization', 'Bearer ' + tokenUser4)
      .catch(err => console.error('find order match failed: ', err))

    expect(response.status).toEqual(400)
    expect(response.body.message.search(errMsg)).not.toEqual(-1)
  }, 10000)
})
