import _ from 'lodash'
import { getMinMaxGasTokenAmount, getOrderData, isValidSignature } from './sdk'
import db from '../../../models'
import BigNumber from 'bignumber.js'
import { getTradeOrderHash } from '../../../helpers/signature'
import { web3 } from '../../../lib/web3'

import { ORDER_STATUS } from '../../../constants/order'
import createError from 'http-errors'
import {
  checkOrderBalanceAllowanceBySide,
  filterByLeftRightSidesCondition,
  filterMakerOrdersNoEnoughBalanceAllowance,
  filterOrdersPassedGasAndFeeCheck,
  getAmount,
  getPrice,
  getValidatedOrder,
  isAmountPrecisionValid,
  isMarketOrderAllow,
  isOrderAmountValid,
  isOrderFeeRateValid,
  isOrderPriceValid,
  validateLimitOrder,
  validateMarketOrder,
  validateTakerMatchedBalance,
} from '../../../helpers/order'

import {
  emitNewOrderToHistory,
} from '../../../helpers/socket'
import { logger } from '../../../helpers/logger'
import {
  getMatchedOrders,
  getLimitOrders,
  processMatchingOrders,
  setOrdersAsMatching,
} from '../../../helpers/order/matching'

const {
  order: Order,
  market: Market,
  sequelize: { Op },
} = db
export const { sequelize } = db

export const getById = async (ctx, next) => {
  await next()
  const {
    params: { orderId },
  } = ctx
  const orderObject = await Order.findByPk(orderId)
  ctx.assert(!_.isNil(orderObject), 500, 'Order not found')
  ctx.status = 200
  ctx.body = orderObject
}

export const findUserOrdersByMarket = async (ctx, next) => {
  await next()
  const {
    marketId,
  } = ctx.params

  const marketWhere = !marketId || marketId === '0' ? {} : { id: marketId }

  let orders = _.invokeMap(
    await Order.findAll({
      where: { userId: ctx.state.user.id },
      order: [['createdAt', 'DESC']],
      attributes: [
        'id',
        'side',
        'baseTokenAmount',
        'quoteTokenAmount',
        'initBaseTokenAmount',
        'initQuoteTokenAmount',
        'createdAt',
        'status',
      ],
      include: {
        model: Market,
        attributes: ['tokens', 'baseTokenDecimals', 'quoteTokenDecimals'],
        where: marketWhere,
      },
    }), 'get', { plain: true })

  for (const order of orders) {
    const { baseTokenAmount, initBaseTokenAmount, initQuoteTokenAmount } = order
    const { baseTokenDecimals, quoteTokenDecimals } = order.market

    const amount = getAmount(baseTokenDecimals, initBaseTokenAmount)
    const availableAmount = getAmount(baseTokenDecimals, baseTokenAmount)
    const confirmedAmount = new BigNumber(amount).minus(availableAmount)
    const price = getPrice(quoteTokenDecimals, baseTokenDecimals, initQuoteTokenAmount, initBaseTokenAmount)

    order.amount = +amount
    order.price = +price
    order.availabelAmount = +amount
    order.confirmedAmount = +confirmedAmount

    delete order.baseTokenAmount
    delete order.initBaseTokenAmount
    delete order.quoteTokenAmount
    delete order.initQuoteTokenAmount
    delete order.signature
    delete order.market.baseTokenDecimals
    delete order.market.quoteTokenDecimals
  }

  ctx.body = orders
}

export const create = async (ctx, next) => {
  await next()

  const { tokens, trader, baseTokenAmount, quoteTokenAmount, gasTokenAmount, data, signature } = ctx.request.body
  const { publicAddress, id: userId } = ctx.state.user

  const orderData = getOrderData(data)

  const {
    side,
    isMarketOrder,
    expiresAt,
    asMakerFeeRate,
    asTakerFeeRate,
  } = orderData.newOrderData

  const market = await Market.findOne({
    where: { tokens: { [Op.iLike]: tokens } },
    attributes: [
      'id',
      'baseTokenAddress',
      'quoteTokenAddress',
      'quoteTokenDecimals',
      'asMakerFeeRate',
      'asTakerFeeRate',
      'baseTokenDecimals',
      'minOrderSize',
      'amountDecimals',
      'supportedOrderTypes',
      'quoteToken',
      'baseToken',
    ],
  })
  ctx.assert(!_.isEmpty(market), 400, 'Market for given tokens not found')

  const amount = getAmount(market.baseTokenDecimals, baseTokenAmount)
  ctx.assert(isOrderAmountValid(amount, market.minOrderSize),
    400, 'Order amount cannot be 0 and it should be more than min order size ' +
    new BigNumber(market.minOrderSize).toPrecision())
  const price = getPrice(market.quoteTokenDecimals, market.baseTokenDecimals, quoteTokenAmount, baseTokenAmount)
  ctx.assert(isOrderPriceValid(price, side, isMarketOrder), 400, `Price can not be 0`)

  try {
    ctx.assert(web3.utils.isHex(data), 500, 'Data is not hex format')
    ctx.assert(data.length === 66, 500, 'Data has wrong length')
    ctx.assert(publicAddress === trader, 500, 'Wrong public address')

    ctx.assert(isOrderFeeRateValid(asMakerFeeRate, market.asMakerFeeRate), 400, 'Maker fee rate is invalid')
    ctx.assert(isOrderFeeRateValid(asTakerFeeRate, market.asTakerFeeRate), 400, 'Taker fee rate is invalid')

    ctx.assert(isMarketOrderAllow(isMarketOrder, market.supportedOrderTypes),
      400, 'Market order does not allow for current market')

    ctx.assert(isAmountPrecisionValid(amount, market.amountDecimals),
      400, `Precision not valid. Amount should have up to ${market.amountDecimals} decimals`)

    isMarketOrder
      ? validateMarketOrder(quoteTokenAmount, side, gasTokenAmount, market.asTakerFeeRate)
      : await validateLimitOrder(expiresAt, quoteTokenAmount, gasTokenAmount, market.asTakerFeeRate)

    const { minGasTokenAmount, maxGasTokenAmount } = await getMinMaxGasTokenAmount()

    // minGasTokenAmount expect about 380000000000000
    ctx.assert(
      !(new BigNumber(gasTokenAmount).isLessThan(new BigNumber(minGasTokenAmount))),
      404,
      'Gas token Amount is too low')
    // maxGasTokenAmount expect be about 2850000000000000
    ctx.assert(
      !(new BigNumber(gasTokenAmount).isGreaterThan(new BigNumber(maxGasTokenAmount))),
      404,
      'Gas Token Amount is too high')

    const traderOrderHash = await getTradeOrderHash(
      orderData.tradeOrder.version,
      trader,
      market.baseTokenAddress,
      market.quoteTokenAddress,
      baseTokenAmount,
      quoteTokenAmount,
      gasTokenAmount,
      orderData.tradeOrder.isSell,
      orderData.tradeOrder.isMarket,
      orderData.tradeOrder.expiredAtSeconds,
      orderData.tradeOrder.asMakerFeeRate,
      orderData.tradeOrder.asTakerFeeRate,
      orderData.tradeOrder.makerRebateRate,
      orderData.tradeOrder.salt
    )

    const isSignatureValid = isValidSignature(publicAddress, signature, traderOrderHash)
    ctx.assert(isSignatureValid, 400, 'Order signature is not valid')

    const newOrder = {
      side,
      isMarketOrder,
      baseTokenAmount,
      quoteTokenAmount,
      initBaseTokenAmount: baseTokenAmount,
      initQuoteTokenAmount: quoteTokenAmount,
      gasTokenAmount,
      data,
      signatureConfig: signature.config,
      signatureR: signature.r,
      signatureS: signature.s,
      expiresAt: expiresAt,
      status: ORDER_STATUS.PENDING,
      userId,
      marketId: market.id,
      createdAt: new Date(),
      updatedAt: new Date(),
    }

    const userOrders = _.invokeMap(await Order.findAll({
      attributes: [
        'id',
        'side',
        'baseTokenAmount',
        'quoteTokenAmount',
        'userId',
      ],
      where: {
        side: newOrder.side,
        marketId: newOrder.marketId,
        userId,
        status: { [Op.in]: [ORDER_STATUS.PENDING, ORDER_STATUS.PARTIALLY_FILLED] },
      },
    }), 'get', { plain: true })

    await checkOrderBalanceAllowanceBySide(publicAddress, [newOrder, ...userOrders], market)

    const newOrderId = _.get(await Order.create(newOrder), ['id'])

    await emitNewOrderToHistory(newOrderId)

    ctx.body = { id: newOrderId }
  } catch (e) {
    logger.error(`Can't create order: ${e.message}.
    amount= ${amount} | price= ${price} 
    body= ${JSON.stringify(ctx.request.body)}`, ctx.request.ip, ctx.state.user.id)
    ctx.throw(e.status || 500, `Can't create order`, e.message)
  }
}

export const cancel = async (ctx, next) => {
  await next()
  const {
    params: { orderId },
  } = ctx
  const { id: userId } = ctx.state.user
  try {
    const orderObject = await Order.findOne({
      where: { id: orderId, userId },
    })
    if (_.isNil(orderObject)) throw createError(400, `No order found with ${orderId} that belongs to current user.`)
    ctx.assert(orderObject.status !== 'CANCELED', 'Order is already canceled.')
    await orderObject.update({ status: ORDER_STATUS.CANCELED })
    ctx.status = 204
  } catch (e) {
    logger.error(`Can't cancel order: ${e.message}. orderId= ${orderId}`, ctx.request.ip, ctx.state.user.id)
    ctx.throw(500, `Can't cancel order ${orderId}`, e.message)
  }
}

export const matchOrder = async (ctx, next) => {
  await next()
  const { orderId } = ctx.params
  const { id: userId } = ctx.state.user

  try {
    const order = await getValidatedOrder(orderId, userId)

    let limitOrders = await getLimitOrders(order, userId)
    logger.info(`For taker order= ${orderId} found maker orders(limitOrders)= ${limitOrders.map(l => l.id)} `,
      ctx.request.ip,
      ctx.state.user.id)

    limitOrders = filterByLeftRightSidesCondition(order, limitOrders)
    limitOrders = filterOrdersPassedGasAndFeeCheck(limitOrders)

    limitOrders = await filterMakerOrdersNoEnoughBalanceAllowance(limitOrders)
    logger.info(`
    Maker orders (limitOrders) after wallet balance check, ids= ${limitOrders.map(l => l.id)} `,
    ctx.request.ip,
    ctx.state.user.id)
    ctx.assert(!_.isEmpty(limitOrders), 204, 'No matching orders at the moment for order id= ' + orderId)

    logger.info(`
    Maker orders (limitOrders) after validations, ids= ${limitOrders.map(l => l.id)} `,
    ctx.request.ip,
    ctx.state.user.id)
    ctx.assert(!_.isEmpty(limitOrders), 204, 'No matching orders at the moment for order id= ' + orderId)

    const {
      matchedOrderToProcess,
      isLastTakerOrderBaseTokenAmountTotallyMatched,
      notMatchTakerOrMakerBaseTokenAmount,
      baseTokenFilledAmounts,
      isTakerBaseTokenAmountFilled,
    } = getMatchedOrders(order, limitOrders, ctx)

    if (_.isEmpty(matchedOrderToProcess)) {
      throw createError(204, 'No matching orders at the moment for order id= ' + orderId)
    }

    await validateTakerMatchedBalance(order, baseTokenFilledAmounts)

    await setOrdersAsMatching([...matchedOrderToProcess, order])

    processMatchingOrders(
      order,
      matchedOrderToProcess,
      isLastTakerOrderBaseTokenAmountTotallyMatched,
      notMatchTakerOrMakerBaseTokenAmount,
      baseTokenFilledAmounts,
      isTakerBaseTokenAmountFilled
    )

    ctx.status = 204
    ctx.body = 'Matching orders...'
  } catch (e) {
    logger.error(`
    Taker orderId = ${orderId}. Find matched orders failed: ${e.message}`, ctx.request.ip, ctx.state.user.id)
    throw createError(e.status || 500, `Find matched orders failed: ${e.message}`)
  }
}
