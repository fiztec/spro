import Router from 'koa-router'
import * as orderController from './controller'
import { authMiddleware } from '../../../middlewares/auth'
const router = new Router()

router
  .get('/order/:orderId', authMiddleware, orderController.getById)
  .post('/', authMiddleware, orderController.create)
  .patch('/order/:orderId', authMiddleware, orderController.cancel)
  .get('/user/:marketId', authMiddleware, orderController.findUserOrdersByMarket)
  .put('/match/:orderId', authMiddleware, orderController.matchOrder)

export default router.routes()
