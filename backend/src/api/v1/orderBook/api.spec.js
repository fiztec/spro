const server = require('../../../index')
const agent = require('supertest').agent(server)
const tokenUser2 = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoyLCJwdWJsaWNBZGRyZXNzIjoiMHhhOD' +
  'BmZjA4ZDA4ODA5YjI5YzczMzEzNWIxYjkzYTA3Nzk5ZTVkMTdmIn0sImlhdCI6MTU1NDI4NzA5NH0.u-tN-7YKjA3MzkTrQlDx5Bkd4' +
  '1lVRE9C8fs3_9e6KuM'

const tokenUser1 = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoxLCJwdWJsaWNBZGRyZXNzIj' +
  'oiMHg5NjI0MTE4NmNhNGFhNzlmNWJkNTkzYTAyYzI5YTM4ZGE3ODUyNTFlIn0sImlhdCI6MTU1NTQyMzE2Mn0.lVzu2XbZ98Cj' +
  '73wOW7phe5py-LT_uq1uFlaQpjsYqho'

describe('OrderBook (API)', function () {
  afterAll(async () => {
    await server.close()
  })

  it('get order history by market id for user 2', async () => {
    expect.assertions(2)
    const response = await agent.get('/api/v1/order-book/history/11')
      .set('Authorization', 'Bearer ' + tokenUser2)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body).toMatchSnapshot()
  })

  it('get order history by market id for user 1', async () => {
    expect.assertions(2)
    const response = await agent.get('/api/v1/order-book/history/11')
      .set('Authorization', 'Bearer ' + tokenUser1)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body).toMatchSnapshot()
  })

  it('Check Auth. Get order history by market id', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/order-book/history/11')
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })

  it('Check market not exist. Get order history by market id for user 2', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/order-book/history/111')
      .set('Authorization', 'Bearer ' + tokenUser2)
      .catch(err => console.error(err))

    expect(response.status).toEqual(404)
    expect(response.body.message).toEqual('Market not found')
  })

  it('Get order history by market id without orders for user 2', async () => {
    expect.assertions(2)

    const response = await agent.get('/api/v1/order-book/history/1')
      .set('Authorization', 'Bearer ' + tokenUser2)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body).toMatchSnapshot()
  })

  it('get order history for all markets', async () => {
    expect.assertions(2)
    const response = await agent.get('/api/v1/order-book/history')
      .set('Authorization', 'Bearer ' + tokenUser2)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body).toMatchSnapshot()
  })
})
