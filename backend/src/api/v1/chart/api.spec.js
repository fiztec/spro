const server = require('../../../index')
const agent = require('supertest').agent(server)
const moment = require('moment')
const timestamp3DaysAgo = moment().subtract(3, 'days').valueOf()
const timestamp9DaysAgo = moment().subtract(9, 'days').valueOf()
const timestamp20DaysAgo = moment().subtract(20, 'days').valueOf()

describe('Chart (API)', function () {
  afterAll(async () => {
    await server.close()
  })

  it('get chart data for last 3 days', async () => {
    expect.assertions(2)
    const response = await agent.get(`/api/v1/chart?marketId=11&from=${Math.round(timestamp3DaysAgo / 1000)}`)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body.length).toEqual(4)
  })

  it('get chart data for last 9 days', async () => {
    expect.assertions(2)
    const response = await agent.get(`/api/v1/chart?marketId=11&from=${Math.round(timestamp9DaysAgo / 1000)}`)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body.length).toEqual(6)
  })

  it('get chart data for last 20 days', async () => {
    expect.assertions(2)
    const response = await agent.get(`/api/v1/chart?marketId=11&from=${Math.round(timestamp20DaysAgo / 1000)}`)
      .catch(err => console.error(err))

    expect(response.status).toEqual(404)
    expect(response.body.message).toEqual('Chart data not available for given from period')
  })

  it('get chart data without timestamp', async () => {
    expect.assertions(2)
    const response = await agent.get(`/api/v1/chart?marketId=11`)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
    expect(response.body.length).toEqual(6)
  })
})
