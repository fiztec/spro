import Router from 'koa-router'
import * as botController from './controller'
import { isAdminMiddleware } from '../../../middlewares'

const router = new Router()

router
  .get('/:marketId', isAdminMiddleware, botController.getBotData)
  .post('/start/:marketId', isAdminMiddleware, botController.botStart)
  .patch('/stop/:marketId', isAdminMiddleware, botController.botStop)

export default router.routes()
