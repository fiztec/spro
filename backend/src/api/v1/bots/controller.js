import _ from 'lodash'
import db from '../../../models'
import { BOT_ACCOUNT } from '../../../constants/bot'
import { RELAYER } from '../../../config/web3'
import { erc20BalanceABI } from '../../../constants/abi'
import { web3 } from '../../../lib/web3'
import crypto from 'crypto'
import BigNumber from 'bignumber.js'
import axios from 'axios' // TODO change on request-promise-native
import { generateOrderData, getOrderHash, padToBytes32, ethSignHex } from '../../../helpers/signature'
import { getOrderData } from '../orders/sdk'
import { ORDER_STATUS } from '../../../constants/order'
import cron from 'node-cron'
import moment from 'moment'
const { sequelize } = db

const {
  liquidityBot: LiquidityBot,
  botSettings: BotSettings,
  market: Market,
  exchangeToken: ExchangeToken,
  order: Order,
  sequelize: { Op },
} = db

const BOT_ATTRIBUTES = [
  'id',
  'marketId',
  'publicAddress',
  'botname',
  'isRunning',
]

const BOT_SETTINGS_ATTRIBUTES = [
  'id',
  'liquidityBotId',
  'centerPrice',
  'minPrice',
  'maxPrice',
  'priceGap',
  'expandInventory',
]

const MARKET_ATTRIBUTES = [
  'id',
  'tokens',
  'baseToken',
  'baseTokenAddress',
  'baseTokenDecimals',
  'quoteToken',
  'quoteTokenAddress',
  'baseTokenDecimals',
  'minOrderSize',
  'pricePrecision',
  'priceDecimals',
  'amountDecimals',
  'asMakerFeeRate',
  'asTakerFeeRate',
]

const tokenPrices = {}
// every 10 seconds
const botPriceWatcher = cron.schedule('*/10 * * * * *', async () => {
  // every 10 seconds
  const runningBots = await LiquidityBot.findAll({
    where: { isRunning: true },
    attributes: ['id', 'marketId'],
    include: {
      model: BotSettings,

    },
  })

  const botMarkets = []
  await Promise.all(runningBots.map(async (bot) => {
    const market = await Market.findByPk(bot.marketId, { attributes: ['baseToken'] })
    botMarkets.push(market)
  }))

  const botBaseTokens = botMarkets.map((market) => market.baseToken)
  await Promise.all(botBaseTokens.map(async (token) => {
    const { tokenSymbol, currentPriceToWETH } = await ExchangeToken.findOne({
      where: { tokenSymbol: token },
      attributes: ['tokenSymbol', 'currentPriceToWETH'],
    })

    if (!tokenPrices.hasOwnProperty(tokenSymbol)) throw Error(`No token price for ${tokenSymbol}`)

    if (tokenPrices[tokenSymbol] !== currentPriceToWETH) {
      console.log(`>>> BOT RESTARTING <<<`)
      const market = await Market.findOne({
        where: { baseToken: tokenSymbol },
        attributes: [
          'id',
          'tokens',
          'baseToken',
          'quoteToken',
          'minOrderSize',
          'baseTokenDecimals',
          'baseTokenAddress',
          'quoteTokenAddress',
        ],
      })

      const botObject = await LiquidityBot.findOne({
        where: { marketId: market.id },
        attributes: [...BOT_ATTRIBUTES],
      })

      // stop BOT
      await cancelBotOrders(botObject.id)

      // start BOT with orders with new price
      const botSettingsWhere = _.isNil(botObject) ? {} : { liquidityBotId: botObject.id }

      let baseTokenPrice = currentPriceToWETH

      let botSettingsObject = await BotSettings.findOne({
        where: botSettingsWhere,
        attributes: [...BOT_SETTINGS_ATTRIBUTES],
      })

      if (_.isNil(botSettingsObject)) throw Error(`Bot settings not found for bot id ${botObject.id}`)

      botSettingsObject = await botSettingsObject.update({
        centerPrice: baseTokenPrice.toFixed(),
      }, { where: { id: botObject.id }, returning: true })

      checkBotParameters(botSettingsObject, baseTokenPrice, market.baseToken)

      const publicAddress = getBotPublicAddress()

      // bot balance > 0 for both tokens
      const baseTokenContract = web3.eth.Contract(erc20BalanceABI, market.baseTokenAddress)
      const quoteTokenContract = web3.eth.Contract(erc20BalanceABI, market.quoteTokenAddress)

      let baseTokenBalance = await baseTokenContract.methods.balanceOf(publicAddress)
        .call()
        .catch(e => {
          throw Error(`Get ${market.baseToken} bot balance returns error: ${e.message}`)
        })

      if (baseTokenBalance <= market.minOrderSize * `1e${market.baseTokenDecimals}`) {
        console.log(`${market.baseToken} bot balance if less than minOrderSize for the ${market.tokens} market.`)
      }
      let quoteTokenBalance = await quoteTokenContract.methods.balanceOf(publicAddress)
        .call()
        .catch(e => {
          throw Error(`Get ${market.quoteToken} bot balance returns error: ${e.message}`)
        })

      baseTokenPrice = new BigNumber(baseTokenPrice)
      baseTokenBalance = new BigNumber(baseTokenBalance)
      quoteTokenBalance = new BigNumber(quoteTokenBalance)
      let minOrderSize = new BigNumber(market.minOrderSize)

      const priceOptions = new PriceOptions(
        quoteTokenBalance,
        minOrderSize,
        baseTokenPrice,
        market.baseTokenDecimals
      )

      if (!isQuoteTokenLessCurrentPriceForMinOrderSize(priceOptions)) {
        throw Error(`${market.quoteToken} bot balance is less than minOrderSize for the ${market.tokens} market.`)
      }

      const botPrices = getBotPrices(quoteTokenBalance, baseTokenBalance, baseTokenPrice)

      const botSettingsValues = {
        minPrice: botSettingsObject.minPrice,
        maxPrice: botSettingsObject.maxPrice,
        priceGap: botSettingsObject.priceGap,
        expandInventory: botSettingsObject.expandInventory,
      }

      const laddersOptions = new LadderOptions({
        ...botPrices,
        ...botSettingsValues,
        marketPricePrecision: market.pricePrecision,
      })

      const { sellLadders, buyLadders } = getLadders(laddersOptions)

      await createBotOrders(sellLadders, true, market, botObject.id)
      await createBotOrders(buyLadders, false, market, botObject.id)

      await updateBotStatus(botObject.id, true)

      // as bot launched successfully update tokenPrices object with new token price
      tokenPrices[tokenSymbol] = currentPriceToWETH

      // console.log(`Bot re-started with new price for market ${market.id}.`)
      botPriceWatcher.start()
    }
  }))
}, {
  scheduled: false,
})

export const getBotData = async (ctx, next) => {
  await next()
  const {
    params: { marketId },
  } = ctx

  const noBot = {
    isRunning: false,
    minPrice: '0',
    maxPrice: '0',
    priceGap: '0',
    expandInventory: -1,
  }

  let botResult

  try {
    const bot = await LiquidityBot.findOne({
      where: { marketId },
      attributes: [...BOT_ATTRIBUTES],
      include: {
        model: BotSettings,
        attributes: [...BOT_SETTINGS_ATTRIBUTES],
      },
    })

    if (_.isNull(bot)) {
      botResult = noBot
    } else {
      botResult = bot.get({ plain: true })
      botResult = _.omit(_.merge(botResult, _.get(botResult, 'botSetting')), 'botSetting')
    }

    ctx.body = botResult
  } catch (e) {
    ctx.throw(500, `Can't get bot data for marketId ${marketId}`, e.message)
  }
}

export const botStop = async (ctx, next) => {
  await next()
  const { params: { marketId } } = ctx
  try {
    const botObject = await LiquidityBot.findOne({
      where: { marketId },
      attributes: [...BOT_ATTRIBUTES],
    })
    ctx.assert(!_.isNil(botObject), 500, `Bot not found for marketId ${marketId}.`)
    ctx.assert(botObject.isRunning, 500, 'Bot is not running. Stop command is not valid.')

    await cancelBotOrders(botObject.id)

    await updateBotStatus(botObject.id, false)

    await botPriceWatcher.stop()

    ctx.status = 204
    ctx.body = `Bot disabled for market ${marketId}. Bot orders canceled.`
  } catch (e) {
    ctx.throw(500, `Can't stop bot for market ${marketId}.`, e.message)
  }
}

export const botStart = async (ctx, next) => {
  await next()
  const { params: { marketId } } = ctx
  const { minPrice, maxPrice, priceGap, expandInventory } = ctx.request.body
  try {
    const botOrdersMany = new BigNumber(maxPrice).minus(minPrice).div(priceGap)
    const botOrdersManyInt = botOrdersMany.integerValue(BigNumber.ROUND_FLOOR)
    const suggestedMaxPriceGap = new BigNumber(maxPrice).minus(minPrice).div(40).toPrecision(3)
    const suggestedMinPriceGap = new BigNumber(maxPrice).minus(minPrice).div(2).toPrecision(3)

    ctx.assert(botOrdersMany.lte(40), 500,
      `priceGap should not be that little (leads to >40 orders creation). Set priceGap > ${suggestedMaxPriceGap}`)
    ctx.assert(new BigNumber(botOrdersManyInt).gt(1), 500,
      `priceGap too little - set at least ${suggestedMinPriceGap}.`)
    ctx.assert(expandInventory > 0, 500, 'expandInventory should be >= 1. 1 is a default value.')
    ctx.assert(new BigNumber(minPrice).gt(0), 500, 'minPrice should be > 0.')

    let botObject = await LiquidityBot.findOne({
      where: { marketId },
      attributes: [...BOT_ATTRIBUTES],
    })
    ctx.assert(!_.isNil(botObject), 500, `Bot not found for marketId ${marketId}`)
    ctx.assert(!botObject.isRunning, 500, 'Bot is running already. Start command is not valid.')

    const market = await Market.findByPk(marketId, { attributes: MARKET_ATTRIBUTES })
    ctx.assert(!_.isNil(market), 500, `Market not found for id ${marketId}`)

    const publicAddress = getBotPublicAddress()

    // bot balance > 0 for both tokens
    const baseTokenContract = web3.eth.Contract(erc20BalanceABI, market.baseTokenAddress)
    const quoteTokenContract = web3.eth.Contract(erc20BalanceABI, market.quoteTokenAddress)

    let baseTokenBalance = await baseTokenContract.methods.balanceOf(publicAddress)
      .call()
      .catch(e => {
        throw Error(`Get ${market.baseToken} bot balance returns error: ${e.message}`)
      })

    ctx.assert(baseTokenBalance > market.minOrderSize * `1e${market.baseTokenDecimals}`,
      500, `${market.baseToken} bot balance if less than minOrderSize for the ${market.tokens} market.`)

    let quoteTokenBalance = await quoteTokenContract.methods.balanceOf(publicAddress)
      .call()
      .catch(e => {
        throw Error(`Get ${market.quoteToken} bot balance returns error: ${e.message}`)
      })

    let { tokenSymbol, currentPriceToWETH: baseTokenPrice } = await ExchangeToken.findOne({
      where: { tokenSymbol: market.baseToken },
      attributes: ['currentPriceToWETH', 'tokenSymbol'],
    })

    tokenPrices[tokenSymbol] = baseTokenPrice

    checkBotParameters(
      { minPrice, maxPrice, priceGap },
      baseTokenPrice,
      market.baseToken
    )

    baseTokenPrice = new BigNumber(baseTokenPrice)
    baseTokenBalance = new BigNumber(baseTokenBalance)
    quoteTokenBalance = new BigNumber(quoteTokenBalance)
    let minOrderSize = new BigNumber(market.minOrderSize)

    const priceOptions = new PriceOptions(
      quoteTokenBalance,
      minOrderSize,
      baseTokenPrice,
      market.baseTokenDecimals
    )
    ctx.assert(isQuoteTokenLessCurrentPriceForMinOrderSize(priceOptions),
      500, `${market.quoteToken} bot balance is less than minOrderSize for the ${market.tokens} market.`)

    const botPrices = getBotPrices(quoteTokenBalance, baseTokenBalance, baseTokenPrice)

    const botSettingsValues = {
      minPrice,
      maxPrice,
      priceGap,
      expandInventory,
    }

    await updateBotSettings(botObject.id, {
      ...botSettingsValues,
      centerPrice: botPrices.botCenterPrice.toFixed(),
    })

    const laddersOptions = new LadderOptions({
      ...botPrices,
      ...botSettingsValues,
      marketPricePrecision: market.pricePrecision,
    })

    const { sellLadders, buyLadders } = getLadders(laddersOptions)

    await createBotOrders(sellLadders, true, market, botObject.id)
    await createBotOrders(buyLadders, false, market, botObject.id)

    await updateBotStatus(botObject.id, true)

    ctx.status = 204
    ctx.body = `Bot started for market ${marketId}.`
    botPriceWatcher.start()
  } catch (e) {
    ctx.throw(e.status || 500, `Can't create bot's order`, e.message)
  }
}

class PriceOptions {
  constructor (
    quoteTokenBalance,
    minOrderSize,
    baseTokenPrice,
    baseTokenDecimals
  ) {
    if (
      !(quoteTokenBalance instanceof BigNumber) ||
      !(minOrderSize instanceof BigNumber) ||
      !(baseTokenPrice instanceof BigNumber)
    ) {
      throw Error('quoteTokenBalance, minOrderSize, baseTokenPrice should be instance of BigNumber')
    }

    this.quoteTokenBalance = quoteTokenBalance
    this.minOrderSize = minOrderSize
    this.baseTokenPrice = baseTokenPrice
    this.baseTokenDecimals = baseTokenDecimals
  }
}

const isQuoteTokenLessCurrentPriceForMinOrderSize = (options) => {
  if (!(options instanceof PriceOptions)) {
    throw Error('Wrong arguments for function isQuoteTokenLessCurrentPriceForMinOrderSize')
  }

  const { quoteTokenBalance, minOrderSize, baseTokenPrice, baseTokenDecimals } = options
  const currentPrice = minOrderSize.times(baseTokenPrice).times(new BigNumber(`1e${baseTokenDecimals}`))

  return quoteTokenBalance.gt(currentPrice)
}

/**
 *
 * @param quoteTokenBalance: BigNumber
 * @param baseTokenBalance: BigNumber
 * @param baseTokenPrice: BigNumber
 * @returns {{
 * botProduct: BigNumber,
 * botBaseBalance: BigNumber,
 * botCenterPrice: BigNumber
 * }}
 */
const getBotPrices = (quoteTokenBalance, baseTokenBalance, baseTokenPrice) => {
  if (
    !(quoteTokenBalance instanceof BigNumber) ||
    !(baseTokenBalance instanceof BigNumber) ||
    !(baseTokenPrice instanceof BigNumber)
  ) {
    throw Error('quoteTokenBalance, baseTokenBalance, baseTokenPrice should be instance of BigNumber')
  }

  // bot allowance >= bot balance for both tokens

  const botBalanceRatio = quoteTokenBalance.div(baseTokenBalance)
  let botBaseBalance, botQuoteBalance

  if (botBalanceRatio.gt(baseTokenPrice)) {
    botBaseBalance = baseTokenBalance
    botQuoteBalance = baseTokenBalance.times(baseTokenPrice)
  } else if (botBalanceRatio.lt(baseTokenPrice)) {
    botBaseBalance = baseTokenBalance.div(baseTokenPrice)
    botQuoteBalance = quoteTokenBalance
  } else {
    botBaseBalance = baseTokenBalance
    botQuoteBalance = quoteTokenBalance
  }

  const botProduct = botBaseBalance.times(botQuoteBalance)
  const botCenterPrice = botQuoteBalance.div(botBaseBalance)

  return { botProduct, botCenterPrice, botBaseBalance }
}

class LadderOptions {
  constructor (o) {
    const {
      botCenterPrice,
      botBaseBalance,
      priceGap,
      maxPrice,
      minPrice,
      botProduct,
      expandInventory,
      marketPricePrecision,
    } = o

    if (_.isNil(botCenterPrice) ||
      _.isNil(botBaseBalance) ||
      _.isNil(priceGap) ||
      _.isNil(maxPrice) ||
      _.isNil(minPrice) ||
      _.isNil(botProduct) ||
      _.isNil(expandInventory) ||
      _.isNil(marketPricePrecision)
    ) {
      throw Error('LadderOptions is not valid. Please check constructor parameters')
    }

    if (
      !(botCenterPrice instanceof BigNumber) ||
      !(botBaseBalance instanceof BigNumber) ||
      !(botProduct instanceof BigNumber)
    ) {
      throw Error('botCenterPrice, botBaseBalance, botProduct should be instance of BigNumber')
    }

    this.botCenterPrice = botCenterPrice
    this.botBaseBalance = botBaseBalance
    this.priceGap = priceGap
    this.maxPrice = maxPrice
    this.minPrice = minPrice
    this.botProduct = botProduct
    this.expandInventory = expandInventory
    this.marketPricePrecision = marketPricePrecision
  }
}

/**
 *
 * @param options
 * @return {
 * {
 * buyLadders: Array<{
 *   upPrice: string
 *   amount: string
 * }>,
 * sellLadders: Array<{
 *   upPrice: string
 *   amount: string
 * }>}}
 */
const getLadders = (options) => {
  if (!(options instanceof LadderOptions)) {
    throw Error('getLadders failed. Options should be instance of LadderOptions')
  }

  const {
    botCenterPrice,
    botBaseBalance,
    priceGap,
    maxPrice,
    minPrice,
    botProduct,
    expandInventory,
    marketPricePrecision } = options

  let sellLadders = []
  let buyLadders = []
  let upPrice
  let downPrice
  let lastBaseAmount
  let amount

  // ask orders (sell); use BigNumber! consider precision and decimals
  downPrice = botCenterPrice
  lastBaseAmount = botBaseBalance

  while (true) {
    upPrice = downPrice.plus(priceGap)
    if (upPrice.gt(maxPrice)) break
    let amountFactor = botProduct.div(upPrice)
    let newBaseAmount = amountFactor.sqrt()
    amount = newBaseAmount.minus(lastBaseAmount).abs().times(expandInventory)
    sellLadders.push({
      upPrice: upPrice.toFixed(),
      amount: amount.toFixed(0),
    })
    downPrice = upPrice
    lastBaseAmount = newBaseAmount
  }

  // bid orders (buy); use BigNumber, consider precision and decimals
  upPrice = botCenterPrice
  lastBaseAmount = botBaseBalance

  while (true) {
    downPrice = upPrice.minus(priceGap)
    if (downPrice.lt(minPrice)) break
    let amountFactor = botProduct.div(downPrice)
    let newBaseAmount = amountFactor.sqrt()
    amount = newBaseAmount.minus(lastBaseAmount).abs().times(expandInventory)
    buyLadders.push({
      upPrice: upPrice.toPrecision(marketPricePrecision),
      amount: amount.toFixed(0),
    })
    upPrice = downPrice
    lastBaseAmount = newBaseAmount
  }

  buyLadders.push({
    upPrice: upPrice.toPrecision(marketPricePrecision),
    amount: amount ? amount.toFixed(0) : '0',
  })

  return { sellLadders, buyLadders }
}

/**
 *
 * @param botId: Number
 * @param values: any
 * @returns {Promise<any>}
 */
const updateBotSettings = async (botId, values) => {
  let botSettingsObject = await BotSettings.findOne({
    where: { liquidityBotId: botId },
    attributes: [...BOT_SETTINGS_ATTRIBUTES],
  })

  if (_.isNil(botSettingsObject)) {
    botSettingsObject = await BotSettings.create(
      {
        liquidityBotId: botId,
        ...values,
      },
      { returning: true })
  } else {
    botSettingsObject = await botSettingsObject.update(
      values,
      { returning: true })
  }

  return botSettingsObject
}

/**
 *
 * @returns {Promise<BigNumber>}
 */
const getGasAmount = async () => {
  const gasData = await axios.get('https://www.etherchain.org/api/gasPriceOracle')
  let standardGasPrice = new BigNumber(gasData.data.standard)
  standardGasPrice = standardGasPrice.div(1e9)
  return standardGasPrice.times(190000)
}

/**
 *
 * @param dataLadders:Array<Object>
 * @param isSell: Boolean
 * @param market: Object
 * @param botObjectId: Integer
 * @returns {Promise<void>}
 */
const createBotOrders = async (dataLadders, isSell, market, botObjectId) => {
  const publicAddress = getBotPublicAddress()
  const PK = getBotPK()

  const gasAmount = await getGasAmount()
  const newBotOrders = []

  await Promise.all(dataLadders.map(async (ladder) => {
    // tokens, trader, baseTokenAmount, quoteTokenAmount, gasTokenAmount, data, signature
    // data: side, isMarketOrder, expiresAt, asMakerFeeRate, asTakerFeeRate
    const botOrder = {
      version: 2, // uint256 public constant SUPPORTED_ORDER_VERSION = 2
      trader: publicAddress, // trader publicAddress
      relayer: RELAYER,
      baseToken: market.baseToken,
      quoteToken: market.quoteToken,
      baseTokenAmount: ladder.amount,
      quoteTokenAmount: new BigNumber(ladder.amount).times(new BigNumber(ladder.upPrice)).toFixed(),
      gasTokenAmount: gasAmount.times(new BigNumber(10).pow(market.baseTokenDecimals)).toFixed(),
      isSell,
      isMarket: false,
      expiredAtSeconds: moment().add(1, 'hours').unix().toString(), // currentTime + expirationTime (1 hour)

      // uint256 public constant FEE_RATE_BASE = 100000
      asMakerFeeRate: new BigNumber(market.asMakerFeeRate).times(1e5).toFixed(),
      // uint256 public constant FEE_RATE_BASE = 100000
      asTakerFeeRate: new BigNumber(market.asTakerFeeRate).times(1e5).toFixed(),
      makerRebateRate: 0, // uint256 public constant REBATE_RATE_BASE = 100
      salt: parseInt(crypto.randomBytes(8).toString('hex'), 16),
    }
    botOrder.data = generateOrderData(botOrder)
    const botOrderHash = getOrderHash(botOrder)
    const botOrderSigVRS = ethSignHex(botOrderHash, PK)
    let botOrderSigConfig = Number(botOrderSigVRS.v).toString(16) + '00' // 00 for EthSign (01 for EIP712)
    botOrderSigConfig = padToBytes32(botOrderSigConfig)

    const botOrderData = getOrderData(botOrder.data)

    const {
      side,
      isMarketOrder,
      expiresAt,
    } = botOrderData.newOrderData

    const newBotOrder = {
      side,
      isMarketOrder,
      baseTokenAmount: botOrder.baseTokenAmount,
      quoteTokenAmount: botOrder.quoteTokenAmount,
      initBaseTokenAmount: botOrder.baseTokenAmount,
      initQuoteTokenAmount: botOrder.quoteTokenAmount,
      gasTokenAmount: botOrder.gasTokenAmount,
      data: botOrder.data,
      signatureConfig: botOrderSigConfig,
      signatureR: botOrderSigVRS.r,
      signatureS: botOrderSigVRS.s,
      expiresAt,
      status: ORDER_STATUS.PENDING,
      liquidityBotId: botObjectId,
      marketId: market.id,
      createdAt: new Date(),
      updatedAt: new Date(),
    }

    newBotOrders.push(newBotOrder)
  }))

  await Order.bulkCreate(newBotOrders)

  // TODO (Roman M) Should we send notification to order history with bot orders
  // const botOrders = _.invokeMap(await Order.bulkCreate(newBotOrders, { returning: true }), 'get', { plain: true })
  // for (const {id} of botOrders){
  //   await emitNewOrderToHistory(id)
  // }
}

const getBotPublicAddress = () => {
  return BOT_ACCOUNT.publicAddress
}

const getBotPK = () => {
  return BOT_ACCOUNT.PK
}

const cancelBotOrders = async (botObjectId) => {
  let botOrders = await Order.findAll({
    where: { liquidityBotId: botObjectId, status: ORDER_STATUS.PENDING || ORDER_STATUS.PARTIALLY_FILLED },
    attributes: ['id'],
  })

  const ids = _.map(botOrders, 'id')

  await Order.update({ status: ORDER_STATUS.CANCELED }, { where: { id: { [Op.in]: ids } } })
}

const updateBotStatus = async (botId, isRunning) => {
  let transaction

  if (_.isNil(botId) || _.isNil(isRunning)) throw Error('Wrong data to change bot status')

  try {
    transaction = await sequelize.transaction()

    const botStatusUpdateResult = await LiquidityBot.update(
      { isRunning },
      { where: { id: botId },
        returning: true,
        transaction }
    )

    console.log(`Bot id ${botId} change running status on ${botStatusUpdateResult[1][0].isRunning}`)

    transaction.commit()
  } catch (e) {
    transaction && await transaction.rollback()
    throw Error(e)
  }
}

const checkBotParameters = (botSettingsObject, baseTokenPrice, marketBaseToken) => {
  const { minPrice, maxPrice, priceGap } = botSettingsObject

  // minPrice, maxPrice, priceGap, expandInventory
  // if (minPrice >= baseTokenPrice) {
  if (new BigNumber(minPrice).gte(baseTokenPrice)) {
    throw Error(`Current ${marketBaseToken} price is ${baseTokenPrice}. Bot minPrice should be less than that.`)
  }
  if (new BigNumber(maxPrice).lte(baseTokenPrice)) {
    throw Error(`Current ${marketBaseToken} price is ${baseTokenPrice}. Bot maxPrice should be larger than that.`)
  }
  if (new BigNumber(priceGap).gte(new BigNumber(maxPrice).minus(baseTokenPrice)) &&
    new BigNumber(priceGap).gte(new BigNumber(baseTokenPrice).minus(minPrice))) {
    throw Error(`priceGap should be less than difference between min/max price and token current price.`)
  }
}
