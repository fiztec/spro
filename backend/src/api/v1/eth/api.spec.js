const server = require('../../../index')
const agent = require('supertest').agent(server)
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXlsb2FkIjp7ImlkIjoxLCJwdWJsaWNBZGRyZXNzIjoiMHg5NjI0MTE4' +
  'NmNhNGFhNzlmNWJkNTkzYTAyYzI5YTM4ZGE3ODUyNTFlIn0sImlhdCI6MTU1NTQyMzE2Mn0.lVzu2XbZ98Cj73wOW7phe5py-LT_uq1uFlaQpjsYqho'

describe('Ethereum (API)', function () {
  afterAll(async () => {
    await server.close()
  })

  it('get trade and exchange wallets fees', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/eth/fees-wallet')
      .set('Authorization', 'Bearer ' + token)
      .catch(err => console.error(err))

    expect(response.status).toEqual(200)
  })

  it('Check Auth. get trade and exchange wallets fees', async () => {
    expect.assertions(1)

    const response = await agent.get('/api/v1/eth/fees-wallet')
      .catch(err => console.error(err))

    expect(response.status).toEqual(401)
  })
})
