import * as web3Config from '../../../config/web3'
import { erc20BalanceABI } from '../../../constants/abi'
import { TRADE_CONTRACT_ADDRESS } from '../../../constants/market'
import { web3 } from '../../../lib/web3'
const { RELAYER: relayerAddress } = web3Config

const getTokenBalance = (contractAddress, walletAddress) => {
  const contractInstance = web3.eth.Contract(erc20BalanceABI, contractAddress)

  return contractInstance.methods.balanceOf(walletAddress)
    .call()
    .catch(e => { throw Error(`Get balance return error: ${e.message}`) })
}

export const getFeesWallet = async (ctx, next) => {
  await next()

  const relayerWethBalanceWei = await getTokenBalance(TRADE_CONTRACT_ADDRESS, relayerAddress)

  ctx.body = {
    feesBalance: web3.utils.fromWei(relayerWethBalanceWei, 'ether'),
    wallet: relayerAddress }
}
