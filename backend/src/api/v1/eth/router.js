import Router from 'koa-router'
import * as ethController from './controller'
import { authMiddleware } from '../../../middlewares/auth'
const router = new Router()

router
  .get('/fees-wallet', authMiddleware, ethController.getFeesWallet)

export default router.routes()
