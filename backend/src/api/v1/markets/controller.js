import _ from 'lodash'
import db from '../../../models'
import { Op } from 'sequelize'
const { market: Market } = db

const MARKET_ATTRIBUTES = [
  'id',
  'tokens',
  'baseToken',
  'baseTokenProjectUrl',
  'baseTokenName',
  'baseTokenDecimals',
  'baseTokenAddress',
  'quoteToken',
  'quoteTokenDecimals',
  'quoteTokenAddress',
  'minOrderSize',
  'pricePrecision',
  'priceDecimals',
  'amountDecimals',
  'asMakerFeeRate',
  'asTakerFeeRate',
  'supportedOrderTypes',
  'marketOrderMaxSlippage',
]

export const findByTokenName = async (ctx, next) => {
  await next()
  const { token } = ctx.query
  const options = token ? {
    where: {
      [Op.or]: [
        { baseToken: { [Op.iLike]: `%${token}%` } },
        { quoteToken: { [Op.iLike]: `%${token}%` } },
      ],
    },
  } : {}

  options.attributes = MARKET_ATTRIBUTES

  ctx.body = await Market.findAll(options)
}

export const getById = async (ctx, next) => {
  await next()
  const {
    params: { marketId },
  } = ctx
  const marketObject = await Market.findByPk(marketId, { attributes: MARKET_ATTRIBUTES })
  ctx.assert(!_.isNil(marketObject), 500, 'Market not found')
  ctx.status = 200
  ctx.body = marketObject
}
