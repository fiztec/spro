import Router from 'koa-router'
import * as marketController from './controller'
import { authMiddleware } from '../../../middlewares/auth'
const router = new Router()

router
  .get('/', marketController.findByTokenName)
  .get('/:marketId', authMiddleware, marketController.getById)

export default router.routes()
