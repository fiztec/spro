import * as ethUtil from 'ethereumjs-util'
import sigUtil from 'eth-sig-util'
import jwt from 'jsonwebtoken'
import { SECRET } from '../../../config/jwt'
import db from '../../../models'
import { Op } from 'sequelize'
import base64url from 'base64url'
import crypto from 'crypto'
const nonceSize = 64

export const create = async (ctx, next) => {
  await next()
  const { signature, publicAddress } = ctx.request.body
  try {
    const userObject = await db.user.findOne({ where: { publicAddress: { [Op.iLike]: publicAddress } } })
    ctx.assert(userObject, 500, `User with publicAddress ${publicAddress} is not found in database`)

    const msg = `MeterQubes authentication with one-time nonce: ${userObject.nonce}`
    const msgBufferHex = ethUtil.bufferToHex(Buffer.from(msg, 'utf8'))
    const address = sigUtil.recoverPersonalSignature({ data: msgBufferHex, sig: signature })

    ctx.assert(address.toLowerCase() === publicAddress.toLowerCase(), 401, 'Signature verification failed')

    userObject.nonce = base64url(crypto.randomBytes(nonceSize))
    await userObject.save()

    const accessToken = await jwt.sign(
      {
        payload: {
          id: userObject.id,
          publicAddress,
        },
      },
      SECRET,
      null
    )
    ctx.status = 200
    ctx.body = accessToken
  } catch (e) {
    ctx.throw(500, `Can't create token: ${e}`)
  }
}
