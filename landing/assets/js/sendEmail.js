const USER_ID = "user_uK9HEZdyKKSEWdOykGPtA";
const SERVICE_ID = "gmail";
const TEMPLATE_ID = "template_9LZJegPx";

(function() {
  emailjs.init(USER_ID);
})();

const form = document.getElementById("sendEmailForm");
const input = form.getElementsByTagName("input")[0];
const submitButton = form.getElementsByTagName("button")[0];

input.addEventListener("input", ({ target: { value } }) => {
  if (value !== "") {
    submitButton.removeAttribute("disabled");
  } else {
    submitButton.setAttribute("disabled", true);
  }
});

form.onsubmit = e => {
  e.preventDefault();

  const templateParams = {
    from_email: input.value
  };

  return emailjs.send(SERVICE_ID, TEMPLATE_ID, templateParams).then(
    response => {
      submitButton.innerText = "Email sent successfully!";
    },
    error => {
      submitButton.innerText = "Email sent Error!";
    }
  );
};
