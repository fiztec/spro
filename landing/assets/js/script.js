jQuery(document).ready(function($) {
  var running = 0;

  $(document).on("scroll", function() {
    if (running == 0) {
      if (
        $(this).scrollTop() +
          $(
            ".introduction-container.introduction-how-it-works-container"
          ).outerHeight() /
            2 >=
        $(
          ".introduction-container.introduction-how-it-works-container"
        ).position().top
      ) {
        running = 1;
        var texts = $(".texts > span");
        var textsCount = texts.length;
        var currentText = 1;

        setInterval(function() {
          texts.removeClass("active");

          $(".texts > span:nth-child(" + currentText + ")").addClass("active");

          if (currentText > textsCount) {
            currentText = 1;
          } else {
            currentText++;
          }
        }, 2000);
      }
    }
  });
});
