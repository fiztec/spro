jQuery(document).ready(function() {
  $.ajax({
    type: "GET",
    url:
      "https://l4chsalter-alternative-me-crypto-v1.p.rapidapi.com/v2/ticker/?convert=EUR&structure=array&sort=id",
    headers: {
      "X-RapidAPI-Host": "l4chsalter-alternative-me-crypto-v1.p.rapidapi.com",
      "X-RapidAPI-Key": "210c998c56msh10fdab85f23a34ap1f094bjsnee07cbf6bde4"
    }
  }).done(function({ data }) {
    for (var i = 0; i < data.length; i++) {
      $("#coinData").append(
        `<div class="price-ticker">
          <div class="currency">${data[i].symbol}</div>
          <div>
            <span class="price">$${data[i].quotes.USD.price}</span>
            <span class="price-change" style="${
              data[i].quotes.USD.percentage_change_1h.toString().match(/\-/gi)
                ? "color: #f8165b;"
                : "color: #67dfb3;"
            }">${data[i].quotes.USD.percentage_change_1h}</span>
          </div>
        </div>`
      );
    }
    if (data.length) {
      $("#coinData").slick({
        dots: false,
        infinite: false,
        speed: 350,
        slidesToShow: 5,
        slidesToScroll: 5,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 4,
              slidesToScroll: 4,
              arrows: true
            }
          },
          {
            breakpoint: 769,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
              arrows: true
            }
          },
          {
            breakpoint: 640,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
              arrows: true
            }
          },
          {
            breakpoint: 420,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: true
            }
          }
          // You can unslick at a given breakpoint now by adding:
          // settings: "unslick"
          // instead of a settings object
        ]
      });
    }
  });
});
