import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { RouterProps } from 'next/router'
import { NextContext, NextComponentClass } from 'next'
import isEmpty from 'lodash/isEmpty'
import { AuthStoreTypes } from '../stores/auth/types'
import { MetamaskStoreTypes } from '../stores/metamask/types'
import { removeUserToken, getUserToken } from './cookieService'
import { MarketsStoreTypes } from '../stores/markets/types'
import { OrdersStoreTypes } from '../stores/order/types'

export const withAuth = (Component: NextComponentClass | any) => {
  @inject(
    ({
      metamaskStore: { publicAddress, metamaskStatus },
      authStore: { authUser, status, setStatus },
      marketsStore: { getMarkets, markets },
      ordersStore: { getOrders, getOrderBook, ordersList, orderBook }
    }) => ({
      publicAddress,
      metamaskStatus,
      authUser,
      status,
      setStatus,
      markets,
      getMarkets,
      getOrders,
      getOrderBook,
      ordersList,
      orderBook
    })
  )
  @observer
  class AuthUserHOC extends React.Component<{
    publicAddress: MetamaskStoreTypes['publicAddress']
    metamaskStatus: MetamaskStoreTypes['metamaskStatus']
    authUser: AuthStoreTypes['authUser']
    status: AuthStoreTypes['status']
    setStatus: AuthStoreTypes['setStatus']
    getMarkets: MarketsStoreTypes['getMarkets']
    markets: MarketsStoreTypes['markets']
    ordersList: OrdersStoreTypes['ordersList']
    getOrders: OrdersStoreTypes['getOrders']
    orderBook: OrdersStoreTypes['orderBook']
    getOrderBook: OrdersStoreTypes['getOrderBook']
    query: RouterProps['query']
  }> {
    public static async getInitialProps(ctx: NextContext) {
      if (Component.getInitialProps) return await Component.getInitialProps(ctx)
    }

    public componentDidMount() {
      const { authUser } = this.props
      const publicAddress = window.web3 && window.web3.eth.coinbase
      const token = getUserToken()
      if (!token) {
        authUser(publicAddress)
      }
    }

    public componentWillReact() {
      const {
        publicAddress,
        metamaskStatus,
        authUser,
        status,
        markets,
        getMarkets,
        ordersList,
        getOrders,
        orderBook,
        getOrderBook,
        query
      } = this.props

      const token = getUserToken()

      if (!token && publicAddress && status !== 'authRequest') {
        authUser(publicAddress)
      }

      if (token) {
        if (!markets) {
          getMarkets()
        }
        if (!ordersList && !isEmpty(query)) {
          getOrders(query ? +query.marketId : 1)
        }
        if (!orderBook && !isEmpty(query)) {
          getOrderBook(query ? +query.marketId : 1)
        }
      }

      if (metamaskStatus === 'logged in') {
        authUser(publicAddress)
      }

      if (metamaskStatus === 'account changed' && status !== 'authRequest') {
        removeUserToken()
        authUser(publicAddress)
      }

      if (metamaskStatus === 'logged out') {
        removeUserToken()
      }
    }

    public render() {
      return <Component {...this.props} />
    }
  }

  return AuthUserHOC
}
