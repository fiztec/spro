import * as React from 'react'
import { inject, observer } from 'mobx-react'
import { MetamaskStoreTypes } from '../stores/metamask/types'
import { NextContext, NextComponentClass } from 'next'

export const metamaskChecker = (Component: NextComponentClass | any) => {
  @inject(({ metamaskStore: { checkMetamask } }) => ({
    checkMetamask
  }))
  @observer
  class MetamaskCheckerHOC extends React.Component<{
    checkMetamask: MetamaskStoreTypes['checkMetamask']
  }> {
    public static async getInitialProps(ctx: NextContext) {
      if (Component.getInitialProps) return await Component.getInitialProps(ctx)
    }

    public componentDidMount() {
      this.props.checkMetamask()
    }

    public render() {
      return <Component {...this.props} />
    }
  }

  return MetamaskCheckerHOC
}
