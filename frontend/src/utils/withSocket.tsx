import * as React from 'react'
import debounce from 'lodash/debounce'
import { inject, observer } from 'mobx-react'
import openSocket from 'socket.io-client'
import { NextContext, NextComponentClass } from 'next'
import { getUserToken } from '../utils/cookieService'
import { OrdersStoreTypes } from '../stores/order/types'
import { TradeHistoryStoreTypes } from '../stores/tradeHistory/types'
import { SOCKET_URL } from '../config/api'

export const withSocket = (Component: NextComponentClass | any) => {
  @inject(
    ({
      ordersStore: { getOrderBook, orderBook, getOrders },
      tradeHistoryStore: { getHistory }
    }) => ({
      getOrderBook,
      orderBook,
      getOrders,
      getHistory
    })
  )
  @observer
  class SocketHOC extends React.Component<{
    query: { marketId: number | string }
    orderBook: OrdersStoreTypes['orderBook']
    getOrderBook: OrdersStoreTypes['getOrderBook']
    getOrders: OrdersStoreTypes['getOrders']
    getHistory: TradeHistoryStoreTypes['getHistory']
  }> {
    public static getInitialProps(ctx: NextContext) {
      if (Component.getInitialProps) return Component.getInitialProps(ctx)
    }

    componentDidMount() {
      const { query, getOrderBook, getOrders, getHistory } = this.props
      const socket = openSocket(SOCKET_URL)
      const marketId = window.location.search.split('=')[1]

      const token = getUserToken()

      let connected = false
      socket.on('connect', () => {
        connected = true
        console.log('[SOCKET CONNECTED]', connected)

        socket.emit('auth', { accessToken: 'Bearer ' + token })
      })

      socket.on('newTrade', data => {
        console.log('[SOCKET MESSAGE] newTrade fired. data=', data)
        getOrderBook(query.marketId || marketId)
        getHistory(query.marketId || marketId)
      })

      socket.on('newOrder', (data: OrdersStoreTypes['orderBook']) => {
        console.log('[SOCKET MESSAGE] newOrder fired. data=', data)
        getOrderBook(query.marketId || marketId)
        getOrders(query.marketId || marketId)
      })

      socket.on('dashboard', data => {
        console.log('[SOCKET MESSAGE] dashboard fired. data=', data)
      })

      socket.on('orderChanged', data => {
        console.log('[SOCKET MESSAGE] orderChanged fired. data=', data)
        getOrderBook(query.marketId || marketId)
        getOrders(query.marketId || marketId)
      })
    }

    createAction = debounce(() => {}, 1000)

    public render() {
      return <Component {...this.props} />
    }
  }

  return SocketHOC
}
