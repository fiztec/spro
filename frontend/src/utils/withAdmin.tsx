import * as React from 'react'
import Router from 'next/router'
import { inject, observer } from 'mobx-react'
import { NextContext, NextComponentClass } from 'next'
import { IAdminStore } from '../stores/admin/types'
import { bind } from '../utils/bind'
import { AuthStoreTypes } from '../stores/auth/types'
import { redirect } from './redirect'
import { MetamaskStoreTypes } from '../stores/metamask/types'

export const withAdmin = (Component: NextComponentClass | any) => {
  @inject(
    ({
      metamaskStore: { checkMetamask, metamaskStatus, publicAddress },
      authStore: { authUser },
      adminStore: { isAdmin, getUser }
    }) => ({
      checkMetamask,
      metamaskStatus,
      publicAddress,
      authUser,
      isAdmin,
      getUser
    })
  )
  @observer
  class AdminCheckerHOC extends React.Component<{
    checkMetamask: MetamaskStoreTypes['checkMetamask']
    metamaskStatus: MetamaskStoreTypes['metamaskStatus']
    publicAddress: MetamaskStoreTypes['publicAddress']
    authUser: AuthStoreTypes['authUser']
    isAdmin: IAdminStore['isAdmin']
    getUser: IAdminStore['getUser']
    pathname: NextContext['pathname']
  }> {
    public static getInitialProps(ctx: NextContext) {
      if (Component.getInitialProps) return Component.getInitialProps(ctx)
    }

    public componentDidMount() {
      this.props.checkMetamask()
      this.onCheckAdmin()
    }

    public componentWillReact() {
      const { publicAddress, metamaskStatus } = this.props

      if (metamaskStatus === 'account changed') {
        this.onCheckAdmin()
      }

      if (metamaskStatus === 'logged out') {
        if (!publicAddress && this.props.pathname !== '/login') {
          redirect(null, '/login')
        }
      }
    }

    @bind private async onCheckAdmin() {
      const { getUser, authUser, pathname } = this.props
      const publicAddress = window.web3.eth.coinbase

      if (pathname === '/login') await authUser(publicAddress)

      if (publicAddress) await getUser()
      if (!publicAddress) await Router.push('/login')
    }

    public render() {
      return <Component {...this.props} />
    }
  }

  return AdminCheckerHOC
}
