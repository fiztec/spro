import axios from 'axios'
import { API_URL } from '../config/api'
import { NextContext } from 'next'
import { cookieParse } from './cookieParse'

export const apiCaller = async (
  url: string,
  method: string = 'GET',
  data?: object,
  headers?: object,
  req?: NextContext['req']
) => {
  try {
    const cookie: any = req
      ? cookieParse(req.headers.cookie)
      : cookieParse(document.cookie)

    const token = cookie.MQ_token

    const reg = new RegExp(API_URL, 'g')
    const thirdPartyUrl = !url.match(reg)

    const res = await axios({
      url,
      method,
      data,
      ...(!thirdPartyUrl
        ? {
            headers: {
              'Content-Type': 'application/json',
              'Access-Control-Allow-Headers': '*',
              ...(token && { Authorization: `Bearer ${token}` })
            }
          }
        : {
            headers: {
              ...headers
            }
          })
    })

    if (res) {
      return res.data
    }
  } catch (err) {
    throw err
  }
}
