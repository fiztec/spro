import Page from '../components/Page'
import { withNamespaces } from '../utils/i18n'
import Grid from '../components/Grid'
import Fees from '../components/Admin/Fees'
import AddToken from '../components/Admin/AddToken'
import Wallets from '../components/Admin/Wallets'
import Transactions from '../components/Admin/Transactions'
import Profile from '../components/Profile'
import Flex from '../components/Flex'
import Loader from '../components/Loader'
import { withAdmin } from '../utils/withAdmin'
import Bot from '../components/Admin/Bot'
import { getUserToken } from '../utils/cookieService'

const Admin = ({ lng, isAdmin }) => {
  const token = getUserToken()
  return (
    <Page
      rtl={lng === 'ar'}
      layout="admin"
      withoutHeader={!isAdmin}
      withoutLogo={!isAdmin}
      withProfile={isAdmin && <Profile />}
    >
      {isAdmin && token ? (
        <Grid layout="admin">
          <Fees />
          <AddToken />
          <Wallets />
          <Transactions />
          <Bot />
        </Grid>
      ) : (
        <Flex style={{ minHeight: '100vh' }} justify="center" align="center">
          <Loader />
        </Flex>
      )}
    </Page>
  )
}

Admin.getInitialProps = async () => {
  return {
    namespacesRequired: ['admin']
  }
}

export default withNamespaces('admin')(withAdmin(Admin))
