import Page from '../components/Page'
import { withNamespaces } from '../utils/i18n'
import { metamaskChecker } from '../utils/metamaskChecker'
import Exchange from '../components/Exchange'
import Container from '../components/Container'
import TokensHeader from '../components/TokensHeader'
import { apiCaller } from '../utils/apiCaller'
import { API_URL } from '../config/api'

const ExchangePage = ({ lng, markets }) => {
  return (
    <>
      <TokensHeader data={markets} />
      <Page rtl={lng === 'ar'} layout="exchange" withFooter withWallet>
        <Container>
          <Exchange />
        </Container>
      </Page>
    </>
  )
}

ExchangePage.getInitialProps = async ({ req }) => {
  try {
    const markets = await apiCaller(
      `${API_URL}/dashboard/landing`,
      'GET',
      null,
      null,
      req
    )
    if (markets) {
      return {
        markets,
        namespacesRequired: ['exchange', 'banner']
      }
    }
  } catch (err) {
    console.error(err)
  }

  return {
    namespacesRequired: ['exchange', 'banner']
  }
}

export default withNamespaces('exchange')(metamaskChecker(ExchangePage))
