import Page from '../components/Page'
import Router from 'next/router'
import { withNamespaces } from '../utils/i18n'

import Heading from '../components/Heading'
import Flex from '../components/Flex'
import Button from '../components/Button'
import { withAdmin } from '../utils/withAdmin'

function Login({ t, lng }) {
  return (
    <Page rtl={lng === 'ar'} layout="login" withoutHeader>
      <Flex direction="column" align="center">
        <Heading.One>{t('title')}</Heading.One>
        <img src="/static/img/metamask-logo.svg" alt="" />
        <br />
        <br />
        <p>You must be logged in as admin in metamask</p>
        <br />
        <br />
        <Button
          text="Go to admin page"
          theme="primary"
          size="lg"
          onClick={() => Router.push('/admin')}
        />
        <br />
        <br />
      </Flex>
    </Page>
  )
}

Login.getInitialProps = async () => {
  return {
    namespacesRequired: ['login']
  }
}

export default withNamespaces('login')(withAdmin(Login))
