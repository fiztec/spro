import Router from 'next/router'
import Text from '../Text'
import * as Styled from './style'
import Dropdown from '../Dropdown'
import { ProfileProps } from './types'

function Profile() {
  const user: ProfileProps = {
    name: 'Admin',
    publicAddress: window.web3.eth.coinbase
  }

  const { name, publicAddress } = user

  const dropdown = [
    {
      image: '/static/img/metamask-logo.svg',
      label: publicAddress,
      disabled: true,
      onClick: () => {}
    },
    {
      icon: 'exit_to_app',
      label: 'Quit',
      onClick: () => Router.push('/trade')
    }
  ]

  return (
    <Styled.Wrapper>
      <Styled.Profile>
        <Text weight="medium">
          Welcome, <span>{name}</span>
        </Text>
      </Styled.Profile>

      <Dropdown list={dropdown} />
    </Styled.Wrapper>
  )
}

export default Profile
