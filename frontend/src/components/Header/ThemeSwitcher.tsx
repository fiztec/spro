import { inject, observer } from 'mobx-react'
import * as Styled from './style'
import Text from '../Text'
import Switcher from '../Switcher'

export default inject('themeStore')(
  observer(({ themeStore: { theme, setTheme } }) => {
    const onChangeTheme = () => {
      if (theme === 'night') {
        setTheme('day')
      } else {
        setTheme('night')
      }
    }

    return (
      <Styled.ThemeSwitcher>
        <img src="/static/img/night_mode.svg" />
        <Text weight="medium">Nightmode</Text>
        <Switcher active={theme === 'night'} onClick={onChangeTheme} />
      </Styled.ThemeSwitcher>
    )
  })
)
