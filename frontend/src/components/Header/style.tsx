import styled, { css } from 'styled-components'
import { colors } from '../../styles/colors'
import { flex, time } from '../../styles/base'
import Container from '../Container'
import { Button } from '../Button/style'
import Text from '../Text'
import { NavList } from '../Nav/style'
import { Logo } from '../Logo/style'
import { HeaderProps } from './types'

export const ThemeSwitcher = styled('div')`
  ${flex.center};
  margin: 0 1rem;
  padding: 0.25rem 1rem;
  height: 34px;
  background: ${colors.white}20;
  border-radius: 5px;

  ${Text} {
    margin-bottom: 0;
    margin-right: 0.5rem;
  }

  img {
    margin-right: 0.5rem;
  }
`

export const WalletTitle = styled('p')`
  color: ${colors.white}80;
  font-weight: 500;
  margin-bottom: 0;
  margin-right: 1rem;
`

export const WalletType = styled('p')`
  color: ${colors.white};
  font-weight: 600;
  margin-bottom: 0;
  margin-right: 0.5rem;
`

export const Wallet = styled('div')`
  ${flex.center};
  margin-left: 1rem;

  a {
    text-decoration: none;
    p {
      transition: color ${time.fast};
    }

    &:hover {
      p {
        color: ${colors.primary} !important;
      }
    }
  }
`

export const Header = styled('header')<HeaderProps>`
  min-height: 80px;
  display: flex;
  align-items: center;
  padding: 1rem 0;

  ${Button} {
    padding-left: 1rem;
    padding-right: 1rem;
  }

  @media only screen and (max-width: 992px) {
    ${NavList} {
      ${ThemeSwitcher},
      ${Button} {
        margin-bottom: 1rem;
      }
    }

    ${Logo} {
      position: absolute;
      left: 4rem;
      top: 23px;
    }

    ${({ withProfile }) =>
      withProfile &&
      css`
        ${Logo} {
          position: relative;
          left: 0;
          top: 0;
        }

        ${Container} {
          flex-direction: row;
        }
      `}
  }
`
