import styled, { css } from 'styled-components'
import { colors } from '../../styles/colors'
import * as InputStyle from '../Input/style'

export const Label = styled('span')`
  color: ${colors.text};
  font-size: 0.75rem;
  font-weight: 500;
  display: flex;
  margin-bottom: 0.25rem;
  pointer-events: none;
`

export const Error = styled('span')`
  color: ${colors.primary};
  font-size: 0.75rem;
  text-align: right;
  width: 100%;
  flex: 1;
`

export const Toggler = styled('button')`
  padding: 0.5rem 3rem 0.5rem 1rem;
  min-height: 42px;
  border: 0;
  width: 100%;
  color: ${colors.white};
  background: ${colors.darkLight};
  border-radius: 4px;
  position: relative;
  text-align: left;
  font-weight: 500;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;

  &:focus {
    outline: none;
  }

  ${({ open }: { open: boolean }) =>
    open &&
    css`
      background: ${colors.hover.darkLight};
    `}
`

export const Caret = styled('span')`
  position: absolute;
  right: 1rem;
`

export const AddonDivider = styled('span')`
  border-right: 1px solid ${colors.text};
  display: inline-block;
  height: 30px;
`

export const Addon = styled('div')`
  /* max-width: 180px; */
`

export const ListItem = styled('li')`
  list-style-type: none;
  padding: 0.5rem 0;
  font-weight: 500;
  cursor: pointer;

  &:hover {
    color: ${colors.primary};
  }

  ${({ active }: { active: boolean }) =>
    active &&
    css`
      font-weight: 600;
      color: ${colors.primary};
    `}
`

export const List = styled('ul')`
  padding: 0.5rem 0;
  margin: 0;
  height: 100%;
  max-height: 215px;
  overflow-y: auto;
`

export const Dropdown = styled('div')`
  position: absolute;
  left: 0;
  top: calc(100% - 2px);
  right: 0;
  background: ${colors.hover.darkLight};
  border-radius: 0 0 4px 4px;
  border-top: 1px solid ${colors.text};
  padding: 0 1rem;
  z-index: 2;

  ${InputStyle.Wrapper} {
    margin: 0;
    width: 100%;
    padding: 1rem 0;

    ${InputStyle.Input} {
      width: 100%;
      padding: 0.35rem 1rem 0.35rem 2rem;
      background-color: ${colors.white}10;
    }
  }
`

export const Select = styled('div')`
  position: relative;
  background: ${colors.darkLight};
  border-radius: 4px;
  display: flex;
  align-items: center;

  ${({ open }: { open: boolean }) =>
    open &&
    css`
      background: ${colors.hover.darkLight};
    `}

  ${InputStyle.Wrapper} {
    margin-bottom: 0;
  }

  ${InputStyle.Input} {
    background: transparent;
  }

  ${InputStyle.Background} {
    background: transparent;
  }

  ${({ size }: any) => {
    if (size === 'sm') {
      return css`
        ${Toggler} {
          min-height: 30px;
          padding: 0.125rem 0.5rem;
        }

        ${ListItem} {
          padding: 0.125rem 0;
        }
      `
    }
  }}
`

export const Wrapper = styled('label')`
  position: relative;
  width: 100%;
`
