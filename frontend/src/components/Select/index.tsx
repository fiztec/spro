import * as Styled from './style'
import { PureComponent, createRef } from 'react'
import { EventChangeOptions } from 'next/router'
import Input from '../Input'
import { SelectProps, SelectState } from './types'
import { bind } from '../../utils/bind'
import Flex from '../Flex'

export default class Select extends PureComponent<SelectProps, SelectState> {
  select: {
    current: HTMLDivElement
  } = createRef<HTMLDivElement>()

  state = {
    open: false,
    searchTerm: ''
  }

  public componentDidMount() {
    window.addEventListener('click', this.handleClickOutside, true)
  }

  public componentWillUnmount() {
    window.removeEventListener('click', this.handleClickOutside, true)
  }

  @bind private handleClickOutside(event: EventChangeOptions) {
    const element = this.select
    if (!element.current.contains(event.target)) {
      this.handleToggleSelect(false)
    }
  }

  @bind private handleToggleSelect(open: boolean) {
    this.setState({ open })
  }

  @bind private handleSetValue(selected: SelectProps['selected']) {
    this.handleToggleSelect(false)
    this.setState({ searchTerm: '' })
    this.props.onSelect && this.props.onSelect(selected)
  }

  @bind private onSearch(searchTerm: string) {
    this.setState({ searchTerm })
    // this.props.onSearch && this.props.onSearch(searchTerm)
  }

  public render() {
    const { open, searchTerm } = this.state
    const {
      selected,
      items,
      addon,
      search,
      caret,
      size,
      label,
      error
    } = this.props
    const selectedObject = items.find(({ id }) => id === selected)
    const selectedLabel = selectedObject && selectedObject.label

    const filteredItems = searchTerm
      ? [...items].filter(item => {
          return (
            item.label.toLowerCase().search(searchTerm.toLowerCase()) !== -1
          )
        })
      : items

    return (
      <Styled.Wrapper>
        <Flex justify="space-between">
          {label && typeof label === 'string' ? (
            <Styled.Label>{label}</Styled.Label>
          ) : (
            label
          )}
          {error && <Styled.Error>{error}</Styled.Error>}
        </Flex>
        <Styled.Select ref={this.select} open={open} size={size}>
          {addon && (
            <>
              <Styled.Addon onFocus={() => this.handleToggleSelect(false)}>
                {addon}
              </Styled.Addon>
              <Styled.AddonDivider />
            </>
          )}

          <Styled.Toggler
            open={open}
            onClick={() => this.handleToggleSelect(!open)}
          >
            {selectedLabel || items[0].label}
            {caret !== false && (
              <Styled.Caret className="material-icons">
                {open ? 'keyboard_arrow_up' : 'keyboard_arrow_down'}
              </Styled.Caret>
            )}
          </Styled.Toggler>

          {open && (
            <Styled.Dropdown>
              {search !== false && (
                <Input
                  value={searchTerm}
                  onChange={this.onSearch}
                  name="search"
                  size="sm"
                  icon="search"
                />
              )}
              <Styled.List>
                {filteredItems.map(({ id, label }, i) => (
                  <Styled.ListItem
                    key={i}
                    active={selected === id}
                    onClick={() => this.handleSetValue(id)}
                  >
                    {label}
                  </Styled.ListItem>
                ))}
              </Styled.List>
            </Styled.Dropdown>
          )}
        </Styled.Select>
      </Styled.Wrapper>
    )
  }
}
