import { observer, inject } from 'mobx-react'
import * as Styled from './style'
import { getColor } from '../../utils/colorByValue'
import { TableProps, TableRowProps } from './types'
import Button from '../Button'

function isOverflown(el) {
  return el.scrollWidth > el.clientWidth
}

function TableComponent({
  head,
  body,
  width,
  align,
  children,
  layout,
  headFilled,
  themeStore: { theme },
  deleteButton,
  onRowClick
}: TableProps & TableRowProps) {
  return (
    <Styled.Table theme={theme}>
      {Array.isArray(head) && (
        <Styled.Head>
          <Styled.Row align={align} layout={layout} headFilled={headFilled}>
            {head.map((label: string, i: number) => (
              <Styled.Col width={width && width[i]} key={i}>
                {label}
              </Styled.Col>
            ))}
          </Styled.Row>
        </Styled.Head>
      )}

      {children}
      {Array.isArray(body) && (
        <Styled.Body>
          {body.map((row: any, rowIdx: number) => {
            return (
              <Styled.Row
                key={row.id || rowIdx}
                align={align}
                layout={layout}
                deleteButton={deleteButton}
                onClick={() => (onRowClick ? onRowClick(row) : {})}
              >
                {Object.values(row).map((col: any, colIdx) => (
                  <Styled.Col
                    key={colIdx}
                    width={width[colIdx]}
                    color={getColor(col)}
                    onMouseOver={(e: any) => {
                      const el = e.target
                      if (isOverflown(el)) {
                        el.classList.add('tooltip')
                        el.setAttribute('tooltip', col)
                      }
                    }}
                    onMouseLeave={(e: any) => {
                      const el = e.target
                      el.classList.remove('tooltip')
                      el.removeAttribute('tooltip', col)
                    }}
                  >
                    {typeof col === 'function' ? col() : col}
                  </Styled.Col>
                ))}
                {deleteButton && row.status === 'PENDING' && (
                  <Button
                    text={deleteButton.text}
                    onClick={() => deleteButton.onClick(row.id)}
                    size="xsm"
                  />
                )}
              </Styled.Row>
            )
          })}
        </Styled.Body>
      )}
    </Styled.Table>
  )
}

export const Table = inject('themeStore')(observer(TableComponent))

export const TableRow = Styled.Row
export const TableCol = Styled.Col
export const TableBody = Styled.Body
