import { ReactNode } from 'react'
import { ThemeStoreTypes } from '../../stores/theme/types'

export type TableRowProps = {
  active?: boolean
  onRowClick?: (row: any) => void
  width?: string[]
  align?: string
  layout?: string
  headFilled?: boolean
  themeStore?: ThemeStoreTypes
  deleteButton?: {
    text: string
    onClick: (data: any) => void
  }
}

export type TableProps = {
  head?: string[]
  body?: ReactNode
  children?: ReactNode
}
