import styled, { css } from 'styled-components'
import { Button } from './Button/style'

type FlexProps = {
  justify?: string
  align?: string
  direction?: string
}

const Flex = styled('div')<FlexProps>`
  display: flex;

  ${({ justify }) =>
    justify &&
    css`
      justify-content: ${justify};
    `};

  ${({ align }) =>
    align &&
    css`
      align-items: ${align};
    `};

  ${({ direction }) =>
    direction &&
    css`
      flex-direction: ${direction};
    `};

  > ${Button} + ${Button} {
    margin-left: 1rem;
  }
`

export default Flex
