import { memo } from 'react'
import * as Styled from './style'
import { InputProps } from './types'
import Flex from '../Flex'

function Input({
  label,
  error,
  icon,
  addon,
  type,
  name,
  rtl,
  ...rest
}: InputProps) {
  const addonType = typeof addon === 'string' ? 'text' : 'button'

  const onChange = ({ target: { value, name } }) => {
    rest.onChange && rest.onChange(value, name)
  }

  const onKeyPress = (event: { which: number; preventDefault: () => void }) => {
    if (type === 'number') {
      if (
        (event.which != 8 && event.which != 0 && event.which < 46) ||
        event.which > 57
      ) {
        event.preventDefault()
      }
    }
  }

  const maxLengthNumber =
    rest.value && rest.value.toString().split('.').length - 1 + 18

  return (
    <Styled.Wrapper fluid={rest.fluid}>
      <Flex justify="space-between">
        {label && typeof label === 'string' ? (
          <Styled.Label>{label}</Styled.Label>
        ) : (
          label
        )}
        {error && <Styled.Error>{error}</Styled.Error>}
      </Flex>
      <Styled.AddonGroup>
        {typeof icon === 'string' ? (
          <i className="material-icons">{icon}</i>
        ) : (
          icon
        )}

        {type === 'file' ? (
          <>
            <Styled.Input
              {...rest}
              type="file"
              name={name}
              onChange={onChange}
              onKeyPress={onKeyPress}
              error={error}
            />
            <Styled.FileButton />
          </>
        ) : (
          <Styled.Input
            {...rest}
            addon={!!addon}
            icon={!!icon}
            name={name}
            onChange={onChange}
            onKeyPress={onKeyPress}
            maxLength={type === 'number' ? maxLengthNumber : 50}
            error={error}
          />
        )}

        <Styled.Background />

        <Styled.Addon rtl={rtl} addonType={addonType}>
          {addon}
        </Styled.Addon>
      </Styled.AddonGroup>
    </Styled.Wrapper>
  )
}

export default memo(Input)
