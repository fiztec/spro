import styled from 'styled-components'
import { Card } from '../../Card/style'
import { Row } from '../../Table/style'

export const Wrapp = styled('div')`
  ${Card} {
    height: 100%;
  }

  ${Row} {
    margin-bottom: 0.25rem;
  }
`
