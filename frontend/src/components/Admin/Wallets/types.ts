import { IAdminStore } from '../../../stores/admin/types'
import { I18nTProps } from '../../../utils/i18n'

export interface IWallets {
  getWallets?: IAdminStore['getWallets']
  activeWallet?: IAdminStore['activeWallet']
  setActiveWallet?: IAdminStore['setActiveWallet']
  wallets?: IAdminStore['wallets']
  t?: I18nTProps
}
