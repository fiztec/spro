import { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { withNamespaces } from '../../../utils/i18n'
import Card from '../../Card'
import Flex from '../../Flex'
import Heading from '../../Heading'
import { Table, TableBody, TableRow, TableCol } from '../../Table'
import * as Styled from './style'
import { IWallets } from './types'
import { bind } from '../../../utils/bind'

@withNamespaces('admin')
@inject(
  ({ adminStore: { getWallets, wallets, activeWallet, setActiveWallet } }) => ({
    getWallets,
    wallets,
    activeWallet,
    setActiveWallet
  })
)
@observer
export default class Wallets extends Component<IWallets> {
  public componentDidMount() {
    this.onGetWallets()
  }

  @bind private onGetWallets() {
    this.props.getWallets()
  }

  render() {
    const { t, wallets, activeWallet, setActiveWallet } = this.props
    return (
      <Styled.Wrapp>
        <Card>
          <Flex justify="space-between">
            <Heading.Two>{t('wallets.title')}</Heading.Two>
          </Flex>
          <Table
            head={[t('wallets.table.rows', { returnObjects: true })]}
            width={['100%']}
            layout="admin"
          >
            <TableBody>
              {Array.isArray(wallets) &&
                wallets.map(({ publicAddress }) => {
                  return (
                    <TableRow
                      key={publicAddress}
                      align="center"
                      onClick={() => {
                        setActiveWallet(publicAddress)
                      }}
                      active={publicAddress === activeWallet}
                    >
                      <TableCol width="100%">
                        <div style={{ padding: '0 5px' }}>{publicAddress}</div>
                      </TableCol>
                    </TableRow>
                  )
                })}
            </TableBody>
          </Table>
        </Card>
      </Styled.Wrapp>
    )
  }
}
