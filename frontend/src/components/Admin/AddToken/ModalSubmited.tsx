import Modal from '../../Modal'
import { withNamespaces, I18nTProps } from '../../../utils/i18n'
import { colors } from '../../../styles/colors'
import Text from '../../Text'

interface ModalProps {
  t?: I18nTProps
  open?: boolean
  onClose?: () => void
}

export default withNamespaces('admin')(
  ({ t, open, onClose }: ModalProps & { weth: string }) => (
    <Modal
      open={open}
      title={
        <>
          <i
            className="material-icons"
            style={{
              fontSize: '2.5rem',
              color: colors.success,
              verticalAlign: 'text-top',
              marginRight: '0.5rem'
            }}
          >
            check
          </i>
          {t('addToken.modalSuccess.title')}
        </>
      }
      onClose={onClose}
      closeButton={t('addToken.modalSuccess.button')}
    >
      <Text weight="600">{t('addToken.modalSuccess.text')}</Text>
    </Modal>
  )
)
