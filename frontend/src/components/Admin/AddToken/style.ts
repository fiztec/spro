import styled from 'styled-components'
import { Button } from '../../Button/style'
import { flex } from '../../../styles/base'

export const InputGroup = styled('div')`
  @media (min-width: 768px) {
    ${flex.center};
  }

  > * {
    margin-bottom: 1rem;
    flex: 1;

    &:not(:last-child) {
      margin-right: 1rem;
    }
  }
`

export const Wrapp = styled('div')`
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding-bottom: 0.5rem;

  ${Button} {
    width: 100%;
    max-width: 200px;
  }

  > div {
    flex: 1;
  }
`
