import { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import Card from '../../Card'
import Input from '../../Input'
import Button from '../../Button'
import Text from '../../Text'
import Flex from '../../Flex'
import { withNamespaces } from '../../../utils/i18n'
import * as Styled from './style'
import { IAddTokenProps, IAddTokenState } from './types'
import Heading from '../../Heading'
import Select from '../../Select'
import { bind } from '../../../utils/bind'
import Modal from './ModalSubmited'
import { IToken } from '../../../stores/admin/types'

@withNamespaces('admin')
@inject(({ adminStore: { addToken } }) => ({ addToken }))
@observer
export default class AddToken extends Component<
  IAddTokenProps,
  IAddTokenState
> {
  public state = {
    status: '',
    orderListType: [
      { label: 'LIMIT', id: 1 },
      { label: 'MARKET', id: 2 },
      { label: 'MARKET, LIMIT', id: 3 }
    ]
  }

  @bind private onSubmit(values: IToken, onError: any) {
    const { addToken } = this.props

    addToken(
      {
        ...values,
        baseTokenDecimals: Number(values.baseTokenDecimals),
        amountDecimals: Number(values.amountDecimals),
        priceDecimals: Number(values.priceDecimals),
        pricePrecision: Number(values.pricePrecision)
      },
      () => {
        this.setState({ status: 'success' })
      },
      errors => {
        this.setState({ status: 'fail' })
        onError(errors)
      }
    )
  }

  public render() {
    const { orderListType } = this.state
    const { t } = this.props

    const AddTokenSchema = Yup.object().shape({
      baseTokenProjectUrl: Yup.string()
        .url('Must be a valid URL')
        .required('This field is required'),
      baseTokenName: Yup.string()
        .min(3, 'Must be at least 3 characters')
        .required('This field is required'),
      baseToken: Yup.string()
        .min(2, 'Must be at least 2 characters')
        .required('This field is required'),
      baseTokenAddress: Yup.string()
        .required('This field is required')
        .test('baseTokenAddress', 'Invalid address', value => {
          // your logic
          var isAddress = window.web3.isAddress(value)
          if (isAddress) {
            return true
          }

          return false
        }),
      baseTokenDecimals: Yup.number()
        .integer()
        .required('This field is required'),
      minOrderSize: Yup.number().required('This field is required'),
      pricePrecision: Yup.number()
        .integer()
        .required('This field is required'),
      priceDecimals: Yup.number()
        .integer()
        .required('This field is required'),
      amountDecimals: Yup.number()
        .integer()
        .required('This field is required'),
      marketOrderMaxSlippage: Yup.number().required('This field is required')
    })

    return (
      <Card>
        <Heading.Two>{t('addToken.title')}</Heading.Two>
        <Styled.Wrapp>
          <Text size="sm">{t('addToken.description')}</Text>
          <Formik
            initialValues={{
              baseTokenProjectUrl: '',
              baseTokenName: '',
              baseToken: '',
              baseTokenAddress: '',
              baseTokenDecimals: '',
              minOrderSize: '',
              pricePrecision: '',
              priceDecimals: '',
              amountDecimals: '',
              supportedOrderTypes: 'LIMIT',
              marketOrderMaxSlippage: ''
            }}
            validationSchema={AddTokenSchema}
            onSubmit={(values, { setSubmitting, setErrors }) => {
              this.onSubmit(values, (err: any) => {
                setErrors({ ...err })
              })
              setSubmitting(false)
            }}
          >
            {({
              values,
              errors,
              touched,
              handleSubmit,
              isSubmitting,
              setFieldValue
            }) => {
              const selected = orderListType.find(
                item => item.label === values.supportedOrderTypes
              ).id

              return (
                <Flex direction="column" justify="space-between">
                  <div style={{ marginTop: '3rem' }}>
                    <Styled.InputGroup>
                      <Input
                        label={t('addToken.form.baseTokenProjectUrl')}
                        name="baseTokenProjectUrl"
                        value={values.baseTokenProjectUrl}
                        onChange={(value, name) => setFieldValue(name, value)}
                        error={
                          touched.baseTokenProjectUrl &&
                          errors.baseTokenProjectUrl
                        }
                        fluid
                      />
                      <Input
                        label={t('addToken.form.baseTokenName')}
                        name="baseTokenName"
                        value={values.baseTokenName}
                        onChange={(value, name) => setFieldValue(name, value)}
                        error={touched.baseTokenName && errors.baseTokenName}
                        fluid
                      />
                    </Styled.InputGroup>

                    <Styled.InputGroup>
                      <Input
                        label={t('addToken.form.baseToken')}
                        name="baseToken"
                        value={values.baseToken}
                        onChange={(value, name) => setFieldValue(name, value)}
                        error={touched.baseToken && errors.baseToken}
                        fluid
                      />

                      <Input
                        label={t('addToken.form.baseTokenAddress')}
                        name="baseTokenAddress"
                        value={values.baseTokenAddress}
                        onChange={(value, name) => setFieldValue(name, value)}
                        error={
                          touched.baseTokenAddress && errors.baseTokenAddress
                        }
                        fluid
                      />
                    </Styled.InputGroup>

                    <Styled.InputGroup>
                      <Input
                        label={t('addToken.form.baseTokenDecimals')}
                        name="baseTokenDecimals"
                        type="number"
                        value={values.baseTokenDecimals}
                        onChange={(value, name) => setFieldValue(name, value)}
                        error={
                          touched.baseTokenDecimals && errors.baseTokenDecimals
                        }
                        fluid
                      />
                      <Input
                        label={t('addToken.form.minOrderSize')}
                        name="minOrderSize"
                        type="number"
                        value={values.minOrderSize}
                        onChange={(value, name) => setFieldValue(name, value)}
                        error={touched.minOrderSize && errors.minOrderSize}
                        fluid
                      />
                    </Styled.InputGroup>

                    <Styled.InputGroup>
                      <Input
                        label={t('addToken.form.pricePrecision')}
                        name="pricePrecision"
                        type="number"
                        value={values.pricePrecision}
                        onChange={(value, name) => setFieldValue(name, value)}
                        error={touched.pricePrecision && errors.pricePrecision}
                        fluid
                      />
                      <Input
                        label={t('addToken.form.priceDecimals')}
                        name="priceDecimals"
                        type="number"
                        value={values.priceDecimals}
                        onChange={(value, name) => setFieldValue(name, value)}
                        error={touched.priceDecimals && errors.priceDecimals}
                        fluid
                      />
                      <Input
                        label={t('addToken.form.amountDecimals')}
                        name="amountDecimals"
                        type="number"
                        value={values.amountDecimals}
                        onChange={(value, name) => setFieldValue(name, value)}
                        error={touched.amountDecimals && errors.amountDecimals}
                        fluid
                      />
                    </Styled.InputGroup>

                    <Styled.InputGroup>
                      <Select
                        label={t('addToken.form.supportedOrderTypes')}
                        items={orderListType}
                        search={false}
                        selected={selected}
                        onSelect={id =>
                          setFieldValue(
                            'supportedOrderTypes',
                            this.state.orderListType.find(
                              item => item.id === id
                            ).label
                          )
                        }
                      />
                      <Input
                        label={t('addToken.form.marketOrderMaxSlippage')}
                        name="marketOrderMaxSlippage"
                        type="number"
                        value={values.marketOrderMaxSlippage}
                        onChange={(value, name) => setFieldValue(name, value)}
                        error={
                          touched.marketOrderMaxSlippage &&
                          errors.marketOrderMaxSlippage
                        }
                        fluid
                      />
                    </Styled.InputGroup>
                  </div>
                  <Button
                    theme="primary"
                    size="lg"
                    text={t('addToken.form.button')}
                    onClick={handleSubmit}
                    disabled={isSubmitting}
                  />
                </Flex>
              )
            }}
          </Formik>
        </Styled.Wrapp>

        <Modal
          open={this.state.status === 'success'}
          onClose={() => this.setState({ status: '' })}
        />
      </Card>
    )
  }
}
