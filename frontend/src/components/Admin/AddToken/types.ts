import { I18nTProps } from '../../../utils/i18n'
import { ItemTypes } from '../../Select/types'
import { IAdminStore } from '../../../stores/admin/types'

export interface IAddTokenState {
  orderListType: ItemTypes[]
  status: string
}

export interface IAddTokenProps {
  t?: I18nTProps
  addToken?: IAdminStore['addToken']
}
