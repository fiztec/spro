import { I18nTProps } from '../../../utils/i18n'
import { IAdminStore } from '../../../stores/admin/types'

export type MainInfoTypes = {
  transaction: string
  exchange: string
}

export type MainInfoPropsTypes = {
  layout?: string
}

export interface IFeesState {
  values: MainInfoTypes
}

export interface IFeesProps {
  t?: I18nTProps
  fees?: IAdminStore['fees']
  getFees?: IAdminStore['getFees']
  changeFees?: IAdminStore['changeFees']
}
