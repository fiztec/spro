import styled, { css } from 'styled-components'
import { Button } from '../../Button/style'
import Text from '../../Text'
import {
  Wrapper,
  Input,
  AddonGroup,
  Label,
  Error,
  Background
} from '../../Input/style'
import { MainInfoPropsTypes } from './types'
import { colors } from '../../../styles/colors'
import Heading from '../../Heading'

export const InputGroup = styled('div')<MainInfoPropsTypes>`
  ${({ layout }) => {
    if (layout === 'mainInfo') {
      return css`
        ${Label} {
          position: relative;
          z-index: 10;
          top: -5px;
        }

        ${Error} {
          right: auto;
          left: 1rem;
          bottom: 0.5rem;
          position: absolute;
          z-index: 50;
          text-align: left;
          overflow: hidden;
          white-space: nowrap;
          text-overflow: ellipsis;
          width: calc(100% - 70px);
        }

        ${Input} {
          font-size: 1.875rem;
          position: absolute;
          left: 0;
          top: 0;
          right: 0;
          bottom: 0;
          width: 100%;
          height: 100%;
          padding-top: 2rem;
          font-weight: 700;

          + ${Background} {
            background: transparent;
          }
        }

        ${AddonGroup} {
          position: static;
        }

        > ${Button} {
          position: absolute;
          right: 0rem;
          bottom: 0rem;
          background: transparent;
          z-index: 60;
        }
      `
    }

    if (layout === 'wallet') {
      return css`
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        height: calc(100% - 40px);

        > div:nth-of-type(1) {
          margin: auto 0;
        }

        > ${Button} {
          position: absolute;
          bottom: auto;
          right: 1rem;
          top: 1rem;
          background: ${colors.white}20;
        }

        ${Input} {
          overflow: hidden;
          text-overflow: ellipsis;
        }

        ${Text} {
          margin-bottom: 0;
        }

        ${Heading.One} {
          margin-bottom: 0.25rem;
          color: ${colors.primary};
          font-size: 2.5rem;
        }

        ${Heading.Three} {
          font-weight: 500;
        }
      `
    }

    if (layout === 'bot') {
      return css`
        display: flex;

        ${Wrapper}:not(:last-child) {
          margin-right: 1rem;
        }

        ${Input} {
          width: 100%;
        }
      `
    }
  }}
`
