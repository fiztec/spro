import { Component } from 'react'
import { Formik } from 'formik'
import isEmpty from 'lodash/isEmpty'
import { inject, observer } from 'mobx-react'
import Grid from '../../Grid'
import Card from '../../Card'
import Input from '../../Input'
import Button from '../../Button'
import { withNamespaces } from '../../../utils/i18n'
import * as Styled from './style'
import { IFeesProps, IFeesState } from './types'
import FeesWallets from '../FeesWallets'
import { bind } from '../../../utils/bind'
import * as Yup from 'yup'
import { IAdminStore } from '../../../stores/admin/types'

@withNamespaces('admin')
@inject(({ adminStore: { fees, getFees, changeFees } }) => ({
  fees,
  getFees,
  changeFees
}))
@observer
export default class Fees extends Component<IFeesProps, IFeesState> {
  public componentDidMount() {
    this.props.getFees()
  }

  @bind private onSubmit(values: Partial<IAdminStore['fees']>) {
    this.props.changeFees(values)
  }

  public render() {
    const { t, fees } = this.props

    const asMakerFeeRateSchema = Yup.object().shape({
      asMakerFeeRate: Yup.number()
        .typeError('Invalid value')
        .min(0.001, 'Must be greater than 0.001')
        .max(10, 'Must be less than 10')
        .required('This field is required')
    })

    const asTakerFeeRateSchema = Yup.object().shape({
      asTakerFeeRate: Yup.number()
        .typeError('Invalid value')
        .min(0.001, 'Must be greater than 0.001')
        .max(10, 'Must be less than 10')
        .required('This field is required')
    })

    if (fees) {
      return (
        <Grid layout="main-info">
          <Formik
            initialValues={fees}
            validationSchema={asMakerFeeRateSchema}
            onSubmit={(
              values,
              { setSubmitting, setFieldTouched, resetForm }
            ) => {
              this.onSubmit({ asMakerFeeRate: values.asMakerFeeRate })
              setSubmitting(false)

              if (values.asMakerFeeRate === fees.asMakerFeeRate) {
                setFieldTouched('asMakerFeeRate', false)
              }

              resetForm(values)
            }}
          >
            {({
              values,
              setFieldValue,
              setFieldTouched,
              touched,
              errors,
              handleSubmit,
              setFieldError
            }) => {
              return (
                <Card>
                  <Styled.InputGroup layout="mainInfo">
                    <Input
                      type="number"
                      label={t('mainInfo.asMakerFeeRate.label')}
                      name="asMakerFeeRate"
                      value={values.asMakerFeeRate}
                      error={touched.asMakerFeeRate && errors.asMakerFeeRate}
                      onChange={(value, name) => {
                        const decimal = value.toString().split('.')[1]
                        if (decimal && decimal.length !== 3) {
                          setTimeout(() => {
                            setFieldError(
                              name,
                              'Must contain 3 digits after comma'
                            )
                          }, 0)
                        }
                        setFieldTouched(name, true)
                        setFieldValue(name, value)
                      }}
                      theme="transparent"
                    />
                    {touched.asMakerFeeRate && (
                      <Button
                        size="sm"
                        theme="primary"
                        transparent
                        type="submit"
                        text={t('mainInfo.buttons.save')}
                        onClick={handleSubmit}
                        disabled={!isEmpty(errors)}
                      />
                    )}
                  </Styled.InputGroup>
                </Card>
              )
            }}
          </Formik>

          <Formik
            initialValues={fees}
            validationSchema={asTakerFeeRateSchema}
            onSubmit={(
              values,
              { setSubmitting, setFieldTouched, resetForm }
            ) => {
              this.onSubmit({ asTakerFeeRate: values.asTakerFeeRate })
              setSubmitting(false)

              if (values.asTakerFeeRate === fees.asTakerFeeRate) {
                setFieldTouched('asTakerFeeRate', false)
              }

              resetForm(values)
            }}
          >
            {({
              values,
              setFieldValue,
              setFieldTouched,
              touched,
              errors,
              handleSubmit,
              setFieldError
            }) => {
              return (
                <Card>
                  <Styled.InputGroup layout="mainInfo">
                    <Input
                      type="number"
                      label={t('mainInfo.asTakerFeeRate.label')}
                      name="asTakerFeeRate"
                      value={values.asTakerFeeRate}
                      error={touched.asTakerFeeRate && errors.asTakerFeeRate}
                      onChange={(value, name) => {
                        const decimal = value.toString().split('.')[1]
                        if (decimal && decimal.length !== 3) {
                          setTimeout(() => {
                            setFieldError(
                              name,
                              'Must contain 3 digits after comma'
                            )
                          }, 0)
                        }
                        setFieldTouched(name, true)
                        setFieldValue(name, value)
                      }}
                      theme="transparent"
                    />
                    {touched.asTakerFeeRate && (
                      <Button
                        size="sm"
                        theme="primary"
                        type="submit"
                        transparent
                        text={t('mainInfo.buttons.save')}
                        onClick={handleSubmit}
                        disabled={!isEmpty(errors)}
                      />
                    )}
                  </Styled.InputGroup>
                </Card>
              )
            }}
          </Formik>

          <FeesWallets />
        </Grid>
      )
    }

    return (
      <Grid layout="main-info">
        <Card>{}</Card>
        <Card>{}</Card>
      </Grid>
    )
  }
}
