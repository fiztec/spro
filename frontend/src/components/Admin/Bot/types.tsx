import { I18nTProps } from '../../../utils/i18n'
import { MarketsStoreTypes } from '../../../stores/markets/types'
import { IBotStore } from '../../../stores/bot/types'

export interface IBot extends Partial<MarketsStoreTypes>, Partial<IBotStore> {
  t?: I18nTProps
}
