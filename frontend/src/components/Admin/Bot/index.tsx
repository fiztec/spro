import { Component } from 'react'
import { inject, observer } from 'mobx-react'
import isEmpty from 'lodash/isEmpty'
import * as Yup from 'yup'
import Card from '../../Card'
import { Formik } from 'formik'
import Input from '../../Input'
import { InputGroup, Wrapp } from './style'
import Grid from '../../Grid'
import { bind } from '../../../utils/bind'
import { Table, TableRow, TableBody, TableCol } from '../../Table'
import { withNamespaces } from '../../../utils/i18n'
import { IBot } from './types'
import Flex from '../../Flex'
import Text from '../../Text'
import Button from '../../Button'
import { getColor } from '../../../utils/colorByValue'
import Heading from '../../Heading'
import ModalError from './Modal/Error'

@withNamespaces('admin')
@inject(
  ({
    marketsStore: { markets, getMarkets, selectMarket, selectedMarket },
    botStore: { getBot, bot, startBot, stopBot, error, setError, status }
  }) => ({
    markets,
    getMarkets,
    selectMarket,
    selectedMarket,
    getBot,
    bot,
    startBot,
    stopBot,
    error,
    setError,
    status
  })
)
@observer
export default class Bot extends Component<IBot> {
  width = ['30%', '25%', '25%', '20%']

  state = {
    modalOpen: null
  }

  public componentDidMount() {
    this.getMarkets()
  }

  public componentWillReact() {
    const {
      markets,
      selectMarket,
      selectedMarket,
      getBot,
      bot,
      error
    } = this.props

    const { modalOpen } = this.state

    if (error && modalOpen !== 'close') {
      this.handleToggleModal('modalError')
    }

    if (!isEmpty(markets) && isEmpty(selectedMarket)) selectMarket(markets[10])

    if (selectedMarket && !bot) {
      getBot(selectedMarket.id)
    }
  }

  public componentDidUpdate(np: IBot) {
    const { selectedMarket, selectMarket, markets, getBot } = this.props

    if (
      !isEmpty(np.selectedMarket) &&
      np.selectedMarket.id !== selectedMarket.id
    ) {
      selectMarket(
        markets.find(
          m => m.id === (!isEmpty(selectedMarket) ? +selectedMarket.id : 1)
        )
      )
      getBot(selectedMarket.id)
    }
  }

  @bind private getMarkets() {
    const { getMarkets } = this.props
    getMarkets()
  }

  @bind private onSubmit(
    { minPrice, maxPrice, priceGap, expandInventory },
    onError
  ) {
    const { selectedMarket, bot, startBot, stopBot } = this.props

    const data = {
      minPrice: +minPrice,
      maxPrice: +maxPrice,
      priceGap: +priceGap,
      expandInventory: +expandInventory
    }

    if (bot.isRunning) {
      stopBot(selectedMarket.id)
    } else {
      startBot(selectedMarket.id, data)
    }

    onError && onError()
  }

  @bind private handleToggleModal(modalOpen: string) {
    this.setState(() => ({ modalOpen }))
  }

  public render() {
    const {
      t,
      selectedMarket,
      markets = [],
      selectMarket,
      bot,
      error,
      setError,
      status
    } = this.props
    const { modalOpen } = this.state
    const initialValues = {
      minPrice: !isEmpty(bot) ? bot.minPrice : '',
      maxPrice: !isEmpty(bot) ? bot.maxPrice : '',
      priceGap: !isEmpty(bot) ? bot.priceGap : '',
      expandInventory: !isEmpty(bot) ? bot.expandInventory : ''
    }

    const startBotSchema = Yup.object().shape({
      minPrice: Yup.number()
        .typeError('Must be a number')
        .required('Field is required'),
      maxPrice: Yup.number()
        .typeError('Must be a number')
        .required('Field is required'),
      priceGap: Yup.number()
        .typeError('Must be a number')
        .required('Field is required'),
      expandInventory: Yup.number()
        .typeError('Must be a number')
        .integer('Must be integer')
        .required('Field is required')
    })

    return (
      <>
        <Grid layout="bot">
          <Card>
            <Flex
              style={{ minHeight: isEmpty(bot) ? '100%' : 0 }}
              justify="center"
              direction="column"
            >
              {isEmpty(bot) ? (
                <Heading.Two>{t('bot.connect')}</Heading.Two>
              ) : (
                <>
                  <Text
                    size="xs"
                    color="text"
                    weight="medium"
                    style={{ marginBottom: 0 }}
                  >
                    {t('bot.name')}
                  </Text>
                  <Text>{bot.botname}</Text>
                  <Text
                    size="xs"
                    color="text"
                    weight="medium"
                    style={{ marginBottom: 0 }}
                  >
                    {t('bot.publicAddress')}
                  </Text>
                  <Text
                    style={{ overflow: 'hidden', textOverflow: 'ellipsis' }}
                  >
                    {bot.publicAddress}
                  </Text>
                </>
              )}
            </Flex>

            {!isEmpty(bot) && (
              <Formik
                initialValues={initialValues}
                validationSchema={startBotSchema}
                onSubmit={(values, { setSubmitting, setErrors }) => {
                  this.onSubmit(values, (err: any) => {
                    setErrors({ ...err })
                  })
                  setSubmitting(false)
                }}
              >
                {({
                  values,
                  setFieldValue,
                  setFieldTouched,
                  touched,
                  errors,
                  handleSubmit
                }) => {
                  return (
                    <form onSubmit={handleSubmit}>
                      <InputGroup layout="bot">
                        <Input
                          label={
                            touched.minPrice && errors.minPrice
                              ? '\u2002'
                              : 'Min price'
                          }
                          name="minPrice"
                          value={values.minPrice}
                          type="number"
                          onChange={(value, name) => {
                            setFieldValue(name, value)
                            setFieldTouched(name, true)
                          }}
                          error={touched.minPrice && errors.minPrice}
                          theme="transparent"
                        />
                        <Input
                          label={
                            touched.maxPrice && errors.maxPrice
                              ? '\u2002'
                              : 'Max price'
                          }
                          name="maxPrice"
                          value={values.maxPrice}
                          type="number"
                          onChange={(value, name) => {
                            setFieldValue(name, value)
                            setFieldTouched(name, true)
                          }}
                          error={touched.maxPrice && errors.maxPrice}
                          theme="transparent"
                        />
                      </InputGroup>
                      <InputGroup layout="bot">
                        <Input
                          label={
                            touched.priceGap && errors.priceGap
                              ? '\u2002'
                              : 'Price gap'
                          }
                          name="priceGap"
                          value={values.priceGap}
                          type="number"
                          onChange={(value, name) => {
                            setFieldValue(name, value)
                            setFieldTouched(name, true)
                          }}
                          error={touched.priceGap && errors.priceGap}
                          theme="transparent"
                        />
                        <Input
                          label={
                            touched.expandInventory && errors.expandInventory
                              ? '\u2002'
                              : 'Expand inventory'
                          }
                          name="expandInventory"
                          value={values.expandInventory}
                          type="number"
                          onChange={(value, name) => {
                            setFieldValue(name, value)
                            setFieldTouched(name, true)
                          }}
                          error={
                            touched.expandInventory && errors.expandInventory
                          }
                          theme="transparent"
                        />
                      </InputGroup>
                      <br />
                      <Flex justify="space-between">
                        {bot.isRunning ? (
                          <Button
                            theme="primary"
                            text="Stop"
                            onClick={() => {}}
                            type="submit"
                            style={{ flex: 1 }}
                            disabled={status === 'request'}
                          />
                        ) : (
                          <Button
                            theme="success"
                            text="Start"
                            onClick={() => {}}
                            type="submit"
                            style={{ flex: 1 }}
                            disabled={status === 'request'}
                          />
                        )}
                      </Flex>
                    </form>
                  )
                }}
              </Formik>
            )}
          </Card>
          <Card title={t('bot.markets.title')}>
            <Wrapp>
              <Table
                head={t('bot.markets.table.rows', { returnObjects: true })}
                width={this.width}
              >
                <TableBody>
                  {Array.isArray(markets) &&
                    markets.map(({ id, tokens, price, dayVol, dayPrice }) => {
                      return (
                        <TableRow
                          key={id}
                          align="center"
                          onClick={() => {
                            selectMarket(
                              markets.find(market => market.id === id)
                            )
                          }}
                          active={selectedMarket && selectedMarket.id === id}
                        >
                          <TableCol width={this.width && this.width[0]}>
                            <div
                              style={{
                                overflow: 'hidden',
                                textOverflow: 'ellipsis'
                              }}
                            >
                              {tokens}
                            </div>
                          </TableCol>
                          <TableCol width={this.width && this.width[1]}>
                            {price}
                          </TableCol>
                          <TableCol width={this.width && this.width[2]}>
                            {dayVol}
                          </TableCol>
                          <TableCol
                            width={this.width && this.width[3]}
                            color={getColor(dayPrice)}
                          >
                            {dayPrice}
                          </TableCol>
                        </TableRow>
                      )
                    })}
                </TableBody>
              </Table>
            </Wrapp>
          </Card>
        </Grid>
        <ModalError
          open={modalOpen === 'modalError'}
          error={error}
          onClose={() => {
            this.handleToggleModal('close')
            setTimeout(() => {
              setError(null)
              this.handleToggleModal(null)
            }, 300)
          }}
        />
      </>
    )
  }
}
