import { inject, observer } from 'mobx-react'
import { Component } from 'react'
import { withNamespaces } from '../../../utils/i18n'
import Card from '../../Card'
import Flex from '../../Flex'
import Heading from '../../Heading'
import { Table } from '../../Table'
import Tabs from '../../Tabs'
import * as Styled from './style'
import { bind } from '../../../utils/bind'
import { ITransaction } from '../../../stores/admin/types'
import { ITransactionProps } from './types'

@withNamespaces('admin')
@inject(
  ({ adminStore: { getTransactionList, transactions, activeWallet } }) => ({
    getTransactionList,
    transactions,
    activeWallet
  })
)
@observer
export default class Transactions extends Component<ITransactionProps> {
  width = ['25%', '15%', '20%', '20%', '20%']
  state = {
    changeWallet: false
  }

  public componentWillReact() {
    const { activeWallet } = this.props
    const { changeWallet } = this.state

    if (activeWallet && !changeWallet) {
      this.props.getTransactionList(activeWallet)
      this.setState(() => ({ changeWallet: true }))
    }
  }

  public componentDidUpdate(np) {
    if (np.activeWallet !== this.props.activeWallet) {
      this.props.getTransactionList(this.props.activeWallet)
    }
  }

  @bind private renderTabContent(body: ITransaction[]) {
    const { t } = this.props
    return (
      <Table
        head={t('transactions.table.rows', { returnObjects: true })}
        width={this.width}
        body={body}
        layout="admin"
      />
    )
  }

  public render() {
    const { t, transactions } = this.props

    const tabs = t('transactions.tabs', { returnObjects: true })
    const transactionsList = transactions.map(({ takerWallet, ...rest }) => ({
      ...rest
    }))

    return (
      <Card>
        <Styled.Wrapper>
          <Flex justify="space-between">
            <Heading.Two>{t('transactions.title')}</Heading.Two>
          </Flex>

          <Tabs
            defaultTab="all"
            tabLink={
              Array.isArray(tabs) &&
              tabs.map((label: string) => ({
                name: label.toLowerCase().replace(' ', '_'),
                label
              }))
            }
            tabContent={[
              {
                name: 'sell',
                content: this.renderTabContent(
                  transactionsList.filter(({ side }) => side === 'SELL')
                )
              },
              {
                name: 'buy',
                content: this.renderTabContent(
                  transactionsList.filter(({ side }) => side === 'BUY')
                )
              },
              {
                name: 'exchange',
                content: this.renderTabContent(
                  transactionsList.filter(({ side }) => side === 'EXCHANGE')
                )
              },
              {
                name: 'all',
                content: this.renderTabContent(transactionsList)
              }
            ]}
          />
        </Styled.Wrapper>
      </Card>
    )
  }
}
