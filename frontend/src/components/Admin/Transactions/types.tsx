import { I18nTProps } from '../../../utils/i18n'
import { IAdminStore } from '../../../stores/admin/types'

export interface ITransactionProps {
  t?: I18nTProps
  transactions?: IAdminStore['transactions']
  getTransactionList?: IAdminStore['getTransactionList']
  activeWallet?: IAdminStore['activeWallet']
}
