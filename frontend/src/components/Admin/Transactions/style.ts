import styled from 'styled-components'
import { Body } from '../../Table/style'

export const Wrapper = styled('div')`
  ${Body} {
    height: 240px;
  }
`
