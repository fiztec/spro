import { IAdminStore } from '../../../stores/admin/types'
import { I18nTProps } from '../../../utils/i18n'

export interface IFeesWalletProps {
  t?: I18nTProps
  getFeesWallet?: IAdminStore['getFeesWallet']
  feesWallet?: IAdminStore['feesWallet']
}

export interface IFeesWalletState {
  disabled: boolean
  publicAddress: string
}
