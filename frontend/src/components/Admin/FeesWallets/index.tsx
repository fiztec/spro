import { Component } from 'react'
import { inject, observer } from 'mobx-react'
import Card from '../../Card'
import Heading from '../../Heading'
import Text from '../../Text'
import * as Styled from '../Fees/style'
import Button from '../../Button'
import Input from '../../Input'
import { copyToClipboard } from '../../../utils/copyToClipboard'
import { bind } from '../../../utils/bind'
import { withNamespaces } from '../../../utils/i18n'
import { IFeesWalletProps, IFeesWalletState } from './types'

@withNamespaces('admin')
@inject(({ adminStore: { getFeesWallet, feesWallet } }) => ({
  getFeesWallet,
  feesWallet
}))
@observer
export default class FeesWallets extends Component<
  IFeesWalletProps,
  IFeesWalletState
> {
  public state = {
    publicAddress: '',
    disabled: true
  }

  public componentDidMount() {
    this.props.getFeesWallet()
  }

  public componentWillReact() {
    this.setState({
      publicAddress: this.props.feesWallet.wallet
    })
  }

  @bind private handleToggleDisabled(disabled: boolean) {
    this.setState({ disabled })
  }

  @bind private handleChange(publicAddress: string) {
    this.setState({ publicAddress })
  }

  public render() {
    const {
      t,
      feesWallet: { feesBalance, feesBalanceUSD }
    } = this.props
    const { disabled, publicAddress } = this.state

    return (
      <Card>
        <Heading.Two>{t('mainInfo.wallet.title')}</Heading.Two>
        <Styled.InputGroup layout="wallet">
          {/* <Button
            icon="edit"
            theme="default"
            size="sm"
            onClick={() => this.handleToggleDisabled(!disabled)}
          /> */}
          <div>
            <Text size="xs" color="text" weight="medium">
              {t('mainInfo.wallet.currentBalance')}
            </Text>
            <Heading.One>{feesBalance} ETH</Heading.One>
            <Heading.Three>{feesBalanceUSD} $</Heading.Three>
          </div>
          <div>
            <Input
              label={t('mainInfo.wallet.address')}
              name="publicAddress"
              value={publicAddress}
              onChange={this.handleChange}
              fluid
              disabled={disabled}
              addon={
                disabled ? (
                  <Button
                    theme="primary"
                    transparent
                    text={t('mainInfo.buttons.copy')}
                    onClick={() => copyToClipboard(publicAddress)}
                  />
                ) : (
                  <Button
                    theme="primary"
                    transparent
                    text={t('mainInfo.buttons.save')}
                    onClick={() => this.handleToggleDisabled(true)}
                  />
                )
              }
            />
          </div>
        </Styled.InputGroup>
      </Card>
    )
  }
}
