import { withNamespaces } from '../../utils/i18n'
import Modal, { ModalStyled } from '../Modal'
import Text from '../Text'
import { colors } from '../../styles/colors'

export default withNamespaces('exchange')(
  ({ t, open, onClose, value, from, to }) => {
    return (
      <Modal open={open} title={t('modalConfirm.title')} onClose={onClose}>
        <Text weight="medium">
          {t('modalConfirm.textStart')}{' '}
          <span style={{ color: colors.primary, fontWeight: 600 }}>
            {value}
          </span>{' '}
          {t('modalConfirm.textEnd', { from, to })}
        </Text>
        <br />
        <ModalStyled.Info>{t('modalConfirm.info')}</ModalStyled.Info>
      </Modal>
    )
  }
)
