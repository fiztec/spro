import { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { Formik } from 'formik'
import { isNaN } from 'lodash'
import * as Yup from 'yup'
import * as Styled from './style'
import Heading from '../Heading'
import { withNamespaces } from '../../utils/i18n'
import Card from '../Card'
import Media from '../Media'
import Select from '../Select'
import { colors } from '../../styles/colors'
import Button from '../Button'
import ModalSuccess from './ModalSuccess'
import ModalConfirm from './ModalConfirm'
import Input from '../Input'
import { ItemTypes } from '../Select/types'
import { bind } from '../../utils/bind'
import { ExchangeProps, ExchangeState } from './types'
import ModalError from './ModalError'
import ModalVerifying from './ModalVerifying'
import { getUserToken } from '../../utils/cookieService'
import Flex from '../Flex'
import Router from 'next/router'
import BigNumber from 'bignumber.js'

export interface TokenTypes extends ItemTypes {}
const significantDigit = 4

@withNamespaces('exchange')
@inject(
  ({
    metamaskStore: { publicAddress },
    exchangeStore: {
      tokens,
      getTokens,
      exchangeTokens,
      error,
      setError,
      status,
      setStatus
    }
  }) => ({
    publicAddress,
    tokens,
    getTokens,
    exchangeTokens,
    error,
    setError,
    status,
    setStatus
  })
)
@observer
export default class Exchange extends Component<ExchangeProps, ExchangeState> {
  state = {
    give: null,
    receive: null,
    modalOpen: null
  }

  public componentDidMount() {
    const { getTokens } = this.props

    getTokens()
  }

  public componentWillReact() {
    const { tokens, error, status } = this.props

    if (status === 'exchange-request') {
      this.handleToggleModal('modalConfirm')
    }
    if (status === 'exchange-verifying') {
      this.handleToggleModal('modalVerifying')
    }
    if (status === 'exchange-success') {
      this.handleToggleModal('modalSuccess')
    }

    const { give, receive } = this.state

    if (tokens.length) {
      if (!give) {
        this.setState(() => ({
          give: {
            id: tokens[0].id,
            label: tokens[0].label,
            image: tokens[0].image
          }
        }))
      }

      if (!receive) {
        this.setState(() => ({
          receive: {
            id: tokens[1].id,
            label: tokens[1].label,
            image: tokens[1].image
          }
        }))
      }
    }

    if (error) {
      this.handleToggleModal('modalError')
    }
  }

  @bind private handleSelect(type: string, id: number) {
    this.setState(state => ({
      ...state,
      [type]: {
        ...state[type],
        id,
        label: this.props.tokens.find(item => id === item.id).label,
        image: this.props.tokens.find(item => id === item.id).image
      }
    }))
  }

  @bind private reverseTokens() {
    this.setState(({ give, receive }) => ({
      give: { ...receive },
      receive: { ...give }
    }))
  }

  @bind private handleToggleModal(modalOpen: string) {
    document.body.classList.remove('modal-open')
    this.setState({ modalOpen })
  }

  @bind private getTokenPriceForExchange(): string {
    const { tokens } = this.props
    const { give, receive } = this.state

    const g: BigNumber = new BigNumber(
      tokens.find(({ id }) => id === give.id).currentPriceToWETH
    )
    const r: BigNumber = new BigNumber(
      tokens.find(({ id }) => id === receive.id).currentPriceToWETH
    )

    return g.div(r).toPrecision(significantDigit)
  }

  @bind private onSubmit(
    values: { giveAmount: string; receiveAmount: string },
    onSuccess: () => void
  ) {
    const { give, receive } = this.state
    const { exchangeTokens } = this.props

    exchangeTokens(
      {
        ...values,
        giveTokenId: give.id,
        receiveTokenId: receive.id
      },
      give.id === 1 ? 'eth' : 'other',
      () => {
        onSuccess && onSuccess()
      }
    )
  }

  public render() {
    const { give, receive, modalOpen } = this.state
    const { t, tokens, publicAddress, error, setStatus } = this.props

    const disabled = !publicAddress

    const AddTokenSchema = Yup.object().shape({
      giveAmount: Yup.number()
        .typeError('Value must be a number')
        .required('This field is required'),
      receiveAmount: Yup.number()
        .typeError('Value must be a number')
        .required('This field is required')
    })

    const token = getUserToken()

    if (!token || !publicAddress) {
      return (
        <Styled.Wrapp>
          <Card>
            <Flex align="center" justify="center" direction="column">
              <div style={{ maxWidth: 500 }}>
                <Heading.Two align="center">{t('login.text')}</Heading.Two>
              </div>
              <br />
              <Button
                theme="primary"
                text="Connect"
                onClick={() => Router.push('/connect')}
              />
            </Flex>
          </Card>
        </Styled.Wrapp>
      )
    }

    return (
      <Styled.Wrapp>
        <Heading.One>{t('title')}</Heading.One>
        {tokens && (
          <Card>
            <Formik
              initialValues={{
                giveAmount: '',
                receiveAmount: ''
              }}
              validationSchema={AddTokenSchema}
              onSubmit={(values, { setSubmitting, resetForm }) => {
                this.onSubmit(values, () => {
                  resetForm()
                })
                setSubmitting(false)
              }}
            >
              {({
                values,
                touched,
                errors,
                setFieldValue,
                setFieldTouched,
                isSubmitting,
                handleSubmit
              }) => {
                const modalSuccessProps = {
                  value: values.giveAmount,
                  from: {
                    tokenName: give && give.label
                  },
                  to: {
                    tokenName: receive && receive.label
                  }
                }

                const modalConfirmProps = {
                  value: values.giveAmount,
                  from: {
                    tokenName: give && give.label
                  },
                  to: {
                    tokenName: receive && receive.label
                  }
                }

                const modalErrorProps = {
                  error
                }

                return (
                  <>
                    <Styled.Exchange>
                      <div>
                        {give && (
                          <>
                            <Media
                              image={give.image}
                              text={t('give')}
                              size="lg"
                              vertical
                              rounded
                            />
                            <Select
                              addon={
                                <Input
                                  onChange={(value, name) => {
                                    setFieldValue(name, value)
                                    if (
                                      !isNaN(
                                        new BigNumber(+value)
                                          .times(
                                            this.getTokenPriceForExchange()
                                          )
                                          .toPrecision(significantDigit)
                                      )
                                    ) {
                                      setFieldValue(
                                        'receiveAmount',
                                        new BigNumber(+value)
                                          .times(
                                            this.getTokenPriceForExchange()
                                          )
                                          .toPrecision(significantDigit)
                                      )
                                    }
                                    setFieldTouched(name, true)
                                  }}
                                  value={values.giveAmount}
                                  type="number"
                                  name="giveAmount"
                                  placeholder="0.00"
                                  disabled={disabled}
                                />
                              }
                              label="&nbsp;"
                              error={touched.giveAmount && errors.giveAmount}
                              items={tokens.filter(
                                ({ id }) => id !== receive.id
                              )}
                              selected={give.id}
                              onSelect={async id => {
                                await this.handleSelect('give', id)
                                await setFieldValue(
                                  'receiveAmount',
                                  new BigNumber(+values.giveAmount)
                                    .times(this.getTokenPriceForExchange())
                                    .toPrecision(significantDigit)
                                )
                              }}
                            />
                          </>
                        )}
                      </div>
                      <Styled.ExchangeDivider
                        onClick={async () => {
                          await this.reverseTokens()
                          await setFieldValue(
                            'receiveAmount',
                            new BigNumber(+values.giveAmount)
                              .times(this.getTokenPriceForExchange())
                              .toPrecision(significantDigit)
                          )
                        }}
                      >
                        <svg
                          width="51"
                          height="41"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M49.977 13.818L37.477 1.2a1.774 1.774 0 0 0-2.525 0 1.81 1.81 0 0 0-.523 1.275v5.407h-12.5c-.987 0-1.786.807-1.786 1.803 0 .995.8 1.802 1.785 1.802h14.287c.986 0 1.785-.807 1.785-1.802v-2.86l8.19 8.267L38 23.36v-2.86c0-.995-.8-1.802-1.785-1.802H16.57V13.29c0-.996-.8-1.803-1.786-1.802-.473 0-.927.19-1.262.527l-12.5 12.618a1.814 1.814 0 0 0 0 2.549l12.5 12.617a1.777 1.777 0 0 0 1.947.392 1.803 1.803 0 0 0 1.101-1.666v-5.407h12.5c.987 0 1.786-.808 1.786-1.803 0-.996-.8-1.803-1.785-1.803H14.786c-.987 0-1.786.807-1.786 1.803v2.859l-8.19-8.267L13 17.641V20.5c0 .995.8 1.802 1.786 1.802h19.643v5.408c0 .995.8 1.802 1.786 1.802.473 0 .927-.19 1.262-.528l12.5-12.617a1.815 1.815 0 0 0 0-2.55z"
                            fill={colors.primary}
                            opacity=".7"
                          />
                        </svg>
                      </Styled.ExchangeDivider>
                      <div>
                        {receive && (
                          <>
                            <Media
                              image={receive.image}
                              text={t('receive')}
                              size="lg"
                              vertical
                              rounded
                            />
                            <Select
                              addon={
                                <Input
                                  onChange={(value, name) => {
                                    setFieldValue(name, value)
                                    if (
                                      !isNaN(
                                        new BigNumber(+value)
                                          .div(this.getTokenPriceForExchange())
                                          .toPrecision(significantDigit)
                                      )
                                    ) {
                                      setFieldValue(
                                        'giveAmount',
                                        new BigNumber(+value)
                                          .div(this.getTokenPriceForExchange())
                                          .toPrecision(significantDigit)
                                      )
                                    }
                                    setFieldTouched(name, true)
                                    setFieldTouched(name, true)
                                  }}
                                  value={values.receiveAmount}
                                  type="number"
                                  name="receiveAmount"
                                  placeholder="0.00"
                                  disabled={disabled}
                                />
                              }
                              label="&nbsp;"
                              error={
                                touched.receiveAmount && errors.receiveAmount
                              }
                              items={tokens.filter(({ id }) => id !== give.id)}
                              selected={receive.id}
                              onSelect={async id => {
                                await this.handleSelect('receive', id)
                                await setFieldValue(
                                  'receiveAmount',
                                  new BigNumber(+values.giveAmount)
                                    .times(this.getTokenPriceForExchange())
                                    .toPrecision(significantDigit)
                                )
                              }}
                            />
                          </>
                        )}
                      </div>
                    </Styled.Exchange>
                    <Button
                      text={t('button')}
                      type="submit"
                      onClick={handleSubmit}
                      theme="primary"
                      size="lg"
                      disabled={isSubmitting || disabled}
                    />
                    <ModalConfirm
                      open={modalOpen === 'modalConfirm'}
                      onClose={() => {
                        setStatus(null)
                        this.handleToggleModal(null)
                      }}
                      {...modalConfirmProps}
                    />
                    <ModalSuccess
                      open={modalOpen === 'modalSuccess'}
                      onClose={() => {
                        setStatus(null)
                        this.handleToggleModal(null)
                      }}
                      {...modalSuccessProps}
                    />
                    <ModalError
                      open={modalOpen === 'modalError'}
                      onClose={() => {
                        setStatus(null)
                        this.handleToggleModal(null)
                        this.props.setError(null)
                      }}
                      {...modalErrorProps}
                    />
                    <ModalVerifying
                      open={modalOpen === 'modalVerifying'}
                      onClose={() => {
                        setStatus(null)
                        this.handleToggleModal(null)
                        this.props.setError(null)
                      }}
                    />
                  </>
                )
              }}
            </Formik>
          </Card>
        )}
      </Styled.Wrapp>
    )
  }
}
