import { withNamespaces } from '../../utils/i18n'
import Modal from '../Modal'
import Text from '../Text'
import Button from '../Button'

export default withNamespaces('exchange')(({ t, open, onClose, error }) => {
  return (
    <Modal open={open} title={t('modalError.title')} onClose={onClose}>
      <Text weight="medium">{error}</Text>
      <Button text={t('modalError.button')} onClick={onClose} theme="primary" />
    </Modal>
  )
})
