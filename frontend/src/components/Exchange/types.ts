import { I18nTProps } from '../../utils/i18n'
import { IToken, IExchangeStore } from '../../stores/exchange/types'
import { MetamaskStoreTypes } from '../../stores/metamask/types'

export interface ExchangeProps {
  tokens?: IExchangeStore['tokens']
  getTokens?: IExchangeStore['getTokens']
  exchangeTokens?: IExchangeStore['exchangeTokens']
  status?: IExchangeStore['status']
  setStatus?: IExchangeStore['setStatus']
  error?: IExchangeStore['error']
  setError?: IExchangeStore['setError']
  publicAddress?: MetamaskStoreTypes['publicAddress']
  t?: I18nTProps
  disabled?: boolean
}

export interface ExchangeState {
  give: Partial<IToken>
  receive: Partial<IToken>
  modalOpen: string
}
