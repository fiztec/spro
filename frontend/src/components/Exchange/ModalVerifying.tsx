import { withNamespaces } from '../../utils/i18n'
import Modal, { ModalStyled } from '../Modal'

export default withNamespaces('exchange')(({ t, open, onClose }) => {
  return (
    <Modal open={open} title={t('modalVerifying.title')} onClose={onClose}>
      <br />
      <ModalStyled.Info>{t('modalVerifying.info')}</ModalStyled.Info>
    </Modal>
  )
})
