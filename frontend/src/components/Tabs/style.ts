import styled, { css } from 'styled-components'
import { colors } from '../../styles/colors'
import { Card } from '../Card/style'

export const TabLinkRow = styled('div')`
  display: flex;
`

export const TabLink = styled('a')<{ disabled?: boolean; active?: boolean }>`
  font-size: 0.75em;
  color: ${colors.text};
  cursor: pointer;
  padding: 0.5rem 0.25rem;
  font-weight: 500;

  &:not(:last-child) {
    margin-right: 1rem;
  }

  &:hover {
    color: ${({ theme }) => colors[theme] || colors.primary};
  }

  ${({ active }) =>
    active &&
    css`
      color: ${({ theme }) => colors[theme] || colors.primary};
      position: relative;

      &::before {
        content: '';
        display: block;
        width: 100%;
        height: 2px;
        background: ${({ theme }) => colors[theme] || colors.primary};
        top: 0;
        left: 0;
        position: absolute;
      }
    `}

  ${({ disabled }) =>
    disabled &&
    css`
      pointer-events: none;
      opacity: 0.75;
    `}
`
export const TabContent = styled('div')`
  height: 100%;
`

export const Tabs = styled('div')`
  height: 100%;

  ${({ layout }: { layout: string }) => {
    if (layout === 'trade') {
      return css`
        > ${TabLinkRow} {
          border-bottom: 1px solid ${colors.text};
          margin-bottom: 1rem;
          padding: 0 1rem;

          > ${TabLink} {
            &::before {
              bottom: -1px;
              top: auto;
            }
          }
        }

        > ${TabContent} {
          padding: 0 1rem;

          ${Card} {
            margin-bottom: 1rem;
          }
        }
      `
    }
  }}
`
