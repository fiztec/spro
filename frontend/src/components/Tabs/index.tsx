import { useState, useEffect, ReactNode, Fragment } from 'react'
import * as Styled from './style'

export type TabLinkProps = {
  name: string
  label: string
  theme?: string
  active?: boolean
}

type TabContentProps = { name: string; content: ReactNode }

type TabsProps = {
  tabLink: TabLinkProps[]
  tabContent: TabContentProps[]
  defaultTab?: string
  onTabClick?: (name: string) => void
  disabled?: boolean
}

const Tabs = ({
  tabLink,
  tabContent,
  defaultTab,
  onTabClick,
  layout,
  disabled
}: TabsProps & { layout?: string }) => {
  const [activeTab, toggleTab] = useState(defaultTab || tabLink[0].name)

  useEffect(() => {
    toggleTab(defaultTab || tabLink[0].name)
  }, [defaultTab])
  return (
    <Styled.Tabs layout={layout}>
      <Styled.TabLinkRow>
        {Array.isArray(tabLink) &&
          tabLink.map(({ name, label, theme }: TabLinkProps) => (
            <Styled.TabLink
              key={name}
              active={name === activeTab}
              onClick={() => {
                toggleTab(name)
                onTabClick && onTabClick(name)
              }}
              disabled={disabled}
              theme={theme}
            >
              {label}
            </Styled.TabLink>
          ))}
      </Styled.TabLinkRow>
      <Styled.TabContent>
        {tabContent.map(
          ({ name, content }: TabContentProps, i: number) =>
            name === activeTab && <Fragment key={i}>{content}</Fragment>
        )}
      </Styled.TabContent>
    </Styled.Tabs>
  )
}

export default Tabs
