import Modal from '../../../Modal'
import { withNamespaces, I18nTProps } from '../../../../utils/i18n'
import Text from '../../../Text'

interface ModalProps {
  t?: I18nTProps
  open?: boolean
  message?: string
  onClose?: () => void
}

export default withNamespaces('trade')(
  ({ t, open, message, onClose }: ModalProps & { weth: string }) => (
    <Modal
      open={open}
      title={t('chart.modalError.title')}
      onClose={onClose}
      closeButton={t('chart.modalError.button')}
    >
      <Text weight="600">{message || t('chart.modalError.text')}</Text>
    </Modal>
  )
)
