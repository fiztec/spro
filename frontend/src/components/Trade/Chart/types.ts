import { ChartingLibraryWidgetOptions } from '../../../static/charting_library/charting_library.min'
import { ThemeStoreTypes } from '../../../stores/theme/types'

export interface ITVChart {
  rtl?: boolean
  query?: {
    marketId: number | string
  }
  symbol: ChartingLibraryWidgetOptions['symbol']
  interval: ChartingLibraryWidgetOptions['interval']
  datafeedUrl?: ChartingLibraryWidgetOptions['datafeed'] | string
  libraryPath: ChartingLibraryWidgetOptions['library_path']
  chartsStorageUrl: ChartingLibraryWidgetOptions['charts_storage_url']
  chartsStorageApiVersion: ChartingLibraryWidgetOptions['charts_storage_api_version']
  clientId: ChartingLibraryWidgetOptions['client_id']
  userId: ChartingLibraryWidgetOptions['user_id']
  fullscreen: ChartingLibraryWidgetOptions['fullscreen']
  autosize: ChartingLibraryWidgetOptions['autosize']
  studiesOverrides?: ChartingLibraryWidgetOptions['studies_overrides']
  containerId: ChartingLibraryWidgetOptions['container_id']
  styleUrl: ChartingLibraryWidgetOptions['custom_css_url']
  theme?: ThemeStoreTypes['theme']
}
