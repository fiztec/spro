import { Component } from 'react'
import { inject, observer } from 'mobx-react'
import { TVChart } from './style'
import Card from '../../Card'
import { colors } from '../../../styles/colors'
import { bind } from '../../../utils/bind'
import ModalError from './Modal/Error'
import Loader from '../../Loader'
import Flex from '../../Flex'
import { ITVChart } from './types'
import { apiCaller } from '../../../utils/apiCaller'
import { API_URL } from '../../../config/api'

declare global {
  interface Window {
    TradingView: any
  }
}

function getLanguageFromURL() {
  const regex = new RegExp('[\\?&]lang=([^&#]*)')
  const results = regex.exec(window.location.search)
  return results === null
    ? null
    : decodeURIComponent(results[1].replace(/\+/g, ' '))
}

@inject(({ themeStore: { theme } }) => ({ theme }))
@observer
export default class TVChartContainer extends Component<ITVChart, any> {
  static defaultProps = {
    symbol: 'MQ',
    interval: 'D',
    containerId: 'tv_chart_container',
    libraryPath: '/static/charting_library/',
    chartsStorageUrl: 'https://saveload.tradingview.com',
    chartsStorageApiVersion: '1.1',
    clientId: 'tradingview.com',
    userId: 'public_user_id',
    fullscreen: false,
    autosize: true,
    styleUrl: '/static/styles/tvchart.css'
  }

  widget: any = null

  supportedResolutions = [
    '1',
    '3',
    '5',
    '15',
    '30',
    '60',
    '120',
    '240',
    'D',
    'W',
    'M'
  ]

  config = {
    supports_search: false,
    supported_resolutions: this.supportedResolutions
  }

  datafeed = {
    onReady: cb => {
      setTimeout(() => cb(this.config), 0)
    },
    resolveSymbol: (
      symbolName,
      onSymbolResolvedCallback
      // onResolveErrorCallback
    ) => {
      // expects a symbolInfo object in response
      var split_data = symbolName.split(/[:/]/)
      var symbol_stub = {
        name: symbolName,
        description: '',
        type: 'crypto',
        session: '24x7',
        timezone: 'Etc/UTC',
        ticker: symbolName,
        exchange: split_data[0],
        minmov: 1,
        pricescale: 100000000,
        has_intraday: true,
        intraday_multipliers: ['1', '60'],
        supported_resolution: this.supportedResolutions,
        volume_precision: 8,
        data_status: 'streaming'
      }

      setTimeout(function() {
        onSymbolResolvedCallback(symbol_stub)
      }, 0)

      // onResolveErrorCallback('Not feeling it today')
    },
    getBars: ({}, {}, {}, {}, onHistoryCallback, onErrorCallback) => {
      this.getBars()
        .then(bars => {
          if (bars.length) {
            onHistoryCallback(bars, { noData: false })
            onHistoryCallback({}, { noData: true })
          } else {
            onHistoryCallback(bars, { noData: true })
          }
        })
        .catch(err => {
          onErrorCallback(err)
        })
    },
    subscribeBars: () => {
      // console.log('=====subscribeBars runnning')
    },
    unsubscribeBars: () => {
      // console.log('=====unsubscribeBars running')
    },
    getServerTime: () => {
      // console.log('=====getServerTime running')
    }
  }

  state = {
    widgetOptions: {
      debug: false,
      symbol: this.props.symbol,
      datafeed: this.datafeed,
      interval: this.props.interval,
      container_id: this.props.containerId,
      library_path: this.props.libraryPath,
      allow_symbol_change: false,
      toolbar_bg: colors.dark,
      locale: getLanguageFromURL() || 'en',
      disabled_features: [
        // 'use_localstorage_for_settings',
        'header_symbol_search',
        'symbol_search_hot_key',
        'loading_screen',
        'header_settings',
        'header_compare',
        'header_saveload',
        'chart_property_page_scales',
        'main_series_scale_menu',
        'context_menus'
      ],
      charts_storage_url: this.props.chartsStorageUrl,
      charts_storage_api_version: this.props.chartsStorageApiVersion,
      client_id: this.props.clientId,
      user_id: this.props.userId,
      fullscreen: this.props.fullscreen,
      autosize: this.props.autosize,
      studies_overrides: this.props.studiesOverrides,
      overrides: {
        // "mainSeriesProperties.showCountdown": true,

        'paneProperties.background':
          this.props.theme === 'day' ? colors.white : colors.dark,
        'paneProperties.vertGridProperties.color': `${colors.text}50`,
        'paneProperties.horzGridProperties.color': `${colors.text}50`,
        'paneProperties.gridProperties.color': colors.dark,
        'symbolWatermarkProperties.transparency': 99,
        'scalesProperties.textColor': colors.text,
        'mainSeriesProperties.candleStyle.wickUpColor': `${
          colors.successLight
        }50`,
        'mainSeriesProperties.candleStyle.wickDownColor': `${
          colors.primaryLight
        }50`
      },
      loading_screen: {
        backgroundColor: this.props.theme === 'day' ? colors.white : colors.dark
      },
      custom_css_url: this.props.styleUrl,
      time_frames: [
        { text: '1h', resolution: 'D', description: '1 Day' },
        { text: '1w', resolution: 'D', description: '1 Week' },
        { text: '1m', resolution: 'D', description: '1 Month' },
        { text: '6m', resolution: 'W', description: '6 Month' },
        { text: '1y', resolution: 'W', description: '1 Year', title: '1y' },
        { text: '5y', resolution: 'M', description: '5 Year', title: '5yrs' },
        { text: '1000y', resolution: 'W', description: 'All', title: 'All' }
      ]
    },
    isLoading: false,
    errorModal: false,
    errorMessage: ''
  }

  public componentDidMount() {
    setTimeout(() => {
      this.widget = new window.TradingView.widget(this.state.widgetOptions)
    }, 0)
  }

  public componentDidUpdate(np) {
    if (this.props.theme) {
      this.widget = new window.TradingView.widget({
        ...this.state.widgetOptions,
        loading_screen: {
          backgroundColor:
            this.props.theme === 'day' ? colors.white : colors.dark
        },
        overrides: {
          ...this.state.widgetOptions.overrides,
          'paneProperties.background':
            this.props.theme === 'day' ? colors.white : colors.dark
        }
      })
    }
    if (this.props.query) {
      if (JSON.stringify(this.props.query) !== JSON.stringify(np.query)) {
        this.setState(
          () => ({ isLoading: true }),
          () => {
            setTimeout(() => {
              this.setState(() => ({ isLoading: false }))
            }, 1000)
          }
        )
        this.widget = new window.TradingView.widget(this.state.widgetOptions)
      }
    }
  }

  @bind private async getBars() {
    const {
      query: { marketId }
    } = this.props

    try {
      const data = await apiCaller(`${API_URL}/chart?marketId=${marketId}`)

      if (data.length) {
        const bars =
          +marketId === 11 &&
          data
            .map(el => {
              return {
                time: +el.time,
                low: +el.low,
                high: +el.high,
                open: +el.open,
                close: +el.close,
                volume: +el.volume
              }
            })
            .sort((a, b) => a.time - b.time)

        return bars
      } else {
        return []
      }
    } catch ({ response: { statusText, data } }) {
      return data
    }
  }

  public render() {
    return (
      <>
        <Card>
          {this.state.isLoading && (
            <Flex
              justify="center"
              align="center"
              style={{
                background:
                  this.props.theme === 'day' ? colors.white : colors.dark,
                borderRadius: '4px',
                position: 'absolute',
                left: 0,
                top: 0,
                right: 0,
                bottom: 0
              }}
            >
              <Loader />
            </Flex>
          )}
          <TVChart id={this.props.containerId} />
        </Card>
        <ModalError
          open={this.state.errorModal}
          onClose={() => this.setState({ errorModal: false })}
          message={this.state.errorMessage}
        />
      </>
    )
  }
}
