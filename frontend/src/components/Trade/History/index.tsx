import { Component } from 'react'
import { inject, observer } from 'mobx-react'
import Card from '../../Card'
import { withNamespaces } from '../../../utils/i18n'
import { Table } from '../../Table'
import Tabs from '../../Tabs'
import { bind } from '../../../utils/bind'
import { TradeHistoryStoreTypes } from '../../../stores/tradeHistory/types'
import { ITradeHistory } from './types'
import * as Styled from './style'

@withNamespaces('trade')
@inject(
  ({
    tradeHistoryStore: { getHistory, setHistory, history },
    metamaskStore: { metamaskStatus, publicAddress }
  }) => ({
    getHistory,
    setHistory,
    history,
    metamaskStatus,
    publicAddress
  })
)
@observer
export default class TradeHistory extends Component<
  ITradeHistory,
  { activeTab: string; status: string }
> {
  private width = ['34%', '16%', '32%', '18%']

  state = {
    status: null,
    activeTab: 'all'
  }

  public componentDidMount() {
    this.onTabClick('all')
  }

  public componentDidUpdate(np: ITradeHistory) {
    const {
      query: { marketId }
    } = this.props

    if (np.query.marketId !== marketId) {
      this.onTabClick('all')
    }
  }

  public componentWillReact() {
    const { metamaskStatus } = this.props
    const { status } = this.state

    if (metamaskStatus === 'account changed') {
      this.onTabClick('all')
    }

    if (metamaskStatus === 'logged in') {
      this.setState({ status: null })
    }

    if (metamaskStatus === 'logged out') {
      if (status !== 'isLoggedOut') {
        this.onTabClick('all')
      }
      this.setState({ status: 'isLoggedOut' })
    }
  }

  @bind private onGetHistory(activeTab?: string) {
    const {
      getHistory,
      query: { marketId }
    } = this.props

    this.setState(() => ({ activeTab }))
    if (activeTab === 'my_trades') {
      getHistory(marketId, true)
    } else {
      getHistory(marketId)
    }
  }

  @bind private onTabClick(name) {
    this.onGetHistory(name)
  }

  @bind private renderTableContent(data: TradeHistoryStoreTypes['history']) {
    const { t } = this.props
    return (
      <Table
        head={t('history.table.rows', { returnObjects: true })}
        width={this.width}
        body={data}
      />
    )
  }

  public render() {
    const { lng, t, history, publicAddress } = this.props
    const tabs = t('history.tabs', { returnObjects: true })
    const { activeTab } = this.state
    return (
      <Styled.Wrapp>
        <Card title={t('history.title')} rtl={lng === 'ar'}>
          <Tabs
            defaultTab={activeTab}
            tabLink={
              Array.isArray(tabs) &&
              tabs.map((label: string) => ({
                name: label.toLowerCase().replace(' ', '_'),
                label
              }))
            }
            tabContent={[
              {
                name: 'all',
                content: this.renderTableContent(history)
              },
              {
                name: 'my_trades',
                content: this.renderTableContent(history)
              }
            ]}
            onTabClick={this.onTabClick}
            disabled={!publicAddress}
          />
        </Card>
      </Styled.Wrapp>
    )
  }
}
