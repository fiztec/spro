import styled from 'styled-components'
import { Card } from '../../Card/style'
import { Col } from '../../Table/style'

export const Wrapp = styled('div')`
  height: 100%;

  ${Card} {
    height: 100%;
  }

  ${Col} {
    &:last-child {
      &.tooltip {
        &::before {
          right: 0;
          left: auto;
        }
      }
    }
  }
`
