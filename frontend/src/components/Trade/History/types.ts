import { I18nTProps } from '../../../utils/i18n'
import { TradeHistoryStoreTypes } from '../../../stores/tradeHistory/types'
import { MetamaskStoreTypes } from '../../../stores/metamask/types'

export interface ITradeHistory {
  t?: I18nTProps
  lng?: string
  query?: {
    marketId: number | string
  }
  getHistory?: TradeHistoryStoreTypes['getHistory']
  setHistory?: TradeHistoryStoreTypes['setHistory']
  history?: TradeHistoryStoreTypes['history']
  metamaskStatus?: MetamaskStoreTypes['metamaskStatus']
  publicAddress?: MetamaskStoreTypes['publicAddress']
}
