import { Component } from 'react'
import Card from '../../Card'
import Input from '../../Input'
import Flex from '../../Flex'
import Heading from '../../Heading'
import { colors } from '../../../styles/colors'
import Button from '../../Button'
import Tabs from '../../Tabs'
import { withNamespaces } from '../../../utils/i18n'
import { inject, observer } from 'mobx-react'
import Modal, { ModalStyled } from '../../Modal'
import * as Styled from './style'
import Text from '../../Text'
import { MarketsStoreTypes } from '../../../stores/markets/types'
import { bind } from '../../../utils/bind'
import Switcher from '../../Switcher'
import { TradeOrdersProps, TradeOrdersState } from './types'
import BigNumber from 'bignumber.js'
import Loader from '../../Loader'

@withNamespaces(['trade'])
@inject(
  ({
    balanceStore: { wethBalance },
    marketsStore: { selectedMarket, getMarket },
    ordersStore: {
      orderStatus,
      setOrderStatus,
      createOrder,
      getOrders,
      error,
      enableOrder,
      handleToggleOrder,
      orderValuesFromBook,
      setOrderValuesFromBook,
      getAllowance
    },
    themeStore: { theme }
  }) => ({
    wethBalance,
    selectedMarket,
    getMarket,
    orderStatus,
    setOrderStatus,
    createOrder,
    getOrders,
    error,
    enableOrder,
    handleToggleOrder,
    orderValuesFromBook,
    setOrderValuesFromBook,
    getAllowance,
    theme
  })
)
@observer
export default class TradeOrder extends Component<
  TradeOrdersProps,
  TradeOrdersState
> {
  private defaultTab = 'buy'

  public state = {
    modal: null,
    activeTab: this.defaultTab
  }

  public static getDerivedStateFromProps({
    orderStatus,
    getOrders,
    selectedMarket,
    orderValuesFromBook: { price, amount }
  }) {
    if (orderStatus === 'success') {
      getOrders(selectedMarket.id)
      return {
        price: '',
        amount: '',
        modal: orderStatus
      }
    }
    if (orderStatus) {
      return {
        modal: orderStatus
      }
    }
    return { price, amount }
  }

  public componentWillReact() {
    if (!!this.props.selectedMarket) {
      this.getAllowance()
    }
  }

  @bind private getAllowance() {
    const { getAllowance, selectedMarket } = this.props
    const { activeTab } = this.state

    const data = {
      isSell: activeTab === 'sell',
      baseToken: selectedMarket.baseTokenAddress,
      quoteToken: selectedMarket.quoteTokenAddress
    }
    getAllowance(data)
  }

  @bind private onChangeTab(activeTab: string) {
    this.setState({ activeTab })
  }

  @bind private handleToggleModal(modal: string) {
    this.setState({ modal })
  }

  @bind private handleChange(value: string, name: string) {
    const { orderValuesFromBook, setOrderValuesFromBook } = this.props
    setOrderValuesFromBook({
      ...orderValuesFromBook,
      [name]: value
    })
  }

  @bind private async onCreateOrder(
    price: string,
    amount: string,
    selectedMarket: MarketsStoreTypes['selectedMarket'],
    isSell: boolean,
    isMarket: boolean = false
  ) {
    const { createOrder, getMarket, setOrderStatus } = this.props
    const market = await getMarket(selectedMarket.id)
    await setOrderStatus(null)

    if (!this.validate(+price, +amount, isSell, isMarket)) {
      return
    }

    createOrder({
      price,
      amount,
      isSell,
      isMarket,
      tokens: selectedMarket.tokens,
      baseToken: selectedMarket.baseTokenAddress,
      quoteToken: selectedMarket.quoteTokenAddress,
      asMakerFeeRate: market.asMakerFeeRate.toString(),
      asTakerFeeRate: market.asTakerFeeRate.toString()
    })
  }

  @bind private validate(
    price: number,
    amount: number,
    isSell: boolean,
    isMarket: boolean = false
  ) {
    const priceBig = new BigNumber(price)
    const amountBig = new BigNumber(amount)
    const leftSide = new BigNumber(Math.pow(10, 18))
      .times(priceBig)
      .times(amountBig)
      .integerValue(BigNumber.ROUND_FLOOR)

    const condition = leftSide.lte(new BigNumber(Math.pow(10, 26)))

    if (!amount || Number(amount) <= 0) {
      this.handleToggleModal('valueError')
    } else if (!Number(price) && !isMarket) {
      this.handleToggleModal('valueError')
    } else if (!Number(price) && !isSell && isMarket) {
      this.handleToggleModal('valueError')
    } else if (Number(price) && isSell && isMarket) {
      this.handleToggleModal('buyMarketValueError')
    } else if (!condition) {
      this.handleToggleModal('tooHighAmount')
    } else {
      return true
    }
  }

  @bind private handleToggleOrder(
    selectedMarket: MarketsStoreTypes['selectedMarket'],
    isSell: boolean
  ) {
    const { enableOrder, handleToggleOrder } = this.props

    if (selectedMarket) {
      handleToggleOrder(
        {
          isSell,
          baseToken: selectedMarket.baseTokenAddress,
          quoteToken: selectedMarket.quoteTokenAddress
        },
        enableOrder
      )
    }
  }

  @bind private renderForm(isSell: boolean, button: boolean = true) {
    const {
      t,
      lng,
      wethBalance,
      selectedMarket,
      enableOrder,
      orderStatus,
      orderValuesFromBook: { price, amount }
    } = this.props

    const total = new BigNumber(price).times(new BigNumber(amount)).toFixed()

    const minOrderSize = this.props.selectedMarket
      ? new BigNumber(this.props.selectedMarket.minOrderSize).toPrecision()
      : 0
    return (
      <>
        <Input
          label={t('trade.order.limit.label')}
          onChange={(value: string, name: string) =>
            this.handleChange(value, name)
          }
          name="price"
          value={price}
          placeholder="0.00"
          addon={
            <div style={{ paddingRight: '1rem' }}>
              {selectedMarket && selectedMarket.quoteToken}
            </div>
          }
          type="number"
          fluid
          disabled={!Number(wethBalance)}
          rtl={lng === 'ar'}
        />
        <Input
          label={t('trade.order.amount.label')}
          onChange={(value: string, name: string) =>
            this.handleChange(value, name)
          }
          name="amount"
          value={amount}
          placeholder="0.00"
          addon={
            <div style={{ paddingRight: '1rem' }}>
              {selectedMarket && selectedMarket.baseToken}
            </div>
          }
          type="number"
          fluid
          disabled={!Number(wethBalance)}
          rtl={lng === 'ar'}
        />
        <Flex justify="space-between">
          <span
            style={{
              maxWidth: 60,
              overflow: 'hidden',
              textOverflow: 'ellipsis'
            }}
          >
            {t('trade.order.minOrderSize')}
          </span>
          <span style={{ whiteSpace: 'nowrap' }}>
            {`(min ${minOrderSize})`}
          </span>
        </Flex>
        <Flex justify="space-between">
          <span>{t('trade.order.total')}</span>
          <Flex justify="flex-end" style={{ width: 'calc(100% - 3rem)' }}>
            <span
              style={{
                whiteSpace: 'nowrap',
                display: 'inline-block',
                paddingRight: '0.25rem',
                overflow: 'hidden',
                textOverflow: 'ellipsis'
              }}
            >
              ≈{!isNaN(Number(total)) ? total : 0}
            </span>
            <span>{t('trade.order.weth')}</span>
          </Flex>
        </Flex>
        <Flex justify="space-between" align="center">
          <span>{t('trade.order.enabled')}</span>
          <Switcher
            active={enableOrder}
            onClick={() => this.handleToggleOrder(selectedMarket, isSell)}
            disabled={
              orderStatus === 'enableRequest' ||
              orderStatus === 'disableRequest'
            }
          />
        </Flex>
        {button && (
          <Flex>
            <Button
              text={t(
                `trade.order.${isSell ? 'buttonSellLimit' : 'buttonBuyLimit'}`
              )}
              onClick={() => {
                this.onCreateOrder(price, amount, selectedMarket, isSell)
              }}
              theme={isSell ? 'primary' : 'success'}
              fluid
              disabled={
                orderStatus === 'enableRequest' ||
                orderStatus === 'disableRequest' ||
                !enableOrder ||
                !amount
              }
            />
            <Button
              text={t(
                `trade.order.${isSell ? 'buttonSellMarket' : 'buttonBuyMarket'}`
              )}
              onClick={() => {
                this.onCreateOrder(price, amount, selectedMarket, isSell, true)
              }}
              theme={isSell ? 'primary' : 'success'}
              fluid
              disabled={
                orderStatus === 'enableRequest' ||
                orderStatus === 'disableRequest' ||
                !enableOrder ||
                !amount
              }
            />
          </Flex>
        )}
      </>
    )
  }

  @bind private renderModals() {
    const {
      t,
      setOrderStatus,
      selectedMarket,
      error,
      orderValuesFromBook: { price, amount }
    } = this.props
    const { modal } = this.state

    return (
      <>
        <Modal
          open={modal === 'sellPending'}
          title={t('trade.modalSell.title')}
          onClose={() => {
            setOrderStatus(null)
            this.handleToggleModal(null)
          }}
        >
          <Heading.Three>{t('trade.modalSell.text')}</Heading.Three>
          <br />
          <Styled.ModalWrapp>{this.renderForm(false, false)}</Styled.ModalWrapp>
          <br />
          <ModalStyled.Info>{t('trade.modalSell.info')}</ModalStyled.Info>
        </Modal>

        <Modal
          open={modal === 'buyPending'}
          title={t('trade.modalBuy.title')}
          onClose={() => {
            setOrderStatus(null)
            this.handleToggleModal(null)
          }}
        >
          <Heading.Three>{t('trade.modalBuy.text')}</Heading.Three>
          <br />
          <Styled.ModalWrapp>{this.renderForm(false, false)}</Styled.ModalWrapp>
          <br />
          <ModalStyled.Info>{t('trade.modalBuy.info')}</ModalStyled.Info>
        </Modal>

        <Modal
          open={modal === 'valueError'}
          title={t('trade.modalValueError.title')}
          closeButton={t('trade.modalValueError.button')}
          onClose={() => {
            setOrderStatus(null)
            this.handleToggleModal(null)
          }}
        >
          <Text>{t('trade.modalValueError.text')}</Text>
        </Modal>

        <Modal
          open={modal === 'tooHighAmount'}
          title={t('trade.tooHighAmount.title')}
          closeButton={t('trade.tooHighAmount.button')}
          onClose={() => {
            setOrderStatus(null)
            this.handleToggleModal(null)
          }}
        >
          <Text>{t('trade.tooHighAmount.text')}</Text>
        </Modal>

        <Modal
          open={modal === 'submitError'}
          title={t('trade.modalSubmitError.title')}
          closeButton={t('trade.modalSubmitError.button')}
          onClose={() => {
            setOrderStatus(null)
            this.handleToggleModal(null)
          }}
        >
          <Text>{t('trade.modalSubmitError.text', { error })}</Text>
        </Modal>

        <Modal
          open={modal === 'canceled'}
          title={t('trade.modalCanceled.title')}
          closeButton={t('trade.modalCanceled.button')}
          onClose={() => {
            setOrderStatus(null)
            this.handleToggleModal(null)
          }}
        />

        <Modal
          open={modal === 'success'}
          title={
            <>
              <i
                className="material-icons"
                style={{
                  fontSize: '2.5rem',
                  color: colors.success,
                  verticalAlign: 'text-top',
                  marginRight: '0.5rem'
                }}
              >
                check
              </i>
              {t('trade.modalSuccess.title')}
            </>
          }
          closeButton={t('trade.modalSuccess.button')}
          onClose={() => {
            setOrderStatus(null)
            this.handleToggleModal(null)
          }}
        >
          {t('trade.modalSuccess.text', {
            returnObjects: true,
            priceValue: price,
            amountValue: amount,
            priceMarket: selectedMarket && selectedMarket.baseToken,
            amountMarket: selectedMarket && selectedMarket.quoteToken
          }).map((text: string, i: number) => (
            <Text key={i}>{text}</Text>
          ))}
        </Modal>

        <Modal
          open={modal === 'enableRequest'}
          title={t('trade.modalEnableOrder.title')}
          onClose={() => {
            this.handleToggleModal(null)
          }}
        >
          <ModalStyled.Info>
            {t('trade.modalEnableOrder.info')}
          </ModalStyled.Info>
        </Modal>

        <Modal
          open={modal === 'disableRequest'}
          title={t('trade.modalDisableOrder.title')}
          onClose={() => {
            this.handleToggleModal(null)
          }}
        >
          <ModalStyled.Info>
            {t('trade.modalDisableOrder.info')}
          </ModalStyled.Info>
        </Modal>

        <Modal
          open={modal === 'disableConfirm' || modal === 'enableConfirm'}
          title={t('trade.modalApprovePending.title')}
          onClose={() => {
            this.handleToggleModal(null)
          }}
        >
          <ModalStyled.Text>
            {t('trade.modalApprovePending.text')}
          </ModalStyled.Text>
          <ModalStyled.Text>
            <span
              style={{
                marginRight: '1rem',
                display: 'inline-block',
                verticalAlign: 'top'
              }}
            >
              <Loader theme="primary" />
            </span>
          </ModalStyled.Text>
        </Modal>

        <Modal
          open={modal === 'lowTokenBalance'}
          title={t('trade.lowBalance.title')}
          onClose={() => {
            setOrderStatus(null)
            this.handleToggleModal(null)
          }}
        >
          {selectedMarket && (
            <ModalStyled.Info>
              {t('trade.lowBalance.text', { token: selectedMarket.baseToken })}
            </ModalStyled.Info>
          )}
        </Modal>

        <Modal
          open={modal === 'lowWethBalance'}
          title={t('trade.lowBalance.title')}
          onClose={() => {
            setOrderStatus(null)
            this.handleToggleModal(null)
          }}
        >
          {selectedMarket && (
            <ModalStyled.Info>
              {t('trade.lowBalance.text', { token: selectedMarket.quoteToken })}
            </ModalStyled.Info>
          )}
        </Modal>

        <Modal
          open={modal === 'buyMarketValueError'}
          title={t('trade.buyMarketValueError.title')}
          closeButton={t('trade.buyMarketValueError.button')}
          onClose={() => {
            setOrderStatus(null)
            this.handleToggleModal(null)
          }}
        >
          <Text>{t('trade.buyMarketValueError.text')}</Text>
        </Modal>
      </>
    )
  }

  public render() {
    const { t, lng, theme } = this.props
    const tabs = t('trade.tabs', { returnObjects: true })
    return (
      <Styled.Wrapp>
        <Card
          title={t('trade.title')}
          justify="space-between"
          rtl={lng === 'ar'}
        >
          <Tabs
            defaultTab="buy"
            onTabClick={this.onChangeTab}
            tabLink={
              Array.isArray(tabs) &&
              tabs.map((label: string) => ({
                name: label.toLowerCase().replace(' ', '_'),
                theme: theme === 'night' ? 'white' : 'dark',
                label
              }))
            }
            tabContent={[
              {
                name: 'buy',
                content: this.renderForm(false)
              },
              {
                name: 'sell',
                content: this.renderForm(true)
              }
            ]}
          />
          {this.renderModals()}
        </Card>
      </Styled.Wrapp>
    )
  }
}
