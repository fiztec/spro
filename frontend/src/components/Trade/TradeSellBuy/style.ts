import styled from 'styled-components'
import { TabContent } from '../../Tabs/style'
import { Card } from '../../Card/style'
import Flex from '../../Flex'
import { colors } from '../../../styles/colors'
import { Button } from '../../Button/style'

export const Wrapp = styled('div')`
  ${TabContent} {
    display: flex;
    flex-direction: column;

    > ${Flex} {
      margin-top: auto;
      color: ${colors.text};
      font-size: 0.875em;
      margin-top: auto;
      margin-bottom: 1.25rem;
      font-weight: 500;
    }
  }

  ${Button} {
    margin-bottom: 0.5rem;
  }

  ${Card} {
    height: 100%;
  }
`

export const ModalWrapp = styled('div')`
  pointer-events: none;

  > ${Flex}:not(:last-child) {
    margin-bottom: 1rem;
  }

  > ${Flex}:last-child {
    display: none;
  }
`
