import { I18nTProps } from '../../../utils/i18n'
import { BalanceStoreTypes } from '../../../stores/balance/types'
import { MarketsStoreTypes } from '../../../stores/markets/types'
import { OrdersStoreTypes } from '../../../stores/order/types'
import { ThemeStoreTypes } from '../../../stores/theme/types'

export interface TradeOrdersProps extends Partial<OrdersStoreTypes> {
  t?: I18nTProps
  lng?: string
  wethBalance?: BalanceStoreTypes['wethBalance']
  selectedMarket?: MarketsStoreTypes['selectedMarket']
  getMarket?: MarketsStoreTypes['getMarket']
  theme?: ThemeStoreTypes['theme']
}

export interface TradeOrdersState {
  modal: string
  activeTab: string
}
