import * as Styled from './style'
import Input from '../../Input'
import { colors } from '../../../styles/colors'

type SearchProps = {
  onSearch: (term: string) => void
  onShowFavorites: () => void
  showFavorites: boolean
}

export default ({ onSearch, onShowFavorites, showFavorites }: SearchProps) => {
  return (
    <Styled.Search>
      <Input
        onChange={onSearch}
        size="sm"
        icon="search"
        placeholder="Search"
        fluid
        name="search"
      />
      <a
        onClick={onShowFavorites}
        style={{
          marginLeft: '1rem',
          cursor: 'pointer',
          color: showFavorites ? colors.primary : 'inherit'
        }}
      >
        <small>Favourites</small>
      </a>
    </Styled.Search>
  )
}
