import { I18nTProps } from '../../../utils/i18n'
import { OrdersStoreTypes } from '../../../stores/order/types'
import { MarketsStoreTypes } from '../../../stores/markets/types'
import { MetamaskStoreTypes } from '../../../stores/metamask/types'

export interface TradeMarketsProps {
  t?: I18nTProps
  lng?: string
  query?: {
    marketId: string
  }
  getOrders?: OrdersStoreTypes['getOrders']
  marketsStore?: MarketsStoreTypes
  metamaskStatus?: MetamaskStoreTypes['metamaskStatus']
}

export interface TradeMarketsState {
  showFavorites: boolean
}
