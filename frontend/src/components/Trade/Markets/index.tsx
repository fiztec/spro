import { Component } from 'react'
import Router from 'next/router'
import { inject, observer } from 'mobx-react'
import Card from '../../Card'
import { withNamespaces } from '../../../utils/i18n'
import { Table, TableBody, TableRow, TableCol } from '../../Table'
import { getColor } from '../../../utils/colorByValue'
import { bind } from '../../../utils/bind'
import Search from './Search'
import Button from '../../Button'
import { TradeMarketsProps, TradeMarketsState } from './types'
import isEmpty from 'lodash/isEmpty'

@withNamespaces('trade')
@inject(({ marketsStore, ordersStore: { getOrders } }) => ({
  marketsStore,
  getOrders
}))
@observer
export default class Markets extends Component<
  TradeMarketsProps,
  TradeMarketsState
> {
  state = {
    showFavorites: false
  }

  public componentDidMount() {
    this.onGetMarkets()
  }

  public componentWillUnmount() {
    const {
      marketsStore: { selectMarket, markets }
    } = this.props
    if (markets) {
      selectMarket(markets.find(m => m.id === 1))
    }
  }

  public componentWillReact() {
    const {
      marketsStore: { selectMarket, markets, selectedMarket },
      query
    } = this.props

    if (isEmpty(selectedMarket)) {
      if (markets && query) {
        selectMarket(markets.find(m => m.id === +query.marketId))
      }
    }
  }

  public componentDidUpdate(np: TradeMarketsProps) {
    const {
      query,
      getOrders,
      marketsStore: { selectMarket, markets }
    } = this.props

    if (query && np.query.marketId !== query.marketId) {
      selectMarket(markets.find(m => m.id === (query ? +query.marketId : 1)))
      getOrders(+query.marketId)
    }
  }

  @bind
  private onGetMarkets(term?: string) {
    const {
      marketsStore: { getMarkets }
    } = this.props
    getMarkets(term)
  }

  @bind
  private onShowFavorites() {
    this.setState(({ showFavorites }) => ({
      showFavorites: !showFavorites
    }))
  }

  public render() {
    const {
      t,
      lng,
      marketsStore: {
        markets,
        getFavoriteMarkets,
        selectedMarket,
        setFavorites
      }
    } = this.props
    const { showFavorites } = this.state
    const width = ['38%', '22%', '23%', '17%']

    const favorites = getFavoriteMarkets
    const marketsList = showFavorites ? favorites : markets

    return (
      <Card
        title={t('markets.title')}
        header={
          <Search
            onSearch={(term: string) => {
              this.onGetMarkets(term)
            }}
            showFavorites={showFavorites}
            onShowFavorites={this.onShowFavorites}
          />
        }
        rtl={lng === 'ar'}
      >
        <Table
          head={t('markets.table.rows', { returnObjects: true })}
          width={width}
        >
          <TableBody>
            {Array.isArray(marketsList) &&
              marketsList.map(
                ({ id, tokens, price, dayVol, dayPrice, favourite }) => {
                  const StarButtonProps = {
                    icon: favourite ? 'star' : 'star_border',
                    onClick: (e: React.SyntheticEvent) => {
                      e.stopPropagation()
                      setFavorites(id)
                    },
                    size: 'xsm',
                    ...(favourite && { theme: 'star-active' })
                  }
                  return (
                    <TableRow
                      key={id}
                      align="center"
                      onClick={() => {
                        Router.replace(`/trade?marketId=${id}`)
                      }}
                      active={selectedMarket && selectedMarket.id === id}
                    >
                      <TableCol width={width && width[0]}>
                        <div
                          style={{
                            overflow: 'hidden',
                            textOverflow: 'ellipsis'
                          }}
                        >
                          <Button {...StarButtonProps} />
                          {tokens}
                        </div>
                      </TableCol>
                      <TableCol width={width && width[1]}>{price}</TableCol>
                      <TableCol width={width && width[2]}>{dayVol}</TableCol>
                      <TableCol
                        width={width && width[3]}
                        color={getColor(dayPrice)}
                      >
                        {dayPrice}
                      </TableCol>
                    </TableRow>
                  )
                }
              )}
          </TableBody>
        </Table>
      </Card>
    )
  }
}
