import { inject, observer } from 'mobx-react'
import { minBy, maxBy, isEmpty } from 'lodash'
import Card from '../../Card'
import { withNamespaces } from '../../../utils/i18n'
import { Table } from '../../Table'
import Text from '../../Text'
import Bar from '../../Bar'
import { colors } from '../../../styles/colors'
import { Component } from 'react'
import { bind } from '../../../utils/bind'
import * as Styled from './style'
import { OrderBookProps } from './types'
import BigNumber from 'bignumber.js'

@withNamespaces('trade')
@inject(
  ({ ordersStore: { getOrderBook, orderBook, setOrderValuesFromBook } }) => ({
    getOrderBook,
    orderBook,
    setOrderValuesFromBook
  })
)
@observer
export default class OrderBook extends Component<OrderBookProps> {
  private width = ['20%', '32%', '25%', '23%']

  public componentDidMount() {
    const {
      getOrderBook,
      query: { marketId }
    } = this.props

    getOrderBook(marketId)
  }

  public componentDidUpdate(np: OrderBookProps) {
    const {
      getOrderBook,
      query: { marketId }
    } = this.props

    if (np.query.marketId !== marketId) {
      getOrderBook(marketId)
    }
  }

  @bind private renderBodyTop(data: OrderBookProps['orderBook']['SELL']) {
    return data.map(({ price, ...rest }) => {
      const sumArray = data.map(({ price }) => +price) || []
      return {
        size: () => (
          <div
            style={{
              paddingRight: '0.5rem',
              width: '100%',
              height: '1.125rem'
            }}
          >
            <Bar amount={+price} data={sumArray} color={colors.primary} />
          </div>
        ),
        price: () => (
          <Text color="primaryLight" ellipsis>
            {price}
          </Text>
        ),
        ...rest
      }
    })
  }

  @bind private renderBodyBottom(data: OrderBookProps['orderBook']['BUY']) {
    return data.map(({ price, ...rest }) => {
      const sumArray = data.map(({ price }) => +price) || []
      return {
        size: () => (
          <div
            style={{
              paddingRight: '0.5rem',
              width: '100%',
              height: '1.125rem'
            }}
          >
            <Bar amount={+price} data={sumArray} color={colors.success} />
          </div>
        ),
        price: () => (
          <Text color="successLight" ellipsis>
            {price}
          </Text>
        ),
        ...rest
      }
    })
  }

  render() {
    const { lng, t, orderBook, setOrderValuesFromBook } = this.props

    let spread: string = '0'
    if (orderBook && !isEmpty(orderBook.SELL) && !isEmpty(orderBook.BUY)) {
      spread = new BigNumber(minBy(orderBook.SELL, o => o.price).price)
        .minus(new BigNumber(maxBy(orderBook.BUY, o => o.price).price))
        .toFixed(8)
    }

    return (
      <Styled.Wrapp>
        <Card title={t('orderBook.title')} rtl={lng === 'ar'}>
          <Table
            head={t('orderBook.table.rows', { returnObjects: true })}
            width={this.width}
            body={this.renderBodyTop(orderBook ? orderBook.SELL : [])}
            onRowClick={({ price, amount }) =>
              setOrderValuesFromBook({
                price: price().props.children,
                amount: amount
              })
            }
          />
          <Table
            head={['Spread', String(spread)]}
            headFilled
            width={this.width}
            body={this.renderBodyBottom(orderBook ? orderBook.BUY : [])}
            onRowClick={({ price, amount }) =>
              setOrderValuesFromBook({
                price: price().props.children,
                amount: amount
              })
            }
          />
        </Card>
      </Styled.Wrapp>
    )
  }
}
