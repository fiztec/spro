import styled from 'styled-components'
import { Body } from '../../Table/style'
import { Card } from '../../Card/style'

export const Wrapp = styled('div')`
  height: 100%;

  ${Body} {
    max-height: 110px;
  }

  ${Card} {
    height: 100%;
  }

  .tooltip {
    &::before {
      right: 0;
      left: auto;
    }
  }
`
