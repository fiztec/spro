import { I18nTProps } from '../../../utils/i18n'
import { OrdersStoreTypes } from '../../../stores/order/types'

export interface OrderBookProps extends Partial<OrdersStoreTypes> {
  t?: I18nTProps
  lng?: string
  query?: {
    marketId: number | string
  }
}
