import Router from 'next/router'
import * as Styled from './style'
import Card from '../../Card'
import Button from '../../Button'
import { withNamespaces } from '../../../utils/i18n'
import { inject, observer } from 'mobx-react'
import Flex from '../../Flex'
import { Component } from 'react'
import { bind } from '../../../utils/bind'
import { ITradeHeader } from './types'
import { colors } from '../../../styles/colors'

@withNamespaces('trade')
@inject(
  ({
    themeStore: { theme },
    marketsStore: {
      markets,
      marketDetails,
      selectMarket,
      selectedMarket,
      getMarketDetails,
      favoriteMarkets
    },
    ordersStore: { getOrders },
    balanceStore: { gasPrice }
  }) => ({
    theme,
    markets,
    marketDetails,
    selectMarket,
    selectedMarket,
    getMarketDetails,
    getOrders,
    gasPrice,
    favoriteMarkets
  })
)
@observer
export default class TradeHeader extends Component<ITradeHeader> {
  interval = null
  public componentDidMount() {
    const {
      query: { marketId }
    } = this.props
    this.onGetMarketDetails(marketId)
    this.interval = setInterval(() => this.onGetMarketDetails(marketId), 60000)
  }

  public componentWillUnmount() {
    clearInterval(this.interval)
  }

  public componentDidUpdate(np: ITradeHeader) {
    const {
      query: { marketId }
    } = this.props

    if (np.query.marketId !== marketId) this.onGetMarketDetails(marketId)
  }

  @bind private onGetMarketDetails(marketId: string | number) {
    const { getMarketDetails } = this.props
    getMarketDetails(+marketId)
  }

  @bind private onNextMarket() {
    const { markets, selectMarket, selectedMarket, getOrders } = this.props
    const market = markets.find(
      (m: { id: number }) => m.id === selectedMarket.id + 1
    )
    if (market) {
      Router.push(`/trade?marketId=${market.id}`)
      selectMarket(market)
      getOrders(market.id)
    } else {
      Router.push(`/trade?marketId=${1}`)
      selectMarket(markets[0])
      getOrders(1)
    }
  }

  @bind private onPrevMarket() {
    const { markets, selectMarket, selectedMarket, getOrders } = this.props
    const market = markets.find(
      (m: { id: number }) => m.id === selectedMarket.id - 1
    )
    if (market) {
      Router.push(`/trade?marketId=${market.id}`)
      selectMarket(market)
      getOrders(market.id)
    } else {
      Router.push(`/trade?marketId=${markets.length}`)
      selectMarket(markets[markets.length - 1])
      getOrders(markets.length)
    }
  }

  render() {
    const {
      selectedMarket,
      marketDetails,
      favoriteMarkets,
      t,
      theme,
      gasPrice
    } = this.props
    const favorite =
      favoriteMarkets &&
      selectedMarket &&
      favoriteMarkets.some(({ id }) => selectedMarket.id === id)
    const headerColsNames = t('header.cols', { returnObjects: true })
    const headerInfo = {
      last_price: {
        point: marketDetails && marketDetails.lastPrice,
        sum: marketDetails && +marketDetails.lastPrice * gasPrice
      },
      day_change: {
        point: marketDetails && marketDetails.priceChange,
        percent: marketDetails
          ? !marketDetails.changeRate.match(/^\-/g) &&
            +marketDetails.changeRate !== 0
            ? `+${marketDetails.changeRate}`
            : marketDetails.changeRate
          : null
      },
      day_low: {
        point: marketDetails && marketDetails.lowPrice
      },
      day_high: {
        point: marketDetails && marketDetails.highPrice
      }
    }

    const token = selectedMarket ? selectedMarket.tokens : ''

    return (
      <Card>
        <Styled.Header>
          <Styled.Controls>
            <Button
              icon="keyboard_arrow_up"
              onClick={this.onPrevMarket}
              size="sm"
            />
            <Button
              icon="keyboard_arrow_down"
              onClick={this.onNextMarket}
              size="sm"
            />
          </Styled.Controls>
          <Styled.Title align="center">
            {selectedMarket && (
              <Flex align="center">
                <span>{token}</span>
                &nbsp;
                <i
                  className="material-icons"
                  style={{ color: favorite ? colors.warning : colors.white }}
                >
                  {favorite ? 'star' : 'star_border'}
                </i>
              </Flex>
            )}
          </Styled.Title>
          <Styled.Row>
            {Object.keys(headerInfo).map((key: string, i: number) => (
              <Styled.Article
                key={i}
                title={headerColsNames[i]}
                text={headerInfo[key].point}
                sum={headerInfo[key].sum}
                percent={headerInfo[key].percent}
                theme={theme}
              />
            ))}
          </Styled.Row>
        </Styled.Header>
      </Card>
    )
  }
}
