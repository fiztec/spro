import { ThemeStoreTypes } from '../../../stores/theme/types'
import { MarketsStoreTypes } from '../../../stores/markets/types'
import { OrdersStoreTypes } from '../../../stores/order/types'
import { I18nTProps } from '../../../utils/i18n'
import { BalanceStoreTypes } from '../../../stores/balance/types'

export interface ITradeHeader {
  query?: { marketId: number | string }
  t?: I18nTProps
  theme?: ThemeStoreTypes['theme']
  markets?: MarketsStoreTypes['markets']
  marketDetails?: MarketsStoreTypes['marketDetails']
  selectMarket?: MarketsStoreTypes['selectMarket']
  selectedMarket?: MarketsStoreTypes['selectedMarket']
  getMarketDetails?: MarketsStoreTypes['getMarketDetails']
  getOrders?: OrdersStoreTypes['getOrders']
  gasPrice?: BalanceStoreTypes['gasPrice']
  favoriteMarkets?: MarketsStoreTypes['favoriteMarkets']
}
