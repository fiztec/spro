import { inject, observer } from 'mobx-react'
import Card from '../../Card'
import { withNamespaces } from '../../../utils/i18n'
import { Table } from '../../Table'
import Tabs from '../../Tabs'
import Placeholder from './Placeholder'
import { Component } from 'react'
import { OrdersStoreTypes } from '../../../stores/order/types'
import { getUserToken } from '../../../utils/cookieService'
import * as Styled from './style'
import Button from '../../Button'
import Text from '../../Text'
import { bind } from '../../../utils/bind'
import Modal from '../../Modal'
import { OrdersProps } from './types'

@withNamespaces('trade')
@inject(
  ({
    ordersStore: {
      getOrders,
      ordersList,
      setOrders,
      cancelOrder,
      getOrderBook
    },
    marketsStore: { selectedMarket },
    metamaskStore: { metamaskStatus }
  }) => ({
    getOrders,
    ordersList,
    setOrders,
    selectedMarket,
    metamaskStatus,
    cancelOrder,
    getOrderBook
  })
)
@observer
export default class Orders extends Component<OrdersProps> {
  width = ['0%', '12%', '10%', '9%', '10%', '13%', '13%', '15%', '11%', '8%']

  state = {
    accountChanged: false,
    modal: null
  }

  public async componentWillReact() {
    const { accountChanged } = this.state
    const { metamaskStatus, getOrders, setOrders, query } = this.props

    if (metamaskStatus === 'account changed') {
      const { accountChanged } = this.state
      if (!accountChanged) {
        setOrders([])
      }
      this.setState({ accountChanged: true })
    }

    const token = await getUserToken()

    if (metamaskStatus === 'logged in' && accountChanged && token) {
      getOrders((query && query.marketId) || 1)
      this.setState({ accountChanged: false })
    }
  }

  public componentDidMount() {
    const { getOrders, query } = this.props

    getOrders((query && query.marketId) || 1)
  }

  @bind private handleToggleModal(modal: string) {
    this.setState(() => ({ modal }))
  }

  @bind private renderTabContent(data: OrdersStoreTypes['ordersList']) {
    const { disabled, t } = this.props
    const text = t('orders.table.cancelButton')

    return (
      <Table
        head={['', ...t('orders.table.rows', { returnObjects: true })]}
        width={this.width}
        body={!disabled && data}
        deleteButton={{
          text,
          onClick: orderId => {
            this.handleToggleModal(`cancelOrder-${orderId}`)
          }
        }}
      >
        {disabled && <Placeholder />}
      </Table>
    )
  }

  public render() {
    const {
      t,
      lng,
      ordersList,
      cancelOrder,
      getOrderBook,
      query,
      getOrders
    } = this.props
    const { modal } = this.state
    const tabs = t('orders.tabs', { returnObjects: true })

    const orders = ordersList || []
    return (
      <Styled.Wrapp>
        <Card title={t('orders.title')} rtl={lng === 'ar'}>
          <Tabs
            defaultTab="all"
            tabLink={
              Array.isArray(tabs) &&
              tabs.map((label: string) => ({
                name: label.toLowerCase().replace(' ', '_'),
                label
              }))
            }
            tabContent={[
              {
                name: 'selling_orders',
                content: this.renderTabContent(
                  orders.filter(({ type }) => type === 'SELL')
                )
              },
              {
                name: 'buying_orders',
                content: this.renderTabContent(
                  orders.filter(({ type }) => type === 'BUY')
                )
              },
              {
                name: 'all',
                content: this.renderTabContent(orders)
              }
            ]}
          />
        </Card>

        <Modal
          open={modal && modal.match(/cancelOrder/)}
          title={t('orders.modalCancelOrder.title')}
          onClose={() => this.handleToggleModal(null)}
        >
          <Text>{t('orders.modalCancelOrder.text')}</Text>
          <br />
          <Button
            text={t('orders.modalCancelOrder.button')}
            theme="primary"
            onClick={() => {
              cancelOrder(Number(modal.split('-')[1]), async () => {
                await getOrders((query && query.marketId) || 1)
                await getOrderBook((query && query.marketId) || 1)
                await this.handleToggleModal(null)
              })
            }}
          />
        </Modal>
      </Styled.Wrapp>
    )
  }
}
