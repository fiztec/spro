import { I18nTProps } from '../../../utils/i18n'
import { MarketsStoreTypes } from '../../../stores/markets/types'
import { OrdersStoreTypes } from '../../../stores/order/types'
import { MetamaskStoreTypes } from '../../../stores/metamask/types'

export interface OrdersProps extends Partial<OrdersStoreTypes> {
  t?: I18nTProps
  lng?: string
  disabled?: boolean
  query?: {
    marketId: number | string
  }
  selectedMarket?: MarketsStoreTypes['selectedMarket']
  metamaskStatus?: MetamaskStoreTypes['metamaskStatus']
}
