import { memo } from 'react'
import * as Styled from './style'
import { ISwitcher } from './types'

function Switcher({ active = false, ...rest }: ISwitcher) {
  return <Styled.Switcher active={active} {...rest} />
}

export default memo(Switcher)
