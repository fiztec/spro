import styled, { css } from 'styled-components'
import { colors } from '../../styles/colors'
import { time } from '../../styles/base'

export const Switcher = styled(({ active, ...props }) => (
  <div {...props}>
    <span />
  </div>
))`
  width: 30px;
  height: 11px;
  border-radius: 20px;
  background: ${colors.text};
  position: relative;
  cursor: pointer;
  transition: background ${time.fast};

  span {
    width: 18px;
    height: 18px;
    border-radius: 50%;
    background: ${colors.white};
    display: block;
    position: absolute;
    top: -4px;
    left: 0;
    transition: transform ${time.fast}, box-shadow ${time.fast};
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.25);
  }

  &:hover {
    span {
      box-shadow: 0 5px 15px rgba(0, 0, 0, 0.5);
    }
  }

  &:active {
    span {
      transform: scale(0.9);
    }
  }

  ${({ active }) =>
    active &&
    css`
      background: ${colors.primary};

      span {
        transform: translateX(13px);
      }

      &:active {
        span {
          transform: scale(0.9) translateX(14px);
        }
      }
    `}

  ${({ disabled }) =>
    disabled &&
    css`
      opacity: 0.5;
      pointer-events: none;
    `}
`
