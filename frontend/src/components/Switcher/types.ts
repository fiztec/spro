export interface ISwitcher {
  active: boolean
  onClick: () => void
  disabled?: boolean
}
