export type DropdownItemTypes = {
  icon?: string
  image?: string
  label: string
  disabled?: boolean
  onClick: () => void
}
