type OrderTypes = {
  pair: string
  type: string
  price: string
  amount: string
  avaliableAmount: string
  confirmedAmount: string
  createdAt: string
}

export interface IOrderBookItem {
  amount: string
  myAmount?: string
  price: string
  total?: string
}

export interface IOrderData {
  price: string
  amount: string
  tokens: string
  isSell: boolean
  isMarket: boolean
  baseToken: string
  quoteToken: string
  asMakerFeeRate: string
  asTakerFeeRate: string
}

export interface OrdersStoreTypes {
  ordersList: OrderTypes[]
  orderStatus: string
  error: string
  relayer: string
  orderBook: {
    marketId: number
    BUY?: IOrderBookItem[]
    SELL?: IOrderBookItem[]
  }
  enableOrder: boolean
  orderValuesFromBook: {
    price: string
    amount: string
  }
  setOrderStatus: (status: string) => void
  createOrder: (orderData: IOrderData) => void
  getOrders: (marketId: number | string) => void
  setOrders: (orders: OrdersStoreTypes['ordersList'] | []) => void
  getOrderBook: (marketId: number | string) => void
  addOrderBook: (order: OrdersStoreTypes['orderBook']) => void
  handleToggleOrder: (data: Partial<IOrderData>, enable: boolean) => void
  cancelOrder: (orderId: number, onSuccess: () => void) => void
  setOrderValuesFromBook: (
    values: OrdersStoreTypes['orderValuesFromBook']
  ) => OrdersStoreTypes['orderValuesFromBook']
  getAllowance: (data: Partial<IOrderData>) => void
}

export interface SignOrderTypes {
  version: number
  trader: string
  relayer: string
  baseToken: string
  quoteToken: string
  baseTokenAmount: number
  quoteTokenAmount: number
  gasTokenAmount: number | string
  isSell: boolean
  isMarket: boolean
  expiredAtSeconds: string | number
  asMakerFeeRate: string
  asTakerFeeRate: string
  makerRebateRate: number
  salt: number
  data: string
}
