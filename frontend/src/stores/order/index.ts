import { observable, action } from 'mobx'
import { useStaticRendering } from 'mobx-react'
import crypto from 'crypto'
import BigNumber from 'bignumber.js'
import moment from 'moment'
import sortBy from 'lodash/sortBy'
import { keccak256, fromRpcSig } from 'ethereumjs-util'
import { isServer } from '../../utils/isServer'
import { OrdersStoreTypes, SignOrderTypes, IOrderData } from './types'
import { apiCaller } from '../../utils/apiCaller'
import { GASPRICE_API_URL, API_URL } from '../../config/api'
import { bind } from '../../utils/bind'

useStaticRendering(isServer)

class OdersStore {
  @observable ordersList = []
  @observable orderStatus = null
  @observable error = null
  @observable relayer = null
  @observable orderBook = null
  @observable enableOrder = false
  @observable orderValuesFromBook = { price: '', amount: '' }
  proxyAddress = '0x7034f03e94977e30210353e68799a933bdd77d37'

  constructor({}, initialState: OrdersStoreTypes) {
    this.ordersList = initialState ? initialState.ordersList : []
    this.error = initialState ? initialState.error : null
    this.relayer = initialState ? initialState.relayer : null
    this.orderBook = initialState ? initialState.orderBook : null
    this.enableOrder = initialState ? initialState.enableOrder : false
  }

  @bind @action public async getOrders() {
    const res = await apiCaller(`${API_URL}/orders/user/0`) // Set 0 for request all orders
    const { relayer } = await apiCaller(`${API_URL}/data/relayer/`)

    if (res) {
      this.ordersList = res.map(
        ({
          id,
          market,
          side,
          price,
          amount,
          availabelAmount,
          confirmedAmount,
          createdAt,
          status
        }) => ({
          id,
          pair: market.tokens,
          type: side,
          price,
          amount,
          availabelAmount,
          confirmedAmount,
          createdAt: moment(createdAt).format('DD/MM/YYYY HH:mm'),
          status
        })
      )
    }

    if (relayer) {
      this.relayer = relayer
    }
  }

  @bind @action public setError(error: OrdersStoreTypes['error']) {
    this.error = error
  }

  @bind @action public setOrderStatus(status: OrdersStoreTypes['orderStatus']) {
    this.orderStatus = status
  }

  @bind @action public setOrders(orders: OrdersStoreTypes['ordersList']) {
    this.ordersList = orders
  }

  @bind @action public async getOrderBook(marketId: number = 1) {
    const res = await apiCaller(`${API_URL}/order-book/history/${marketId}`)
    const data = {
      ...res,
      SELL: res.SELL.map(({ price, amount }) => ({
        price,
        amount,
        total: new BigNumber(price).times(new BigNumber(amount)).toFixed()
      })),
      BUY: res.BUY.map(({ price, amount }) => ({
        price,
        amount,
        total: new BigNumber(price).times(new BigNumber(amount)).toFixed()
      }))
    }
    if (res) this.orderBook = data
  }

  @bind @action public addOrderBook(orderBook: OrdersStoreTypes['orderBook']) {
    if (this.orderBook && +this.orderBook.marketId === orderBook.marketId) {
      this.orderBook.SELL = sortBy(
        [...this.orderBook.SELL, ...orderBook.SELL],
        ({ price }) => price
      ).reverse()

      this.orderBook.BUY = sortBy(
        [...this.orderBook.BUY, ...orderBook.BUY],
        ({ price }) => price
      ).reverse()
    }
  }

  @bind @action private async signOrder({
    price,
    amount,
    tokens,
    baseToken,
    quoteToken,
    isSell,
    isMarket,
    asMakerFeeRate,
    asTakerFeeRate
  }) {
    if (!isServer && window.web3 !== 'undefined') {
      const w3 = window.web3
      const publicAddress = w3.eth.coinbase
      const gasPrice = await apiCaller(GASPRICE_API_URL)

      if (gasPrice) {
        this.setError(null)
        this.setOrderStatus(isSell ? 'sellPending' : 'buyPending')
        const sha3ToHex = (message: string) => {
          return '0x' + keccak256(message).toString('hex')
        }

        const FEE_RATE_BASE = 100000

        const EIP712_DOMAIN_TYPEHASH = sha3ToHex('EIP712Domain(string name)')

        const EIP712_ORDER_TYPE = sha3ToHex(
          'Order(address trader,address relayer,address baseToken,address quoteToken,uint256 baseTokenAmount,uint256 quoteTokenAmount,uint256 gasTokenAmount,bytes32 data)'
        )

        const generateOrderData = (order: SignOrderTypes) => {
          const {
            version,
            isSell,
            isMarket,
            expiredAtSeconds,
            asMakerFeeRate,
            asTakerFeeRate,
            makerRebateRate,
            salt
          } = order

          let res = '0x'
          res += addLeadingZero(new BigNumber(version).toString(16), 2)
          res += isSell ? '01' : '00'
          res += isMarket ? '01' : '00'
          res += addLeadingZero(
            new BigNumber(expiredAtSeconds).toString(16),
            5 * 2
          )
          res += addLeadingZero(
            new BigNumber(asMakerFeeRate).toString(16),
            2 * 2
          )
          res += addLeadingZero(
            new BigNumber(asTakerFeeRate).toString(16),
            2 * 2
          )
          res += addLeadingZero(
            new BigNumber(makerRebateRate).toString(16),
            2 * 2
          )
          res += addLeadingZero(new BigNumber(salt).toString(16), 8 * 2)

          return addTailingZero(res, 66)
        }

        function addLeadingZero(str: string, length: number) {
          let len = str.length
          return '0'.repeat(length - len) + str
        }

        function addTailingZero(str: string, length: number) {
          let len = str.length
          return str + '0'.repeat(length - len)
        }

        const getOrderHash = (order: SignOrderTypes) => {
          return getEIP712MessageHash(
            sha3ToHex(
              EIP712_ORDER_TYPE +
                addLeadingZero(order.trader.slice(2), 64) +
                addLeadingZero(order.relayer.slice(2), 64) +
                addLeadingZero(order.baseToken.slice(2), 64) +
                addLeadingZero(order.quoteToken.slice(2), 64) +
                addLeadingZero(
                  new BigNumber(order.baseTokenAmount).toString(16),
                  64
                ) +
                addLeadingZero(
                  new BigNumber(order.quoteTokenAmount).toString(16),
                  64
                ) +
                addLeadingZero(
                  new BigNumber(order.gasTokenAmount).toString(16),
                  64
                ) +
                order.data.slice(2)
            )
          )
        }

        function getEIP712MessageHash(message) {
          return sha3ToHex(
            '0x1901' + getDomainSeparator().slice(2) + message.slice(2)
          )
        }

        function getDomainSeparator() {
          return sha3ToHex(
            EIP712_DOMAIN_TYPEHASH + sha3ToHex('Hydro Protocol').slice(2)
          )
        }

        // ************************

        const relayer = this.relayer
        const contractGas = 190000
        const baseDecimals = 18 // get decimals from token smart contract
        const quoteDecimals = 18 // get decimals from token smart contract
        const expiresAt = new Date()
        const baseTokenAmount = +new BigNumber(amount)
          .times(new BigNumber(Math.pow(10, baseDecimals)))
          .toFixed()

        const getGasAmount = (gasPrice: number) => {
          /**
           *
           * @param gasPrice - for test orders = 10
           * @param baseDecimals - from market, for test = 18
           * @returns {string}
           */
          return new BigNumber(gasPrice)
            .div(Math.pow(10, 9))
            .multipliedBy(new BigNumber(contractGas))
            .multipliedBy(new BigNumber(Math.pow(10, baseDecimals))) // Gwei to Wei
            .toFixed(0)
        }

        const gasTokenAmount = Number(getGasAmount(gasPrice.standard))

        const quoteTokenAmount = price
          ? Number(
              new BigNumber(amount)
                .multipliedBy(new BigNumber(price))
                .multipliedBy(new BigNumber(Math.pow(10, quoteDecimals)))
            )
          : 0

        const traderOrder = {
          version: 2, // uint256 public constant SUPPORTED_ORDER_VERSION = 2
          trader: publicAddress,
          relayer,
          baseToken,
          quoteToken,
          baseTokenAmount,
          quoteTokenAmount,
          gasTokenAmount,
          isSell, // buy or sell
          isMarket,
          expiredAtSeconds: Math.floor(
            expiresAt.setMonth(expiresAt.getMonth() + 2) / 1000
          ),
          asMakerFeeRate: new BigNumber(asMakerFeeRate)
            .times(new BigNumber(FEE_RATE_BASE))
            .toString(), // Fee rate from selected market
          asTakerFeeRate: new BigNumber(asTakerFeeRate)
            .times(new BigNumber(FEE_RATE_BASE))
            .toString(), // Fee rate from selected market
          makerRebateRate: 0, // uint256 public constant REBATE_RATE_BASE = 100
          salt: parseInt(crypto.randomBytes(6).toString('hex'), 16),
          data: null
        }

        traderOrder.data = generateOrderData(traderOrder)

        const traderOrderHash = getOrderHash(traderOrder)

        w3.personal.sign(
          w3.toHex(traderOrderHash),
          w3.eth.coinbase,
          (
            err: { code: number | string; message: string },
            signature: string
          ) => {
            this.setOrderStatus(null)
            if (err) {
              this.setOrderStatus('canceled')
              this.setError(err.message)
              return
            }

            function padToBytes32(n: string) {
              while (n.length < 64) n += '0'
              return '0x' + n
            }

            const traderOrderSigVRS = fromRpcSig(signature)
            let traderOrderSigConfig =
              Number(traderOrderSigVRS.v).toString(16) + '00'
            traderOrderSigConfig = padToBytes32(traderOrderSigConfig)
            const _traderOrderSig = {
              config: traderOrderSigConfig,
              r: '0x' + traderOrderSigVRS.r.toString('hex'),
              s: '0x' + traderOrderSigVRS.s.toString('hex')
            }

            // **********************************************
            // SUBMIT TO /orders/
            const traderOrderParam = {
              trader: traderOrder.trader,
              baseTokenAmount: Math.floor(traderOrder.baseTokenAmount),
              quoteTokenAmount: Math.floor(traderOrder.quoteTokenAmount),
              gasTokenAmount: Math.floor(traderOrder.gasTokenAmount),
              data: traderOrder.data,
              tokens: tokens,
              signature: _traderOrderSig
            }

            if (traderOrderParam) {
              apiCaller(`${API_URL}/orders`, 'POST', traderOrderParam)
                .then(res => {
                  if (res) {
                    apiCaller(`${API_URL}/orders/match/${res.id}`, 'PUT')
                      .then(() => {
                        this.setOrderStatus('success')
                        this.setOrderValuesFromBook({ price: '', amount: '' })
                      })
                      .catch(({ response: { data: { message, error } } }) => {
                        if (message || error) {
                          this.setError(message || error)
                          this.setOrderStatus('submitError')
                        }
                      })
                  }
                })
                .catch(({ response: { data: { message, error } } }) => {
                  if (message || error) {
                    this.setError(message || error)
                    this.setOrderStatus('submitError')
                  }
                })
            }
          }
        )
      }
    }
  }

  @bind @action public createOrder(data: IOrderData) {
    const web3 = window.web3
    const publicAddress = web3.eth.coinbase
    const targetAddress = data.isSell ? data.baseToken : data.quoteToken
    const abiBalance = [
      {
        constant: true,
        inputs: [{ name: '_owner', type: 'address' }],
        name: 'balanceOf',
        outputs: [{ name: '', type: 'uint256' }],
        payable: false,
        type: 'function'
      }
    ]

    const token = web3.eth.contract(abiBalance).at(targetAddress)
    token.balanceOf.call(publicAddress, (err: string, b: number) => {
      if (!err) {
        const balance = web3.fromWei(b, 'ether') + ''
        const total = new BigNumber(data.price)
          .times(new BigNumber(data.amount))
          .toFixed()

        if (data.isSell && +balance < +data.amount) {
          this.orderStatus = 'lowTokenBalance'
          return
        }
        if (!data.isSell && +balance < +total) {
          this.orderStatus = 'lowWethBalance'
          return
        }

        this.signOrder(data)
      } else {
        this.error === err
      }
    })
  }

  @bind @action public async getAllowance(data: Partial<IOrderData>) {
    const web3 = window.web3
    const tokenAddress = data.isSell ? data.baseToken : data.quoteToken
    const abi = [
      {
        constant: true,
        inputs: [
          {
            name: '_owner',
            type: 'address'
          },
          {
            name: '_spender',
            type: 'address'
          }
        ],
        name: 'allowance',
        outputs: [
          {
            name: '',
            type: 'uint256'
          }
        ],
        payable: false,
        stateMutability: 'view',
        type: 'function'
      }
    ]

    const userAddress = web3.eth.coinbase

    web3.eth
      .contract(abi)
      .at(tokenAddress)
      .allowance(userAddress, this.proxyAddress, (err, res) => {
        if (err) {
          if (err) {
            this.error = err
            this.orderStatus = this.enableOrder ? 'enableError' : 'disableError'
          }
        }
        if (res) {
          const allowance = Number(new BigNumber(res).toString())
          if (allowance > 0) {
            this.enableOrder = true
            if (this.enableOrder && this.orderStatus === 'enableConfirm') {
              this.setOrderStatus('enableSuccess')
            }
          } else {
            this.enableOrder = false
            if (!this.enableOrder && this.orderStatus === 'disableConfirm') {
              this.setOrderStatus('disableSuccess')
            }
          }
        }
      })
  }

  @bind @action public handleToggleOrder(data: IOrderData, enable: boolean) {
    const web3 = window.web3

    const tokenAddress = data.isSell ? data.baseToken : data.quoteToken

    const abi = [
      {
        constant: false,
        inputs: [
          {
            name: 'guy',
            type: 'address'
          },
          {
            name: 'wad',
            type: 'uint256'
          }
        ],
        name: 'approve',
        outputs: [
          {
            name: '',
            type: 'bool'
          }
        ],
        payable: false,
        stateMutability: 'nonpayable',
        type: 'function'
      }
    ]

    this.orderStatus = enable ? 'disableRequest' : 'enableRequest'
    web3.eth
      .contract(abi)
      .at(tokenAddress)
      .approve(
        this.proxyAddress,
        enable ? 0 : new web3.BigNumber(2).pow(256).minus(1),
        (err, res) => {
          if (err) {
            this.error = err
            this.orderStatus = enable ? 'disableError' : 'enableError'
          }

          if (!err) {
            this.orderStatus = enable ? 'disableConfirm' : 'enableConfirm'
          }

          if (res) {
            web3.currentProvider.publicConfigStore.on('update', () => {
              this.getAllowance(data)
            })
          }
        }
      )
  }

  @bind @action public async cancelOrder(
    orderId: number,
    onSuccess: () => void
  ) {
    try {
      await apiCaller(`${API_URL}/orders/order/${orderId}`, 'PATCH')
      await onSuccess()
    } catch ({ response }) {
      this.error = response.data.message
    }
  }

  @bind @action setOrderValuesFromBook(
    values: OrdersStoreTypes['orderValuesFromBook']
  ) {
    this.orderValuesFromBook = values
  }
}

export const initOrdersStore = initialState => {
  return new OdersStore(isServer, initialState)
}
