import { observable, action } from 'mobx'
import { useStaticRendering } from 'mobx-react'
import { isServer } from '../../utils/isServer'
import { IBotStore, IStartBotData } from './types'
import { bind } from '../../utils/bind'
import { apiCaller } from '../../utils/apiCaller'
import { API_URL } from '../../config/api'

useStaticRendering(isServer)

class BotStore {
  @observable bot: IBotStore['bot']
  @observable error: IBotStore['error']
  @observable status: IBotStore['status']

  @bind @action public async getBot(marketId: number) {
    try {
      const res = await apiCaller(`${API_URL}/bots/${marketId}`)
      this.bot = res
    } catch ({ response }) {
      this.bot = null
      this.error = response.data.message
    }
  }

  @bind @action public async startBot(marketId: number, data: IStartBotData) {
    this.setStatus('request')
    try {
      await apiCaller(`${API_URL}/bots/start/${marketId}`, 'POST', data)
      await this.getBot(marketId)
      await this.setStatus('success')
    } catch ({ response }) {
      this.error = response.data.message
      this.setStatus('error')
    }
  }

  @bind @action public async stopBot(marketId: number) {
    this.setStatus('request')
    try {
      await apiCaller(`${API_URL}/bots/stop/${marketId}`, 'PATCH')
      await this.getBot(marketId)
      await this.setStatus('success')
    } catch ({ response }) {
      this.error = response.data.message
      this.setStatus('error')
    }
  }

  @bind @action public async setError(error: string | null) {
    this.error = error
  }

  @bind @action setStatus(status: IBotStore['status']) {
    this.status = status
  }
}

export const initBotStore = () => {
  return new BotStore()
}
