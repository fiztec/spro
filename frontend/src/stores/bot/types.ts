export interface IStartBotData {
  minPrice: number
  maxPrice: number
  priceGap: number
  expandInventory: number
}

export interface IBotStore {
  bot: {
    botname: string
    publicAddress: string
    isRunning: boolean
    minPrice: string
    maxPrice: string
    priceGap: string
    expandInventory: string
  }
  error: string
  status: 'request' | 'success' | 'error'
  getBot: (marketId: number) => void
  startBot: (marketId: number, data: IStartBotData) => void
  stopBot: (marketId: number) => void
  setError: (error: string | null) => void
  setStatus: (status: string | null) => void
}
