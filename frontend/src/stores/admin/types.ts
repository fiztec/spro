interface IWallet {
  publicAddress: string
}

export interface IToken {
  baseTokenProjectUrl: string
  baseTokenName: string
  baseToken: string
  baseTokenDecimals: number | string
  baseTokenAddress: string
  minOrderSize: string
  pricePrecision: number | string
  priceDecimals: number | string
  amountDecimals: number | string
  supportedOrderTypes: string
  marketOrderMaxSlippage: string
}

export interface ITransaction {
  amount: string
  createdAt: string
  id: number
  price: string
  side: string
  tokens: string
  takerWallet?: string
}

export interface IAdminStore {
  publicAddress: string
  wallets: IWallet[]
  activeWallet: string
  error: string | object
  isAdmin: boolean
  transactions: ITransaction[]
  feesWallet: {
    feesBalance: string
    feesBalanceUSD: string
    wallet: string
  }
  fees: {
    asMakerFeeRate: string
    asTakerFeeRate: string
  }
  getWallets: () => void
  addToken: (
    data: IToken,
    onSuccess: (res: IToken) => void,
    onError: (error: any) => void
  ) => void
  getUser: () => void
  getTransactionList: (takerAddress: string) => void
  getFeesWallet: () => void
  getFees: () => IAdminStore['fees']
  changeFees: (values: Partial<IAdminStore['fees']>) => void
  setActiveWallet: (takerAddress: string) => void
}
