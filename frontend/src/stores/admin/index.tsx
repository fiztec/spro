import { observable, action } from 'mobx'
import { useStaticRendering } from 'mobx-react'
import moment from 'moment'
import { isServer } from '../../utils/isServer'
import { IAdminStore, IToken, ITransaction } from './types'
import { apiCaller } from '../../utils/apiCaller'
import { API_URL, ETHPRICE_API_URL, ETHPRICE_KEY } from '../../config/api'
import { bind } from '../../utils/bind'
import { redirect } from '../../utils/redirect'
import {
  getUserToken,
  setUserToken,
  removeUserToken
} from '../../utils/cookieService'

useStaticRendering(isServer)

class AdminStore {
  @observable wallets: IAdminStore['wallets']
  @observable activeWallet: IAdminStore['activeWallet']
  @observable error: IAdminStore['error']
  @observable isAdmin: IAdminStore['isAdmin'] = null
  @observable transactions: IAdminStore['transactions'] = []
  @observable feesWallet: IAdminStore['feesWallet'] = {
    feesBalance: '',
    feesBalanceUSD: '',
    wallet: ''
  }

  @observable fees: IAdminStore['fees']
  @observable publicAddress: IAdminStore['publicAddress']
  @observable signRequested: boolean = false

  @bind @action private authUser(publicAddress: string, nonce: string) {
    this.signRequested = true
    if (!this.signRequested) {
      window.web3.personal.sign(
        `MeterQubes authentication with one-time nonce: ${nonce}`,
        publicAddress,
        async (err: string, signature: string) => {
          if (err) {
            this.error = err
          } else {
            const accessToken = await apiCaller(`${API_URL}/auth`, 'POST', {
              publicAddress,
              signature
            })

            setUserToken(accessToken)
          }
        }
      )
    }
  }

  @bind @action public async createUser(publicAddress: string) {
    const { nonce } = await apiCaller(`${API_URL}/users`, 'POST', {
      publicAddress
    })

    if (nonce && !this.signRequested) {
      this.authUser(publicAddress, nonce)
    }
  }

  @bind @action public async getUser() {
    const publicAddress = window.web3.eth.coinbase
    const token = getUserToken()

    try {
      const { isAdmin, nonce } = await apiCaller(
        `${API_URL}/users?publicAddress=${publicAddress}`
      )

      if (!isAdmin) {
        redirect(null, '/login')
      }

      if (!token) {
        if (!this.signRequested) this.authUser(publicAddress, nonce)
        await this.getWallets()
        await this.getTransactionList(this.activeWallet)
        this.getFees()
        this.getFeesWallet()
      }

      this.isAdmin = isAdmin
      // this.signRequested = false
    } catch ({ response }) {
      if (response && response.status === 404) {
        this.createUser(publicAddress)
        this.getUser()
      }
    }
  }

  @bind @action public async getWallets() {
    try {
      const { addresses } = await apiCaller(`${API_URL}/admin/users/wallets`)
      this.wallets = addresses.map((publicAddress: string) => ({
        publicAddress
      }))
      this.activeWallet = this.wallets[0].publicAddress
    } catch (error) {
      this.error = error
    }
  }

  @bind @action public async addToken(data: IToken, onSuccess, onError) {
    try {
      const res = await apiCaller(`${API_URL}/admin/token`, 'POST', {
        ...data,
        baseTokenAddress: data.baseTokenAddress.toLowerCase(),
        supportedOrderTypes:
          data.supportedOrderTypes === 'MARKET, LIMIT'
            ? ['MARKET', 'LIMIT']
            : [data.supportedOrderTypes]
      })
      await onSuccess(res)
    } catch ({ response }) {
      this.error = response
      let errors
      response &&
        response.data &&
        response.data.errors &&
        response.data.errors.forEach(item => {
          if (item.path === 'tokens') {
            errors = {
              ...errors,
              baseToken: 'Token already exists'
            }
          }

          if (item.path === 'baseTokenAddress') {
            errors = {
              ...errors,
              baseTokenAddress: 'Address already exists'
            }
          }
        })

      onError(errors)
    }
  }

  @bind @action public async getTransactionList(takerAddress: string) {
    try {
      const res = await apiCaller(`${API_URL}/admin/transactions/list`)

      const transactions = res.transactions
        .map(({ tokens, side, price, amount, createdAt, takerWallet }) => ({
          tokens,
          side,
          price,
          amount,
          takerWallet,
          createdAt: moment(createdAt).format('DD/MM/YYYY HH:mm')
        }))
        .filter((t: ITransaction) => t.takerWallet === takerAddress)

      this.transactions = transactions
    } catch ({ response }) {
      if (response && (response.status === 403 || response.status === 401)) {
        removeUserToken()
        redirect(null, '/login')
      }
      this.error = response
    }
  }

  @bind @action public async getFees() {
    try {
      const res = await apiCaller(`${API_URL}/admin/fees`)

      this.fees = res
    } catch ({ response }) {
      if (response && (response.status === 403 || response.status === 401)) {
        removeUserToken()
        redirect(null, '/login')
      }
      this.error = response
    }
  }

  @bind @action public async changeFees(values: IAdminStore['fees']) {
    try {
      await apiCaller(`${API_URL}/admin/fees/trade`, 'PATCH', values)
      await this.getFees()
    } catch ({ response }) {
      if (response && (response.status === 403 || response.status === 401)) {
        removeUserToken()
        redirect(null, '/login')
      }
      this.error = response
    }
  }

  @bind @action public async getFeesWallet() {
    try {
      const res = await apiCaller(`${API_URL}/eth/fees-wallet`)
      const {
        result: { ethusd }
      } = await apiCaller(
        `${ETHPRICE_API_URL}api?module=stats&action=ethprice&apikey=${ETHPRICE_KEY}`
      )

      this.feesWallet = {
        ...res,
        feesBalanceUSD: res.feesBalance * ethusd
      }
    } catch ({ response }) {
      if (response && (response.status === 403 || response.status === 401)) {
        removeUserToken()
        redirect(null, '/login')
      }
      this.error = response
    }
  }

  @bind @action public setActiveWallet(takerAddress: string) {
    this.activeWallet = takerAddress
  }
}

export const initAdminStore = () => {
  return new AdminStore()
}
