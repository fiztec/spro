interface ITradeHistoryItem {
  amount: string
  createdAt: string
  id?: number
  price: string
  side: string
}

export interface TradeHistoryStoreTypes {
  history: ITradeHistoryItem[]
  getHistory: (marketId: number | string, myHistory?: boolean) => void
  setHistory: (history: ITradeHistoryItem | []) => void
}
