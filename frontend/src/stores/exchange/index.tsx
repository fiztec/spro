import { observable, action } from 'mobx'
import { useStaticRendering } from 'mobx-react'
import { isServer } from '../../utils/isServer'
import { IExchangeStore, IExchangeToken } from './types'
import { bind } from '../../utils/bind'
import { API_URL, GASPRICE_API_URL } from '../../config/api'
import { apiCaller } from '../../utils/apiCaller'

useStaticRendering(isServer)

// mobx store for exchange

class ExchangeStore {
  @observable error: IExchangeStore['error']
  @observable tokens: IExchangeStore['tokens']
  @observable status: IExchangeStore['status']

  constructor({}, initialState: IExchangeStore) {
    this.error = initialState ? initialState.error : null
    this.tokens = initialState ? initialState.tokens : []
    this.status = initialState ? initialState.status : null
  }

  @bind @action public async getTokens() {
    const res = await apiCaller(`${API_URL}/exchange/token`)

    const tokenSymbols = res.map(({ tokenSymbol }) => tokenSymbol)
    const { RAW } = await apiCaller(
      `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${tokenSymbols}&tsyms=USD`
    )
    const tokens = await res.map(props => {
      const symbol = RAW[props.tokenSymbol]
      const image = symbol
        ? `https://www.cryptocompare.com${symbol.USD.IMAGEURL}`
        : '/static/img/ethereum.svg'
      return {
        ...props,
        label: props.tokenName,
        image
      }
    })
    this.tokens = tokens
  }

  @bind @action private exchangeWeth(
    {
      exchangeOrderData,
      exchangeOrderSignature,
      exchangeOrderIdEth,
      ethAmount,
      wethAddress
    },
    type: string,
    onSuccess: (transaction: string) => void
  ) {
    const web3 = window.web3
    const proxyAddress = '0x619e403af18365d09719456923c4a93b41ad3db1'

    const hydroSwapABI = [
      {
        constant: false,
        inputs: [
          {
            name: 'id',
            type: 'bytes32'
          },
          {
            name: 'orderAddresses',
            type: 'address[5]'
          },
          {
            name: 'orderValues',
            type: 'uint256[6]'
          },
          {
            name: 'v',
            type: 'uint8'
          },
          {
            name: 'r',
            type: 'bytes32'
          },
          {
            name: 's',
            type: 'bytes32'
          }
        ],
        name: 'swap',
        outputs: [
          {
            name: 'takerTokenFilledAmount',
            type: 'uint256'
          }
        ],
        payable: true,
        stateMutability: 'payable',
        type: 'function'
      }
    ]

    let orderAddresses = [
      exchangeOrderData.maker,
      exchangeOrderData.taker,
      exchangeOrderData.makerToken,
      exchangeOrderData.takerToken,
      exchangeOrderData.feeRecipient
    ]

    let orderValues = [
      exchangeOrderData.makerTokenAmount.toString(),
      exchangeOrderData.takerTokenAmount.toString(),
      exchangeOrderData.makerFee.toString(),
      exchangeOrderData.takerFee.toString(),
      exchangeOrderData.expirationTimestampInSec.toString(),
      exchangeOrderData.salt.toString()
    ]

    let { v, r, s } = exchangeOrderSignature

    const HydroSwapContract = web3.eth
      .contract(hydroSwapABI)
      .at(exchangeOrderData.taker)

    web3.eth
      .contract([
        {
          constant: false,
          inputs: [
            {
              name: 'guy',
              type: 'address'
            },
            {
              name: 'wad',
              type: 'uint256'
            }
          ],
          name: 'approve',
          outputs: [
            {
              name: '',
              type: 'bool'
            }
          ],
          payable: false,
          stateMutability: 'nonpayable',
          type: 'function'
        }
      ])
      .at(wethAddress)
      .approve(
        proxyAddress,
        new web3.BigNumber(2).pow(256).minus(1), // MAX_INT, unlimited allowance
        async (err, res) => {
          if (err) {
            this.setError(err.message)
          }

          if (res) {
            const gasPrice = await apiCaller(GASPRICE_API_URL)

            await HydroSwapContract.swap(
              exchangeOrderIdEth,
              orderAddresses,
              orderValues,
              v,
              r,
              s,
              {
                from: web3.eth.coinbase,
                value: type === 'eth' ? web3.toWei(ethAmount, 'ether') : 0, // ethAmount - giveAmount from exchange form || 0
                gas: type === 'eth' ? '300000' : '400000',
                gasPrice: web3.toWei(gasPrice.fast, 'gwei') // GET fast gas price from gas API
              },
              (err, result) => {
                if (err) this.setError(err.message)
                if (result) {
                  onSuccess(result)
                }
              }
            )
          }
        }
      )
  }

  @bind @action public async exchangeTokens(
    data: IExchangeToken,
    type: string,
    onSuccess: (transaction: string) => void
  ) {
    try {
      await this.setStatus('exchange-request')
      const res = await apiCaller(`${API_URL}/exchange`, 'POST', data)
      await this.exchangeWeth(
        {
          ...res,
          ethAmount: data.giveAmount,
          wethAddress: res.exchangeOrderData.takerToken
        },
        type,
        async txHash => {
          this.setStatus('exchange-verifying')
          try {
            await apiCaller(`${API_URL}/exchange/tx`, 'POST', {
              txHash,
              exchangeOrderId: res.exchangeOrderIdDB
            })
            await onSuccess(txHash)
            await this.setStatus('exchange-success')
          } catch ({ response }) {
            this.setError(response && response.data.message)
            this.setStatus('exchange-error')
          }
        }
      )
    } catch ({ response }) {
      this.setError(response && response.data.message)
      this.setStatus('exchange-error')
    }
  }

  @bind @action public async setError(error: string) {
    this.error = error
  }

  @bind @action public async setStatus(status: string) {
    this.status = status
  }
}

export const initExchangeStore = initialState => {
  return new ExchangeStore(isServer, initialState)
}
