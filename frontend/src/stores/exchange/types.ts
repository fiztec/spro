export interface IToken {
  id: number
  tokenSymbol: string
  label: string
  tokenAddress: string
  tokenDecimals: number
  minOrderSize: string
  currentPriceToWETH: string
  image: string
}

export interface IExchangeToken {
  giveTokenId: number
  giveAmount: string
  receiveTokenId: number
  receiveAmount: string
}

export interface IExchangeStore {
  error: string
  tokens: IToken[]
  status: string
  setError: (error: string) => void
  getTokens: () => Partial<IToken>
  exchangeTokens: (
    data: IExchangeToken,
    type: string,
    onSuccess: (transaction: string) => void
  ) => void
  setStatus: (status: string) => void
}
