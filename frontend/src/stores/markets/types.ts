export interface MarketTypes {
  id: number
  tokens: string
  price: string
  dayVol: string
  dayPrice: string
  favourite?: boolean
  baseToken: string
  baseTokenAddress: string
  quoteToken: string
  quoteTokenAddress: string
  asMakerFeeRate: number
  asTakerFeeRate: number
  minOrderSize: string
}

export interface MarketsStoreTypes {
  markets: MarketTypes[]
  favoriteMarkets: { id: number }[]
  selectedMarket: MarketTypes
  marketDetails: {
    changeRate: string
    firstPrice: string
    highPrice: string
    lastPrice: string
    lowPrice: string
    priceChange: string
  }
  getMarkets: (term?: string) => MarketTypes
  getFavoriteMarkets: () => void
  setFavorites: (marketId: number) => void
  selectMarket: (object: MarketTypes) => void
  getMarketDetails: (marketId: number) => void
  getMarket: (marketId: number) => MarketTypes
}
