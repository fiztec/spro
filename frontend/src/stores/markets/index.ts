import { observable, action, computed } from 'mobx'
import { useStaticRendering } from 'mobx-react'
import sortBy from 'lodash/sortBy'
import debounce from 'lodash/debounce'
import { isServer } from '../../utils/isServer'
import { MarketsStoreTypes, MarketTypes } from './types'
import { apiCaller } from '../../utils/apiCaller'
import { API_URL } from '../../config/api'
import { bind } from '../../utils/bind'
import { getUserToken } from '../../utils/cookieService'

useStaticRendering(isServer)

class MarketsStore {
  @observable markets: MarketsStoreTypes['markets']
  @observable favoriteMarkets: MarketsStoreTypes['favoriteMarkets']
  @observable selectedMarket: MarketsStoreTypes['selectedMarket']
  @observable marketDetails: MarketsStoreTypes['marketDetails']

  constructor({}, initialState: MarketsStoreTypes) {
    this.markets = initialState ? initialState.markets : []
    this.selectedMarket = initialState ? initialState.selectedMarket : null
    this.marketDetails = initialState ? initialState.marketDetails : null
  }

  @bind @action selectMarket(market: MarketsStoreTypes['selectedMarket']) {
    if (market) {
      this.selectedMarket = market
    }
  }

  @action getMarkets = debounce(async term => {
    const markets = await apiCaller(
      `${API_URL}/markets${term ? `?token=${term}` : ''}`
    )

    let marketHistory

    if (getUserToken()) {
      marketHistory = await apiCaller(`${API_URL}/order-book/history`)
    }

    this.favoriteMarkets = await apiCaller(`${API_URL}/favorites`)

    const newData: MarketsStoreTypes['markets'] = markets.map(
      ({
        id,
        tokens,
        baseToken,
        baseTokenAddress,
        quoteToken,
        quoteTokenAddress,
        asMakerFeeRate,
        asTakerFeeRate,
        minOrderSize
      }: MarketTypes) => {
        if (marketHistory) {
          const { amount, lastPrice, changeRate } = marketHistory.find(
            (item: { tokens: string }) => item.tokens === tokens
          )
          return {
            id,
            tokens,
            baseToken,
            baseTokenAddress,
            quoteToken,
            quoteTokenAddress,
            dayVol: amount,
            price: lastPrice,
            asMakerFeeRate,
            asTakerFeeRate,
            dayPrice:
              !changeRate.match(/^-/g) && +changeRate !== 0
                ? `+${changeRate}`
                : changeRate,
            favourite: this.favoriteMarkets
              ? this.favoriteMarkets.some(item => item.id === id)
              : null,
            minOrderSize
          }
        }

        return {
          id,
          tokens,
          baseToken,
          baseTokenAddress,
          quoteToken,
          quoteTokenAddress,
          favourite: this.favoriteMarkets
            ? this.favoriteMarkets.some(item => item.id === id)
            : null
        }
      }
    )

    this.markets = sortBy(newData, o => o.id)
  }, 500)

  @bind @action public async getMarket(marketId: number) {
    return await apiCaller(`${API_URL}/markets/${marketId}`)
  }

  @computed public get getFavoriteMarkets() {
    if (this.markets && this.favoriteMarkets) {
      return this.markets.filter(market =>
        this.favoriteMarkets.some(({ id }) => market.id === id)
      )
    }
  }

  @bind @action public async setFavorites(marketId: number) {
    if (this.favoriteMarkets) {
      const method = this.favoriteMarkets.find(({ id }) => id === marketId)
        ? 'DELETE'
        : 'PUT'
      await apiCaller(`${API_URL}/favorites/${marketId}`, method)
      await this.getMarkets('')
    }
  }

  @bind @action public async getMarketDetails(marketId: number) {
    const data = await apiCaller(`${API_URL}/dashboard/trade/${marketId || 1}`)

    this.marketDetails = data
  }
}

export const initMarketsStore = initialState => {
  return new MarketsStore(isServer, initialState)
}
