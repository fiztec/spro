export interface AuthStoreTypes {
  error: string | []
  status: string
  setStatus: (status: string) => string
  getToken: (publicAddress: string, nonce: string) => void
  createUser: (publicAddress: string) => void
  authUser: (publicAddress: string) => void
}
