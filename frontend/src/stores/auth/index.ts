import { observable, action } from 'mobx'
import { useStaticRendering } from 'mobx-react'
import { isServer } from '../../utils/isServer'
import { AuthStoreTypes } from './types'
import { getUserToken, setUserToken } from '../../utils/cookieService'
import { apiCaller } from '../../utils/apiCaller'
import { API_URL } from '../../config/api'
import { bind } from '../../utils/bind'

useStaticRendering(isServer)

class AuthStore {
  @observable status: AuthStoreTypes['status']
  @observable error: AuthStoreTypes['error']

  public constructor({}) {
    this.status = null
    this.error = null
  }

  @bind @action private setError(error) {
    this.error = error
  }

  @bind @action private createSignature(publicAddress: string, nonce: string) {
    window.web3.personal.sign(
      `MeterQubes authentication with one-time nonce: ${nonce}`,
      publicAddress,
      async (err: string, signature: string) => {
        if (err) {
          this.setError(err)
          this.status = 'authError'
        } else {
          const accessToken = await apiCaller(`${API_URL}/auth`, 'POST', {
            publicAddress,
            signature
          })

          setUserToken(accessToken)
          this.status = 'authSuccess'
        }
      }
    )
  }

  @bind @action public async createUser(publicAddress: string) {
    const { nonce } = await apiCaller(`${API_URL}/users`, 'POST', {
      publicAddress
    })

    if (nonce) {
      this.createSignature(publicAddress, nonce)
    }
  }

  @bind @action public async authUser(publicAddress: string) {
    const token = getUserToken()
    if (!token) {
      try {
        const { nonce } = await apiCaller(
          `${API_URL}/users?publicAddress=${publicAddress}`
        )
        if (this.status !== 'authRequest') {
          await this.createSignature(publicAddress, nonce)
          await this.setStatus('authRequest')
        }
      } catch (err) {
        this.createUser(publicAddress)
      }
    }
  }

  @bind @action setStatus(status: string | null) {
    this.status = status
  }
}

export const initAuthStore = () => {
  return new AuthStore(isServer)
}
